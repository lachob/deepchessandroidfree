#ifndef POLYBOOK_H_INCLUDED
#define POLYBOOK_H_INCLUDED

#include "position.h"
#include <vector>

struct PolyHash {
    uint64_t key;
    uint16_t move;
    uint16_t weight;
    uint32_t learn;
};

class PolyBook
{
public:

    PolyBook();
    ~PolyBook();

    void init(const std::string& bookfile);
    void set_best_book_move(bool best_book_move);
    void set_book_depth(int book_depth);

    Move probe(PositionAI& pos);

private:

    Key polyglot_key(const PositionAI& pos);
    Move pg_move_to_sf_move(const PositionAI & pos, unsigned short pg_move);

    int find_first_key(uint64_t key);
    int get_key_data();

    bool check_do_search(const PositionAI & pos);
    bool check_draw(Move m, PositionAI& pos);

    void byteswap_polyhash(PolyHash *ph);
    uint64_t rand64();

    bool is_little_endian();
    uint64_t swap_uint64(uint64_t d);
    uint32_t swap_uint32(uint32_t d);
    uint16_t swap_uint16(uint16_t d);

    int keycount;
    std::vector<PolyHash> polyhash;

    bool use_best_book_move;
    int max_book_depth;
    int book_depth_count;

    int index_first;
    int index_count;
    int index_best;
    int index_rand;
    int index_weight_count;

    uint64_t sr;

    Bitboard last_position;
    Bitboard akt_position;
    int last_anz_pieces;
    int akt_anz_pieces;
    int search_counter;

    bool enabled, do_search;
};

extern PolyBook polybook;

#endif // #ifndef POLYBOOK_H_INCLUDED

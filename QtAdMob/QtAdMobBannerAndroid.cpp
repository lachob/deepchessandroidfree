#include "QtAdMobBannerAndroid.h"

#if (__ANDROID_API__ >= 9)

#include <QAndroidJniObject>
#include <qpa/qplatformnativeinterface.h>
#include <QGuiApplication>
#include <QAndroidJniEnvironment>

#ifdef __cplusplus
extern "C" {
#endif

bool QtAdMobBannerAndroid::mIsBannerClosed = false;

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onBannerLoaded(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobBannerAndroid::TInstances& instances = QtAdMobBannerAndroid::Instances();
    QtAdMobBannerAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobBannerAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobBannerAndroid::mIsBannerClosed)
            emit beg.value()->loaded();

        beg++;
    }
}

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onBannerLoading(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobBannerAndroid::TInstances& instances = QtAdMobBannerAndroid::Instances();
    QtAdMobBannerAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobBannerAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobBannerAndroid::mIsBannerClosed)
            emit beg.value()->loading();

        beg++;
    }
}

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onBannerClosed(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobBannerAndroid::TInstances& instances = QtAdMobBannerAndroid::Instances();
    QtAdMobBannerAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobBannerAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobBannerAndroid::mIsBannerClosed)
            emit beg.value()->closed();

        beg++;
    }
}

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onBannerClicked(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobBannerAndroid::TInstances& instances = QtAdMobBannerAndroid::Instances();
    QtAdMobBannerAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobBannerAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobBannerAndroid::mIsBannerClosed)
            emit beg.value()->clicked();

        beg++;
    }
}

#ifdef __cplusplus
}
#endif

int QtAdMobBannerAndroid::s_Index = 0;
QtAdMobBannerAndroid::TInstances QtAdMobBannerAndroid::s_Instances;

QtAdMobBannerAndroid::QtAdMobBannerAndroid(const bool isInit)
    : m_BannerSize(IQtAdMobBanner::Banner)
    //, m_Activity(0)
    , m_Index(s_Index++)
{
    mIsBannerClosed = false;
    s_Instances[m_Index] = this;

    QPlatformNativeInterface* interface = QGuiApplication::platformNativeInterface();
    jobject activity = (jobject)interface->nativeResourceForIntegration("QtActivity");
    if (activity)
    {
        try {
            m_Activity = std::make_unique< QAndroidJniObject>(activity);
        } catch(...) {
            m_Activity = nullptr;
        }
    } else {
        jobject admobactivity = (jobject)interface->nativeResourceForIntegration("QtAdMobActivity");
        if (admobactivity)
        {
            try {
                m_Activity = std::make_unique< QAndroidJniObject>(admobactivity);
            } catch(...) {
                m_Activity = nullptr;
            }
        } else
            m_Activity = nullptr;
    }

    if((isInit) && (m_Activity) && (m_Activity != nullptr)){
        m_Activity->callMethod<void>("InitializeAdBanner");
        QAndroidJniEnvironment env; if (env->ExceptionCheck()) { env->ExceptionClear();  }
    }

}

QtAdMobBannerAndroid::~QtAdMobBannerAndroid()
{
    s_Instances.remove(m_Index);

    //setVisible(false);
//    if (isValid())
//    {
//        m_Activity->callMethod<void>("ShutdownAdBanner");
//    }
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

void QtAdMobBannerAndroid::setUnitId(const QString& unitId,const bool &isInit)
{
    if (!(isValid()))
    {
        return;
    }
    if(!isInit){
        return;
    }
    QAndroidJniObject param1 = QAndroidJniObject::fromString(unitId);
    m_Activity->callMethod<void>("SetAdBannerUnitId", "(Ljava/lang/String;)V", param1.object<jstring>());
    m_UnitId = unitId;
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

//TEST
int QtAdMobBannerAndroid::getMemory(){
    if (!(isValid()))
    {
        return 0;
    }

    int memory = m_Activity->callMethod<int>("GetRuntimeMemoryAvailable");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    return memory;
}

const QString& QtAdMobBannerAndroid::unitId() const
{
    return m_UnitId;
}

void QtAdMobBannerAndroid::setSize(IQtAdMobBanner::Sizes size)
{
    if (!(isValid()))
    {
        return;
    }

    m_Activity->callMethod<void>("SetAdBannerSize", "(I)V", (int)size);
    m_BannerSize = size;
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

IQtAdMobBanner::Sizes QtAdMobBannerAndroid::size() const
{
    return m_BannerSize;
}

QSize QtAdMobBannerAndroid::sizeInPixels()
{
    int width = 0;
    int height = 0;
    if (isValid())
    {
        width = m_Activity->callMethod<int>("GetAdBannerWidth");
        height = m_Activity->callMethod<int>("GetAdBannerHeight");
        QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    }

    return QSize(width, height);
}

void QtAdMobBannerAndroid::setPosition(const QPoint& position)
{    
    if (!(isValid()))
    {
        return;
    }

    m_Activity->callMethod<void>("SetAdBannerPosition", "(II)V", position.x(), position.y());
    m_Position = position;
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

const QPoint& QtAdMobBannerAndroid::position() const
{
    return m_Position;
}

void QtAdMobBannerAndroid::setVisible(bool isVisible)
{
    if (!(isValid()))
    {
        return;
    }

    if (isVisible)
    {
        m_Activity->callMethod<void>("ShowAdBanner");
        QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    }
    else
    {
        m_Activity->callMethod<void>("HideAdBanner");
        QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    }
}

bool QtAdMobBannerAndroid::visible()
{
    if (!(isValid()))
    {
        return false;
    }

    bool isVisible = m_Activity->callMethod<jboolean>("IsAdBannerShowed", "()Z");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    return isVisible;
}

bool QtAdMobBannerAndroid::isLoaded()
{
    if (!(isValid()))
    {
        return false;
    }

    bool isLoaded = m_Activity->callMethod<jboolean>("IsAdBannerLoaded", "()Z");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    return isLoaded;
}

void QtAdMobBannerAndroid::addTestDevice(const QString& hashedDeviceId)
{
    if (!(isValid()))
    {
        return;
    }

    QAndroidJniObject param1 = QAndroidJniObject::fromString(hashedDeviceId);
    m_Activity->callMethod<void>("AddAdTestDevice", "(Ljava/lang/String;)V", param1.object<jstring>());
}
void QtAdMobBannerAndroid::shutdownBanner(){
    if (!(isValid()))
    {
        return;
    }

    m_Activity->callMethod<void>("ShutdownAdBanner");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}
int QtAdMobBannerAndroid::checkEuUser(){
    if (!(isValid()))
    {
        return 0;
    }

    int b = m_Activity->callMethod<int>("checkEuUser");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    return b;
}

void QtAdMobBannerAndroid::closeBannerAd() {
    if (!(isValid()))
    {
        return;
    }

    mIsBannerClosed = true;
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

const QtAdMobBannerAndroid::TInstances& QtAdMobBannerAndroid::Instances()
{
    return s_Instances;
}

bool QtAdMobBannerAndroid::isValid() const
{
    return ((m_Activity) && (m_Activity != nullptr));
}

#endif // __ANDROID_API__

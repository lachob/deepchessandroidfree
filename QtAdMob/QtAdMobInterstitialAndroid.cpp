#include "QtAdMobInterstitialAndroid.h"

#if (__ANDROID_API__ >= 9)

#include <QAndroidJniObject>
#include <qpa/qplatformnativeinterface.h>
#include <QGuiApplication>
#include <QAndroidJniEnvironment>
#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

bool QtAdMobInterstitialAndroid::mIsInterstitialClosed = false;

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onInterstitialLoaded(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobInterstitialAndroid::TInstances& instances = QtAdMobInterstitialAndroid::Instances();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobInterstitialAndroid::mIsInterstitialClosed)
            emit beg.value()->loaded();

        beg++;
    }
}

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onInterstitialLoading(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobInterstitialAndroid::TInstances& instances = QtAdMobInterstitialAndroid::Instances();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobInterstitialAndroid::mIsInterstitialClosed)
            emit beg.value()->loading();

        beg++;
    }
}

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onInterstitialWillPresent(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobInterstitialAndroid::TInstances& instances = QtAdMobInterstitialAndroid::Instances();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobInterstitialAndroid::mIsInterstitialClosed)
            emit beg.value()->willPresent();

        beg++;
    }
}

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onInterstitialClicked(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobInterstitialAndroid::TInstances& instances = QtAdMobInterstitialAndroid::Instances();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobInterstitialAndroid::mIsInterstitialClosed)
            emit beg.value()->clicked();

        beg++;
    }
}

JNIEXPORT void JNICALL Java_org_dreamdev_QtAdMob_QtAdMobActivity_onInterstitialClosed(JNIEnv *env, jobject thiz)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    const QtAdMobInterstitialAndroid::TInstances& instances = QtAdMobInterstitialAndroid::Instances();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator beg = instances.begin();
    QtAdMobInterstitialAndroid::TInstances::ConstIterator end = instances.end();
    while(beg != end)
    {
        if(beg.value() && !QtAdMobInterstitialAndroid::mIsInterstitialClosed)
            emit beg.value()->closed();

        beg++;
    }
}

#ifdef __cplusplus
}
#endif

int QtAdMobInterstitialAndroid::s_Index = 0;
QtAdMobInterstitialAndroid::TInstances QtAdMobInterstitialAndroid::s_Instances;

QtAdMobInterstitialAndroid::QtAdMobInterstitialAndroid()
    //: m_Activity(0)
    : m_Index(s_Index++)
{
    mIsInterstitialClosed = false;
    s_Instances[m_Index] = this;

    QPlatformNativeInterface* interface = QGuiApplication::platformNativeInterface();
    jobject activity = (jobject)interface->nativeResourceForIntegration("QtActivity");

    if (activity)
    {
        try {
            m_Activity = std::make_unique< QAndroidJniObject>(activity);
        } catch(...) {
            m_Activity = nullptr;
        }
    } else {
        jobject admobactivity = (jobject)interface->nativeResourceForIntegration("QtAdMobActivity");
        if (admobactivity)
        {
            try {
                m_Activity = std::make_unique< QAndroidJniObject>(admobactivity);
            } catch(...) {
                m_Activity = nullptr;
            }
        } else
            m_Activity = nullptr;
    }
}

QtAdMobInterstitialAndroid::~QtAdMobInterstitialAndroid()
{
    s_Instances.remove(m_Index);
}

void QtAdMobInterstitialAndroid::setUnitId(const QString& unitId)
{
    if (!(isValid()))
    {
        return;
    }

    QAndroidJniObject param1 = QAndroidJniObject::fromString(unitId);
    m_Activity->callMethod<void>("LoadAdInterstitialWithUnitId", "(Ljava/lang/String;)V", param1.object<jstring>());
    m_UnitId = unitId;
    m_Activity->callMethod<void>("LoadAdRewarded");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

void QtAdMobInterstitialAndroid::closeInterstitial()
{
    if (!(isValid()))
    {
        return;
    }

    mIsInterstitialClosed = true;
    m_Activity->callMethod<void>("CloseInterstitial");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

void QtAdMobInterstitialAndroid::reloadRewarded()
{
    if (!(isValid()))
    {
        return;
    }

    m_Activity->callMethod<void>("LoadAdRewarded");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

const QString& QtAdMobInterstitialAndroid::unitId() const
{
    return m_UnitId;
}

void QtAdMobInterstitialAndroid::setVisible(bool isRewarded)
{
    if (!(isValid()))
    {
        return;
    }
    // TODO: implement hide
    try {
        if(m_Activity){
            if(isRewarded) {
                m_Activity->callMethod<void>("ShowAdRewarded");
            } else {
                m_Activity->callMethod<void>("ShowAdInterstitial");
            }
            QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
        }
    } catch (...) {
        throw;
    }

}

bool QtAdMobInterstitialAndroid::visible()
{
    return false; // TODO: implement retrieve visibility
}

bool QtAdMobInterstitialAndroid::isLoaded()
{
    if (!(isValid()))
    {
        return false;
    }

    bool isLoaded = m_Activity->callMethod<jboolean>("IsAdInterstitialLoaded", "()Z");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    return isLoaded;
}

bool QtAdMobInterstitialAndroid::isRewardedLoaded() {
    if (!(isValid()))
    {
        return false;
    }

    bool isLoaded = m_Activity->callMethod<jboolean>("IsAdRewardedLoaded", "()Z");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    return isLoaded;
}

bool QtAdMobInterstitialAndroid::isAdsSDKInitialized() {
    if (!(isValid()))
    {
        return false;
    }

    bool isInit = m_Activity->callMethod<jboolean>("IsAdsSDKInitialized", "()Z");
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
    return isInit;
}

void QtAdMobInterstitialAndroid::addTestDevice(const QString& hashedDeviceId)
{
    if (!(isValid()))
    {
        return;
    }

    QAndroidJniObject param1 = QAndroidJniObject::fromString(hashedDeviceId);
    m_Activity->callMethod<void>("AddAdTestDevice", "(Ljava/lang/String;)V", param1.object<jstring>());
    QAndroidJniEnvironment env; if (env->ExceptionCheck()) {   env->ExceptionClear();  }
}

const QtAdMobInterstitialAndroid::TInstances& QtAdMobInterstitialAndroid::Instances()
{
    return s_Instances;
}

bool QtAdMobInterstitialAndroid::isValid() const
{
    return ((m_Activity) && (m_Activity != nullptr));
}

#endif // __ANDROID_API__

#include "QtAdMobInterstitialDummy.h"

QtAdMobInterstitialDummy::QtAdMobInterstitialDummy()
{
}

QtAdMobInterstitialDummy::~QtAdMobInterstitialDummy()
{
}

void QtAdMobInterstitialDummy::setUnitId(const QString& unitId)
{
    m_UnitId = unitId;
}

void QtAdMobInterstitialDummy::closeInterstitial() {}

void QtAdMobInterstitialDummy::reloadRewarded() {}

const QString& QtAdMobInterstitialDummy::unitId() const
{
    return m_UnitId;
}

void QtAdMobInterstitialDummy::setVisible(bool isVisible)
{
    Q_UNUSED(isVisible);
}

bool QtAdMobInterstitialDummy::visible()
{
    return false;
}

bool QtAdMobInterstitialDummy::isLoaded()
{
    return false;
}

bool QtAdMobInterstitialDummy::isRewardedLoaded()
{
    return false;
}

bool QtAdMobInterstitialDummy::isAdsSDKInitialized() {
    return false;
}

void QtAdMobInterstitialDummy::addTestDevice(const QString& hashedDeviceId)
{
    Q_UNUSED(hashedDeviceId);
}

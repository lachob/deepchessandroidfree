#ifndef ADJACENCYMATRIXGRAPH_HPP_INCLUDED
#define ADJACENCYMATRIXGRAPH_HPP_INCLUDED
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include "Figures.hpp"
#include "Player.hpp"
#include "chessfield.h"
#define NO_EDGE -1
#define BOARD_SIZE 8

struct BackMoveAdj{
    std::vector<std::vector<int>> adjMatrixBack{BOARD_SIZE, std::vector<int>(BOARD_SIZE, NO_EDGE)};
    std::vector<std::shared_ptr<Figure>> whiteFigsBack;
    std::vector<std::shared_ptr<Figure>> blackFigsBack;
    int lastMovFig {NO_EDGE};
    int prevLastMovFig{NO_EDGE};
    int openCheckFig {NO_EDGE};
    bool whiteChecked{false};
    bool blackChecked{false};
};

class StartPosGraph{
    public:
    std::vector<std::vector<int>> adjMatrix{BOARD_SIZE, std::vector<int>(BOARD_SIZE, NO_EDGE)};
    private:
        //Move Back
        std::stack<BackMoveAdj> backMoveData;
        int numVertices;
        int prevLastMovFig{NO_EDGE};
        int openCheckFig {NO_EDGE};
        int counterForFiftyMovesDraw{0};
        bool fiftyMovesTake{false};
        Player whitePlayer;
        Player blackPlayer;
        std::vector<Position> openChMoves;
        std::vector<std::shared_ptr<Figure>> whiteFigs;
        std::vector<std::shared_ptr<Figure>> blackFigs;
        bool isOpenCheck{false};
    public:
        int counterForFiftyMovesDrawPawns{0};
        char castle{' '};
        int lastMovFig {NO_EDGE};
        explicit StartPosGraph();
        explicit StartPosGraph(int);
        StartPosGraph(const StartPosGraph&);
        ~StartPosGraph();
        void init();
        void moveBack();
        void startGame(std::string &, QString &gameEndResultTr, int &, Position &, Position &, char &playerColor, bool &isKCastle, bool &isQCastle, char &fig);
        void gameInit(int &, std::atomic<bool> &, std::string &, QString &gameEndResultTr, char &playerColor, int sRow, int sCol, int dRow, int dCol, bool &isKCastle, bool &isQCastle, char &fig);
        void findFigureAvailableMoves(std::vector<AvMove> &, int, char &playerColor);
        void addOpenChMove(Position&);
        inline std::vector<Position> &getOpenChMoves(){return openChMoves;}
        void eraseOpenChMoves();
        inline std::vector<std::shared_ptr<Figure>> &getWhiteFigs(){return whiteFigs;}
        inline std::vector<std::shared_ptr<Figure>> &getBlackFigs(){return blackFigs;}
        void checkForDrawPosition();
        bool checkForStalemate(char);
        void removeFigure(char,int);
        void setNumVertices(int);
        int getNumVertices();
        std::vector<std::vector<int>> &getTable();
        template<typename T> bool moveFigure(Position&, Position&, char, T&, int,bool &isKCastle,bool &isQCastle);
        template<typename T> char moveFig(Position&, Position&, char, T&, int, int, int&);
        bool move(Position &, Position &, char, int &pawnTransfer, bool &isKCastle, bool &isQCastle, char &fig);
        void setAllFigsAvMoves();
        void setLastMovFig(int);
        int getLastMovFig()const;
        void setOpenCheckFig(int);
        int getOpenCheckFig()const;
        void sayCheck(char);
        void findCheckMoves(char);
        inline Player &getWhitePlayer(){return whitePlayer;}
        inline Player &getBlackPlayer(){return blackPlayer;}
};
#endif // ADJACENCYMATRIXGRAPH_HPP_INCLUDED

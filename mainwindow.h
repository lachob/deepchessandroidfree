#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "chessfield.h"
#include "AdjacencyMatrixGraph.hpp"
#include "chessai.h"
#include "dbmanager.hpp"
#include "QtAdMob/QtAdMobBanner.h"
#include "QtAdMob/QtAdMobInterstitial.h"
#include "bitboard.h"
#include "position.h"
#include "thread.h"
#include "tt.h"
#include "syzygy/tbprobe.h"
#include "Figures.hpp"
#include "functions.hpp"
#include "chessfield.h"
#include "polybook.h"
#include "ui_mainwindow.h"

#include <QMainWindow>
#include <QFont>
#include <qevent.h>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QNetworkInterface>
#include <QInputDialog>
#include <QElapsedTimer>
#include <QtAndroidExtras>
#include <QScreen>
#include <QApplication>
#include <QThread>
#include <QDesktopServices>
#include <QFileDialog>
#include <QMessageBox>
#include <QSound>
#include <QPixmapCache>

#include <memory>
#include <sstream>


#define  HINT_PUZZLE_ACTIVE_COUNT  (5)
#define  BANNER_ADS_COUNT          (1000000)

namespace Ui {
class MainWindow;
struct AvMove;
}

struct BackMove
{
    QString iconUrl;
    QString borderColor;
    QString _boardPos;
    std::string uciMoveCmd;
    std::string uciMoveCmdHint;
    QString PGNString;
    int PGNMoveNum{0};
    int figId{-1};
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(bool isHintActivated, bool isPuzzle, bool figuresChoosed, int bannerCnt, QChar &_theme,
                        QWidget *parent = nullptr, const bool isBaner = true, int gamesCnt = 0);
    ~MainWindow();
    void resetAvMoves();
    void changeBtnIcon(ChessFieldButton &btn, const QString &newPath, const QSize &size);
    void setBtnMode(ChessFieldButton& btn, bool isMove, QChar color);
    void setActiveFigureIndex(int index);
    void setUciMoveCommand();
    void setUciMoveCommandHint();
    void highliteLastMove(int start, int dest);
    void highliteLastMove(int start, int dest, int pStart, int pDest);
    void clearAllButtons();
    void disposeMainWindow();
    bool isCorrectInitialization{true};
    Thread *uiThread = nullptr;
    std::atomic<bool> isBoardFlipped{false};
    std::string computerPlayLevel;
    std::string gameEndResult;
    QString gameEndResultTr;
    QWidget *gameInfo = nullptr;
    QLabel *gameText = nullptr;
    QString levelForDB;
    QLabel *statisticsLabel = nullptr;
    QLabel *congratsLabel = nullptr;
    std::unique_ptr<DBManager> connection;
    IQtAdMobBanner *banner = nullptr;
    IQtAdMobInterstitial *m_Interstitial = nullptr;
    int gamesCount{0};
    int puzzleCnt{0};
    int playLevel{1};
    int winsOnLevelCounter{0};
    bool isHintActivatedInMainWindow{false};
    bool isGameResetted{false};
    bool newFiguresSelected{true};
    QChar theme{'s'};
    bool isHumanWon{false};
    bool isBanner{true};
    bool isPuzzleAct{false};
    bool isSoundsAval{true};
signals:
    void sendAvMoves(int index);
    void changePawnPromoVis();
    void changeGameInfoVis();
    void uciMoveChanged();
    void uciMoveCmdChanged();
    void restartGameChanged();
    void hintMoveFound();
public slots:
    void findAvailableMoves(int index);
    void findAvailableMoves(int index, bool isComp);
    void setAsActiveButton(int index);
    void moveGuiFigure(int index);
    void moveGuiFigureUci();
    void disableAllButtons();
    void keyPressEventQ();
    void keyPressEventR();
    void keyPressEventK();
    void keyPressEventB();
    void setPawnPromoVis();
    void setPawnPromoHidden();
    void setGameInfoVis(QWidget *widget, QLabel *label);
    void setGameInfoHidden(QWidget *widget, QLabel *label);
    void startChessAI();
    void chooseChessAIPlayingStrength(QString command_text);
    void chooseChessAIPlayingStrengthShow();
    void changeEmitFlag();
    void chessAiErr(QString err);
    void getChessAiMove();
    void setUciMoveCmd(const std::string &cmd);
    std::string &getUciMoveCmd();
    void startChessAICalculation();
    void flipGUIBoardChessAiColor();
    void initPuzzle();
    void puzzleClicked();
    void setPuzzleBoard(const QString &pos);
    void puzzleBtnClick(int index);
    void gameResigned();
    void proposeDraw();
    void repaintMainWindow();
    void analyzisModeNotAvailable();
    void startAnalyzisMode();
    void changeWidgetsGeometry();
    void resizeButtons(double fieldSize, double icsize);
    void OnInterstitialLoaded();
    void OnInterstitialLoading();
    void OnInterstitialClicked();
    void OnInterstitialClosed();
    void OnBannerLoaded();
    void OnBannerLoading();
    void OnBannerClicked();
    void OnBannerClosed();
    bool checkForPlayerWon();
    void wonCongratulations();
    void newGameClicked();
    void congratsBannerClose();
    void achievementsBannerClose();
    void showAdsBanner();
    void showHintMove();
    void startSearchHintMove();
    void increaseLevelStrength();
    void decreaseLevelStrength();
    void whiteColorChosen();
    void blackColorChosen();
    void okButtonClicked();
    void cancelButtonClicked();
    void resetButtonClicked();
    void newFiguresChoosed();
    void oldFiguresChoosed();
    void removeAdsClicked();
    void closeRateClicked();
    void rateClicked();
    void saveGameDB();
    void closeSaveGameDB();
    void loadGameClicked();
    void decreaseLoadedGames();
    void increaseLoadedGames();
    void loadGameOkClicked();
    void loadGameCancelClicked();
    void prevMoveLG();
    void nextMoveLG();
    void lGMoveFig(const QSize &icSize, QString &str, QString &picUrl, std::string &result, int &figId, int &index, bool dir, const QChar promo);
    void deleteGame();
    void exportGame(bool isGreaterThan11);
    void exportGameClicked();
    void changeColors();
    void rotateBoard();
    void gdprAgreed();
    void gdprPaidClicked();
    void moveBackGui();
    void filesButtonClicked();
    void addBookClicked();
    void removeBookClicked();
    void filesClosedClicked();
    void openBookFileDialog();
    void RequestPermission(const QString, const QString);
    void RequestPermissionsResults(const QtAndroid::PermissionResultMap &ResultMap);
    void addToPGN();
    void rewardedActivateClicked();
    void rewardedCloseClicked();

protected:
    void resizeEvent(QResizeEvent* event);
private:
    std::unique_ptr<Ui::MainWindow> ui;
    QWidget *widget = nullptr;
    QGridLayout *grid = nullptr;
    std::vector<ChessFieldButton *> buttons;
    ChessAI *lastChessAi = nullptr;
    std::vector<AvMove> avMoves;
    QString activeFigurePicUrl;
    std::unique_ptr<StartPosGraph> startBoard;
    QPushButton *starAnalyzisButton = nullptr;
    QWidget *pawnPromotion = nullptr;
    QPushButton *pawnPromoQ = nullptr;
    QPushButton *pawnPromoR = nullptr;
    QPushButton *pawnPromoK = nullptr;
    QPushButton *pawnPromoB = nullptr;
    QWidget *sideBar = nullptr;
    QWidget *settingsWidget = nullptr;
    QWidget *okOrCancelWidget = nullptr;
    QPushButton *okSettingsButton = nullptr;
    QPushButton *cancelSettingsButton = nullptr;
    QPushButton *restartGameButton = nullptr;
    QWidget *levelInfoWidget = nullptr;
    QLabel *levelInfoText = nullptr;
    QWidget *colorInfoWidget = nullptr;
    QLabel *colorInfoText = nullptr;
    QWidget *titleInfoWidget = nullptr;
    QLabel *titleInfoText = nullptr;
    QPushButton *removeAdsButton = nullptr;
    QWidget *chooseStrengthWidget = nullptr;
    QPushButton *upStrengthButton = nullptr;
    QPushButton *downStrengthButton = nullptr;
    QLabel *levelStatusText = nullptr;
    QWidget *chooseColorWidget = nullptr;
    QPushButton *playWhiteColorButton = nullptr;
    QPushButton *playBlackColorButton = nullptr;
    QWidget *starAnalysisWidget = nullptr;
    QPushButton *newFiguresButton = nullptr;
    QPushButton *oldFiguresButton = nullptr;
    QPushButton *resetButton = nullptr;
    QWidget *rateWidget = nullptr;
    QLabel *rateTitle = nullptr;
    QLabel *rateText_1 = nullptr;
    QPushButton *rateText_2 = nullptr;
    QPushButton *closeRate = nullptr;
    QPushButton *playAgainstComputer = nullptr;
    std::string uciMove;
    std::string uciMoveHint;
    std::string uciMoveCommandSetUp;
    std::string uciMoveCommandSetUp2;
    std::string uciMoveCommand;
    std::string uciMoveCmd{"position startpos moves"};
    QLabel *startlbl = nullptr;
    QMovie *startmovie = nullptr;
    QWidget *resignWidget = nullptr;
    QPushButton *resignImageLabel = nullptr;
    QPushButton *resignButton = nullptr;
    QWidget *drawWidget = nullptr;
    QPushButton *drawButton = nullptr;
    QWidget *newGameWidget = nullptr;
    QPushButton *newGameButton = nullptr;
    std::vector<std::string> treefoldRepetition;
    QScreen *screen;
    QWidget *hintWidget = nullptr;
    QHBoxLayout *hintWidgetLayout = nullptr;
    QPushButton *hintButton = nullptr;
    std::string hintMoveStr;
    std::string hintDepth;
    std::string uciMoveCommandHint;
    std::string uciMoveCmdHint;
    QWidget *congratsBannerWidget = nullptr;
    QLabel *congratsImageLabel = nullptr;
    QPushButton *congratsCloseButton = nullptr;
    QWidget *achievementBannerWidget = nullptr;
    QLabel *achievementLabelText = nullptr;
    QString achievementType;
    QPushButton *achievementLabelImage = nullptr;
    QPushButton *achievementsCloseButton = nullptr;
    QString achievementsLabelPath{"assets:/images/bronzestar.png"};
    QWidget *saveGameWidget = nullptr;
    QPushButton *saveGameButton = nullptr;
    QPushButton *closeGameButton = nullptr;
    QInputDialog *inputDialog = nullptr;
    QString gameName;
    QPushButton *loadGameButton = nullptr;
    QWidget *loadGameInfoWidget = nullptr;
    QLabel *loadGameInfoText = nullptr;
    QWidget *loadGameWidget = nullptr;
    QPushButton *exportPGNButton = nullptr;
    QPushButton *upLoadGameButton = nullptr;
    QPushButton *downLoadGameButton = nullptr;
    QLabel *loadGameStatusText = nullptr;
    QString currentLoadGameName;
    QWidget *okOrCancelLoadGameWidget = nullptr;
    QPushButton *okLoadGameButton = nullptr;
    QPushButton *cancelLoadGameButton = nullptr;
    QPushButton *prevMoveButton = nullptr;
    QPushButton *nextMoveButton = nullptr;
    QWidget *deleteGameWidget = nullptr;
    QPushButton *deleteGameButton = nullptr;
    QPushButton *filesButton = nullptr;
    QHBoxLayout *filesLayout = nullptr;
    QVBoxLayout *filesLayout_1 = nullptr;
    QHBoxLayout *titleHorizLayout = nullptr;
    QPushButton *exportGameButton = nullptr;
    std::vector<QString> loadedGameNames;
    QString currLoadedGameString;
    QStringList sList;
    QStringList expList;
    QString exportGameString;
    QString lGEnpass;
    QString puzzlePos;
    QStringList puzzleMov;
    QString figURL;
    QWidget *colorWidget = nullptr;
    QHBoxLayout *colorLayout = nullptr;
    QPushButton *colorButton = nullptr;
    QPushButton *rotateBoardButton = nullptr;
    QString whitesColor{"rgb(215, 223, 234)"};
    QString blacksColor{"rgb(131, 156, 196)"};
    QString buttonsColor{"rgb(93, 109, 135)"};
    QString chooseColor{"rgb(79, 93, 115)"};
    QWidget *gdprWidget = nullptr;
    QLabel *gdprText = nullptr;
    QPushButton *gdprAgreeButton = nullptr;
    QPushButton *gdprPaidButton = nullptr;
    std::stack<std::vector<BackMove>> backMoveData;
    QPushButton *undoMoveButton = nullptr;
    QWidget *filesWidget = nullptr;
    QLabel *addBookLabel = nullptr;
    QPushButton *addBookButton = nullptr;
    QPushButton *removeBookButton = nullptr;
    QPushButton *importGameButton = nullptr;
    QPushButton *closeFilesButton = nullptr;
    QWidget *rewardedWidget = nullptr;
    QLabel *rewardedText = nullptr;
    QPushButton *rewardedActivateButton = nullptr;
    QPushButton *rewardedPaidButton = nullptr;
    QPushButton *rewardedCloseButton = nullptr;
    QString PGNString;
    QString resizedWood;
    double fieldSize{0};
    double icsize{0};
    double btnFontSiz{0};
    double titleFont{0};
    int PGNMoveNum{0};
    QChar PGNPromoSymb{' '};
    int PGNLastActiveFig{NO_EDGE};
    int PGNMoveInd{NO_EDGE};
    int PGNFigID{NO_EDGE};
    int PGNLastActiveFigId{NO_EDGE};
    bool addPGNNum{false};
    bool isUgrdButton{true};
    bool isBannerVisibleCond{true};
    bool wrongBanerPos{false};
    int btnNumFontS{0};
    bool resetButtonClickedCheck{false};
    int activeFigureIndex{NO_EDGE};
    bool restartGame{false};
    bool isNotStartPosition{false};
    bool isStartedFromPosition{false};
    char colorFromNotStartPos{' '};
    bool moveMadeWithoutComputer{false};
    bool isAnalyzisMode{false};
    bool emitSignalFlag{false};
    int pawnTransfer{NO_EDGE};
    std::atomic<bool> gameEnd{false};
    char playerColor{'w'};
    bool isKCastle{false};
    bool isQCastle{false};
    char fig{' '};
    int pawnPromoIndex{NO_EDGE};
    bool isComputer{false};
    int time{200};
    char promotionSymbChessAi{' '};
    char chessAiColor{' '};
    int enpassantId{-1};
    bool isButtonsDisabled{false};
    bool isResized{false};
    int gameInfoWidth{0};
    bool isThreefoldRepetition{false};
    int comboBoxFontInc{11};
    int movesCounterDraw{0};
    int mobWidth{0};
    int checkHeight{0};
    bool adInterstCurrentUsed{false};
    bool adInterstCurrentClicked{false};
    bool bannerShut{false};
    int bannerHNotAv{0};
    int bannerHAvRes{0};
    int bannerHAvNotRes{0};
    std::atomic<bool> isMoveHintActive{false};
    bool ischesssAIStarted{false};
    int resignImageLabelWidth{0};
    int resignImageLabelHeight{0};
    bool windowCrashed{false};
    int gameCounter{0};
    char lGStatus{' '};
    char lGColor{' '};
    bool isLoadGame{false};
    bool isGameName{false};
    int lGMoveCnt{0};
    bool puzzleMode{false};
    int enpassInd{-1};
    int lasmovePawn{-1};
    std::atomic<bool> islGActive{false};
    bool isPonder{false};
    QChar puzzleColor{' '};
    int movesInd{0};
    int figIndex{0};
    bool toMove{false};
    bool isPuzzleActivated{true};
    bool isCompsTurn{false};
    bool undoMoveClicked{false};
    bool undoTurnChanged{false};
    bool undoButtonClicked{false};
    int drawMovesCnt{0};
    bool readPermAsk{false};
    bool writePermAsk{false};
    std::atomic<bool> repaintMainWindowCalled{false};
    bool threadsAreUp{false};
    bool isSaveGameClosed{false};
    bool isBannerClicked{false};
    bool isRewardedFirstTry{true};
    std::atomic<bool> isChessAISearchActive{false};
    std::mutex mMtx;
};

#endif // MAINWINDOW_H

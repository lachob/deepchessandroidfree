#ifndef DEEPCHESS_H
#define DEEPCHESS_H

#include "mainwindow.h"

#include <QObject>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <string>
#include <queue>
#include <memory>

class DeepChess : public QWidget
{
    Q_OBJECT

public:
    explicit DeepChess();
    ~DeepChess();
    void setMainWindow();
signals:
    void dbStatusChanged();
public slots:
    void startNewGame();

private:
    Thread *uiThread = nullptr;
    std::queue<MainWindow *> mWindows;
    bool isGameResseted{false};
    bool isHintActivated{false};
    int gamesWithHintCount{0};
    int wonResult{0};
    int bannerCnt{0};
    std::string stopChessAI{"quit"};
    bool isPuzzleActiv{false};
    int gamesWithPuzz{0};
    QChar theme{'s'};
};

#endif // DEEPCHESS_H


#ifndef QUEEN_HPP_INCLUDED
#define QUEEN_HPP_INCLUDED
#include "Figures.hpp"

class Queen : public Figure{
    private:
        char color;
        void setColor(char);
        char ch{'N'};
        char chMov{'N'};
    public:
        Queen();
        Queen(std::string,Position&, int, int, char);
        Queen(const Queen &);
        Queen(const Figure &);
        ~Queen() override;
        char getColor()const override;
        std::string getTitle()const;
        void setPosition(Position&);
        Position& getPosition();
        void setAvailableMoves(int, Position&) override;
        void setAvailableMovesCnt(int) override;
        std::vector<Position> getAvMoves()const override;
        int getAvMovesCnt()const override;
        Queen &operator=(const Queen &);
        int getId()const;
        void addAvailableMoves(std::vector<std::vector<int>> &adjMatrix,char)override;
        char isAvailableMove(Position&);
        char move(Position&,std::vector<std::vector<int>> &adjMatrix, char color)override;

};
#endif // QUEEN_HPP_INCLUDED

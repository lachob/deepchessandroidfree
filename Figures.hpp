#ifndef FIGURES_HPP_INCLUDED
#define FIGURES_HPP_INCLUDED
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>

#define NO_EDGE -1
#define BOARD_SIZE 8
#define BK 4
#define WK 20
#define WO1 18
#define WO2 31
#define BO1 2
#define BO2 15
#define WR1 16
#define WR2 29
#define BR1 0
#define BR2 13
#define WQ 19
#define BQ 3
#define WH1 17
#define WH2 30
#define BH1 1
#define BH2 14
#define WP_FIRST 21
#define WP_LAST 28
#define WP_ENPASS_FIRST -21//(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)
#define WP_ENPASS_LAST -28
#define WP_TRANSF_FIRST 2100
#define WP_TRANSF_LAST 2850
#define BP_FIRST 5
#define BP_LAST 12
#define BP_ENPASS_FIRST -5
#define BP_ENPASS_LAST -12
#define BP_TRANSF_FIRST 500
#define BP_TRANSF_LAST 1250
#define WK 20
#define BK 4
//Directions first digit is direction second is figure ID
//2-UP, 3-UP_LEFT, 4-UP_RIGHT, -2-DOWN, -4-DOWN_LEFT, -3-DOWN_RIGHT, 5-LEFT, -5-RIGHT, 9-HORSE_DIR
//Figure IDs
//1-PAWN, 2-ROOK, 3-OFFICER, 4-QUEEN, 5-HORSE
#define P_UP 21
#define WP_LEFT -31
#define WP_RIGHT -41
#define BP_LEFT 41
#define BP_RIGHT 31
#define R_LEFT 52
#define R_RIGHT -52
#define R_UP 22
#define R_DOWN -22
#define O_UPLEFT 33
#define O_UPRIGHT 43
#define O_DOWNLEFT -43
#define O_DOWNRIGHT -33
#define Q_UPLEFT 34
#define Q_UPRIGHT 44
#define Q_UP 24
#define Q_DOWNLEFT -44
#define Q_DOWNRIGHT -34
#define Q_DOWN -24
#define Q_LEFT 54
#define Q_RIGHT -54
#define H_DIR 95

namespace LastMoveInfo{
  extern volatile int lastMoveFigure;
}

class Position{
    public:
    int posId {-1};
    int dirId {-1};
    int prot {-1};
    int row {-1};
    char col {' '};
    bool isCheck {false};
    bool isTake {false};
    bool opensCh {false};
    bool isPawnTransf {false};
    bool pawnHit {false};
    int enPassant {-1};
    int removedRow{-1};
    char removedCol{' '};
    bool isRemoved{false};
    Position():row(0),col(' '){}
    ~Position(){}
    Position(const Position &other){
        posId = other.posId;
        dirId = other.dirId;
        prot = other.prot;
        row = other.row;
        col = other.col;
        isCheck = other.isCheck;
        isTake = other.isTake;
        opensCh = other.opensCh;
        isPawnTransf = other.isPawnTransf;
        pawnHit = other.pawnHit;
        enPassant = other.enPassant;
        removedRow = other.removedRow;
        removedCol = other.removedCol;
        isRemoved = other.isRemoved;
    }
    Position &operator=(const Position &other){
        posId = other.posId;
        dirId = other.dirId;
        prot = other.prot;
        row = other.row;
        col = other.col;
        isCheck = other.isCheck;
        isTake = other.isTake;
        opensCh = other.opensCh;
        isPawnTransf = other.isPawnTransf;
        pawnHit = other.pawnHit;
        enPassant = other.enPassant;
        removedRow = other.removedRow;
        removedCol = other.removedCol;
        isRemoved = other.isRemoved;
        return *this;
    }
    void setP(int r, char c, int id, bool isPawnTransf){
        this->row = r;
        this->col = c;
        posId = id;
        this->isPawnTransf = isPawnTransf;
    }
    void setP(int r, char c, int id,bool isCh,bool isT, bool isPawnTransf){
        this->row = r;
        this->col = c;
        posId = id;
        isCheck = isCh;
        isTake = isT;
        this->isPawnTransf = isPawnTransf;
    }
    void setP(int r, char c, int id,bool isCh,bool isT, bool isPawnTransf, int dir, int protect){
        this->row = r;
        this->col = c;
        posId = id;
        isCheck = isCh;
        isTake = isT;
        this->isPawnTransf = isPawnTransf;
        dirId = dir;
        prot = protect;
    }
    void setP(int r, char c, int id,bool isCh,bool isT, int dir, int protect){
        this->row = r;
        this->col = c;
        posId = id;
        isCheck = isCh;
        isTake = isT;
        dirId = dir;
        prot = protect;
    }
    void setP(int r, char c, int id,bool isCh,bool isT, int dir){
        this->row = r;
        this->col = c;
        posId = id;
        isCheck = isCh;
        isTake = isT;
        dirId = dir;
    }
    void setP(int r, char c, int id,bool isCh,bool isT){
        this->row = r;
        this->col = c;
        posId = id;
        isCheck = isCh;
        isTake = isT;
    }
    void setP(int r, char c, int id){
        this->row = r;
        this->col = c;
        posId = id;
    }
    void setP(int r, char c, int id, int dirID){
        this->row = r;
        this->col = c;
        posId = id;
        this->dirId = dirID;
    }
    void setP(int r, char c, int id, int dirID, int protects){
        this->row = r;
        this->col = c;
        posId = id;
        this->dirId = dirID;
        prot = protects;
    }
    void setP(int r, char c){
        this->row = r;
        this->col = c;
    }
};
class Figure{
    protected:
        int id;
        std::string title;
        Position currPos;
        int availableMovesCnt;
        bool isStartPosWhite = true;
        bool isStartPosBlack = true;
        bool hasAvailableMoves = false;
        bool hasMoved = false;
        bool isQueensCastle = false;
        bool isKingsCastle = false;
        bool checked = false;
        std::vector<Position> availableMoves;
        void setId(int);
    public:
        Figure();
        Figure(std::string,Position&, int, int);
        Figure(const Figure &);
        virtual ~Figure();
        void setTitle(std::string);
        std::string getTitle()const;
        void setPosition(Position&);
        int incAvMovesCnt();
        Position &getPosition();
        virtual void setChecked(bool);
        virtual bool check()const;
        virtual void setQueensCastle(bool);
        virtual bool isQueenCastle()const;
        virtual void setKingsCastle(bool);
        virtual bool isKingCastle()const;
        virtual void setMoved(bool);
        virtual bool hasmoved()const;
        virtual void setHasAvMoves(bool);
        virtual bool hasAvMoves()const;
        virtual void setStartPosWhite(bool);
        virtual bool isStPosWhite()const;
        virtual void setStartPosBlack(bool);
        virtual bool isStPosBlack()const;
        virtual void setAvailableMoves(int, Position&);
        virtual void setAvailableMovesCnt(int);
        virtual void addAvailableMoves(std::vector<std::vector<int>> &adjMatrix,char);
        virtual std::vector<Position> getAvMoves()const;
        virtual int getAvMovesCnt()const;
        virtual char move(Position&,std::vector<std::vector<int>> &, char);
        virtual char getColor()const;
        virtual void checkMoves(std::vector<std::vector<int>> &,int,int,int,char,int);
        int getId()const;
        Figure &operator=(const Figure &);
};
#endif // FIGURES_HPP_INCLUDED

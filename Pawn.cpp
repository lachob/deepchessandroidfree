#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <stdexcept>

#include "Pawn.hpp"
#include "functions.hpp"
#define NO_EDGE -1
#define BOARD_SIZE 8

Pawn::Pawn():Figure(){
    setColor(' ');
}
Pawn::Pawn(std::string title, Position& p, int avMovesCnt, int id, char color):Figure(title,p,avMovesCnt,id){
    setColor(color);
    setStartPosWhite(true);
    setStartPosBlack(true);
}
Pawn::Pawn(const Pawn &other):Figure(other){
    color = other.color;
}
Pawn::Pawn(const Figure &other):Figure(other){
    if(other.getTitle()[0]=='b'){
        color = 'b';
    }
    else{
        color = 'w';
    }
}
Pawn::~Pawn(){}

int Pawn::getId()const{
    return Figure::getId();
}
void Pawn::setColor(char color){
    this->color = color;
}

char Pawn::getColor()const{
    return color;
}
void Pawn::setPosition(Position &p){
    Figure::setPosition(p);
}
Position &Pawn::getPosition(){
    return Figure::getPosition();
}

void Pawn::setAvailableMoves(int, Position& p){
    availableMoves.push_back(p);
}

void Pawn::setAvailableMovesCnt(int newCount){
    availableMovesCnt = newCount+1;
}

std::vector<Position> Pawn::getAvMoves()const{
    return availableMoves;
}
int Pawn::getAvMovesCnt()const{
   return availableMovesCnt;
}
void Pawn::setStartPosWhite(bool isstart){
    isStartPosWhite = isstart;
}
bool Pawn::isStPosWhite()const{
    return isStartPosWhite;
}
void Pawn::setStartPosBlack(bool isSt){
    isStartPosBlack = isSt;
}
bool Pawn::isStPosBlack()const{
    return isStartPosBlack;
}
void Pawn::addAvailableMoves(std::vector<std::vector<int>> &adjMatrix, char color){
    int row = getPosition().row;
    int col = chooseNumLetter(getPosition().col);
    int a=0;
    a=col-1<8&&col-1>-1?adjMatrix[row][col-1]:-2;
    int b=0;
    b=col+1<8&&col+1>-1?adjMatrix[row][col+1]:-2;
    int c=0;
    c=row-1<8&&row-1>-1?adjMatrix[row-1][col]:-2;
    int d=0;
    d=row+1<8&&row+1>-1?adjMatrix[row+1][col]:-2;

    if(a>=-1||b>=-1||c>=-1||d>=-1){
       int counter = -1;
        //White Pawns
       if(color=='w'){
        //Check moves from start position
        if(isStPosWhite()){
          for(int i=row+1;i<=row+2&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col;j<=col;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                if(i==BOARD_SIZE-1){
                    p.setP(i,chooseCol(j),getId(),true);
                    p.dirId = P_UP;
                    setAvailableMoves(++counter, p);
                }
                else{
                    p.setP(i,chooseCol(j),getId(),false);
                    if(i==row+1){
                        adjMatrix[i][j] = getId()*NO_EDGE;
                    }
                    p.dirId = P_UP;
                    setAvailableMoves(++counter, p);
                }
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        }
         else{
          for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col;j<=col;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                if(i==BOARD_SIZE-1){
                    p.setP(i,chooseCol(j),getId(),true);
                    p.dirId = P_UP;
                    setAvailableMoves(++counter, p);
                }
                else{
                    p.setP(i,chooseCol(j),getId(),false);
                    p.dirId = P_UP;
                    setAvailableMoves(++counter, p);
                }
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        }
        //Check moves takes figure left
        for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
               if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                  p.setP(i,chooseCol(j),getId(),WP_LEFT,NO_EDGE);
                  p.pawnHit = true;
                  setAvailableMoves(++counter, p);
                  if(adjMatrix[i][j]>=BP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST&&(adjMatrix[i][j]*NO_EDGE)==LastMoveInfo::lastMoveFigure){
                      bool b = false;
                      for(int i=0;i<BOARD_SIZE;i++){
                          for(int j=0;j<BOARD_SIZE;j++){
                              if(adjMatrix[i][j]==LastMoveInfo::lastMoveFigure&&i==4){
                                  b = true;
                                  break;
                              }
                          }
                          if(b)
                              break;
                      }
                      if(b){
                          p.setP(i,chooseCol(j),getId(),WP_LEFT,NO_EDGE);
                          p.enPassant = adjMatrix[i][j]*NO_EDGE;
                          p.pawnHit = false;
                          p.isTake = true;
                          setAvailableMoves(++counter, p);
                      }
                  }
                  flag = 1;
                  break;
               }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),WP_LEFT);
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    setAvailableMoves(++counter, p);
                    flag = 1;
                    break;
                }
                else{
                    p.isTake = true;
                    if(i==BOARD_SIZE-1){
                        p.isPawnTransf = true;
                        setAvailableMoves(++counter, p);
                        flag = 1;
                        break;
                    }
                    else{
                        setAvailableMoves(++counter, p);
                        flag = 1;
                        break;
                    }
                }
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check moves takes figure right
        for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
               if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                  p.setP(i,chooseCol(j),getId(),WP_RIGHT,NO_EDGE);
                  p.pawnHit = true;
                  setAvailableMoves(++counter, p);
                  if(adjMatrix[i][j]>=BP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST&&(adjMatrix[i][j]*NO_EDGE)==LastMoveInfo::lastMoveFigure){
                      bool b = false;
                      for(int i=0;i<BOARD_SIZE;i++){
                          for(int j=0;j<BOARD_SIZE;j++){
                              if(adjMatrix[i][j]==LastMoveInfo::lastMoveFigure&&i==4){
                                  b = true;
                                  break;
                              }
                          }
                          if(b)
                              break;
                      }
                      if(b){
                          p.setP(i,chooseCol(j),getId(),WP_RIGHT,NO_EDGE);
                          p.enPassant = adjMatrix[i][j]*NO_EDGE;
                          p.pawnHit = false;
                          p.isTake = true;
                          setAvailableMoves(++counter, p);
                      }
                  }
                  flag = 1;
                  break;
               }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),WP_RIGHT);
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    setAvailableMoves(++counter, p);
                    flag = 1;
                    break;
                }
                else{
                    p.isTake = true;
                    if(i==BOARD_SIZE-1){
                        p.isPawnTransf = true;
                        setAvailableMoves(++counter, p);
                        flag = 1;
                        break;
                    }
                    else{
                        setAvailableMoves(++counter, p);
                        flag = 1;
                        break;
                    }
                }
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
       }
        //Black pawns
       else if(color=='b'){
        //Check moves from start position
        if(isStPosBlack()){
          for(int i=row-1;i>=row-2&&i>=0;i--){
           Position p;
           int flag = 0;
           for(int j=col;j<=col;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                if(i==0){
                    p.setP(i,chooseCol(j),getId(),true);
                    p.dirId = P_UP;
                    setAvailableMoves(++counter, p);
                }
                else{
                    p.setP(i,chooseCol(j),getId(),false);
                    if(i==row-1){
                        adjMatrix[i][j] = getId()*NO_EDGE;
                    }
                    p.dirId = P_UP;
                    setAvailableMoves(++counter, p);
                }
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        }
         else{
          for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col;j<=col;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                if(i==0){
                    p.setP(i,chooseCol(j),getId(),true);
                    p.dirId = P_UP;
                    setAvailableMoves(++counter, p);
                }
                else{
                    p.setP(i,chooseCol(j),getId(),false);
                    p.dirId = P_UP;
                    setAvailableMoves(++counter, p);
                }
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        }
        //Check moves takes figure left
        for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
               if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                  p.setP(i,chooseCol(j),getId(),BP_LEFT,NO_EDGE);
                  p.pawnHit = true;
                  setAvailableMoves(++counter, p);
                  if(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=WP_ENPASS_FIRST&&(adjMatrix[i][j]*NO_EDGE)==LastMoveInfo::lastMoveFigure){
                      bool b = false;
                      for(int i=0;i<BOARD_SIZE;i++){
                          for(int j=0;j<BOARD_SIZE;j++){
                              if(adjMatrix[i][j]==LastMoveInfo::lastMoveFigure&&i==3){
                                  b = true;
                                  break;
                              }
                          }
                          if(b)
                              break;
                      }
                      if(b){
                          p.setP(i,chooseCol(j),getId(),BP_LEFT,NO_EDGE);
                          p.enPassant = adjMatrix[i][j]*NO_EDGE;
                          p.pawnHit = false;
                          p.isTake = true;
                          setAvailableMoves(++counter, p);
                      }
                  }
                  flag = 1;
                  break;
               }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),BP_LEFT);
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    setAvailableMoves(++counter, p);
                    flag = 1;
                    break;
                }
                else{
                    p.isTake = true;
                    if(i==0){
                        p.isPawnTransf = true;
                        setAvailableMoves(++counter, p);
                        flag = 1;
                        break;
                    }
                    else{
                        setAvailableMoves(++counter, p);
                        flag = 1;
                        break;
                    }
                }
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check moves takes figure right
        for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId(),BP_RIGHT,NO_EDGE);
                p.pawnHit = true;
                setAvailableMoves(++counter, p);
                if(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=WP_ENPASS_FIRST&&(adjMatrix[i][j]*NO_EDGE)==LastMoveInfo::lastMoveFigure){
                    bool b = false;
                    for(int i=0;i<BOARD_SIZE;i++){
                        for(int j=0;j<BOARD_SIZE;j++){
                            if(adjMatrix[i][j]==LastMoveInfo::lastMoveFigure&&i==3){
                                b = true;
                                break;
                            }
                        }
                        if(b)
                            break;
                    }
                    if(b){
                        p.setP(i,chooseCol(j),getId(),BP_RIGHT,NO_EDGE);
                        p.enPassant = adjMatrix[i][j]*NO_EDGE;
                        p.pawnHit = false;
                        p.isTake = true;
                        setAvailableMoves(++counter, p);
                    }
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),BP_RIGHT);
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    setAvailableMoves(++counter, p);
                    flag = 1;
                    break;
                }
                else{
                    p.isTake = true;
                    if(i==0){
                        p.isPawnTransf = true;
                        setAvailableMoves(++counter, p);
                        flag = 1;
                        break;
                    }
                    else{
                        setAvailableMoves(++counter, p);
                        flag = 1;
                        break;
                    }
                }
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
       }

        setAvailableMovesCnt(counter);
    }
    else{
        return;
    }
}
char Pawn::isAvailableMove(Position& p){
    ch = 'N';
    for(size_t i=0;i<getAvMoves().size();i++){
        if(getAvMoves()[i].col==p.col&&getAvMoves()[i].row==p.row&&getAvMoves()[i].prot==NO_EDGE){
            if(getAvMoves()[i].isCheck){
                ch = 'C';
                break;
            }
            else if(getAvMoves()[i].isTake&&getAvMoves()[i].isPawnTransf){
                ch='F';
                break;
            }
            else if(getAvMoves()[i].isTake&&!getAvMoves()[i].isPawnTransf&&getAvMoves()[i].enPassant == NO_EDGE){
                ch = 'T';
                break;
            }
            else if(!getAvMoves()[i].isTake&&getAvMoves()[i].isPawnTransf){
                ch = 'P';
                break;
            }
            else if(getAvMoves()[i].isTake&&getAvMoves()[i].enPassant > NO_EDGE){
                ch = 'E';
                break;
            }
            ch = 'Y';
        }
    }
    return ch;
}
char Pawn::move(Position& dest, std::vector<std::vector<int>> &adjMatrix, char color){
    chMov = isAvailableMove(dest);
    Position p;
    //cout<<"Is av move: "<<ch<<endl;
    if(chMov!='N'){
        if(chMov=='C'){
            return chMov;
        }
        if(chMov=='E'){
            p.setP(getPosition().row,getPosition().col,getId());
            adjMatrix[p.row][chooseNumLetter(p.col)] = NO_EDGE;
            int enpId = adjMatrix[dest.row][chooseNumLetter(dest.col)]*NO_EDGE;
            adjMatrix[dest.row][chooseNumLetter(dest.col)] = getId();
            setPosition(dest);
            for(int i=0;i<BOARD_SIZE;i++){
                for(int j=0;j<BOARD_SIZE;j++){
                    if(adjMatrix[i][j]==enpId){
                        adjMatrix[i][j] = NO_EDGE;
                    }
                }
            }
        }
        p.setP(getPosition().row,getPosition().col,getId());
        adjMatrix[p.row][chooseNumLetter(p.col)] = NO_EDGE;
        adjMatrix[dest.row][chooseNumLetter(dest.col)] = getId();
        setPosition(dest);
        if(chMov=='T'){
            if(color=='w'&&isStPosWhite()&&
              (adjMatrix[p.row+1][chooseNumLetter(p.col)]>=WP_ENPASS_LAST&&adjMatrix[p.row+1][chooseNumLetter(p.col)]<=WP_ENPASS_FIRST)){
                adjMatrix[p.row+1][chooseNumLetter(p.col)] = NO_EDGE;
            }
            else if(color=='b'&&isStPosBlack()&&
                   (adjMatrix[p.row-1][chooseNumLetter(p.col)]>=BP_ENPASS_LAST&&adjMatrix[p.row-1][chooseNumLetter(p.col)]<=BP_ENPASS_FIRST)){
               adjMatrix[p.row-1][chooseNumLetter(p.col)] = NO_EDGE;
            }
            return chMov;
        }
        else if(chMov=='Y'){
            return chMov;
        }
        else if(chMov=='P'){
            return chMov;
        }
        else if(chMov=='F'){
            return chMov;
        }
    }
    else{
       return chMov;
    }
    return chMov;
}
Pawn &Pawn::operator=(const Pawn &other){
    Figure::operator=(other);
    color = other.color;
    isStartPosWhite = other.isStartPosWhite;
    isStartPosBlack = other.isStartPosBlack;
    return *this;
}
Pawn &Pawn::operator=(const Figure &other){
    Figure::operator=(other);
    if(other.getTitle()[0]=='b'){
        color = 'b';
    }
    else{
        color = 'w';
    }
    return *this;
}

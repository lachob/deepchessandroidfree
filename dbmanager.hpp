#ifndef DBMANAGER_HPP
#define DBMANAGER_HPP

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QNetworkInterface>
#include <QtGlobal>
#include <QFile>
#include <QDir>
#include <vector>
#include <memory>
#include <string>
#include <iostream>
#include <cstdlib>
#include <QSqlRecord>
#include <QStandardPaths>
#include <QStringLiteral>

#define  GAMES_PASSED_FOR_RATE     (100)

class DBManager
{

public:
    DBManager();
    virtual ~DBManager();
    void queryDBGames(std::vector<QString> &v);
    QString queryDBGamesByName(const QString &name, const int type = 0);
    bool saveGame(const QString &name, QString game, QString PGNString, QString &gameEndResultTr);
    bool deleteGame(const QString &name);
    bool updateDBAchievemnts(QString level,int result);
    int queryDBAchievemnts(QString level);
    int queryRateCount();
    bool isUsedRate();
    void setUsedRate();
    //EXPORT
    QString exportFile(const QString &name, QChar &color, int plLevel, bool isGreaterThan11);
    //PUZZLE
    bool loadPuzzle(QString &puzzleFen, QStringList &puzzleMoves);//TESTPUZZ
    void updatePuzzleStatus(int &puzzCnt);
    bool checkPuzzle();
    int checkGameCnt();
    void disablePuzzle(bool condition);
    void updateGameCnt(int gameCnt);
    //GDPR
    bool checkGDPR();
    void updateGDPR();
    //POLYBOOK
    QString checkBooks();
    void updateBooks(const QString);
    //LASTLEVEL
    void updateLastLevel(int level);
    int getLastLevel();
    QString dbExistsInfo{""};
    bool isDbOpened{false};
private:
    QSqlDatabase userData;
    QSqlDatabase puzzleData;
    const QString DRIVER{"QSQLITE"};
    int puzzleId{0};
    static int connNumber;
    //bool dropDB() ;
};

#endif // DBMANAGER_HPP

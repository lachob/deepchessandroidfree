
#include "deepchess.h"

#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    //    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    //    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setAttribute(Qt::AA_DisableHighDpiScaling);
    QApplication::setAttribute(Qt::AA_DisableHighDpiScaling);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough);

    QApplication app(argc, argv);
    //QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough);

    QTranslator translator;
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QString locale = QLocale::system().name();
    QString lang = locale.mid(0,2).toLower();
    translator.load((QString("assets:translations/deepchess_") + lang + QString(".qm")));
    app.installTranslator(&translator);

    DeepChess game;
    return app.exec();
}

#include <string>
#include <sstream>
#include "functions.hpp"
#include "Figures.hpp"

int chooseNumLetter(char s){
  if(s=='A'){
    return 0;
  }
  else if(s=='B'){
    return 1;
  }
   else if(s=='C'){
    return 2;
  }
   else if(s=='D'){
    return 3;
  }
  else if(s=='E'){
    return 4;
  }
   else if(s=='F'){
    return 5;
  }
  else if(s=='G'){
    return 6;
  }
   else if(s=='H'){
    return 7;
  }
   else{
    return -1;
   }
}
char chooseCol(int row){
  switch(row){
    case 0:
        return 'A';
        break;
    case 1:
        return 'B';
        break;
    case 2:
        return 'C';
        break;
    case 3:
        return 'D';
        break;
    case 4:
        return 'E';
        break;
    case 5:
        return 'F';
        break;
    case 6:
        return 'G';
        break;
    case 7:
        return 'H';
        break;
    default:
        return 'N';
  }
}
char chooseColLow(int row){
  switch(row){
    case 0:
        return 'a';
    case 1:
        return 'b';
    case 2:
        return 'c';
    case 3:
        return 'd';
    case 4:
        return 'e';
    case 5:
        return 'f';
    case 6:
        return 'g';
    case 7:
        return 'h';
    default:
        return 'N';
  }
}

QString chooseCommandByLevel(const QString &command_text)
{
    QString result = "";
    if(command_text=="Level 1"){
        result = "go depth 1";
    }
    else if(command_text=="Level 2"){
        result = "go depth 2";
    }
    else if(command_text=="Level 3"){
        result = "go depth 3";
    }
    else if(command_text=="Level 4"){
        result = "go depth 4";
    }
    else if(command_text=="Level 5"){
        result = "go depth 5";
    }
    else if(command_text=="Level 6"){
        result = "go depth 6";
    }
    else if(command_text=="Level 7"){
        result = "go depth 7";
    }
    else if(command_text=="Level 8"){
        result = "go depth 8";
    }
    else if(command_text=="Level 9"){
        result = "go depth 9";
    }
    else if(command_text=="Level 10"){
        result = "go depth 10";
    }
    else if(command_text=="Level 11"){
        result = "go depth 11";
    }
    else if(command_text=="Level 12"){
        result = "go depth 12";
    }
    else if(command_text=="Level 13"){
        result = "go depth 13";
    }
    else if(command_text=="Level 14"){
        result = "go depth 14";
    }
    else if(command_text=="Level 15"){
        result = "go depth 15";
    }
    else if(command_text=="Level 16"){
        result = "go depth 16";
    }
    else if(command_text=="Level 17"){
        result = "go depth 17";
    }
    else if(command_text=="Level 18"){
        result = "go depth 18";
    }
    else if(command_text=="Level 19"){
        result = "go depth 19 movetime 3000";
    }
    else if(command_text=="Level 20"){
        result = "go depth 27 movetime 3500";
    }
    return result;
}

std::string chooseHintMoveLevel(const QString &command_text)
{
    std::string result = "";
    if(command_text=="Level 1"){
        result = "go depth 3";
    }
    else if(command_text=="Level 2"){
        result = "go depth 4";
    }
    else if(command_text=="Level 3"){
        result = "go depth 5";
    }
    else if(command_text=="Level 4"){
        result = "go depth 6";
    }
    else if(command_text=="Level 5"){
        result = "go depth 7";
    }
    else if(command_text=="Level 6"){
        result = "go depth 8";
    }
    else if(command_text=="Level 7"){
        result = "go depth 9";
    }
    else if(command_text=="Level 8"){
        result = "go depth 10";
    }
    else if(command_text=="Level 9"){
        result = "go depth 11";
    }
    else if(command_text=="Level 10"){
        result = "go depth 12";
    }
    else if(command_text=="Level 11"){
        result = "go depth 13";
    }
    else if(command_text=="Level 12"){
        result = "go depth 14";
    }
    else if(command_text=="Level 13"){
        result = "go depth 15";
    }
    else if(command_text=="Level 14"){
        result = "go depth 16";
    }
    else if(command_text=="Level 15"){
        result = "go depth 16";
    }
    else if(command_text=="Level 16"){
        result = "go depth 17";
    }
    else if(command_text=="Level 17"){
        result = "go depth 18";
    }
    else if(command_text=="Level 18"){
        result = "go depth 19";
    }
    else if(command_text=="Level 19"){
        result = "go depth 20 movetime 3000";
    }
    else if(command_text=="Level 20"){
        result = "go depth 27 movetime 3500";
    }
    return result;
}

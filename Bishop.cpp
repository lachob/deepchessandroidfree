#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <stdexcept>

#include "Bishop.hpp"
#include "functions.hpp"
#define NO_EDGE -1
#define BOARD_SIZE 8

Bishop::Bishop():Figure(){
    setColor(' ');
}
Bishop::Bishop(std::string title, Position& p, int avMovesCnt, int id, char color):Figure(title,p,avMovesCnt,id){
    setColor(color);
}
Bishop::Bishop(const Bishop &other):Figure(other){
    color = other.color;
}
Bishop::Bishop(const Figure &other):Figure(other){
    if(other.getTitle()[0]=='b'){
        color = 'b';
    }
    else{
        color = 'w';
    }
}
Bishop::~Bishop(){}
void Bishop::setId(int id){
    Figure::setId(id);
}
int Bishop::getId()const{
    return Figure::getId();
}
void Bishop::setColor(char color){
    this->color = color;
}
void Bishop::setTitle(std::string title){
    Figure::setTitle(title);
}
std::string Bishop::getTitle()const{
    return Figure::getTitle();
}
char Bishop::getColor()const{
    return color;
}
void Bishop::setPosition(Position &p){
    Figure::setPosition(p);
}
Position& Bishop::getPosition(){
    return Figure::getPosition();
}
int Bishop::incAvMovesCnt(){
    return Figure::incAvMovesCnt();
}
void Bishop::setAvailableMoves(int index, Position& p){
    availableMoves.push_back(p);
}
void Bishop::setAvailableMovesCnt(int newCount){
    availableMovesCnt = newCount+1;
}
std::vector<Position> Bishop::getAvMoves()const{
    return availableMoves;
}
int Bishop::getAvMovesCnt()const{
   return availableMovesCnt;
}
void Bishop::addAvailableMoves(std::vector<std::vector<int>> &adjMatrix, char color){
    int row = getPosition().row;
    int col = chooseNumLetter(getPosition().col);
    int a=0;
    a=col-1<8&&col-1>-1?adjMatrix[row][col-1]:-2;
    int b=0;
    b=col+1<8&&col+1>-1?adjMatrix[row][col+1]:-2;
    int c=0;
    c=row-1<8&&row-1>-1?adjMatrix[row-1][col]:-2;
    int d=0;
    d=row+1<8&&row+1>-1?adjMatrix[row+1][col]:-2;

    if(a>=-1||b>=-1||c>=-1||d>=-1){
       int counter = -1;
       int colCnt = 0;
        //White Officers
       if(color=='w'){
        //Check moves up-left
        int flag = 0;
        int openChPr = 0;
        int openChMe = 0;
        int protFigCnt = 0;
         for(int i=row+1;i<BOARD_SIZE;i++){
           Position p;
           Position p1;
           colCnt--;
           for(int j=col+colCnt;j<=col+colCnt;j++){
             if(j<0){break;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),O_UPLEFT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==BK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,O_UPLEFT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=BK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==BK){
                p.setP(row,chooseCol(col),openChMe,O_UPLEFT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),O_UPLEFT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),O_UPLEFT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                 protFigCnt++;
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),O_UPLEFT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i+1<BOARD_SIZE)&&(j-1>NO_EDGE)&&((adjMatrix[i+1][j-1]==NO_EDGE)||(adjMatrix[i+1][j-1]>=WR1&&adjMatrix[i+1][j-1]<=WO2)
                                                         ||(adjMatrix[i+1][j-1]>=WP_TRANSF_FIRST&&adjMatrix[i+1][j-1]<=WP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i+1,chooseCol(j-1),getId(),O_UPLEFT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),O_UPLEFT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check moves up-right
         flag = 0;
         openChPr = 0;
         openChMe = 0;
         protFigCnt = 0;
         colCnt = 0;
         for(int i=row+1;i<BOARD_SIZE;i++){
           Position p;
           Position p1;
           colCnt++;
           for(int j=col+colCnt;j<=col+colCnt;j++){
             if(j>BOARD_SIZE-1){break;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),O_UPRIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==BK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,O_UPRIGHT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=BK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==BK){
                p.setP(row,chooseCol(col),openChMe,O_UPRIGHT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),O_UPRIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),O_UPRIGHT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),O_UPRIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i+1<BOARD_SIZE)&&(j+1<BOARD_SIZE)&&((adjMatrix[i+1][j+1]==NO_EDGE)||(adjMatrix[i+1][j+1]>=WR1&&adjMatrix[i+1][j+1]<=WO2)||
                                                         (adjMatrix[i+1][j+1]>=WP_TRANSF_FIRST&&adjMatrix[i+1][j+1]<=WP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i+1,chooseCol(j+1),getId(),O_UPRIGHT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),O_UPRIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check moves down-left
         flag = 0;
         openChPr = 0;
         openChMe = 0;
         protFigCnt = 0;
         colCnt = 0;
         for(int i=row-1;i>=0;i--){
           Position p;
           Position p1;
           colCnt--;
           for(int j=col+colCnt;j<=col+colCnt;j++){
             if(j<0){break;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),O_DOWNLEFT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==BK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,O_DOWNLEFT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=BK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==BK){
                p.setP(row,chooseCol(col),openChMe,O_DOWNLEFT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),O_DOWNLEFT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),O_DOWNLEFT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),O_DOWNLEFT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i-1>NO_EDGE)&&(j-1>NO_EDGE)&&((adjMatrix[i-1][j-1]==NO_EDGE)||(adjMatrix[i-1][j-1]>=WR1&&adjMatrix[i-1][j-1]<=WO2)||
                                                         (adjMatrix[i-1][j-1]>=WP_TRANSF_FIRST&&adjMatrix[i-1][j-1]<=WP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i-1,chooseCol(j-1),getId(),O_DOWNLEFT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),O_DOWNLEFT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check moves down-right
         flag = 0;
         openChPr = 0;
         openChMe = 0;
         protFigCnt = 0;
         colCnt = 0;
         for(int i=row-1;i>=0;i--){
           Position p;
           Position p1;
           colCnt++;
           for(int j=col+colCnt;j<=col+colCnt;j++){
             if(j>BOARD_SIZE-1){break;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==BK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,O_DOWNRIGHT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=BK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==BK){
                p.setP(row,chooseCol(col),openChMe,O_DOWNRIGHT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i-1>NO_EDGE)&&(j+1<BOARD_SIZE)&&((adjMatrix[i-1][j+1]==NO_EDGE)||(adjMatrix[i-1][j+1]>=WR1&&adjMatrix[i-1][j+1]<=WO2)||
                                                         (adjMatrix[i-1][j+1]>=WP_TRANSF_FIRST&&adjMatrix[i-1][j+1]<=WP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i-1,chooseCol(j+1),getId(),O_DOWNRIGHT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
       }
        //Black officers
       else if(color=='b'){
        //Check moves up-left
        int flag = 0;
         int openChPr = 0;
         int openChMe = 0;
         int protFigCnt = 0;
        colCnt = 0;
         for(int i=row+1;i<BOARD_SIZE;i++){
           Position p;
           Position p1;
           colCnt--;
           for(int j=col+colCnt;j<=col+colCnt;j++){
             if(j<0){break;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),O_UPLEFT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==WK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,O_UPLEFT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=WK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==WK){
                p.setP(row,chooseCol(col),openChMe,O_UPLEFT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),O_UPLEFT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),O_UPLEFT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),O_UPLEFT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i+1<BOARD_SIZE)&&(j-1>NO_EDGE)&&((adjMatrix[i+1][j-1]>=BR1&&adjMatrix[i+1][j-1]<=BO2)||
                                                         (adjMatrix[i+1][j-1]>=BP_TRANSF_FIRST&&adjMatrix[i+1][j-1]<=BP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i+1,chooseCol(j-1),getId(),O_UPLEFT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),O_UPLEFT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check moves up-right
         flag = 0;
         openChPr = 0;
         openChMe = 0;
         protFigCnt = 0;
         colCnt = 0;
         for(int i=row+1;i<BOARD_SIZE;i++){
           Position p;
           Position p1;
           colCnt++;
           for(int j=col+colCnt;j<=col+colCnt;j++){
             if(j>BOARD_SIZE-1){break;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),O_UPRIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==WK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,O_UPRIGHT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=WK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==WK){
                p.setP(row,chooseCol(col),openChMe,O_UPRIGHT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),O_UPRIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),O_UPRIGHT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),O_UPRIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i+1<BOARD_SIZE)&&(j+1<BOARD_SIZE)&&((adjMatrix[i+1][j+1]>=BR1&&adjMatrix[i+1][j+1]<=BO2)||
                                                         (adjMatrix[i+1][j+1]>=BP_TRANSF_FIRST&&adjMatrix[i+1][j+1]<=BP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i+1,chooseCol(j+1),getId(),O_UPRIGHT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),O_UPRIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check moves down-left
         flag = 0;
         openChPr = 0;
         openChMe = 0;
         protFigCnt = 0;
         colCnt = 0;
         for(int i=row-1;i>=0;i--){
           Position p;
           Position p1;
           colCnt--;
           for(int j=col+colCnt;j<=col+colCnt;j++){
             if(j<0){break;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),O_DOWNLEFT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==WK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,O_DOWNLEFT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=WK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==WK){
                p.setP(row,chooseCol(col),openChMe,O_DOWNLEFT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),O_DOWNLEFT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),O_DOWNLEFT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),O_DOWNLEFT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i-1>NO_EDGE)&&(j-1>NO_EDGE)&&((adjMatrix[i-1][j-1]>=BR1&&adjMatrix[i-1][j-1]<=BO2)||
                                                         (adjMatrix[i-1][j-1]>=BP_TRANSF_FIRST&&adjMatrix[i-1][j-1]<=BP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i-1,chooseCol(j-1),getId(),O_DOWNLEFT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),O_DOWNLEFT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
          //Check moves down-right
          flag = 0;
         openChPr = 0;
         openChMe = 0;
         protFigCnt = 0;
          colCnt = 0;
         for(int i=row-1;i>=0;i--){
           Position p;
           Position p1;
           colCnt++;
           for(int j=col+colCnt;j<=col+colCnt;j++){
             if(j>BOARD_SIZE-1){break;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==WK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,O_DOWNRIGHT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=WK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==WK){
                p.setP(row,chooseCol(col),openChMe,O_DOWNRIGHT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i-1>NO_EDGE)&&(j+1<BOARD_SIZE)&&((adjMatrix[i-1][j+1]>=BR1&&adjMatrix[i-1][j+1]<=BO2)||
                                                         (adjMatrix[i-1][j+1]>=BP_TRANSF_FIRST&&adjMatrix[i-1][j+1]<=BP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i-1,chooseCol(j+1),getId(),O_DOWNRIGHT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),O_DOWNRIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
        }

        setAvailableMovesCnt(counter);
    }
    else{
        return;
    }
}
char Bishop::isAvailableMove(Position &p){
    ch = 'N';
    for(size_t i=0;i<getAvMoves().size();i++){
        if(getAvMoves()[i].col==p.col&&getAvMoves()[i].row==p.row&&getAvMoves()[i].prot==NO_EDGE){
            if(getAvMoves()[i].isCheck){
                ch = 'C';
                break;
            }
            else if(getAvMoves()[i].isTake){
                ch = 'T';
                break;
            }
            ch = 'Y';
        }
    }
    return ch;
}

char Bishop::move(Position &dest, std::vector<std::vector<int>> &adjMatrix, char color){
    chMov = isAvailableMove(dest);
    Position p;
    //cout<<"Is av move: "<<ch<<endl;
    if(chMov!='N'){
        if(chMov=='C'){
            return chMov;
        }
        p.setP(getPosition().row,getPosition().col,getId());
        adjMatrix[p.row][chooseNumLetter(p.col)] = NO_EDGE;
        adjMatrix[dest.row][chooseNumLetter(dest.col)] = getId();
        setPosition(dest);
        addAvailableMoves(adjMatrix,color);
        if(chMov=='T'){
            return chMov;
        }
        else if(chMov=='Y'){
            return chMov;
        }
    }
    else{
       return chMov;
    }
    return chMov;
}
Bishop &Bishop::operator=(const Bishop &other){
    Figure::operator=(other);
    color = other.color;
    return *this;
}

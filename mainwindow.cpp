#include "mainwindow.h"

#include <QPixmapCache>

namespace PSQT {
void init();
}
namespace Search{
volatile int skillLevel = 0;
volatile int skillLevelPlay = 0;
}

MainWindow::MainWindow(bool isHintActivated, bool isPuzzle, bool figuresChoosed, int bannerCnt, QChar &_theme, QWidget *parent, const bool isBaner, int gamesCnt) :
    QMainWindow(parent),
    connection(std::make_unique< DBManager>()),
    ui(std::make_unique<Ui::MainWindow>()),
    startBoard(std::make_unique< StartPosGraph>()),
    achievementType(tr("Bronze Star")),
    currentLoadGameName(tr("No saved games"))
{
    if(!ui) {
        isCorrectInitialization = false;
        return;
    }
    ui->setupUi(this);
    setWindowTitle("Deep Chess");
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    if(!startBoard) {
        isCorrectInitialization = false;
        return;
    }
    startBoard->init();

//    auto dpiValue = QGuiApplication::primaryScreen()->logicalDotsPerInch();
//    //QMessageBox::information(this, "Info", ("DPI="+QString::number(dpiValue)));
//    if(dpiValue >= 96.0) {
//        try {
//            QPixmapCache::setCacheLimit((QPixmapCache::cacheLimit() + ((QPixmapCache::cacheLimit() * 20) / 100)));
//            //QPixmapCache::setCacheLimit(2048);
//        } catch(...) {}
//    }

    newFiguresSelected = figuresChoosed;
    theme = _theme;
    if(theme=='s'){
        whitesColor = "rgb(215, 223, 234)";
        blacksColor = "rgb(131, 156, 196)";
        buttonsColor = "rgb(93, 109, 135)";
        chooseColor = "rgb(79, 93, 115)";
    }
    else if(theme=='k'){
        whitesColor = "rgb(238,211,124)";
        blacksColor = "rgb(124,96,5)";
        buttonsColor = "rgb(65,51,3)";
        chooseColor = "rgb(44, 34, 2)";
    }
    else if(theme=='z'){
        whitesColor = "rgb(189,252,182)";
        blacksColor = "rgb(93,190,80)";
        buttonsColor = "rgb(14, 87, 5);";//"rgb(18,112,7)";
        chooseColor = "rgb(15, 91, 6)";
    }
    centralWidget()->setStyleSheet("background-color:"+buttonsColor+";");
    //HINT
    isHintActivatedInMainWindow = isHintActivated;
    //ADS
    isBanner = isBaner;
    isPuzzleAct = isPuzzle;
    gamesCount = gamesCnt;
    if(bannerCnt >= BANNER_ADS_COUNT)
        bannerShut=true;

    screen = QApplication::screens().at(0);
    mobWidth = screen->size().width();
    checkHeight = screen->size().height();
    double ratio = 0;
    double geomHeight = 0;
    double geomWidth = 0;
    double geomButtonHeight = 0;

    if(mobWidth > checkHeight){
        gameInfoWidth = mobWidth;
        mobWidth = checkHeight-46;
        fieldSize = (mobWidth*0.125)-2;
        icsize = (mobWidth*0.11805555)-2;
        isResized = true;
        btnNumFontS = checkHeight*0.027;
    }
    else{
        isResized = false;
        fieldSize = mobWidth*0.125;
        icsize = mobWidth*0.11805555;
        btnNumFontS =  mobWidth*0.027;
    }
    ratio = 0;
    geomHeight = 0;
    geomWidth = 0;

    // The memory recollection in QT5 works without the need to call delete for every new as far as there is a tree hierarchy for the objects starting from parent(usually it is QMainWindow)
    // Then when the parent is deleted all its children are deleted as well
    if(!connection) {
        isCorrectInitialization = false;
        return;
    }
    widget = new QWidget(this);
    if(!widget) {
        isCorrectInitialization = false;
        return;
    }
    btnFontSiz = mobWidth*0.04555555;//0.05555555
    ratio = mobWidth*1.6944444;
    widget->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
    bannerHNotAv = static_cast<int>(ratio+1)+3000;
    widget->adjustSize();

    grid = new QGridLayout(widget);
    grid->setContentsMargins(0, 0, 0, 0);
    grid->setSpacing(0);
    grid->setHorizontalSpacing(0);
    grid->setVerticalSpacing(0);
    grid->setSizeConstraint(QLayout::SetFixedSize);

    ratio = mobWidth*0.11111;
    int sqW = static_cast<int>(ratio+1);
    int sqH = static_cast<int>(ratio+1);

    //LASTLEVEL
    playLevel = connection->getLastLevel();

    //BOOKS
    Search::bookSettings.path = connection->checkBooks().toLocal8Bit().constData();
    if(Search::bookSettings.path != "<empty>"){
        Search::bookSettings.ownBook = true;
    }
    else{
        Search::bookSettings.ownBook = false;
    }

    //SAVE GAME
    saveGameWidget = new QWidget(this);
    if(!saveGameWidget) {
        isCorrectInitialization = false;
        return;
    }
    saveGameWidget->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
    saveGameWidget->setStyleSheet("background-color:"+whitesColor+";");
    QVBoxLayout *saveGameLayout = new QVBoxLayout(saveGameWidget);
    if(!saveGameLayout) {
        isCorrectInitialization = false;
        return;
    }
    saveGameButton = new QPushButton(tr("Save"), saveGameWidget);
    if(!saveGameButton) {
        isCorrectInitialization = false;
        return;
    }
    saveGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    saveGameButton->setMinimumWidth(sqW);
    saveGameButton->setMinimumHeight(sqH);
    saveGameButton->adjustSize();
    connect(saveGameButton,&QPushButton::clicked,this,&MainWindow::saveGameDB);
    closeGameButton = new QPushButton(tr("Close"), saveGameWidget);
    if(!closeGameButton) {
        isCorrectInitialization = false;
        return;
    }
    closeGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                   +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    closeGameButton->setMinimumWidth(sqW);
    closeGameButton->setMinimumHeight(sqH);
    closeGameButton->adjustSize();
    connect(closeGameButton,&QPushButton::clicked,this,&MainWindow::closeSaveGameDB);
    inputDialog = new QInputDialog(saveGameWidget);
    if(!inputDialog) {
        isCorrectInitialization = false;
        return;
    }
    inputDialog->setMinimumWidth(sqW);
    inputDialog->setMinimumHeight(sqH);
    inputDialog->adjustSize();
    QHBoxLayout *inputDialogLayout = new QHBoxLayout(saveGameWidget);
    if(!inputDialogLayout) {
        isCorrectInitialization = false;
        return;
    }
    inputDialogLayout->addWidget(inputDialog);
    QHBoxLayout *saveButtonsLayout = new QHBoxLayout(saveGameWidget);
    if(!saveButtonsLayout) {
        isCorrectInitialization = false;
        return;
    }
    saveButtonsLayout->addWidget(saveGameButton);
    saveButtonsLayout->addWidget(closeGameButton);
    saveGameLayout->addLayout(saveButtonsLayout);
    saveGameLayout->addLayout(inputDialogLayout);
    saveGameWidget->hide();

    sideBar = new QWidget(this);
    if(!sideBar) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.2777777;
    sideBar->setGeometry(0,mobWidth,mobWidth,static_cast<int>(ratio+1));
    sideBar->setStyleSheet("background-color:"+whitesColor+";");
    QHBoxLayout *chooseGameTypeLayout = new QHBoxLayout(sideBar);
    if(!chooseGameTypeLayout) {
        isCorrectInitialization = false;
        return;
    }
    playAgainstComputer = new QPushButton(tr("Start"),sideBar);
    if(!playAgainstComputer) {
        isCorrectInitialization = false;
        return;
    }
    playAgainstComputer->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                       +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    geomWidth = mobWidth*0.375;
    geomHeight = mobWidth*0.1388888;
    playAgainstComputer->setMinimumWidth(static_cast<int>(geomWidth+1));
    playAgainstComputer->setMinimumHeight(static_cast<int>(geomHeight+1));
    playAgainstComputer->adjustSize();
    //LOAD GAME
    prevMoveButton = new QPushButton("<<",sideBar);
    if(!prevMoveButton) {
        isCorrectInitialization = false;
        return;
    }
    prevMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    prevMoveButton->setMinimumWidth(static_cast<int>(geomWidth/2-5));
    prevMoveButton->setMinimumHeight(static_cast<int>(geomHeight+1));
    prevMoveButton->adjustSize();
    connect(prevMoveButton,&QPushButton::clicked,this,&MainWindow::prevMoveLG);
    prevMoveButton->hide();
    nextMoveButton = new QPushButton(">",sideBar);
    if(!nextMoveButton) {
        isCorrectInitialization = false;
        return;
    }
    nextMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    nextMoveButton->setMinimumWidth(static_cast<int>(geomWidth/2-5));
    nextMoveButton->setMinimumHeight(static_cast<int>(geomHeight+1));
    nextMoveButton->adjustSize();
    connect(nextMoveButton,&QPushButton::clicked,this,&MainWindow::nextMoveLG);
    nextMoveButton->hide();
    //SETTINGS
    resetButton = new QPushButton(tr("Reset"),sideBar);
    if(!resetButton) {
        isCorrectInitialization = false;
        return;
    }
    resetButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                               +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    resetButton->setMinimumWidth(static_cast<int>(geomWidth+1));
    resetButton->setMinimumHeight(static_cast<int>(geomHeight+1));
    resetButton->adjustSize();
    connect(resetButton,&QPushButton::clicked,this,&MainWindow::resetButtonClicked);

    chooseGameTypeLayout->addWidget(playAgainstComputer,0,Qt::AlignLeft);
    chooseGameTypeLayout->addWidget(prevMoveButton);
    chooseGameTypeLayout->addWidget(nextMoveButton);
    chooseGameTypeLayout->addWidget(resetButton,0,Qt::AlignRight);
    //SETTINGS
    connect(playAgainstComputer, SIGNAL(clicked()), this, SLOT(chooseChessAIPlayingStrengthShow()));

    newGameWidget = new QWidget(this);
    if(!newGameWidget) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.277777;
    newGameWidget->setGeometry(0,mobWidth,mobWidth,static_cast<int>(ratio+1));
    newGameWidget->setStyleSheet("background-color:"+whitesColor+";");
    QHBoxLayout *newGameLayout = new QHBoxLayout(newGameWidget);
    if(!newGameLayout) {
        isCorrectInitialization = false;
        return;
    }
    newGameButton = new QPushButton(tr("New Game"),newGameWidget);
    if(!newGameButton) {
        isCorrectInitialization = false;
        return;
    }
    newGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                 +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    newGameButton->setMinimumWidth(static_cast<int>(geomWidth+1));
    newGameButton->setMinimumHeight(static_cast<int>(geomHeight+1));
    newGameButton->adjustSize();
    newGameLayout->addWidget(newGameButton,1,Qt::AlignHCenter);
    newGameWidget->setLayout(newGameLayout);
    connect(newGameButton, &QPushButton::clicked, this, &MainWindow::newGameClicked);
    newGameWidget->hide();

    resignWidget = new QWidget(this);
    if(!resignWidget) {
        isCorrectInitialization = false;
        return;
    }
    resignWidget->setGeometry(0,mobWidth,mobWidth,static_cast<int>(ratio+1));
    resignWidget->setStyleSheet("background-color:"+whitesColor+";");
    QHBoxLayout *resignWidgetLayout = new QHBoxLayout(resignWidget);
    if(!resignWidgetLayout) {
        isCorrectInitialization = false;
        return;
    }

    resignButton = new QPushButton(tr("Resign"),resignWidget);
    if(!resignButton) {
        isCorrectInitialization = false;
        return;
    }
    resignButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    resignButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
    resignButton->setMinimumHeight(static_cast<int>(geomHeight+1));
    resignButton->adjustSize();
    resignWidgetLayout->addWidget(resignButton,0,Qt::AlignLeft);

    resignWidget->setLayout(resignWidgetLayout);
    connect(resignButton,&QPushButton::clicked,this,&MainWindow::gameResigned);

    resignImageLabel = new QPushButton(resignWidget);
    if(!resignImageLabel) {
        isCorrectInitialization = false;
        return;
    }
    resignImageLabelWidth = mobWidth-static_cast<int>((geomWidth*2)-(mobWidth*0.0277777));
    resignImageLabelHeight = static_cast<int>(geomHeight-(mobWidth*0.0277777));
    resignImageLabel->setMinimumWidth(resignImageLabelWidth);
    resignImageLabel->setMinimumHeight(resignImageLabelHeight);
    resignImageLabel->adjustSize();
    resignImageLabel->setStyleSheet("background-color:"+whitesColor+";border:none;outline:none");
    resignImageLabel->setIcon(QIcon(achievementsLabelPath));
    resignImageLabel->setIconSize(QSize(resignImageLabelWidth,resignImageLabelHeight));
    resignWidgetLayout->addWidget(resignImageLabel);
    resignImageLabel->hide();

    drawButton = new QPushButton(tr("Draw"),resignWidget);
    if(!drawButton) {
        isCorrectInitialization = false;
        return;
    }
    drawButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                              +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    drawButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
    drawButton->setMinimumHeight(static_cast<int>(geomHeight+1));
    drawButton->adjustSize();
    resignWidgetLayout->addWidget(drawButton,0,Qt::AlignRight);
    connect(drawButton,&QPushButton::clicked,this,&MainWindow::proposeDraw);
    resignWidget->hide();

    double levelGeomHeight = mobWidth*1.277777;
    ratio = mobWidth*0.18055555;

    //HINT
    if(isHintActivatedInMainWindow){
        hintWidget = new QWidget(this);
        if(!hintWidget) {
            isCorrectInitialization = false;
            return;
        }
        hintWidget->setGeometry(0,static_cast<int>(levelGeomHeight+1),mobWidth,static_cast<int>(ratio+1));
        hintWidget->setStyleSheet("background-color:"+whitesColor+";");
        hintWidgetLayout = new QHBoxLayout(hintWidget);
        if(!hintWidgetLayout) {
            isCorrectInitialization = false;
            return;
        }
        hintButton = new QPushButton(tr("Hint"),hintWidget);
        if(!hintButton) {
            isCorrectInitialization = false;
            return;
        }
        hintButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
        hintButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
        hintButton->setMinimumHeight(static_cast<int>(geomHeight-(mobWidth*0.01388888)));
        hintButton->adjustSize();
        //Move Back
        undoMoveButton = new QPushButton(tr("Undo"),hintWidget);
        if(!undoMoveButton) {
            isCorrectInitialization = false;
            return;
        }
        undoMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
        undoMoveButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
        undoMoveButton->setMinimumHeight(static_cast<int>(geomHeight-(mobWidth*0.01388888)));
        undoMoveButton->adjustSize();
        hintWidgetLayout->addWidget(hintButton,1,Qt::AlignLeft);
        hintWidgetLayout->addWidget(undoMoveButton,1,Qt::AlignRight);
        connect(hintButton, &QPushButton::clicked, this, &MainWindow::startSearchHintMove);
        connect(this, &MainWindow::hintMoveFound, this, &MainWindow::showHintMove);
        connect(undoMoveButton, &QPushButton::clicked, this, &MainWindow::moveBackGui);
        hintWidget->hide();
    }
    gameInfo = new QWidget(this);
    if(!gameInfo) {
        isCorrectInitialization = false;
        return;
    }
    geomHeight = mobWidth*1.458333;
    ratio = mobWidth*0.1388888;
    gameInfo->setGeometry(0,static_cast<int>(levelGeomHeight+1)+static_cast<int>(ratio+1),mobWidth,static_cast<int>(ratio+1));
    gameInfo->setStyleSheet("background-color:"+buttonsColor+";");

    gameText = new QLabel(gameInfo);
    if(!gameText) {
        isCorrectInitialization = false;
        return;
    }
    QString str = QString::fromUtf8(gameEndResult.c_str());
    gameText->setText(str);
    //gameText->setWordWrap(true);
    double fontsize = mobWidth*0.0494444;//0.0694444
    gameText->setStyleSheet("font-size:"+QString::number(static_cast<int>(fontsize))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    QHBoxLayout *gInfoLayout = new QHBoxLayout(gameInfo);
    if(!gInfoLayout) {
        isCorrectInitialization = false;
        return;
    }
    gInfoLayout->addWidget(gameText,1,Qt::AlignHCenter);
    //COLOR
    colorWidget = new QWidget(this);
    if(!colorWidget) {
        isCorrectInitialization = false;
        return;
    }
    colorWidget->setGeometry(0,checkHeight-static_cast<int>(mobWidth*0.23/*0.2407*/),mobWidth,static_cast<int>(mobWidth*0.2/*0.0925*/));
    colorWidget->setStyleSheet("background-color:"+buttonsColor+";");
    colorLayout = new QHBoxLayout(colorWidget);
    if(!colorLayout) {
        isCorrectInitialization = false;
        return;
    }
    colorButton = new QPushButton(tr("Board Color"),colorWidget);
    if(!colorButton) {
        isCorrectInitialization = false;
        return;
    }
    colorButton->setMinimumWidth((static_cast<int>(mobWidth*0.2314)*2/3));
    colorButton->setMinimumHeight(static_cast<int>(mobWidth*0.11));//0.094
    colorButton->setStyleSheet("background-color:"+chooseColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                               +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    colorButton->adjustSize();
    connect(colorButton, &QPushButton::clicked, this, &MainWindow::changeColors);
    //ROTATE BOARD
    rotateBoardButton = new QPushButton(tr("Rotate Board"),colorWidget);
    if(!rotateBoardButton) {
        isCorrectInitialization = false;
        return;
    }
    rotateBoardButton->setMinimumWidth((static_cast<int>(mobWidth*0.2314)*2/3));
    rotateBoardButton->setMinimumHeight(static_cast<int>(mobWidth*0.11));//0.094
    rotateBoardButton->setStyleSheet("background-color:"+chooseColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                     +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    rotateBoardButton->adjustSize();
    connect(rotateBoardButton, &QPushButton::clicked, this, &MainWindow::rotateBoard);
    colorLayout->addWidget(colorButton,0,Qt::AlignLeft | Qt::AlignBottom);
    colorLayout->addWidget(rotateBoardButton,0,Qt::AlignRight | Qt::AlignBottom);

    //ADS
    banner = CreateQtAdMobBanner(true);

    //SETTINGS
    settingsWidget = new QWidget(this);
    if(!settingsWidget) {
        isCorrectInitialization = false;
        return;
    }
    settingsWidget->setGeometry(0,0,mobWidth,checkHeight);
    settingsWidget->adjustSize();
    settingsWidget->setStyleSheet(
        ".QWidget { "
        " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
        "}");

    geomHeight = 0;
    titleInfoWidget = new QWidget(settingsWidget);
    if(!titleInfoWidget) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.3;
    geomHeight+=ratio;

    titleInfoText = new QLabel(titleInfoWidget);
    if(!titleInfoText) {
        isCorrectInitialization = false;
        return;
    }
    titleInfoText->setText("Deep Chess");
    titleInfoText->setAlignment(Qt::AlignLeft);
    titleInfoText->setAlignment(Qt::AlignTop);
    titleFont = mobWidth*0.0494444;//0.0694444
    titleInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

    removeAdsButton = new QPushButton(tr("Upgrade"),titleInfoWidget);
    if(!removeAdsButton) {
        isCorrectInitialization = false;
        return;
    }
    removeAdsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                   +QString::number(static_cast<int>(titleFont))+"px;");
    removeAdsButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    removeAdsButton->adjustSize();

    connect(removeAdsButton,&QPushButton::clicked,this,&MainWindow::removeAdsClicked);

    //LOAD GAME
    loadGameButton = new QPushButton(tr("Load"),titleInfoWidget);
    if(!loadGameButton) {
        isCorrectInitialization = false;
        return;
    }
    loadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(titleFont))+"px;");
    geomWidth = (mobWidth-20) / 2;
    geomButtonHeight = mobWidth*0.1388888;
    //loadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
    loadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    loadGameButton->adjustSize();
    connect(loadGameButton,&QPushButton::clicked,this,&MainWindow::loadGameClicked);
    //POLYBOOK
    filesButton = new QPushButton(tr("Files"),titleInfoWidget);
    if(!filesButton) {
        isCorrectInitialization = false;
        return;
    }
    filesButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                               +QString::number(static_cast<int>(titleFont))+"px;");
    filesButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    filesButton->adjustSize();
    connect(filesButton,&QPushButton::clicked,this,&MainWindow::filesButtonClicked);

    filesLayout = new QHBoxLayout();
    filesLayout->addWidget(loadGameButton);
    filesLayout->addWidget(filesButton);
    titleHorizLayout = new QHBoxLayout();
    titleHorizLayout->addWidget(titleInfoText);//,1,Qt::AlignHCenter
    titleHorizLayout->addWidget(removeAdsButton);

    QVBoxLayout *titleInfoVertLayout = new QVBoxLayout(titleInfoWidget);
    if(!titleInfoVertLayout) {
        isCorrectInitialization = false;
        return;
    }
    titleInfoVertLayout->addLayout(titleHorizLayout);
    titleInfoVertLayout->addLayout(filesLayout);

    levelInfoWidget = new QWidget(settingsWidget);
    if(!levelInfoWidget) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.15;
    geomHeight+=ratio;
    levelInfoWidget->setGeometry(0,static_cast<int>(mobWidth*0.3),mobWidth,static_cast<int>(ratio));
    levelInfoText = new QLabel(levelInfoWidget);
    if(!levelInfoText) {
        isCorrectInitialization = false;
        return;
    }
    levelInfoText->setText(tr("Level"));
    levelInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    QHBoxLayout *levelInfoLayout = new QHBoxLayout(levelInfoWidget);
    if(!levelInfoLayout) {
        isCorrectInitialization = false;
        return;
    }
    levelInfoLayout->addWidget(levelInfoText,1,Qt::AlignHCenter);

    //LOAD GAME
    loadGameInfoWidget = new QWidget(settingsWidget);
    if(!loadGameInfoWidget) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.15;
    geomHeight+=ratio;
    loadGameInfoWidget->setGeometry(0,static_cast<int>(mobWidth*0.2),mobWidth,static_cast<int>(ratio));
    loadGameInfoText = new QLabel(loadGameInfoWidget);
    if(!loadGameInfoText) {
        isCorrectInitialization = false;
        return;
    }
    loadGameInfoText->setText(tr("Load Game"));
    loadGameInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    exportPGNButton = new QPushButton(tr("ExportPGN"),loadGameInfoWidget);
    if(!exportPGNButton) {
        isCorrectInitialization = false;
        return;
    }
    exportPGNButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                   +QString::number(static_cast<int>(titleFont))+"px;");
    exportPGNButton->setMinimumWidth(static_cast<int>(geomWidth));
    exportPGNButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    exportPGNButton->adjustSize();
    connect(exportPGNButton,&QPushButton::clicked,this,&MainWindow::exportGameClicked);
    QVBoxLayout *loadGameInfoLayout = new QVBoxLayout(loadGameInfoWidget);
    if(!loadGameInfoLayout) {
        isCorrectInitialization = false;
        return;
    }
    loadGameInfoLayout->addWidget(loadGameInfoText,1,Qt::AlignHCenter);
    loadGameInfoLayout->addWidget(exportPGNButton,1,Qt::AlignHCenter);
    loadGameInfoWidget->hide();

    chooseStrengthWidget = new QWidget(settingsWidget);
    if(!chooseStrengthWidget) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.32222;
    geomWidth = (mobWidth-20) / 3;
    geomButtonHeight = mobWidth*0.1388888;
    chooseStrengthWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
    QHBoxLayout *chuseStrengthLayout = new QHBoxLayout(chooseStrengthWidget);
    if(!chuseStrengthLayout) {
        isCorrectInitialization = false;
        return;
    }

    downStrengthButton = new QPushButton("-",chooseStrengthWidget);
    if(!downStrengthButton) {
        isCorrectInitialization = false;
        return;
    }
    downStrengthButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(titleFont))+"px;");
    downStrengthButton->setMinimumWidth(static_cast<int>(geomWidth));
    downStrengthButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    downStrengthButton->adjustSize();
    chuseStrengthLayout->addWidget(downStrengthButton,1,Qt::AlignLeft);
    connect(downStrengthButton,&QPushButton::clicked,this,&MainWindow::decreaseLevelStrength);

    levelStatusText = new QLabel(chooseStrengthWidget);
    if(!levelStatusText) {
        isCorrectInitialization = false;
        return;
    }
    levelStatusText->setText(QString::number(playLevel));
    levelStatusText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    chuseStrengthLayout->addWidget(levelStatusText,1,Qt::AlignCenter);

    upStrengthButton = new QPushButton("+",chooseStrengthWidget);
    if(!upStrengthButton) {
        isCorrectInitialization = false;
        return;
    }
    upStrengthButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    upStrengthButton->setMinimumWidth(static_cast<int>(geomWidth));
    upStrengthButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    upStrengthButton->adjustSize();
    chuseStrengthLayout->addWidget(upStrengthButton,1,Qt::AlignRight);
    connect(upStrengthButton,&QPushButton::clicked,this,&MainWindow::increaseLevelStrength);

    //LOAD GAME
    loadGameWidget = new QWidget(settingsWidget);
    if(!loadGameWidget) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.277777;
    geomWidth = (mobWidth-10) / 12;
    loadGameWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
    geomHeight+=ratio;
    QHBoxLayout *loadGameLayout = new QHBoxLayout(loadGameWidget);
    if(!loadGameLayout) {
        isCorrectInitialization = false;
        return;
    }

    downLoadGameButton = new QPushButton("<",loadGameWidget);
    if(!downLoadGameButton) {
        isCorrectInitialization = false;
        return;
    }
    downLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(titleFont))+"px;");
    downLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
    downLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    downLoadGameButton->adjustSize();
    loadGameLayout->addWidget(downLoadGameButton,1,Qt::AlignLeft);
    connect(downLoadGameButton,&QPushButton::clicked,this,&MainWindow::decreaseLoadedGames);
    loadGameStatusText = new QLabel(loadGameWidget);
    if(!loadGameStatusText) {
        isCorrectInitialization = false;
        return;
    }
    loadGameStatusText->setText(currentLoadGameName);
    loadGameStatusText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    loadGameLayout->addWidget(loadGameStatusText);

    upLoadGameButton = new QPushButton(">",loadGameWidget);
    if(!upLoadGameButton) {
        isCorrectInitialization = false;
        return;
    }
    upLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    upLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
    upLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    upLoadGameButton->adjustSize();
    loadGameLayout->addWidget(upLoadGameButton,1,Qt::AlignRight);
    connect(upLoadGameButton,&QPushButton::clicked,this,&MainWindow::increaseLoadedGames);
    loadGameWidget->hide();

    starAnalysisWidget = new QWidget(settingsWidget);
    if(!starAnalysisWidget) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.277777;
    geomWidth = (mobWidth-40) / 2;
    starAnalysisWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
    QHBoxLayout *analisysWidgetLayout = new QHBoxLayout(starAnalysisWidget);
    if(!analisysWidgetLayout) {
        isCorrectInitialization = false;
        return;
    }
    starAnalyzisButton = new QPushButton(tr("BothSide"),starAnalysisWidget);
    if(!starAnalyzisButton) {
        isCorrectInitialization = false;
        return;
    }
    starAnalyzisButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(titleFont))+"px;");
    starAnalyzisButton->setMinimumWidth(static_cast<int>(geomWidth));
    starAnalyzisButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    starAnalyzisButton->adjustSize();
    analisysWidgetLayout->addWidget(starAnalyzisButton);//,1,Qt::AlignLeft
    connect(starAnalyzisButton,&QPushButton::clicked,this,&MainWindow::analyzisModeNotAvailable);

    newFiguresButton = new QPushButton(starAnalysisWidget);
    if(!newFiguresButton) {
        isCorrectInitialization = false;
        return;
    }
    newFiguresButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    newFiguresButton->setMinimumWidth(static_cast<int>(geomWidth/2-(mobWidth*0.01388888)));
    newFiguresButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    newFiguresButton->adjustSize();
    newFiguresButton->setIcon(QIcon("assets:/images/wK.png"));
    newFiguresButton->setIconSize(QSize(newFiguresButton->width()-4,newFiguresButton->height()-4));
    connect(newFiguresButton,&QPushButton::clicked,this,&MainWindow::newFiguresChoosed);
    analisysWidgetLayout->addWidget(newFiguresButton);

    oldFiguresButton = new QPushButton(starAnalysisWidget);
    if(!oldFiguresButton) {
        isCorrectInitialization = false;
        return;
    }
    oldFiguresButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    oldFiguresButton->setMinimumWidth(static_cast<int>(geomWidth/2-(mobWidth*0.01388888)));
    oldFiguresButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    oldFiguresButton->adjustSize();
    oldFiguresButton->setIcon(QIcon("assets:/images/WhiteKing.png"));
    oldFiguresButton->setIconSize(QSize(oldFiguresButton->width()-4,oldFiguresButton->height()-4));
    connect(oldFiguresButton,&QPushButton::clicked,this,&MainWindow::oldFiguresChoosed);
    analisysWidgetLayout->addWidget(oldFiguresButton);

    //LOAD GAME
    deleteGameWidget = new QWidget(settingsWidget);
    if(!deleteGameWidget) {
        isCorrectInitialization = false;
        return;
    }
    deleteGameWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
    geomHeight+=ratio;
    QHBoxLayout *deleteGameLayout = new QHBoxLayout(deleteGameWidget);
    if(!deleteGameLayout) {
        isCorrectInitialization = false;
        return;
    }
    deleteGameButton = new QPushButton(tr("Delete"),deleteGameWidget);
    if(!deleteGameButton) {
        isCorrectInitialization = false;
        return;
    }
    deleteGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    deleteGameButton->setMinimumWidth(static_cast<int>(geomWidth));
    deleteGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    deleteGameButton->adjustSize();
    deleteGameLayout->addWidget(deleteGameButton,1,Qt::AlignHCenter);//,1,Qt::AlignLeft
    connect(deleteGameButton,&QPushButton::clicked,this,&MainWindow::deleteGame);
    //EXPORT
    exportGameButton = new QPushButton(tr("Puzzle"),deleteGameWidget);
    if(!exportGameButton) {
        isCorrectInitialization = false;
        return;
    }
    exportGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    exportGameButton->setMinimumWidth(static_cast<int>(geomWidth));
    exportGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    exportGameButton->adjustSize();
    deleteGameLayout->addWidget(exportGameButton,1,Qt::AlignRight);//,1,Qt::AlignLeft
    connect(exportGameButton,&QPushButton::clicked,this,&MainWindow::puzzleClicked);
    if((!isPuzzleAct&&gamesCount!=0)||(!connection->checkPuzzle()))
        isPuzzleActivated = false;

    deleteGameWidget->hide();

    colorInfoWidget = new QWidget(settingsWidget);
    if(!colorInfoWidget) {
        isCorrectInitialization = false;
        return;
    }
    ratio = mobWidth*0.15;
    colorInfoWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
    geomHeight+=ratio;
    colorInfoText = new QLabel(colorInfoWidget);
    if(!colorInfoText) {
        isCorrectInitialization = false;
        return;
    }
    colorInfoText->setText(tr("Color"));
    colorInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                 "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    QHBoxLayout *colorInfoLayout = new QHBoxLayout(colorInfoWidget);
    if(!colorInfoLayout) {
        isCorrectInitialization = false;
        return;
    }
    colorInfoLayout->addWidget(colorInfoText,1,Qt::AlignHCenter);

    chooseColorWidget = new QWidget(settingsWidget);
    if(!chooseColorWidget) {
        isCorrectInitialization = false;
        return;
    }
    geomWidth = (mobWidth-40) / 2;
    chooseColorWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
    geomHeight+=ratio;
    QHBoxLayout *chuseColorWidgetLayout = new QHBoxLayout(chooseColorWidget);
    if(!chuseColorWidgetLayout) {
        isCorrectInitialization = false;
        return;
    }
    playWhiteColorButton = new QPushButton(tr("White"),chooseColorWidget);
    if(!playWhiteColorButton) {
        isCorrectInitialization = false;
        return;
    }
    playWhiteColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    playWhiteColorButton->setMinimumWidth(static_cast<int>(geomWidth));
    playWhiteColorButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    playWhiteColorButton->adjustSize();
    chuseColorWidgetLayout->addWidget(playWhiteColorButton,1,Qt::AlignLeft);
    connect(playWhiteColorButton,&QPushButton::clicked,this,&MainWindow::whiteColorChosen);

    playBlackColorButton = new QPushButton(tr("Black"),chooseColorWidget);
    if(!playBlackColorButton) {
        isCorrectInitialization = false;
        return;
    }
    playBlackColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    playBlackColorButton->setMinimumWidth(static_cast<int>(geomWidth));
    playBlackColorButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    playBlackColorButton->adjustSize();
    chuseColorWidgetLayout->addWidget(playBlackColorButton,1,Qt::AlignRight);
    connect(playBlackColorButton,&QPushButton::clicked,this,&MainWindow::blackColorChosen);

    okOrCancelWidget = new QWidget(settingsWidget);
    if(!okOrCancelWidget) {
        isCorrectInitialization = false;
        return;
    }
    okOrCancelWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
    QHBoxLayout *okOrCancelLayout = new QHBoxLayout(okOrCancelWidget);
    if(!okOrCancelLayout) {
        isCorrectInitialization = false;
        return;
    }
    okSettingsButton = new QPushButton(tr("OK"),okOrCancelWidget);
    if(!okSettingsButton) {
        isCorrectInitialization = false;
        return;
    }
    okSettingsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    okSettingsButton->setMinimumWidth(static_cast<int>(geomWidth));
    okSettingsButton->setMinimumHeight(static_cast<int>(geomButtonHeight));

    okOrCancelLayout->addWidget(okSettingsButton,1,Qt::AlignLeft);
    connect(okSettingsButton,&QPushButton::clicked,this,&MainWindow::okButtonClicked);

    cancelSettingsButton = new QPushButton(tr("CANCEL"),okOrCancelWidget);
    if(!cancelSettingsButton) {
        isCorrectInitialization = false;
        return;
    }
    cancelSettingsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    cancelSettingsButton->setMinimumWidth(static_cast<int>(geomWidth));
    cancelSettingsButton->setMinimumHeight(static_cast<int>(geomButtonHeight));

    okOrCancelLayout->addWidget(cancelSettingsButton,1,Qt::AlignRight);
    connect(cancelSettingsButton,&QPushButton::clicked,this,&MainWindow::cancelButtonClicked);

    //LOAD GAME
    okOrCancelLoadGameWidget = new QWidget(settingsWidget);
    if(!okOrCancelLoadGameWidget) {
        isCorrectInitialization = false;
        return;
    }
    okOrCancelLoadGameWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
    QHBoxLayout *okOrCancelLoadGameLayout = new QHBoxLayout(okOrCancelLoadGameWidget);
    if(!okOrCancelLoadGameLayout) {
        isCorrectInitialization = false;
        return;
    }
    okLoadGameButton = new QPushButton(tr("OK"),okOrCancelLoadGameWidget);
    if(!okLoadGameButton) {
        isCorrectInitialization = false;
        return;
    }
    okLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    okLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
    okLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    connect(okLoadGameButton,&QPushButton::clicked,this,&MainWindow::loadGameOkClicked);

    cancelLoadGameButton = new QPushButton(tr("CANCEL"),okOrCancelLoadGameWidget);
    if(!cancelLoadGameButton) {
        isCorrectInitialization = false;
        return;
    }
    cancelLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    cancelLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
    cancelLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    okOrCancelLoadGameLayout->setSpacing(0);
    okOrCancelLoadGameLayout->setContentsMargins(10,0,10,0);
    okOrCancelLoadGameLayout->addWidget(okLoadGameButton,0,Qt::AlignTop);
    okOrCancelLoadGameLayout->addWidget(cancelLoadGameButton,0,Qt::AlignTop);
    connect(cancelLoadGameButton,&QPushButton::clicked,this,&MainWindow::loadGameCancelClicked);
    okOrCancelLoadGameWidget->hide();
    settingsWidget->hide();
    //SETTINGS

    //ADS
    if(banner)
        isUgrdButton = (((banner->checkEuUser()) == 1) ? false : true);
    if(isUgrdButton == false){
        removeAdsButton->hide();
    }
    //RATE LOGIC
    int rateCnt = connection->queryRateCount();
    if(rateCnt == GAMES_PASSED_FOR_RATE && !connection->isUsedRate() && isUgrdButton) {
        geomHeight = 0;
        ratio = mobWidth*0.2;
        rateWidget = new QWidget(this);
        if(!rateWidget) {
            isCorrectInitialization = false;
            return;
        }
        rateWidget->setGeometry(0,0,mobWidth,checkHeight);
        rateWidget->adjustSize();
        rateWidget->setStyleSheet(
            ".QWidget { "
            " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
            "}");
        QVBoxLayout *rateLayout = new QVBoxLayout(rateWidget);
        if(!rateLayout) {
            isCorrectInitialization = false;
            return;
        }
        rateTitle = new QLabel(rateWidget);
        if(!rateTitle) {
            isCorrectInitialization = false;
            return;
        }
        titleFont = mobWidth*0.07;
        rateTitle->setText(tr("Thank You for using Deep Chess!"));
        rateTitle->setWordWrap(true);
        rateTitle->setAlignment(Qt::AlignCenter);
        rateTitle->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        rateText_1 = new QLabel(rateWidget);
        if(!rateText_1) {
            isCorrectInitialization = false;
            return;
        }
        rateText_1->setText(tr("Upgrade to use more advanced features.Time settings and more and remove ads.Thank You!"));
        rateText_1->setWordWrap(true);
        rateText_1->setAlignment(Qt::AlignCenter);
        rateText_1->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        rateText_2 = new QPushButton(tr("Upgrade"),rateWidget);
        if(!rateText_2) {
            isCorrectInitialization = false;
            return;
        }
        rateText_2->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(titleFont))+"px;");
        rateText_2->setMinimumWidth(static_cast<int>(geomWidth));
        rateText_2->setMinimumHeight(static_cast<int>(geomButtonHeight));
        rateText_2->adjustSize();
        connect(rateText_2,&QPushButton::clicked,this,&MainWindow::gdprPaidClicked);//rateClicked

        closeRate = new QPushButton(tr("Close"),rateWidget);
        if(!closeRate) {
            isCorrectInitialization = false;
            return;
        }

        closeRate->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                 +QString::number(static_cast<int>(titleFont))+"px;");
        closeRate->setMinimumWidth(static_cast<int>(geomWidth));
        closeRate->setMinimumHeight(static_cast<int>(geomButtonHeight));
        closeRate->adjustSize();
        connect(closeRate,&QPushButton::clicked,this,&MainWindow::closeRateClicked);
        rateLayout->addWidget(rateTitle,0,Qt::AlignCenter);
        rateLayout->addWidget(rateText_1,1,Qt::AlignCenter);
        rateLayout->addWidget(rateText_2,1,Qt::AlignCenter);
        rateLayout->addWidget(closeRate,1,Qt::AlignCenter);//,1,Qt::AlignLeft
        connection->setUsedRate();
    }

    //GDPR
    //    gameEndResult = "EU:"+to_string(banner->checkEuUser())+" DB:"+to_string(connection->checkGDPR());
    //    setGameInfoVis(gameInfo,gameText);
    if(/*banner->checkEuUser()==1&&*/!connection->checkGDPR()){
        gdprWidget = new QWidget(this);
        if(!gdprWidget) {
            isCorrectInitialization = false;
            return;
        }
        gdprWidget->setGeometry(0,0,mobWidth,checkHeight);
        gdprWidget->setStyleSheet(
            ".QWidget { "
            " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
            "}");
        gdprWidget->adjustSize();

        gdprText = new QLabel(gdprWidget);
        if(!gdprText) {
            isCorrectInitialization = false;
            return;
        }
        gdprText->setWordWrap(true);
        gdprText->setText(tr("Dear Player, This App contains Ads By clicking Accept, You agree to receive Ads. Alternatively please consider to install the paid version."));
        double font = mobWidth*0.045;
        gdprText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(font))+
                                "px;color:rgb(204, 163, 0);font-weight:bold;text-align:left;");
        gdprText->adjustSize();
        QVBoxLayout *gdprLayout = new QVBoxLayout(gdprWidget);
        if(!gdprLayout) {
            isCorrectInitialization = false;
            return;
        }
        gdprLayout->addWidget(gdprText,1,Qt::AlignTop);

        gdprAgreeButton = new QPushButton(tr("Accept"),gdprWidget);
        if(!gdprAgreeButton) {
            isCorrectInitialization = false;
            return;
        }
        gdprAgreeButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                       +QString::number(static_cast<int>(titleFont))+"px;");
        double geomButtonHeight = mobWidth*0.1388888;
        gdprAgreeButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        gdprAgreeButton->adjustSize();
        connect(gdprAgreeButton,&QPushButton::clicked,this,&MainWindow::gdprAgreed);
        gdprLayout->addWidget(gdprAgreeButton,1,Qt::AlignTop);

        gdprPaidButton = new QPushButton(tr("Install Paid Version"),gdprWidget);
        if(!gdprPaidButton) {
            isCorrectInitialization = false;
            return;
        }
        gdprPaidButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(titleFont))+"px;");
        gdprPaidButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        gdprPaidButton->adjustSize();
        connect(gdprPaidButton,&QPushButton::clicked,this,&MainWindow::gdprPaidClicked);
        gdprLayout->addWidget(gdprPaidButton,1,Qt::AlignTop);
        gdprWidget->show();
    }
    // REWARDED
    rewardedWidget = new QWidget(this);
    if(!rewardedWidget) {
        isCorrectInitialization = false;
        return;
    }
    rewardedWidget->setGeometry(0,0,mobWidth,checkHeight);
    rewardedWidget->setStyleSheet(
        ".QWidget { "
        " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
        "}");
    rewardedWidget->adjustSize();

    rewardedText = new QLabel(rewardedWidget);
    if(!rewardedText) {
        isCorrectInitialization = false;
        return;
    }
    //rewardedText->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
    rewardedText->setWordWrap(true);
    rewardedText->setText(tr("Dear Player,You can Activate Undo,Hints and Puzzles by watching Rewarded ads.Alternatively please consider to install the paid version and get permanent access to those features together with many others."));
    double fontRewarded = mobWidth*0.045;
    rewardedText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(fontRewarded))+
                            "px;color:rgb(204, 163, 0);font-weight:bold;text-align:left;");
    //rewardedText->adjustSize();
    QVBoxLayout *rewardedLayout = new QVBoxLayout(rewardedWidget);
    //rewardedLayout->setSizeConstraint(QLayout::SetMinimumSize);
    if(!rewardedLayout) {
        isCorrectInitialization = false;
        return;
    }
    rewardedLayout->addWidget(rewardedText,2,Qt::AlignTop);

    rewardedActivateButton = new QPushButton(tr("Activate"),rewardedWidget);
    if(!rewardedActivateButton) {
        isCorrectInitialization = false;
        return;
    }
    rewardedActivateButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                   +QString::number(static_cast<int>(titleFont))+"px;");
    double geomButtonHeightRewarded = mobWidth*0.1388888;
    rewardedActivateButton->setMinimumHeight(static_cast<int>(geomButtonHeightRewarded));
    rewardedActivateButton->adjustSize();
    connect(rewardedActivateButton,&QPushButton::clicked,this,&MainWindow::rewardedActivateClicked);
    rewardedLayout->addWidget(rewardedActivateButton,1,Qt::AlignTop);

    rewardedPaidButton = new QPushButton(tr("Install Paid Version"),rewardedWidget);
    if(!rewardedPaidButton) {
        isCorrectInitialization = false;
        return;
    }
    rewardedPaidButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(titleFont))+"px;");
    rewardedPaidButton->setMinimumHeight(static_cast<int>(geomButtonHeightRewarded));
    rewardedPaidButton->adjustSize();
    connect(rewardedPaidButton,&QPushButton::clicked,this,&MainWindow::gdprPaidClicked);
    rewardedLayout->addWidget(rewardedPaidButton,1,Qt::AlignTop);

    rewardedCloseButton = new QPushButton(tr("Close"),rewardedWidget);
    if(!rewardedCloseButton) {
        isCorrectInitialization = false;
        return;
    }
    rewardedCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(titleFont))+"px;");
    rewardedCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeightRewarded));
    rewardedCloseButton->adjustSize();
    connect(rewardedCloseButton,&QPushButton::clicked,this,&MainWindow::rewardedCloseClicked);
    rewardedLayout->addWidget(rewardedCloseButton,1,Qt::AlignTop);
    rewardedWidget->hide();
    flipGUIBoardChessAiColor();
    //Move Back
    std::vector<BackMove> backMoveV;
    for(int i=0;i<buttons.size();++i){
        BackMove backMove;
        backMove.figId = buttons[i]->figId;
        backMove.iconUrl = buttons[i]->iconUrl;
        backMove.borderColor = buttons[i]->borderColor;
        backMove._boardPos = buttons[i]->_boardPos;
        backMove.uciMoveCmd = uciMoveCmd;
        backMove.uciMoveCmdHint = uciMoveCmdHint;
        backMove.PGNString = PGNString;
        backMove.PGNMoveNum = PGNMoveNum;
        backMoveV.push_back(backMove);
    }
    backMoveData.push(backMoveV);
    //ADS
    if((!gdprWidget) || (gdprWidget == nullptr)) {
        if(banner && isBanner){
            banner->setUnitId("",true);
            //banner->setUnitId("", true);
            banner->setSize(IQtAdMobBanner::AdaptiveBanner);//SmartBanner
            double bannerOffset = ((checkHeight - mobWidth) <= 561 ? 0.07 : 0.08);
            bannerHAvRes = bannerHAvNotRes = static_cast<int>(((levelGeomHeight+1)+(mobWidth * bannerOffset)));
           // QMessageBox::information(this, "Info", ("OFFSET CASE!"+QString::number((checkHeight - mobWidth))));
            if((checkHeight - mobWidth) >= 1045) {
                //QMessageBox::information(this, "Info", "OFFSET CASE!");
                double add = static_cast<double>(checkHeight * 0.035);
                bannerHAvNotRes += static_cast<int>(add);
            }
            QPoint bannerPos(0,bannerHAvNotRes);
            banner->setPosition(bannerPos);
        }
    }
    m_Interstitial = CreateQtAdMobInterstitial();
    if(banner && m_Interstitial) {
        m_Interstitial->setUnitId("");
        //m_Interstitial->setUnitId("", true);
        connect(banner, SIGNAL(loaded()), this, SLOT(OnBannerLoaded()));
        connect(banner, SIGNAL(loading()), this, SLOT(OnBannerLoading()));
        connect(banner, SIGNAL(clicked()), this, SLOT(OnBannerClicked()));
        connect(banner, SIGNAL(closed()), this, SLOT(OnBannerClosed()));
        connect(m_Interstitial, SIGNAL(loaded()), this, SLOT(OnInterstitialLoaded()));
        connect(m_Interstitial, SIGNAL(loading()), this, SLOT(OnInterstitialLoading()));
        connect(m_Interstitial, SIGNAL(clicked()), this, SLOT(OnInterstitialClicked()));
        connect(m_Interstitial, SIGNAL(closed()), this, SLOT(OnInterstitialClosed()));
    }
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    QMainWindow::resizeEvent(event);
    screen = QApplication::screens().at(0);
    mobWidth = screen->size().width();
    checkHeight = screen->size().height();
    if(mobWidth > checkHeight){
        isResized = true;
        gameInfoWidth = mobWidth;
        btnNumFontS =  checkHeight*0.027;
        if(mobWidth >= 2600)
            resizedWood = "url(\"assets:/images/WoodResizedLaptop.jpg\")";
        else
            resizedWood = "url(\"assets:/images/WoodResized.jpg\")";
    }
    else{
        isResized = false;
        btnNumFontS =  mobWidth*0.027;
    }
    changeWidgetsGeometry();
}

void MainWindow::changeWidgetsGeometry()
{
    double ratio = 0;
    int sqW = 0;
    int sqH = 0;
    double geomWidth = 0;
    double geomHeight = 0;
    double levelWidth = 0;
    double levelGeomHeight = 0;
    double fontsize = 0;

    if(isResized){
        mobWidth = checkHeight-46;
        ratio = mobWidth*1.6944444;
        btnFontSiz = mobWidth*0.04555555;//0.05555555
        double stBtnFontSiz = mobWidth*0.03;
        fieldSize = (mobWidth*0.125)-2;
        icsize = (mobWidth*0.11805555)-2;

        resizeButtons(fieldSize,icsize);
        widget->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
        bannerHNotAv = static_cast<int>(ratio+1)+3000;
        widget->adjustSize();
        widget->updateGeometry();
        grid->update();

        ratio = mobWidth*0.11111;
        sqW = static_cast<int>(ratio+1);
        sqH = static_cast<int>(ratio+1);
        ratio = mobWidth*0.236111;
        if(pawnPromotion && pawnPromotion!=nullptr){
            pawnPromotion->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
            pawnPromoQ->setMinimumWidth(sqW);
            pawnPromoQ->setMinimumHeight(sqH);
            pawnPromoQ->adjustSize();
            pawnPromoQ->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            pawnPromoR->setMinimumWidth(sqW);
            pawnPromoR->setMinimumHeight(sqH);
            pawnPromoR->adjustSize();
            pawnPromoR->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            pawnPromoK->setMinimumWidth(sqW);
            pawnPromoK->setMinimumHeight(sqH);
            pawnPromoK->adjustSize();
            pawnPromoK->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            pawnPromoB->setMinimumWidth(sqW);
            pawnPromoB->setMinimumHeight(sqH);
            pawnPromoB->adjustSize();
            pawnPromoB->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            pawnPromotion->repaint();
        }
        //SAVE GAME
        saveGameWidget->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
        saveGameButton->setMinimumWidth(sqW);
        saveGameButton->setMinimumHeight(sqH);
        saveGameButton->adjustSize();
        saveGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
        closeGameButton->setMinimumWidth(sqW);
        closeGameButton->setMinimumHeight(sqH);
        closeGameButton->adjustSize();
        closeGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                       +QString::number(static_cast<int>(btnFontSiz+1))+"px;");

        inputDialog->setMinimumWidth(sqW);
        inputDialog->setMinimumHeight(sqH);
        inputDialog->adjustSize();
        saveGameWidget->repaint();

        ratio = mobWidth*0.2777777;
        geomWidth = mobWidth*0.375;
        geomHeight = mobWidth*0.1388888;
        if(sideBar && sideBar!=nullptr){
            sideBar->setGeometry(mobWidth,0,gameInfoWidth-mobWidth,static_cast<int>(ratio+1));
            playAgainstComputer->setMinimumWidth(((gameInfoWidth-mobWidth)/2)-20);
            playAgainstComputer->setMinimumHeight(((static_cast<int>(geomHeight+1))*2)/3);
            playAgainstComputer->adjustSize();
            playAgainstComputer->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                               +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            //LOAD GAME
            prevMoveButton->setMinimumWidth((((gameInfoWidth-mobWidth)/2)-20)/2-5);
            prevMoveButton->setMinimumHeight(((static_cast<int>(geomHeight+1))*2)/3);
            prevMoveButton->adjustSize();
            prevMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            nextMoveButton->setMinimumWidth((((gameInfoWidth-mobWidth)/2)-20)/2-5);
            nextMoveButton->setMinimumHeight(((static_cast<int>(geomHeight+1))*2)/3);
            nextMoveButton->adjustSize();
            nextMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            resetButton->setMinimumWidth(((gameInfoWidth-mobWidth)/2)-20);
            resetButton->setMinimumHeight(((static_cast<int>(geomHeight+1))*2)/3);
            resetButton->adjustSize();
            if(resetButton->isEnabled()){
                resetButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            }
            else{
                resetButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:normal;font-size:"
                                           +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            }
            sideBar->repaint();
        }
        if(colorWidget && colorWidget!=nullptr){//resized
            colorWidget->setGeometry(mobWidth,checkHeight-static_cast<int>(gameInfoWidth*0.29/*0.1500*/),gameInfoWidth-mobWidth,static_cast<int>(gameInfoWidth*0.17/*0.27*/));
            colorButton->setMinimumWidth((static_cast<int>(gameInfoWidth*0.2314)*3/4));
            colorButton->setMinimumHeight(static_cast<int>(gameInfoWidth*0.054/*0.038*/));
            colorButton->setStyleSheet("background-color:"+chooseColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                       +QString::number(static_cast<int>(stBtnFontSiz))+"px;");
            colorButton->adjustSize();
            //ROTATE BOARD
            rotateBoardButton->setMinimumWidth((static_cast<int>(gameInfoWidth*0.2314)*3/4));
            rotateBoardButton->setMinimumHeight(static_cast<int>(gameInfoWidth*0.054/*0.038*/));
            rotateBoardButton->setStyleSheet("background-color:"+chooseColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                             +QString::number(static_cast<int>(stBtnFontSiz))+"px;");
            rotateBoardButton->adjustSize();
            colorWidget->repaint();
        }
        if(newGameWidget && newGameWidget != nullptr){
            newGameWidget->setGeometry(mobWidth,0,gameInfoWidth-mobWidth,static_cast<int>(ratio+1));
            newGameButton->setMinimumWidth(((gameInfoWidth-mobWidth)/2)-20);
            newGameButton->setMinimumHeight(((static_cast<int>(geomHeight+1))*2)/3);
            newGameButton->adjustSize();
            newGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                         +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            newGameWidget->repaint();
        }

        if(resignWidget && resignWidget!=nullptr){
            resignWidget->setGeometry(mobWidth,0,gameInfoWidth-mobWidth,static_cast<int>(ratio+1));
            if(resignImageLabel->isVisible()){
                resignButton->setMinimumWidth((((gameInfoWidth-mobWidth)/2)-20)-static_cast<int>(0.1285185*mobWidth));
                resignButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(btnFontSiz+1)-(0.12*mobWidth))+"px;");
                resignImageLabel->setMinimumWidth(resignImageLabelWidth-static_cast<int>(0.07314*mobWidth));
            }
            else{
                resignButton->setMinimumWidth(((gameInfoWidth-mobWidth)/2)-20);
                resignButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            }
            resignButton->setMinimumHeight(((static_cast<int>(geomHeight+1))*2)/3);
            resignButton->adjustSize();

            if(resignImageLabel->isVisible()){
                drawButton->setMinimumWidth((((gameInfoWidth-mobWidth)/2)-20)-static_cast<int>(0.1285185*mobWidth));
                drawButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1)-(0.12*mobWidth))+"px;");
            }
            else{
                drawButton->setMinimumWidth(((gameInfoWidth-mobWidth)/2)-20);
                drawButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            }
            drawButton->setMinimumHeight(((static_cast<int>(geomHeight+1))*2)/3);
            drawButton->adjustSize();

            resignWidget->repaint();
        }

        double buttHeight = geomHeight;
        geomHeight = ratio;
        ratio = mobWidth*0.1388888;
        levelWidth = mobWidth*0.4986111;

        //HINT
        ratio = mobWidth*0.2777777;
        if((hintWidget) && (hintWidget != nullptr)){
            hintWidget->setGeometry(mobWidth,static_cast<int>(geomHeight),(gameInfoWidth-mobWidth),static_cast<int>(ratio+1));
            hintButton->setMinimumWidth(((gameInfoWidth-mobWidth)/2)-20);
            hintButton->setMinimumHeight(((static_cast<int>(buttHeight+1))*2)/3);
            hintButton->adjustSize();
            hintButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            //Move Back
            undoMoveButton->setMinimumWidth(((gameInfoWidth-mobWidth)/2)-20);
            undoMoveButton->setMinimumHeight(((static_cast<int>(buttHeight+1))*2)/3);
            undoMoveButton->adjustSize();
            undoMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            hintWidget->repaint();
        }
        //ADS
        if(!bannerShut){
            if((banner) && (banner != nullptr)) {
                bannerHAvRes = (static_cast<int>(geomHeight) + (mobWidth * 0.09));
                QPoint bannerPos(mobWidth,bannerHAvRes);//resized
                banner->setPosition(bannerPos);
            }
        }

        gameInfo->setGeometry(mobWidth,static_cast<int>((geomHeight*2)),(gameInfoWidth-mobWidth),250);
        fontsize = mobWidth*0.0494444;//0.0694444
        if(islGActive.load()){
            double fontsize = mobWidth*0.04;//0.0694444;
            gameText->setStyleSheet("font-size:"+QString::number(static_cast<int>(fontsize))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        }
        else{
            gameText->setStyleSheet("font-size:"+QString::number(static_cast<int>(fontsize-5.0))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:left;");
        }
        gameInfo->repaint();

        //SETTINGS
        double geomButtonHeight = ((gameInfoWidth*0.1388888)/3);
        if((settingsWidget) && (settingsWidget != nullptr)){
            settingsWidget->setGeometry(0,0,gameInfoWidth,checkHeight);
            settingsWidget->resize(gameInfoWidth,checkHeight);
            settingsWidget->setStyleSheet(
                ".QWidget { "
                " background: " + resizedWood + ";background-size:cover;"
                "}");

            geomHeight = 0;
            ratio = (gameInfoWidth*0.5)/4;
            geomHeight+=ratio;
            titleInfoWidget->setGeometry(0,0,gameInfoWidth,static_cast<int>(ratio+1));
            titleInfoText->setText("Deep Chess");
            titleFont = (gameInfoWidth*0.0494444)/3;//0.0694444
            titleInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            //LOAD GAME
            geomWidth = (gameInfoWidth-20) / 2;
            geomButtonHeight = (gameInfoWidth*0.1188888)/3.0;//0.1388888
            loadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(titleFont))+"px;");
            //loadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            loadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            loadGameButton->adjustSize();

            filesButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                       +QString::number(static_cast<int>(titleFont))+"px;");
            filesButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            removeAdsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
            removeAdsButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            titleInfoWidget->repaint();

            ratio = (gameInfoWidth*0.15)/4.5;//4
            levelInfoWidget->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio));
            levelInfoText->setText(tr("Level"));
            levelInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            levelInfoWidget->repaint();
            //LOAD GAME
            ratio = (gameInfoWidth*0.15)/2;
            loadGameInfoWidget->setGeometry(0,static_cast<int>(geomHeight-(0.055*gameInfoWidth)),gameInfoWidth,static_cast<int>((gameInfoWidth*0.20)/2));
            loadGameInfoText->setText(tr("Load Game"));
            loadGameInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            loadGameInfoWidget->repaint();
            geomHeight+=ratio;

            ratio = (gameInfoWidth*0.277777)/4;
            geomWidth = (gameInfoWidth-20) / 3;
            geomButtonHeight = (gameInfoWidth*0.1388888)/3;
            chooseStrengthWidget->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio));

            downStrengthButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                              +QString::number(static_cast<int>(titleFont))+"px;");
            downStrengthButton->setMinimumWidth(static_cast<int>(geomWidth));
            downStrengthButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            downStrengthButton->adjustSize();

            levelStatusText->setText(QString::number(playLevel));
            levelStatusText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

            upStrengthButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            upStrengthButton->setMinimumWidth(static_cast<int>(geomWidth));
            upStrengthButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            upStrengthButton->adjustSize();
            chooseStrengthWidget->repaint();

            //LOAD GAME
            loadGameWidget->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio));
            geomHeight+=ratio;
            geomWidth = (gameInfoWidth-20) / 12;
            loadGameStatusText->setText(currentLoadGameName);
            loadGameStatusText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            downLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                              +QString::number(static_cast<int>(titleFont))+"px;");
            downLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            downLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            downLoadGameButton->adjustSize();

            upLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            upLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            upLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            upLoadGameButton->adjustSize();
            loadGameWidget->repaint();

            ratio = (gameInfoWidth*0.277777)/4;
            geomWidth = (gameInfoWidth-40) / 2;
            starAnalysisWidget->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio));
            if(starAnalyzisButton->isEnabled())
                starAnalyzisButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                  +QString::number(static_cast<int>(titleFont))+"px;");
            else
                starAnalyzisButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                  +QString::number(static_cast<int>(titleFont))+"px;");

            starAnalyzisButton->setMinimumWidth(static_cast<int>(geomWidth));
            starAnalyzisButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            starAnalyzisButton->adjustSize();
            newFiguresButton->setMinimumWidth(static_cast<int>(geomWidth/2-(mobWidth*0.01388888)));
            newFiguresButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            newFiguresButton->adjustSize();
            newFiguresButton->setIcon(QIcon("assets:/images/wK.png"));
            newFiguresButton->setIconSize(QSize(static_cast<int>(geomWidth/2-(mobWidth*0.01388888))-4,static_cast<int>(geomButtonHeight)-4));
            newFiguresButton->repaint();
            oldFiguresButton->setMinimumWidth(static_cast<int>(geomWidth/2-(mobWidth*0.01388888)));
            oldFiguresButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            oldFiguresButton->adjustSize();
            oldFiguresButton->setIcon(QIcon("assets:/images/WhiteKing.png"));
            oldFiguresButton->setIconSize(QSize(static_cast<int>(geomWidth/2-(mobWidth*0.01388888))-4,static_cast<int>(geomButtonHeight)-4));
            oldFiguresButton->repaint();
            starAnalysisWidget->repaint();

            //LOAD GAME
            deleteGameWidget->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio-25));
            geomHeight+=ratio;
            deleteGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            deleteGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            deleteGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            deleteGameButton->adjustSize();
            exportPGNButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
            exportPGNButton->setMinimumWidth(static_cast<int>(geomWidth));
            exportPGNButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            exportPGNButton->adjustSize();
            //EXPORT
            exportGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            exportGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            exportGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            exportGameButton->adjustSize();
            deleteGameWidget->repaint();

            ratio = (gameInfoWidth*0.15)/3.12;//2
            colorInfoWidget->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio));
            geomHeight+=ratio;
            colorInfoWidget->setStyleSheet("background-color:lightgrey;");
            colorInfoText->setText(tr("Color"));
            colorInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                         "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            colorInfoWidget->repaint();
            geomWidth = (gameInfoWidth-40) / 2;
            chooseColorWidget->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio));
            geomHeight+=ratio;
            playWhiteColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                +QString::number(static_cast<int>(titleFont))+"px;");
            playWhiteColorButton->setMinimumWidth(static_cast<int>(geomWidth));
            playWhiteColorButton->setMinimumHeight(static_cast<int>(geomButtonHeight));

            playBlackColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                +QString::number(static_cast<int>(titleFont))+"px;");
            playBlackColorButton->setMinimumWidth(static_cast<int>(geomWidth));
            playBlackColorButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            chooseColorWidget->repaint();

            ratio = (gameInfoWidth*0.15)/2;
            okOrCancelWidget->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio));
            okSettingsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            okSettingsButton->setMinimumWidth(static_cast<int>(geomWidth));
            okSettingsButton->setMinimumHeight(static_cast<int>(geomButtonHeight));

            cancelSettingsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                +QString::number(static_cast<int>(titleFont))+"px;");
            cancelSettingsButton->setMinimumWidth(static_cast<int>(geomWidth));
            cancelSettingsButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            okOrCancelWidget->repaint();

            //LOAD GAME
            okOrCancelLoadGameWidget->setGeometry(0,static_cast<int>(geomHeight-(mobWidth*0.055/*0.03472222*/)),gameInfoWidth,static_cast<int>(ratio));//resized
            okLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            okLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            okLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));

            cancelLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                +QString::number(static_cast<int>(titleFont))+"px;");
            cancelLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            cancelLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            okOrCancelLoadGameWidget->repaint();
            settingsWidget->repaint();
            if(!settingsWidget->isVisible() && !titleInfoWidget->isVisible()){
                settingsWidget->hide();
                if(hintWidget&&hintWidget!=nullptr){
                    if(!hintWidget->isVisible()&&!bannerShut&&banner&&banner!=nullptr){
                        if((!gdprWidget||gdprWidget==nullptr) && (!rewardedWidget || !rewardedWidget->isVisible())){
                            banner->setVisible(true);
                            isBannerVisibleCond = true;
                        }
                    }
                }
                else{
                    if(!bannerShut&&banner&&banner!=nullptr){
                        if((!gdprWidget||gdprWidget==nullptr) && (!rewardedWidget || !rewardedWidget->isVisible())){
                            banner->setVisible(true);
                            isBannerVisibleCond = true;
                        }
                    }
                }
            }
            else{
                if(!bannerShut&&banner&&banner!=nullptr){
                    banner->setVisible(false);
                    isBannerVisibleCond = false;
                }
                settingsWidget->show();
            }
        }
        if(filesWidget && filesWidget!=nullptr){//resized
            if(!bannerShut&&banner&&banner!=nullptr){
                if(filesWidget->isVisible())
                    isBannerVisibleCond = false;
                banner->setVisible(isBannerVisibleCond);
            }
            filesWidget->setGeometry(0,0,gameInfoWidth,checkHeight);
            filesWidget->resize(gameInfoWidth,checkHeight);
            filesWidget->setStyleSheet(
                ".QWidget { "
                " background: " + resizedWood + ";background-size:cover;"
                "}");

            addBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                         +QString::number(static_cast<int>(titleFont))+"px;");
            addBookButton->setMinimumWidth(static_cast<int>(geomWidth));
            addBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            addBookButton->adjustSize();
            removeBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            removeBookButton->setMinimumWidth(static_cast<int>(geomWidth));
            removeBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            removeBookButton->adjustSize();
            //            importGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
            //                                                +QString::number(static_cast<int>(titleFont))+"px;");
            //            importGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            //            importGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            //            importGameButton->adjustSize();
            closeFilesButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            closeFilesButton->setMinimumWidth(static_cast<int>(geomWidth));
            closeFilesButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            closeFilesButton->adjustSize();
            filesWidget->repaint();
        }
        //SETTINGS
        //ACHIEVEMENTS
        if(congratsBannerWidget && congratsBannerWidget!=nullptr){
            congratsBannerWidget->setGeometry(0,0,gameInfoWidth,checkHeight);
            congratsBannerWidget->resize(gameInfoWidth,checkHeight);
            congratsBannerWidget->setStyleSheet(
                ".QWidget { "
                " background: " + resizedWood + ";background-size:cover;"
                "}");

            geomHeight = 0;
            ratio = (gameInfoWidth*0.2)/4;
            geomHeight+=ratio;
            congratsLabel->setGeometry(0,0,gameInfoWidth,static_cast<int>(ratio+1));
            congratsLabel->setText(tr("Great! You won!"));
            titleFont = (gameInfoWidth*0.0494444)/3;//0.0694444
            congratsLabel->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+
                                         "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

            congratsImageLabel->setGeometry((gameInfoWidth*25)/100,static_cast<int>(geomHeight),(gameInfoWidth*50)/100,static_cast<int>(ratio+1));
            congratsImageLabel->setMinimumWidth((gameInfoWidth*50)/100);
            congratsImageLabel->setMinimumHeight(static_cast<int>(geomHeight+1));
            congratsImageLabel->adjustSize();
            geomHeight+=ratio;
            congratsImageLabel->setStyleSheet("background: url(\"assets:/images/win.png\") no-repeat center;background-size:"+
                                              QString::number(congratsImageLabel->width())+"px;");

            statisticsLabel->setGeometry(0,static_cast<int>(geomHeight),gameInfoWidth,static_cast<int>(ratio*2));
            geomHeight+=ratio;
            statisticsLabel->setText(tr("You have ")+QString::number(winsOnLevelCounter)+tr(" wins on")+levelForDB+"!");
            statisticsLabel->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                           "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

            ratio = (gameInfoWidth*0.277777)/4;
            geomWidth = (gameInfoWidth-40) / 2;
            congratsCloseButton->setGeometry((gameInfoWidth-static_cast<int>(geomWidth))/2,static_cast<int>(geomHeight),
                                             static_cast<int>(geomWidth),static_cast<int>(ratio));
            congratsCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                               +QString::number(static_cast<int>(titleFont))+"px;");
            congratsCloseButton->setMinimumWidth(static_cast<int>(geomWidth));
            congratsCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            congratsCloseButton->adjustSize();
            congratsBannerWidget->repaint();
            if(!congratsBannerWidget->isVisible()){
                congratsBannerWidget->hide();
            }
            else{
                congratsBannerWidget->show();
            }
        }
        //GDPR
        if(gdprWidget && gdprWidget!=nullptr){
            gdprWidget->setGeometry(0,0,gameInfoWidth,checkHeight);
            gdprWidget->resize(gameInfoWidth,checkHeight);
            gdprWidget->setStyleSheet(
                ".QWidget { "
                " background: " + resizedWood + ";background-size:cover;"
                "}");

            gdprText->setText(tr("Dear Player, This App contains Ads By clicking Accept, You agree to receive Ads. Alternatively please consider to install the paid version."));
            gdprText->adjustSize();
            gdprText->setMaximumHeight(static_cast<int>(0.2777777*gameInfoWidth));
            titleFont = (gameInfoWidth*0.0494444)/3.1;//0.0694444
            gdprText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+
                                    "px;color:rgb(204, 163, 0);font-weight:bold;text-align:left;");

            gdprAgreeButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
            gdprAgreeButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            gdprAgreeButton->adjustSize();
            gdprPaidButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(titleFont))+"px;");
            gdprPaidButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            gdprPaidButton->adjustSize();
            gdprWidget->repaint();
        }

        if(rewardedWidget && rewardedWidget!=nullptr){
            rewardedWidget->setGeometry(0,0,gameInfoWidth,checkHeight);
            rewardedWidget->resize(gameInfoWidth,checkHeight);
            rewardedWidget->setStyleSheet(
                ".QWidget { "
                " background: " + resizedWood + ";background-size:cover;"
                "}");

            rewardedText->setText(tr("Dear Player,You can Activate Undo,Hints and Puzzles by watching Rewarded ads.Alternatively please consider to install the paid version and get permanent access to those features together with many others."));
            //rewardedText->adjustSize();
            //rewardedText->setMaximumHeight(static_cast<int>(0.2777777*gameInfoWidth));
            //rewardedText->setMinimumHeight(static_cast<int>(0.2777777*gameInfoWidth));
            titleFont = (gameInfoWidth*0.0494444)/3.1;//0.0694444
            rewardedText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                    "px;color:rgb(204, 163, 0);font-weight:bold;text-align:left;");

            rewardedActivateButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
            rewardedActivateButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            rewardedActivateButton->adjustSize();
            rewardedPaidButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(titleFont))+"px;");
            rewardedPaidButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            rewardedPaidButton->adjustSize();
            rewardedCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                              +QString::number(static_cast<int>(titleFont))+"px;");
            rewardedCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            rewardedCloseButton->adjustSize();
            rewardedWidget->repaint();
        }

        if(achievementBannerWidget && achievementBannerWidget!=nullptr){
            achievementBannerWidget->setGeometry(0,0,gameInfoWidth,checkHeight);
            achievementBannerWidget->resize(gameInfoWidth,checkHeight);
            achievementBannerWidget->setStyleSheet(
                ".QWidget { "
                " background: " + resizedWood + ";background-size:cover;"
                "}");

            geomHeight = 0;
            ratio = (gameInfoWidth*0.2)/4;
            geomHeight+=ratio;
            achievementLabelText->setGeometry(0,0,gameInfoWidth,static_cast<int>(ratio+1));
            achievementLabelText->setText(tr("Great!You won!")+achievementType+"!");
            titleFont = (gameInfoWidth*0.0494444)/3;//0.0694444
            achievementLabelText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+
                                                "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            achievementLabelImage->setGeometry((gameInfoWidth*2)/100,static_cast<int>(geomHeight),(gameInfoWidth*60)/100,static_cast<int>(ratio+1));
            achievementLabelImage->setMinimumWidth((gameInfoWidth*60)/100);
            achievementLabelImage->setMinimumHeight(static_cast<int>(geomHeight+1));
            achievementLabelImage->setStyleSheet("background:transparent;border:none;outline:none;");
            achievementLabelImage->adjustSize();
            geomHeight+=ratio;
            achievementLabelImage->setIcon(QIcon(achievementsLabelPath));
            achievementLabelImage->setIconSize(QSize(achievementLabelImage->width(),achievementLabelImage->height()));

            ratio = (gameInfoWidth*0.277777)/4;
            geomWidth = (gameInfoWidth-40) / 2;
            achievementsCloseButton->setGeometry((gameInfoWidth-static_cast<int>(geomWidth))/2,static_cast<int>(geomHeight),
                                                 static_cast<int>(geomWidth),static_cast<int>(ratio));
            achievementsCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                   +QString::number(static_cast<int>(titleFont))+"px;");
            achievementsCloseButton->setMinimumWidth(static_cast<int>(geomWidth));
            achievementsCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            achievementsCloseButton->adjustSize();
            achievementBannerWidget->repaint();
            if(!achievementBannerWidget->isVisible()){
                achievementBannerWidget->hide();
            }
            else{
                achievementBannerWidget->show();
            }
        }

        //RATE GAME
        if(rateWidget && rateWidget != nullptr){
            if(rateWidget->isVisible()){
                rateWidget->setGeometry(0,0,gameInfoWidth,checkHeight);
                rateWidget->resize(gameInfoWidth,checkHeight);
                rateWidget->setStyleSheet(
                    ".QWidget { "
                    " background: " + resizedWood + ";background-size:cover;"
                    "}");
                titleFont = (gameInfoWidth*0.0494444)/3.0;//0.0694444

                rateText_2->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(titleFont))+"px;");
                rateText_2->setMinimumWidth(static_cast<int>(geomWidth));
                rateText_2->setMinimumHeight(static_cast<int>(geomButtonHeight));
                rateText_2->adjustSize();
                closeRate->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                         +QString::number(static_cast<int>(titleFont))+"px;");
                closeRate->setMinimumWidth(static_cast<int>(geomWidth));
                closeRate->setMinimumHeight(static_cast<int>(geomButtonHeight));
                closeRate->adjustSize();
                rateWidget->repaint();
            }
        }
    }
    else{
        btnFontSiz = mobWidth*0.04555555;//0.05555555
        double stBtnFontSiz = mobWidth*0.032;
        ratio = mobWidth*1.6944444;
        fieldSize = mobWidth*0.125;
        icsize = mobWidth*0.11805555;
        resizeButtons(fieldSize,icsize);
        widget->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
        bannerHNotAv = static_cast<int>(ratio+1)+3000;
        widget->adjustSize();
        widget->resize(mobWidth,static_cast<int>(ratio+1));

        ratio = mobWidth*0.236111;
        geomHeight = mobWidth*1.458333;
        ratio = mobWidth*0.11111;
        sqW = static_cast<int>(ratio+1);
        sqH = static_cast<int>(ratio+1);
        ratio = mobWidth*0.236111;
        if(pawnPromotion && pawnPromotion!=nullptr){
            pawnPromotion->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
            pawnPromoQ->setMinimumWidth(sqW);
            pawnPromoQ->setMinimumHeight(sqH);
            pawnPromoQ->adjustSize();
            pawnPromoQ->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            pawnPromoR->setMinimumWidth(sqW);
            pawnPromoR->setMinimumHeight(sqH);
            pawnPromoR->adjustSize();
            pawnPromoR->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            pawnPromoK->setMinimumWidth(sqW);
            pawnPromoK->setMinimumHeight(sqH);
            pawnPromoK->adjustSize();
            pawnPromoK->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            pawnPromoB->setMinimumWidth(sqW);
            pawnPromoB->setMinimumHeight(sqH);
            pawnPromoB->adjustSize();
            pawnPromoB->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            pawnPromotion->repaint();
        }
        //SAVE GAME
        saveGameWidget->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
        saveGameButton->setMinimumWidth(sqW);
        saveGameButton->setMinimumHeight(sqH);
        saveGameButton->adjustSize();
        saveGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
        closeGameButton->setMinimumWidth(sqW);
        closeGameButton->setMinimumHeight(sqH);
        closeGameButton->adjustSize();
        closeGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                       +QString::number(static_cast<int>(btnFontSiz+1))+"px;");

        inputDialog->setMinimumWidth(sqW);
        inputDialog->setMinimumHeight(sqH);
        inputDialog->adjustSize();
        saveGameWidget->repaint();

        ratio = mobWidth*0.2777777;
        geomWidth = mobWidth*0.375;
        geomHeight = mobWidth*0.1388888;
        if(sideBar && sideBar!=nullptr){
            sideBar->setGeometry(0,mobWidth,mobWidth,static_cast<int>(ratio+1));
            playAgainstComputer->setMinimumWidth(static_cast<int>(geomWidth+1));
            playAgainstComputer->setMinimumHeight(static_cast<int>(geomHeight+1));
            playAgainstComputer->adjustSize();
            playAgainstComputer->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                               +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            //LOAD GAME
            prevMoveButton->setMinimumWidth(static_cast<int>(geomWidth/2-5));
            prevMoveButton->setMinimumHeight(static_cast<int>(geomHeight+1));
            prevMoveButton->adjustSize();
            prevMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            nextMoveButton->setMinimumWidth(static_cast<int>(geomWidth/2-5));
            nextMoveButton->setMinimumHeight(static_cast<int>(geomHeight+1));
            nextMoveButton->adjustSize();
            nextMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            resetButton->setMinimumWidth(static_cast<int>(geomWidth+1));
            resetButton->setMinimumHeight(static_cast<int>(geomHeight+1));
            resetButton->adjustSize();
            if(resetButton->isEnabled()){
                resetButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            }
            else{
                resetButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:normal;font-size:"
                                           +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            }
            sideBar->repaint();
        }
        if(colorWidget && colorWidget!=nullptr){//not resized
            colorWidget->setGeometry(0,checkHeight-static_cast<int>(mobWidth*0.33),mobWidth,static_cast<int>(mobWidth*0.25));
            colorButton->setMinimumWidth((static_cast<int>(mobWidth*3/4)/2));
            colorButton->setMinimumHeight(static_cast<int>(mobWidth*0.075));//0.059
            colorButton->setStyleSheet("background-color:"+chooseColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                       +QString::number(static_cast<int>(stBtnFontSiz))+"px;");
            colorButton->adjustSize();
            //ROTATE BOARD
            rotateBoardButton->setMinimumWidth((static_cast<int>(mobWidth*3/4)/2));
            rotateBoardButton->setMinimumHeight(static_cast<int>(mobWidth*0.075));//0.059
            rotateBoardButton->setStyleSheet("background-color:"+chooseColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                             +QString::number(static_cast<int>(stBtnFontSiz))+"px;");
            rotateBoardButton->adjustSize();
            colorWidget->repaint();
        }
        ratio = mobWidth*0.277777;
        if(newGameWidget && newGameWidget != nullptr){
            newGameWidget->setGeometry(0,mobWidth,mobWidth,static_cast<int>(ratio+1));
            newGameButton->setMinimumWidth(static_cast<int>(geomWidth+1));
            newGameButton->setMinimumHeight(static_cast<int>(geomHeight+1));
            newGameButton->adjustSize();
            newGameButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                         +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            newGameWidget->repaint();
        }

        if(resignWidget && resignWidget!=nullptr){
            resignWidget->setGeometry(0,mobWidth,mobWidth,static_cast<int>(ratio+1));
            resignButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
            resignButton->setMinimumHeight(static_cast<int>(geomHeight+1));
            resignButton->adjustSize();
            resignButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            resignImageLabel->setMinimumWidth(resignImageLabelWidth);
            resignImageLabel->setMinimumHeight(resignImageLabelHeight);
            resignImageLabel->adjustSize();
            drawButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
            drawButton->setMinimumHeight(static_cast<int>(geomHeight+1));
            drawButton->adjustSize();
            drawButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            resignWidget->repaint();
        }
        levelGeomHeight = mobWidth*1.277777;
        levelWidth = mobWidth*0.4986111;
        ratio = mobWidth*0.1388888;

        //HINT
        ratio = mobWidth*0.18055555;
        if(hintWidget && hintWidget!=nullptr){
            hintWidget->setGeometry(0,static_cast<int>(levelGeomHeight+1),mobWidth,static_cast<int>(ratio+1));
            hintButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
            hintButton->setMinimumHeight(static_cast<int>(geomHeight-(mobWidth*0.01388888)));
            hintButton->adjustSize();
            hintButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            //Move Back
            undoMoveButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
            undoMoveButton->setMinimumHeight(static_cast<int>(geomHeight-(mobWidth*0.01388888)));
            undoMoveButton->adjustSize();
            undoMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
            hintWidget->repaint();
        }
        //ADS
        if(!bannerShut){
            if(banner&&banner!=nullptr){//not resized
                double bannerOffset = ((checkHeight - mobWidth) <= 561 ? 0.07 : 0.08);
                bannerHAvNotRes = static_cast<int>(((levelGeomHeight+1)+(mobWidth * bannerOffset)));
                // QMessageBox::information(this, "Info", ("OFFSET CASE!"+QString::number((checkHeight - mobWidth))));
                if((checkHeight - mobWidth) >= 1045) {
                    // QMessageBox::information(this, "Info", "OFFSET CASE!");
                    double add = static_cast<double>(checkHeight * 0.035);
                    bannerHAvNotRes += static_cast<int>(add);
                }
                QPoint bannerPos(0,bannerHAvNotRes);
                banner->setPosition(bannerPos);
            }
        }

        gameInfo->setGeometry(0,(static_cast<int>(levelGeomHeight+1)+static_cast<int>(ratio+1)),mobWidth,static_cast<int>(ratio+1));
        fontsize = mobWidth*0.0494444;//0.0694444
        if(islGActive.load()){
            double fontsize = mobWidth*0.04;
            gameText->setStyleSheet("font-size:"+QString::number(static_cast<int>(fontsize))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        }
        else{
            gameText->setStyleSheet("font-size:"+QString::number(static_cast<int>(fontsize-10.0))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        }
        gameInfo->repaint();

        //SETTINGS
        double geomButtonHeight = mobWidth*0.1388888;
        if(settingsWidget && settingsWidget!=nullptr){
            settingsWidget->setGeometry(0,0,mobWidth,checkHeight);
            settingsWidget->resize(mobWidth,checkHeight);
            settingsWidget->setStyleSheet(
                ".QWidget { "
                " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
                "}");

            geomHeight = 0;
            ratio = mobWidth*0.3;
            geomHeight+=ratio;
            titleInfoWidget->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
            titleInfoText->setText("Deep Chess");
            titleFont = mobWidth*0.0494444;//0.0694444
            titleInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            //LOAD GAME
            geomWidth = (mobWidth-20) / 2;
            geomButtonHeight = mobWidth*0.1188888;//0.1388888
            loadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(titleFont))+"px;");
            //loadGameButton->setMinimumWidth(static_cast<int>(geomWidth)+static_cast<int>(mobWidth*0.019));
            loadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            filesButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                       +QString::number(static_cast<int>(titleFont))+"px;");
            filesButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            removeAdsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
            titleInfoWidget->repaint();

            ratio = mobWidth*0.15;
            geomHeight+=ratio;
            levelInfoWidget->setGeometry(0,static_cast<int>(mobWidth*0.3),mobWidth,static_cast<int>(ratio));
            levelInfoText->setText(tr("Level"));
            levelInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            levelInfoWidget->repaint();
            //LOAD GAME
            ratio = mobWidth*0.35;
            loadGameInfoWidget->setGeometry(0,static_cast<int>(mobWidth*0.10),mobWidth,static_cast<int>(ratio));
            loadGameInfoText->setText(tr("Load Game"));
            loadGameInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            loadGameInfoWidget->repaint();

            ratio = mobWidth*0.277777;;
            geomWidth = (mobWidth-20) / 3;
            geomButtonHeight = mobWidth*0.1388888;
            chooseStrengthWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));


            downStrengthButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                              +QString::number(static_cast<int>(titleFont))+"px;");
            downStrengthButton->setMinimumWidth(static_cast<int>(geomWidth));
            downStrengthButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            downStrengthButton->adjustSize();

            levelStatusText->setText(QString::number(playLevel));
            levelStatusText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

            upStrengthButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            upStrengthButton->setMinimumWidth(static_cast<int>(geomWidth));
            upStrengthButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            upStrengthButton->adjustSize();
            chooseStrengthWidget->repaint();

            //LOAD GAME
            loadGameWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
            geomHeight+=ratio;
            geomWidth = (mobWidth-20) / 12;

            downLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                              +QString::number(static_cast<int>(titleFont))+"px;");
            downLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            downLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            downLoadGameButton->adjustSize();

            loadGameStatusText->setText(currentLoadGameName);
            loadGameStatusText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

            upLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            upLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            upLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            upLoadGameButton->adjustSize();
            loadGameWidget->repaint();

            ratio = mobWidth*0.277777;
            geomWidth = (mobWidth-40) / 2;
            starAnalysisWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
            if(starAnalyzisButton->isEnabled())
                starAnalyzisButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                  +QString::number(static_cast<int>(titleFont))+"px;");
            else
                starAnalyzisButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                  +QString::number(static_cast<int>(titleFont))+"px;");

            starAnalyzisButton->setMinimumWidth(static_cast<int>(geomWidth));
            starAnalyzisButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            starAnalyzisButton->adjustSize();
            newFiguresButton->setMinimumWidth(static_cast<int>(geomWidth/2-(mobWidth*0.01388888)));
            newFiguresButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            newFiguresButton->adjustSize();
            newFiguresButton->setIconSize(QSize(static_cast<int>(geomWidth/2-(mobWidth*0.01388888))-4,static_cast<int>(geomButtonHeight)-4));
            newFiguresButton->repaint();
            oldFiguresButton->setMinimumWidth(static_cast<int>(geomWidth/2-(mobWidth*0.01388888)));
            oldFiguresButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            oldFiguresButton->adjustSize();
            oldFiguresButton->setIconSize(QSize(static_cast<int>(geomWidth/2-(mobWidth*0.01388888))-4,static_cast<int>(geomButtonHeight)-4));
            oldFiguresButton->repaint();
            starAnalysisWidget->repaint();

            //LOAD GAME
            deleteGameWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
            geomHeight+=ratio;
            deleteGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");

            deleteGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            deleteGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            deleteGameButton->adjustSize();
            exportPGNButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
            exportPGNButton->setMinimumWidth(static_cast<int>(geomWidth));
            exportPGNButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            exportPGNButton->adjustSize();
            //EXPORT
            exportGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");

            exportGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            exportGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            exportGameButton->adjustSize();
            deleteGameWidget->repaint();

            ratio = mobWidth*0.15;
            colorInfoWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
            geomHeight+=ratio;
            colorInfoWidget->setStyleSheet("background-color:lightgrey;");
            colorInfoText->setText(tr("Color"));
            colorInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                         "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            colorInfoWidget->repaint();
            geomWidth = (mobWidth-40) / 2.0;
            chooseColorWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
            geomHeight+=ratio;
            playWhiteColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                +QString::number(static_cast<int>(titleFont))+"px;");
            playWhiteColorButton->setMinimumWidth(static_cast<int>(geomWidth));
            playWhiteColorButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            playWhiteColorButton->adjustSize();

            playBlackColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                +QString::number(static_cast<int>(titleFont))+"px;");
            playBlackColorButton->setMinimumWidth(static_cast<int>(geomWidth));
            playBlackColorButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            playBlackColorButton->adjustSize();
            chooseColorWidget->repaint();

            okOrCancelWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
            okSettingsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            okSettingsButton->setMinimumWidth(static_cast<int>(geomWidth));
            okSettingsButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            okSettingsButton->adjustSize();

            cancelSettingsButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                +QString::number(static_cast<int>(titleFont))+"px;");
            cancelSettingsButton->setMinimumWidth(static_cast<int>(geomWidth));
            cancelSettingsButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            cancelSettingsButton->adjustSize();
            okOrCancelWidget->repaint();

            //LOAD GAME
            okOrCancelLoadGameWidget->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio));
            okLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            okLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            okLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            okLoadGameButton->adjustSize();

            cancelLoadGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                +QString::number(static_cast<int>(titleFont))+"px;");
            cancelLoadGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            cancelLoadGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            cancelLoadGameButton->adjustSize();
            okOrCancelLoadGameWidget->repaint();
            settingsWidget->repaint();
            if(!settingsWidget->isVisible() && !titleInfoWidget->isVisible()){
                settingsWidget->hide();
                if(hintWidget&&hintWidget!=nullptr){
                    if((!hintWidget->isVisible())&&(!bannerShut)&&(banner)&&(banner!=nullptr)&&(connection->checkGDPR())) {
                        if((!gdprWidget || gdprWidget == nullptr) && (!rewardedWidget || !rewardedWidget->isVisible())) {
                            banner->setVisible(true);
                            isBannerVisibleCond = true;
                        }
                    }
                }
                else{
                    if((!bannerShut)&&(banner)&&(banner!=nullptr)&&(connection->checkGDPR())) {
                        if((!gdprWidget || gdprWidget == nullptr) && (!rewardedWidget || !rewardedWidget->isVisible())) {
                            banner->setVisible(true);
                            isBannerVisibleCond = true;
                        }
                    }
                }
            }
            else{
                if((!bannerShut)&&(banner)&&(banner!=nullptr)){
                    banner->setVisible(false);
                    isBannerVisibleCond = false;
                }
                settingsWidget->show();
            }
        }

        if(filesWidget && filesWidget!=nullptr){
            if(!bannerShut&&banner&&banner!=nullptr){
                if(filesWidget->isVisible())
                    isBannerVisibleCond = false;
                banner->setVisible(isBannerVisibleCond);
            }
            titleFont = mobWidth*0.0494444;//0.0694444
            filesWidget->setGeometry(0,0,mobWidth,checkHeight);
            filesWidget->resize(mobWidth,checkHeight);
            filesWidget->setStyleSheet(
                ".QWidget { "
                " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
                "}");
            addBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                         +QString::number(static_cast<int>(titleFont))+"px;");
            addBookButton->setMinimumWidth(static_cast<int>(geomWidth));
            addBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            addBookButton->adjustSize();

            removeBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            removeBookButton->setMinimumWidth(static_cast<int>(geomWidth));
            removeBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            removeBookButton->adjustSize();
            //            importGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
            //                                                +QString::number(static_cast<int>(titleFont))+"px;");
            //            importGameButton->setMinimumWidth(static_cast<int>(geomWidth));
            //            importGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            //            importGameButton->adjustSize();
            closeFilesButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                            +QString::number(static_cast<int>(titleFont))+"px;");
            closeFilesButton->setMinimumWidth(static_cast<int>(geomWidth));
            closeFilesButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            closeFilesButton->adjustSize();
            filesWidget->repaint();
        }
        //SETTINGS
        //ACHIEVEMENTS
        if(congratsBannerWidget && congratsBannerWidget!=nullptr){
            congratsBannerWidget->setGeometry(0,0,mobWidth,checkHeight);
            congratsBannerWidget->resize(mobWidth,checkHeight);
            congratsBannerWidget->setStyleSheet(
                ".QWidget { "
                " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
                "}");

            geomHeight = 0;
            ratio = mobWidth*0.2;
            geomHeight+=ratio;
            congratsLabel->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
            congratsLabel->setText(tr("Great! You won!"));
            titleFont = mobWidth*0.0494444;//0.0694444
            congratsLabel->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+
                                         "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

            congratsImageLabel->setGeometry((mobWidth*25)/100,static_cast<int>(geomHeight),(mobWidth*50)/100,static_cast<int>(ratio+1));
            congratsImageLabel->setMinimumWidth((mobWidth*50)/100);
            congratsImageLabel->setMinimumHeight(static_cast<int>(geomHeight+1));
            congratsImageLabel->adjustSize();
            geomHeight+=ratio;
            congratsImageLabel->setStyleSheet("background: url(\"assets:/images/win.png\") no-repeat center;background-size:"+
                                              QString::number(congratsImageLabel->width())+"px;");

            statisticsLabel->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio*2));
            geomHeight+=ratio;
            statisticsLabel->setText(tr("You have ")+QString::number(winsOnLevelCounter)+tr(" wins on")+levelForDB+"!");
            statisticsLabel->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                           "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

            ratio = mobWidth*0.277777;
            geomWidth = (mobWidth-40) / 2;
            congratsCloseButton->setGeometry((mobWidth-static_cast<int>(geomWidth))/2,static_cast<int>(geomHeight),
                                             static_cast<int>(geomWidth),static_cast<int>(ratio));
            congratsCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                               +QString::number(static_cast<int>(titleFont))+"px;");
            congratsCloseButton->setMinimumWidth(static_cast<int>(geomWidth));
            congratsCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            congratsCloseButton->adjustSize();
            congratsBannerWidget->repaint();
            if(!congratsBannerWidget->isVisible()){
                congratsBannerWidget->hide();
            }
            else{
                congratsBannerWidget->show();
            }
        }
        //GDPR
        if(gdprWidget && gdprWidget != nullptr){
            gdprWidget->setGeometry(0,0,mobWidth,checkHeight);
            gdprWidget->resize(mobWidth,checkHeight);
            gdprWidget->setStyleSheet(
                ".QWidget { "
                " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
                "}");

            gdprText->setText(tr("Dear Player, This App contains Ads By clicking Accept, You agree to receive Ads. Alternatively please consider to install the paid version."));
            gdprText->adjustSize();
            gdprText->setMaximumHeight(static_cast<int>(0.4777777*mobWidth));
            double font = (mobWidth*0.04);
            gdprText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(font)+5)+
                                    "px;color:rgb(204, 163, 0);font-weight:bold;text-align:left;");

            gdprAgreeButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
            gdprAgreeButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            gdprAgreeButton->adjustSize();
            gdprPaidButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(titleFont))+"px;");
            gdprPaidButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            gdprPaidButton->adjustSize();
            gdprWidget->repaint();
        }

        if(rewardedWidget && rewardedWidget!=nullptr){
            rewardedWidget->setGeometry(0,0,mobWidth,checkHeight);
            rewardedWidget->resize(mobWidth,checkHeight);
            rewardedWidget->setStyleSheet(
                ".QWidget { "
                " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
                "}");

            rewardedText->setText(tr("Dear Player,You can Activate Undo,Hints and Puzzles by watching Rewarded ads.Alternatively please consider to install the paid version and get permanent access to those features together with many others."));
            //rewardedText->adjustSize();
            //rewardedText->setMaximumHeight(static_cast<int>(0.4777777*mobWidth));
            //rewardedText->setMinimumHeight(static_cast<int>(checkHeight / 3));
            double font = (mobWidth*0.04);
            rewardedText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(font))+
                                    "px;color:rgb(204, 163, 0);font-weight:bold;text-align:left;");

            rewardedActivateButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
            rewardedActivateButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            rewardedActivateButton->adjustSize();
            rewardedPaidButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(titleFont))+"px;");
            rewardedPaidButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            rewardedPaidButton->adjustSize();
            rewardedCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                              +QString::number(static_cast<int>(titleFont))+"px;");
            rewardedCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            rewardedCloseButton->adjustSize();
            rewardedWidget->repaint();
        }

        if(achievementBannerWidget && achievementBannerWidget!=nullptr){
            achievementBannerWidget->setGeometry(0,0,mobWidth,checkHeight);
            achievementBannerWidget->resize(mobWidth,checkHeight);
            achievementBannerWidget->setStyleSheet(
                ".QWidget { "
                " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
                "}");

            geomHeight = 0;
            ratio = mobWidth*0.2;
            geomHeight+=ratio;
            achievementLabelText->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
            achievementLabelText->setText(tr("Great!You won ")+achievementType+"!");
            titleFont = mobWidth*0.0494444;//0.0694444
            achievementLabelText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+
                                                "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
            achievementLabelImage->setGeometry((mobWidth*2)/100,static_cast<int>(geomHeight),(mobWidth*60)/100,static_cast<int>(ratio+1));
            achievementLabelImage->setMinimumWidth((mobWidth*60)/100);
            achievementLabelImage->setMinimumHeight(static_cast<int>(geomHeight+1));
            achievementLabelImage->setStyleSheet("background:transparent;border:none;outline:none;");
            achievementLabelImage->adjustSize();
            geomHeight+=ratio;
            achievementLabelImage->setIcon(QIcon(achievementsLabelPath));
            achievementLabelImage->setIconSize(QSize(achievementLabelImage->width(),achievementLabelImage->height()));

            ratio = mobWidth*0.277777;
            geomWidth = (mobWidth-40) / 2;
            achievementsCloseButton->setGeometry((mobWidth-static_cast<int>(geomWidth))/2,static_cast<int>(geomHeight),
                                                 static_cast<int>(geomWidth),static_cast<int>(ratio));
            achievementsCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                   +QString::number(static_cast<int>(titleFont))+"px;");
            achievementsCloseButton->setMinimumWidth(static_cast<int>(geomWidth));
            achievementsCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
            achievementsCloseButton->adjustSize();
            achievementBannerWidget->repaint();
            if(!achievementBannerWidget->isVisible()){
                achievementBannerWidget->hide();
            }
            else{
                achievementBannerWidget->show();
            }
        }

        //RATE GAME
        if(rateWidget && rateWidget != nullptr){
            if(rateWidget->isVisible()){
                rateWidget->setGeometry(0,0,mobWidth,checkHeight);
                rateWidget->resize(mobWidth,checkHeight);
                rateWidget->setStyleSheet(
                    ".QWidget { "
                    " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
                    "}");
                titleFont = mobWidth*0.06;
                rateText_2->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(titleFont))+"px;");
                rateText_2->setMinimumWidth(static_cast<int>(geomWidth));
                rateText_2->setMinimumHeight(static_cast<int>(geomButtonHeight));
                rateText_2->adjustSize();
                closeRate->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                         +QString::number(static_cast<int>(titleFont))+"px;");
                closeRate->setMinimumWidth(static_cast<int>(geomWidth));
                closeRate->setMinimumHeight(static_cast<int>(geomButtonHeight));
                closeRate->adjustSize();
                rateWidget->repaint();
            }
        }
    }
}
void MainWindow::resizeButtons(double fieldSize, double icsize)
{
    QSize icSize(static_cast<int>(icsize), static_cast<int>(icsize));
    for(int i =0;i<buttons.size();i++){
        buttons.at(i)->setMinimumHeight(static_cast<int>(fieldSize));
        buttons.at(i)->setMaximumHeight(static_cast<int>(fieldSize));
        buttons.at(i)->setMinimumWidth(static_cast<int>(fieldSize));
        buttons.at(i)->setMaximumWidth(static_cast<int>(fieldSize));
        buttons.at(i)->setIconSize(icSize);
        buttons.at(i)->adjustSize();
    }
}

void MainWindow::OnInterstitialLoaded()
{
}

void MainWindow::OnInterstitialLoading()
{
}

void MainWindow::OnInterstitialClicked()
{
    if(isHintActivatedInMainWindow)
        return;
    isHintActivatedInMainWindow = true;
    adInterstCurrentUsed = true;
    adInterstCurrentClicked = true;
    isPuzzleAct = true;
    if((settingsWidget) && (settingsWidget != nullptr)){
        isPuzzleActivated = true;
    }
    connection->disablePuzzle(false);
    //OnInterstitialClosed();
}

void MainWindow::OnInterstitialClosed()
{
    adInterstCurrentUsed = true;
    if(adInterstCurrentClicked){
        QMessageBox::information(this, "Info", tr("Thank You! Undo and Hints are active for 5 games!"));
        adInterstCurrentClicked = false;
    }
    if(rewardedWidget && rewardedWidget->isVisible()) {
        rewardedWidget->hide();
        if(m_Interstitial)
            m_Interstitial->reloadRewarded();
    }
}

void MainWindow::OnBannerLoaded()
{
    bool isBannerVis = true;
    if((hintWidget) && (hintWidget != nullptr)){
        if(hintWidget->isVisible())
            isBannerVis = false;
    }

    if((gdprWidget) && (gdprWidget != nullptr)) {
        if(gdprWidget->isVisible())
            isBannerVis = false;
    }

    if((rewardedWidget) && (rewardedWidget != nullptr)) {
        if(rewardedWidget->isVisible())
            isBannerVis = false;
    }

    if((settingsWidget) && (settingsWidget != nullptr)){
        if((settingsWidget->isVisible()) || (titleInfoWidget && (titleInfoWidget->isVisible())) || (filesWidget && filesWidget->isVisible())){
            isBannerVisibleCond = false;
            return;
        }
    }
    isBannerVisibleCond = true;

    if((banner) && (banner != nullptr)){
        if(!bannerShut){
            if((isBannerVisibleCond == false) || (isBannerVis == false) || (isHintActivatedInMainWindow == true)){
                QPoint bannerPos(0,bannerHNotAv);
                banner->setPosition(bannerPos);
                banner->setVisible(false);
            }
            else{
                if(isResized){
                    QPoint bannerPos(mobWidth,bannerHAvRes);//resized
                    banner->setPosition(bannerPos);
                }
                else{
                    QPoint bannerPos(0,bannerHAvNotRes);
                    banner->setPosition(bannerPos);
                }
                banner->setVisible(true);
            }

            if(wrongBanerPos){
                isBannerVisibleCond = false;
                QPoint bannerPos(0,bannerHNotAv);
                banner->setPosition(bannerPos);
                banner->setVisible(false);
            }
            else{
                isBannerVisibleCond = true;
                if(isResized){
                    QPoint bannerPos(mobWidth,bannerHAvRes);//resized
                    banner->setPosition(bannerPos);
                }
                else{
                    QPoint bannerPos(0,bannerHAvNotRes);
                    banner->setPosition(bannerPos);
                }
                banner->setVisible(true);
            }
        }
    }
}

void MainWindow::OnBannerLoading()
{
    if((settingsWidget) && (settingsWidget != nullptr)){
        if((settingsWidget->isVisible()) || (titleInfoWidget && (titleInfoWidget->isVisible()))){
            isBannerVisibleCond = false;
            if(banner) {
                QPoint bannerPos(0,bannerHNotAv);
                banner->setPosition(bannerPos);
                banner->setVisible(false);
            }
        } else
            isBannerVisibleCond = true;
    }
    else{
        isBannerVisibleCond = true;
    }

    if(wrongBanerPos){
        isBannerVisibleCond = false;
        if(banner) {
            QPoint bannerPos(0,bannerHNotAv);
            banner->setPosition(bannerPos);
            banner->setVisible(false);
        }
    }
}

void MainWindow::OnBannerClicked()
{
    if(isHintActivatedInMainWindow)
        return;
    isBannerClicked = true;
    isHintActivatedInMainWindow = true;
    isPuzzleAct = true;
    if((settingsWidget) && (settingsWidget != nullptr)){
        isPuzzleActivated = true;
    }

    if((!bannerShut) && (banner) && (banner != nullptr))
        banner->setVisible(false);

    if((!hintWidget) || (hintWidget == nullptr)){
        double geomWidth = mobWidth*0.375;
        double geomHeight = mobWidth*0.1388888;
        double levelGeomHeight = mobWidth*1.277777;
        double ratio = mobWidth*0.18055555;
        hintWidget = new QWidget(this);
        hintWidget->setGeometry(0,static_cast<int>(levelGeomHeight+1),mobWidth,static_cast<int>(ratio+1));
        hintWidget->setStyleSheet("background-color:"+whitesColor+";");
        hintWidgetLayout = new QHBoxLayout(hintWidget);
        hintButton = new QPushButton(tr("Hint"),hintWidget);
        hintButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
        hintButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
        hintButton->setMinimumHeight(static_cast<int>(geomHeight-(mobWidth*0.01388888)));
        hintButton->adjustSize();
        hintButton->setDisabled(true);
        //Move Back
        undoMoveButton = new QPushButton(tr("Undo"),hintWidget);
        undoMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                      +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
        undoMoveButton->setMinimumWidth(static_cast<int>(geomWidth-(0.031*mobWidth)));
        undoMoveButton->setMinimumHeight(static_cast<int>(geomHeight-(mobWidth*0.01388888)));
        undoMoveButton->adjustSize();
        hintWidgetLayout->addWidget(hintButton,1,Qt::AlignLeft);
        hintWidgetLayout->addWidget(undoMoveButton,1,Qt::AlignRight);
        connect(hintButton, &QPushButton::clicked, this, &MainWindow::startSearchHintMove);
        connect(this, &MainWindow::hintMoveFound, this, &MainWindow::showHintMove);
        connect(undoMoveButton, &QPushButton::clicked, this, &MainWindow::moveBackGui);
        hintWidget->hide();
    }
    connection->disablePuzzle(false);
}

void MainWindow::OnBannerClosed()
{
    if(isBannerClicked) {
        if(resignWidget&&resignWidget!=nullptr&&sideBar&&sideBar!=nullptr){
            if(hintWidget&&hintWidget!=nullptr&&(resignButton->isVisible()||nextMoveButton->isVisible())){
                hintWidget->show();
                hintButton->setDisabled(false);
            }
        }
        isBannerClicked = false;
        QMessageBox::information(this, "Info", tr("Thank You! Undo and Hints are active for 5 games!"));
    } else {
        QMessageBox::information(this, "Info", tr("Thank You! Undo and Hints are already active!"));
    }
}

void MainWindow::rewardedActivateClicked() {
    if((m_Interstitial) && (m_Interstitial != nullptr)) {
        if(!m_Interstitial->isAdsSDKInitialized()) {
            QMessageBox::information(this, "Info", tr("Sorry an error occurred! Please check Your internet connection and try again!"));
            return;
        }
        if(m_Interstitial->isRewardedLoaded()) {
            try {
                m_Interstitial->setVisible(true);
            }
            catch (...) {
                OnInterstitialClicked();
                QMessageBox::information(this, "Info", tr("Thank You! Undo and Hints are active for 5 games!"));
            }
        } else {
            if(isRewardedFirstTry) {
                QMessageBox::information(this, "Info", tr("Sorry an error occurred! Please check Your internet connection and try again!"));
                isRewardedFirstTry = false;
            } else {
                OnInterstitialClicked();
                QMessageBox::information(this, "Info", tr("Thank You! Undo and Hints are active for 5 games!"));
            }
        }
        return;
    }
    OnInterstitialClicked();
    QMessageBox::information(this, "Info", tr("Thank You! Undo and Hints are active for 5 games!"));
}

void MainWindow::rewardedCloseClicked() {
    rewardedWidget->hide();
}

bool MainWindow::checkForPlayerWon()
{
    bool b = false;
    winsOnLevelCounter = connection->queryDBAchievemnts(levelForDB);
    ++winsOnLevelCounter;
    b = connection->updateDBAchievemnts(levelForDB,winsOnLevelCounter);
    winsOnLevelCounter = connection->queryDBAchievemnts(levelForDB);
    if (b && connection->isDbOpened && connection->dbExistsInfo == "Wincount selected!")
        return true;
    else
        return false;
}

void MainWindow::wonCongratulations()
{
    //Move Back
    if(undoButtonClicked){
        setGameInfoVis(gameInfo,gameText);
        return;
    }

    if(((gameEndResult == "Checkmate!Black Wins!"&&chessAiColor == 'w')||
         (gameEndResult == "Checkmate!White Wins!"&&chessAiColor=='b'))&&!isHumanWon) {
        isHumanWon = true;
        congratsBannerWidget = new QWidget(this);
        congratsBannerWidget->setGeometry(0,0,mobWidth,checkHeight);
        congratsBannerWidget->setStyleSheet(
            ".QWidget { "
            " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
            "}");
        congratsBannerWidget->adjustSize();

        double geomHeight = 0;
        congratsLabel = new QLabel(congratsBannerWidget);
        double ratio = mobWidth*0.2;
        geomHeight+=ratio;
        congratsLabel->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
        congratsLabel->setText(tr("Great! You won!"));
        titleFont = mobWidth*0.0494444;//0.0694444
        congratsLabel->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+
                                     "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        QVBoxLayout *congratsBannerLayout = new QVBoxLayout(congratsBannerWidget);
        congratsBannerLayout->addWidget(congratsLabel,1,Qt::AlignHCenter);

        congratsImageLabel = new QLabel(congratsBannerWidget);
        congratsImageLabel->setGeometry((mobWidth*25)/100,static_cast<int>(geomHeight),(mobWidth*50)/100,static_cast<int>(ratio+1));
        congratsImageLabel->setMinimumWidth((mobWidth*50)/100);
        congratsImageLabel->setMinimumHeight(static_cast<int>(geomHeight+1));
        congratsImageLabel->adjustSize();
        geomHeight+=ratio;
        congratsImageLabel->setStyleSheet("background: url(\"assets:/images/win.png\") no-repeat center;background-size:"+
                                          QString::number(congratsImageLabel->width())+"px;");
        congratsBannerLayout->addWidget(congratsImageLabel,1,Qt::AlignHCenter);

        statisticsLabel = new QLabel(congratsBannerWidget);
        statisticsLabel->setGeometry(0,static_cast<int>(geomHeight),mobWidth,static_cast<int>(ratio*2));
        geomHeight+=ratio;
        statisticsLabel->setText(tr("You have ")+QString::number(winsOnLevelCounter)+tr(" wins on")+levelForDB+"!");
        statisticsLabel->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                       "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        congratsBannerLayout->addWidget(statisticsLabel,1,Qt::AlignHCenter);

        ratio = mobWidth*0.277777;
        double geomWidth = (mobWidth-40) / 2;
        congratsCloseButton = new QPushButton(tr("Close"),congratsBannerWidget);
        congratsCloseButton->setGeometry((mobWidth-static_cast<int>(geomWidth))/2,static_cast<int>(geomHeight),
                                         static_cast<int>(geomWidth),static_cast<int>(ratio));
        congratsCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                           +QString::number(static_cast<int>(titleFont))+"px;");
        congratsCloseButton->setMinimumWidth(static_cast<int>(geomWidth));
        double geomButtonHeight = mobWidth*0.1388888;
        congratsCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        congratsCloseButton->adjustSize();
        connect(congratsCloseButton,&QPushButton::clicked,this,&MainWindow::congratsBannerClose);
        congratsBannerLayout->addWidget(congratsCloseButton,1,Qt::AlignCenter);
        bool isWon = checkForPlayerWon();
        if(isWon)
            statisticsLabel->setText(tr("You have ")+QString::number(winsOnLevelCounter)+tr(" wins on ")+levelForDB+"!");
        else
            statisticsLabel->setText(tr("Problem ")+connection->dbExistsInfo+tr(" on ")+levelForDB+"!");

        congratsBannerWidget->show();
        changeWidgetsGeometry();
        if(!bannerShut && banner && banner!=nullptr){
            QPoint bannerPos(0,bannerHNotAv);
            banner->setPosition(bannerPos);
            banner->setVisible(false);
        }
    }
}

void MainWindow::newGameClicked()
{
    if(isHumanWon||adInterstCurrentUsed){
        if(adInterstCurrentUsed)
            adInterstCurrentUsed=false;

        repaintMainWindow();
        return;
    }
    else
        showAdsBanner();
}

void MainWindow::congratsBannerClose()
{
    if(congratsBannerWidget&&congratsBannerWidget!=nullptr){
        congratsBannerWidget->hide();
    }
    if(winsOnLevelCounter == 3 || winsOnLevelCounter == 5 || winsOnLevelCounter == 7){
        achievementBannerWidget = new QWidget(this);
        achievementBannerWidget->setGeometry(0,0,mobWidth,checkHeight);
        achievementBannerWidget->adjustSize();
        achievementBannerWidget->setStyleSheet(
            ".QWidget { "
            " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
            "}");

        double geomHeight = 0;
        achievementLabelText = new QLabel(achievementBannerWidget);
        double ratio = mobWidth*0.2;
        geomHeight+=ratio;
        achievementLabelText->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
        achievementLabelText->setText(tr("Great!You won ")+achievementType+"!");
        titleFont = mobWidth*0.0494444;//0.0694444
        achievementLabelText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+
                                            "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        QVBoxLayout *achievementsBannerLayout = new QVBoxLayout(achievementBannerWidget);
        achievementsBannerLayout->addWidget(achievementLabelText,1,Qt::AlignHCenter);

        achievementLabelImage = new QPushButton(achievementBannerWidget);
        achievementLabelImage->setGeometry((mobWidth*2)/100,static_cast<int>(geomHeight),(mobWidth*60)/100,static_cast<int>(ratio+1));
        achievementLabelImage->setMinimumWidth((mobWidth*60)/100);
        achievementLabelImage->setMinimumHeight(static_cast<int>(geomHeight+1));
        achievementLabelImage->setStyleSheet("background:transparent;border:none;outline:none;");
        achievementLabelImage->adjustSize();
        geomHeight+=ratio;
        achievementLabelImage->setIcon(QIcon(achievementsLabelPath));
        achievementLabelImage->setIconSize(QSize(achievementLabelImage->width(),achievementLabelImage->height()));
        achievementsBannerLayout->addWidget(achievementLabelImage,1,Qt::AlignHCenter);

        ratio = mobWidth*0.277777;
        double geomWidth = (mobWidth-40) / 2;
        achievementsCloseButton = new QPushButton(tr("Close"),achievementBannerWidget);
        achievementsCloseButton->setGeometry((mobWidth-static_cast<int>(geomWidth))/2,static_cast<int>(geomHeight),
                                             static_cast<int>(geomWidth),static_cast<int>(ratio));
        achievementsCloseButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                               +QString::number(static_cast<int>(titleFont))+"px;");
        achievementsCloseButton->setMinimumWidth(static_cast<int>(geomWidth));
        double geomButtonHeight = mobWidth*0.1388888;
        achievementsCloseButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        achievementsCloseButton->adjustSize();
        connect(achievementsCloseButton,&QPushButton::clicked,this,&MainWindow::achievementsBannerClose);
        achievementsBannerLayout->addWidget(achievementsCloseButton,1,Qt::AlignCenter);
        achievementBannerWidget->hide();
    } else {
        if(!bannerShut && banner && banner != nullptr) {
            if(isResized){
                QPoint bannerPos(mobWidth,bannerHAvRes);//resized
                banner->setPosition(bannerPos);
            }
            else{
                QPoint bannerPos(0,bannerHAvNotRes);
                banner->setPosition(bannerPos);
            }
            banner->setVisible(true);
        }
    }

    if(winsOnLevelCounter == 3){
        achievementsLabelPath = "assets:/images/bronzestar.png";
        achievementType = tr("Bronze Star");
        achievementLabelText->setText(tr("Great!You won ")+achievementType+"!");
        achievementLabelText->setWordWrap(true);
        achievementLabelImage->setIcon(QIcon(achievementsLabelPath));
        achievementLabelImage->setIconSize(QSize(achievementLabelImage->width(),achievementLabelImage->height()));
        achievementBannerWidget->show();
        changeWidgetsGeometry();
        return;
    }
    else if(winsOnLevelCounter == 5){
        achievementsLabelPath = "assets:/images/silverstar.png";
        achievementType = tr("Silver Star");
        achievementLabelText->setText(tr("Great!You won ")+achievementType+"!");
        achievementLabelText->setWordWrap(true);
        achievementLabelImage->setIcon(QIcon(achievementsLabelPath));
        achievementLabelImage->setIconSize(QSize(achievementLabelImage->width(),achievementLabelImage->height()));
        achievementBannerWidget->show();
        changeWidgetsGeometry();
        return;
    }
    else if(winsOnLevelCounter == 7){
        achievementsLabelPath = "assets:/images/goldstar.png";
        achievementType = tr("Gold Star");
        achievementLabelText->setText(tr("Great!You won ")+achievementType+"!");
        achievementLabelText->setWordWrap(true);
        achievementLabelImage->setIcon(QIcon(achievementsLabelPath));
        achievementLabelImage->setIconSize(QSize(achievementLabelImage->width(),achievementLabelImage->height()));
        achievementBannerWidget->show();
        changeWidgetsGeometry();
        return;
    }
    if(windowCrashed){
        repaintMainWindow();
        return;
    }
    saveGameWidget->show();
}

void MainWindow::achievementsBannerClose()
{
    if(achievementBannerWidget&&achievementBannerWidget!=nullptr){
        achievementBannerWidget->hide();
    }

    if(isUgrdButton){
        rateWidget = new QWidget(this);
        rateWidget->setGeometry(0,0,mobWidth,checkHeight);
        rateWidget->adjustSize();
        rateWidget->setStyleSheet(
            ".QWidget { "
            " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
            "}");
        QVBoxLayout *rateLayout = new QVBoxLayout(rateWidget);
        rateTitle = new QLabel(rateWidget);
        titleFont = mobWidth*0.07;
        rateTitle->setText(tr("Thank You for using Deep Chess!"));
        rateTitle->setWordWrap(true);
        rateTitle->setAlignment(Qt::AlignCenter);
        rateTitle->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        rateText_1 = new QLabel(rateWidget);
        rateText_1->setText(tr("Upgrade to use more advanced features.Time settings and more and remove ads.Thank You!"));
        rateText_1->setWordWrap(true);
        rateText_1->setAlignment(Qt::AlignCenter);
        rateText_1->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        rateText_2 = new QPushButton(tr("Upgrade"),rateWidget);
        rateText_2->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                  +QString::number(static_cast<int>(titleFont))+"px;");
        double geomWidth = (mobWidth-40) / 2;
        double geomButtonHeight = mobWidth*0.1388888;
        rateText_2->setMinimumWidth(static_cast<int>(geomWidth));
        rateText_2->setMinimumHeight(static_cast<int>(geomButtonHeight));
        rateText_2->adjustSize();
        connect(rateText_2,&QPushButton::clicked,this,&MainWindow::gdprPaidClicked);//rateClicked

        closeRate = new QPushButton(tr("Close"),rateWidget);

        closeRate->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                 +QString::number(static_cast<int>(titleFont))+"px;");
        closeRate->setMinimumWidth(static_cast<int>(geomWidth));
        closeRate->setMinimumHeight(static_cast<int>(geomButtonHeight));
        closeRate->adjustSize();
        connect(closeRate,&QPushButton::clicked,this,&MainWindow::closeRateClicked);
        rateLayout->addWidget(rateTitle,0,Qt::AlignCenter);
        rateLayout->addWidget(rateText_1,1,Qt::AlignCenter);
        rateLayout->addWidget(rateText_2,1,Qt::AlignCenter);
        rateLayout->addWidget(closeRate,1,Qt::AlignCenter);
        if(!bannerShut && banner && banner!=nullptr){
            QPoint bannerPos(0,bannerHNotAv);
            banner->setPosition(bannerPos);
            banner->setVisible(false);
        }
        rateWidget->show();
        changeWidgetsGeometry();
    } else {
        if(!bannerShut && banner && banner != nullptr) {
            if(isResized){
                QPoint bannerPos(mobWidth,bannerHAvRes);//resized
                banner->setPosition(bannerPos);
            }
            else{
                QPoint bannerPos(0,bannerHAvNotRes);
                banner->setPosition(bannerPos);
            }
            banner->setVisible(true);
        }
    }
    saveGameWidget->show();
    if(windowCrashed){
        repaintMainWindow();
        return;
    }
}

void MainWindow::showAdsBanner()
{
    if((m_Interstitial) && (m_Interstitial != nullptr)){
        if((!adInterstCurrentUsed) && (m_Interstitial->isLoaded())) {
            try {
                m_Interstitial->setVisible(false);
                adInterstCurrentUsed = true;
            }
            catch (...) {
                repaintMainWindow();
                return;
            }
        }
        else{
            repaintMainWindow();
            adInterstCurrentUsed = false;
        }
    }
    else{
        repaintMainWindow();
        adInterstCurrentUsed = false;
    }
}

void MainWindow::showHintMove()
{
    transform(hintMoveStr.begin(), hintMoveStr.end(), hintMoveStr.begin(), ::toupper);
    int startCol = chooseNumLetter(hintMoveStr[0]),pStCol = 0;
    int destCol = chooseNumLetter(hintMoveStr[2]),pDestCol = 0;
    int startRow = ((BOARD_SIZE)-(hintMoveStr[1]-'0')),pStRow = 0;
    int destRow = ((BOARD_SIZE)-(hintMoveStr[3]-'0')),pDestRow = 0;

    if(isPonder&&Search::variantsForLG.size()>3){
        std::string s = Search::variantsForLG;
        transform(s.begin(), s.end(), s.begin(), ::toupper);
        pStCol = chooseNumLetter(s[0]);
        pDestCol = chooseNumLetter(s[2]);
        pStRow = ((BOARD_SIZE)-(s[1]-'0'));
        pDestRow = ((BOARD_SIZE)-(s[3]-'0'));
    }

    int startIndex = 0,destIndex = 0,startPonder = -1,destPonder = -1;
    for(int i=0;i<buttons.size();i++){
        if((buttons.at(i)->row)==startRow&&buttons.at(i)->col==startCol){
            startIndex = i;
            break;
        }
    }
    for(int i=0;i<buttons.size();i++){
        if((buttons.at(i)->row)==destRow&&buttons.at(i)->col==destCol){
            destIndex = i;
            break;
        }
    }
    if(isPonder){
        for(int i=0;i<buttons.size();i++){
            if((buttons.at(i)->row)==pStRow&&buttons.at(i)->col==pStCol){
                startPonder = i;
                break;
            }
        }
        for(int i=0;i<buttons.size();i++){
            if((buttons.at(i)->row)==pDestRow&&buttons.at(i)->col==pDestCol){
                destPonder = i;
                break;
            }
        }
    }
    highliteLastMove(startIndex,destIndex,startPonder,destPonder);

    isMoveHintActive.store(false);
    if(gameEndResult=="Please Wait!"){
        gameEndResult = "";
        gameEndResultTr = "";
        setGameInfoVis(gameInfo,gameText);
    }
    if(islGActive.load()){
        gameEndResult = uciMoveHint +" "+ ((Search::variantsForLG=="no move")?(""):(Search::variantsForLG));
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
    }
    Search::variantsForLG = "no move";
}

void MainWindow::startSearchHintMove()
{
    if(isAnalyzisMode){
        gameEndResult = "No Hints in this mode";
        gameEndResultTr = tr("No Hints in this mode");
        setGameInfoVis(gameInfo,gameText);
        return;
    }
    if(puzzleMode){
        if(movesInd==0){
            std::string str=puzzleMov[0].toStdString();
            std::string st = "";
            std::string start = str.substr(0,2);
            std::string end = str.substr(2,2);
            for(int i=0;i<buttons.size();++i){
                st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                if(st==start||st==end){
                    setAsActiveButton(i);
                }
            }
            return;
        }
        else{
            gameEndResult = "Sorry,no more hints";
            gameEndResultTr = tr("Sorry,no more hints");
            setGameInfoVis(gameInfo,gameText);
            return;
        }
    }
    if(isMoveHintActive.load()){
        gameEndResult = "Please Wait!";
        gameEndResultTr = tr("Please Wait!");
        setGameInfoVis(gameInfo,gameText);
        return;
    }

    isMoveHintActive.store(true);
    uciMoveCmdHint = uciMoveCmd;
    setUciMoveCommandHint();
    emit uciMoveCmdChanged();
}

void MainWindow::increaseLevelStrength()
{
    if(playLevel<20){
        ++playLevel;
        levelStatusText->setText(QString::number(playLevel));
    }
    else if(playLevel==20){
        playLevel = 1;
        levelStatusText->setText(QString::number(playLevel));
    }
}

void MainWindow::decreaseLevelStrength()
{
    if(playLevel>1){
        --playLevel;
        levelStatusText->setText(QString::number(playLevel));
    }
    else if(playLevel==1){
        playLevel = 20;
        levelStatusText->setText(QString::number(playLevel));
    }
}

void MainWindow::whiteColorChosen()
{
    if(isNotStartPosition){
        QMessageBox::information(this, "Info", tr("You can choose color only from start position."));
        return;
    }
    chessAiColor = 'b';
    emitSignalFlag = false;
    flipGUIBoardChessAiColor();
    playWhiteColorButton->setStyleSheet("background-color:rgba(235, 244, 66,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    playBlackColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    colorInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                 "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
}

void MainWindow::blackColorChosen()
{
    if(isNotStartPosition){
        QMessageBox::information(this, "Info", tr("You can choose color only from start position."));
        return;
    }
    chessAiColor = 'w';
    emitSignalFlag = true;
    flipGUIBoardChessAiColor();
    playBlackColorButton->setStyleSheet("background-color:rgba(235, 244, 66,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    playWhiteColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    colorInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                 "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
}

void MainWindow::okButtonClicked()
{
    if(chessAiColor==' ' && !isNotStartPosition){
        colorInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                     "px;color:rgb(235, 244, 66);font-weight:bold;text-align:center;");
        return;
    }

    if(ischesssAIStarted){
        return;
    }
    ischesssAIStarted = true;

    if(chessAiColor!=' '&&isNotStartPosition){
        chessAiColor = ' ';
    }
    QString str = "Level " + QString::number(playLevel);
    if(playLevel==1){
        Search::skillLevel = Search::skillLevelPlay = 1;
        if(Search::bookSettings.ownBook){
            Search::bookSettings.depth = 2;
            Search::bookSettings.bestBookMove = false;
        }
    }
    else if(playLevel>=2&&playLevel<=3){
        Search::skillLevel = Search::skillLevelPlay = playLevel;
        if(Search::bookSettings.ownBook){
            Search::bookSettings.depth = 5;
            Search::bookSettings.bestBookMove = false;
        }
    }
    else if(playLevel>3&&playLevel<=10){
        Search::skillLevel = Search::skillLevelPlay = playLevel;
        if(Search::bookSettings.ownBook){
            Search::bookSettings.depth = 50;
            Search::bookSettings.bestBookMove = false;
        }
    }
    else if(playLevel>10 && playLevel<=13){
        Search::skillLevel = Search::skillLevelPlay = playLevel;
        if(Search::bookSettings.ownBook){
            Search::bookSettings.depth = 150;
            Search::bookSettings.bestBookMove = false;
        }
    }
    else if(playLevel>13){
        Search::skillLevel = Search::skillLevelPlay = 20;
        if(Search::bookSettings.ownBook){
            Search::bookSettings.depth = 255;
            Search::bookSettings.bestBookMove = false;
        }
    }
    else{
        Search::skillLevel = Search::skillLevelPlay = 20;
        if(Search::bookSettings.ownBook){
            Search::bookSettings.depth = 255;
            Search::bookSettings.bestBookMove = false;
        }
    }

    //LASTLEVEL
    connection->updateLastLevel(playLevel);

    chooseChessAIPlayingStrength(str);
    if(settingsWidget&&settingsWidget!=nullptr){
        settingsWidget->hide();
    }
    if(colorWidget&&colorWidget!=nullptr){
        colorWidget->hide();
    }
    isBannerVisibleCond = true;
    wrongBanerPos = false;
    if(hintWidget&&hintWidget!=nullptr){
        if(!hintWidget->isVisible()&&!bannerShut&&banner&&banner!=nullptr){
            if(isResized){
                QPoint bannerPos(mobWidth,bannerHAvRes);
                banner->setPosition(bannerPos);
            }
            else{
                QPoint bannerPos(0,bannerHAvNotRes);
                banner->setPosition(bannerPos);
            }
            banner->setVisible(true);
        }
        else{
            banner->setVisible(false);
        }
    }
    else{
        if(!bannerShut&&banner&&banner!=nullptr){
            if(isResized){
                QPoint bannerPos(mobWidth,bannerHAvRes);//resized
                banner->setPosition(bannerPos);
            }
            else{
                QPoint bannerPos(0,bannerHAvNotRes);
                banner->setPosition(bannerPos);
            }
            banner->setVisible(true);
        }
    }
}

void MainWindow::cancelButtonClicked()
{
    playLevel = connection->getLastLevel();
    chessAiColor = ' ';
    isAnalyzisMode = false;
    if(settingsWidget&&settingsWidget!=nullptr)
        settingsWidget->hide();
    isBannerVisibleCond = true;
    wrongBanerPos = false;
    if(hintWidget&&hintWidget!=nullptr){
        if(!hintWidget->isVisible()&&!bannerShut&&banner&&banner!=nullptr){
            if(isResized){
                QPoint bannerPos(mobWidth,bannerHAvRes);//resized
                banner->setPosition(bannerPos);
            }
            else{
                QPoint bannerPos(0,bannerHAvNotRes);
                banner->setPosition(bannerPos);
            }
            banner->setVisible(true);
        }
        else{
            banner->setVisible(false);
        }
    }
    else{
        if(!bannerShut&&banner&&banner!=nullptr){
            if(isResized){
                QPoint bannerPos(mobWidth,bannerHAvRes);//resized
                banner->setPosition(bannerPos);
            }
            else{
                QPoint bannerPos(0,bannerHAvNotRes);
                banner->setPosition(bannerPos);
            }
            banner->setVisible(true);
        }
    }

    playWhiteColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    playBlackColorButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
    colorInfoText->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont))+
                                 "px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    levelStatusText->setText(QString::number(playLevel));
}

void MainWindow::resetButtonClicked()
{
    if((resetButtonClickedCheck && !puzzleMode && !islGActive.load()) || (!moveMadeWithoutComputer && !puzzleMode && !islGActive.load()))
        return;

    if(!puzzleMode && !islGActive.load() && !saveGameWidget->isVisible() && !isSaveGameClosed){
        saveGameWidget->show();
        return;
    }
    else if(!puzzleMode&&saveGameWidget->isVisible()) {
        saveGameWidget->hide();
        resetButtonClickedCheck = true;
    }
    if(!isLoadGame) {
        isGameResetted = true;
        resetButtonClickedCheck = true;
    }
    repaintMainWindow();
}

void MainWindow::newFiguresChoosed()
{
    if(isNotStartPosition){
        QMessageBox::information(this, "Info", tr("You can choose pieces only from start position."));
        return;
    }
    newFiguresSelected = true;
    oldFiguresButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");

    newFiguresButton->setStyleSheet("background-color:rgba(235, 244, 66,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    flipGUIBoardChessAiColor();
}

void MainWindow::oldFiguresChoosed()
{
    if(isNotStartPosition){
        QMessageBox::information(this, "Info", tr("You can choose pieces only from start position."));
        return;
    }
    newFiguresSelected = false;
    newFiguresButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    oldFiguresButton->setStyleSheet("background-color:rgba(235, 244, 66,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    flipGUIBoardChessAiColor();
}

void MainWindow::removeAdsClicked()
{
    if(!rewardedWidget)
        return;
    rewardedWidget->show();

}

void MainWindow::closeRateClicked()
{
    if(rateWidget && rateWidget != nullptr){
        rateWidget->hide();
    }
    saveGameWidget->show();
    adInterstCurrentUsed = true;
    if(!bannerShut && banner && banner != nullptr) {
        if(isResized){
            QPoint bannerPos(mobWidth,bannerHAvRes);//resized
            banner->setPosition(bannerPos);
        }
        else{
            QPoint bannerPos(0,bannerHAvNotRes);
            banner->setPosition(bannerPos);
        }
        banner->setVisible(true);
    }
}

void MainWindow::rateClicked()
{
    QString link = "market://details?id=org.deepchess.deepchess";
    QDesktopServices::openUrl(QUrl(link));
    connection->setUsedRate();
    if(rateWidget && rateWidget != nullptr){
        rateWidget->hide();
    }
    saveGameWidget->show();
    adInterstCurrentUsed = true;
}

void MainWindow::saveGameDB()
{
    try {
        gameName = inputDialog->getText(this,tr("Enter Name"),tr("Game Name"),QLineEdit::Normal,"",&isGameName);
    }
    catch (...) {
        gameName = "";
    }

    if(isGameName && !gameName.isEmpty()){
        QString s = QString::fromStdString(uciMoveCmd);
        if(s=="position startpos moves"){
            gameEndResult="Cannot save empty game!";
            gameEndResultTr = tr("Cannot save empty game!");
            setGameInfoVis(gameInfo,gameText);
            return;
        }
        if(chessAiColor=='w')
            s+="|b";
        else if(chessAiColor=='b')
            s+="|w";
        else if(chessAiColor==' '){
            if(colorFromNotStartPos=='w')
                s+="|w";
            else if(colorFromNotStartPos=='b')
                s+="|b";
            else{
                s+="|w";
            }
        }
        if(startBoard->getWhitePlayer().mate()||startBoard->getBlackPlayer().mate())
            s+="#m";
        else if(startBoard->getWhitePlayer().stalemate()||startBoard->getBlackPlayer().stalemate())
            s+="#s";

        if(!startBoard->getWhitePlayer().mate()&&!startBoard->getBlackPlayer().mate()&&
            !startBoard->getWhitePlayer().stalemate()&&!startBoard->getBlackPlayer().stalemate()){
            if(startBoard->getWhitePlayer().checked())
                s+="#x";
            else if(startBoard->getBlackPlayer().checked())
                s+="#y";
        }
        PGNString = "[Date \""+QDateTime::currentDateTime().toString()+"\"] \n"+PGNString;
        bool b = connection->saveGame(gameName, s, PGNString, gameEndResultTr);
        if(b){
            connection->queryDBGamesByName(gameName);
        }
        gameEndResult=connection->dbExistsInfo.toLocal8Bit().constData();
        setGameInfoVis(gameInfo,gameText);
        isSaveGameClosed = true;
    }
    else{
        gameEndResult="Name is empty!Try again!";
        gameEndResultTr = tr("Name is empty!Try again!");
        setGameInfoVis(gameInfo,gameText);
    }

}

void MainWindow::closeSaveGameDB()
{
    saveGameWidget->hide();
    isSaveGameClosed = true;
}

void MainWindow::loadGameClicked()
{
    resetButtonClickedCheck = false;
    isBannerVisibleCond = false;
    if((banner) && (banner != nullptr)){
        if(!bannerShut){
            banner->setVisible(false);
        }
    }

    isLoadGame = true;
    connection->queryDBGames(loadedGameNames);
    if(!loadedGameNames.empty()){
        currentLoadGameName = loadedGameNames.at(0);
        if(currentLoadGameName.isEmpty() || currentLoadGameName == "")
            currentLoadGameName = tr("No saved games");
        loadGameStatusText->setText(currentLoadGameName);
    }
    titleInfoText->setAlignment(Qt::AlignHCenter);
    removeAdsButton->hide();
    loadGameButton->hide();
    filesButton->hide();
    levelInfoWidget->hide();
    loadGameInfoWidget->show();
    chooseStrengthWidget->hide();
    starAnalysisWidget->hide();
    colorInfoWidget->hide();
    chooseColorWidget->hide();
    loadGameWidget->show();
    okOrCancelWidget->hide();
    okOrCancelLoadGameWidget->show();
    playAgainstComputer->hide();
    prevMoveButton->show();
    nextMoveButton->show();
    if(isPuzzleAct)
        isPuzzleActivated = true;
    deleteGameWidget->show();

}

void MainWindow::decreaseLoadedGames()
{
    if(!loadedGameNames.empty()){
        --gameCounter;
        if(gameCounter<0)
            gameCounter = 0;

        currentLoadGameName = loadedGameNames.at(unsigned(gameCounter));
        if(currentLoadGameName.isEmpty() || currentLoadGameName == "")
            currentLoadGameName = tr("No saved games");
        loadGameStatusText->setText(currentLoadGameName);
    }
    else{
        currentLoadGameName = tr("No saved games");
        loadGameStatusText->setText(currentLoadGameName);
    }
}

void MainWindow::increaseLoadedGames()
{
    if(!loadedGameNames.empty()){
        ++gameCounter;
        if(gameCounter>=static_cast<int>(loadedGameNames.size()))
            gameCounter = static_cast<int>(loadedGameNames.size()-1);

        currentLoadGameName = loadedGameNames.at(unsigned(gameCounter));
        if(currentLoadGameName.isEmpty() || currentLoadGameName == "")
            currentLoadGameName = tr("No saved games");
        loadGameStatusText->setText(currentLoadGameName);
    }
    else{
        currentLoadGameName = tr("No saved games");
        loadGameStatusText->setText(currentLoadGameName);
    }
}

void MainWindow::loadGameOkClicked()
{
    if(currentLoadGameName == tr("No saved games")){
        loadGameCancelClicked();
        return;
    }
    uciMoveCmd = "position startpos moves";
    if(settingsWidget&&settingsWidget!=nullptr)
        settingsWidget->hide();
    if(colorWidget&&colorWidget!=nullptr){
        colorWidget->hide();
    }
    if(hintWidget&&hintWidget!=nullptr){
        if(isHintActivatedInMainWindow){
            undoMoveButton->hide();
            hintWidgetLayout->removeWidget(undoMoveButton);
            hintWidget->show();
            hintButton->setDisabled(false);
            hintWidgetLayout->removeWidget(hintButton);
            hintWidgetLayout->addWidget(hintButton,Qt::AlignHCenter);
        }
        else if(!bannerShut&&banner&&banner!=nullptr)
            banner->setVisible(true);
    }
    else{
        if(!bannerShut&&banner&&banner!=nullptr)
            banner->setVisible(true);
    }
    currLoadedGameString = connection->queryDBGamesByName(currentLoadGameName);
    if(!currLoadedGameString.isEmpty()){
        lGColor = currLoadedGameString.mid(currLoadedGameString.indexOf('|',Qt::CaseInsensitive),2).at(1).toLatin1();
        if(currLoadedGameString.contains('#')){
            lGStatus = currLoadedGameString.mid(currLoadedGameString.indexOf('#',Qt::CaseInsensitive),2).at(1).toLatin1();
            currLoadedGameString.remove(currLoadedGameString.indexOf('#',Qt::CaseInsensitive),2);
        }
        currLoadedGameString.remove(currLoadedGameString.indexOf('|',Qt::CaseInsensitive),2);
        gameEndResult = currLoadedGameString.toLocal8Bit().constData();
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
        if(lGColor=='w')
            chessAiColor = 'b';
        else if(lGColor=='b')
            chessAiColor = 'w';

        QString cleared = currLoadedGameString.mid(24);
        double fontsize = mobWidth*0.04;//0.0694444;
        gameText->setStyleSheet("font-size:"+QString::number(static_cast<int>(fontsize))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
        gameEndResult=cleared.toLocal8Bit().constData();
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
        sList = cleared.split(' ',QString::SkipEmptyParts);
        flipGUIBoardChessAiColor();
        hintDepth = chooseHintMoveLevel("Level 20");
        islGActive.store(true);
        startChessAI();
    }
    else{
        gameEndResult=connection->dbExistsInfo.toLocal8Bit().constData();
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
    }

}
void MainWindow::prevMoveLG()
{
    resetAvMoves();
    if(!currLoadedGameString.isEmpty()){
        lGMoveCnt = 0;
        flipGUIBoardChessAiColor();
        uciMoveCmd = "position startpos moves";
    }
    else{
        gameEndResult=connection->dbExistsInfo.toLocal8Bit().constData();
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
    }
}

void MainWindow::nextMoveLG()
{
    if(puzzleMode){
        initPuzzle();
        return;
    }
    resetAvMoves();
    if(!currLoadedGameString.isEmpty()){

        if(lGMoveCnt>=sList.size()){
            return;
        }
        else if(lGMoveCnt<0){
            lGMoveCnt = 0;
        }

        QString str = sList.at(lGMoveCnt);
        lGMoveCnt++;
        QChar promo = ' ';
        if(str.size()==5){
            promo = str[4];
            str.remove(4,1);
        }
        std::string result = " ";
        QString picUrl = "";
        int figId = NO_EDGE;
        int index = NO_EDGE;
        QSize icSize(static_cast<int>(icsize), static_cast<int>(icsize));
        lGMoveFig(icSize, str,picUrl,result,figId,index,true,promo);
        gameEndResult=result;
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
        uciMoveCmd+=result;
    }
    else{
        gameEndResult=connection->dbExistsInfo.toLocal8Bit().constData();
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
    }
}

void MainWindow::lGMoveFig(const QSize &icSize, QString &str,QString &picUrl,std::string &result,int &figId,int &index, bool dir,const QChar promo)
{
    bool isenpass = false;
    QString start = "";
    if(dir)
        start = str.mid(0,2);
    else
        start = str.mid(2,2);
    QString st = "";
    if(dir)
        st = str.mid(2,2);
    else
        st = str.mid(0,2);
    for(int i=0;i<buttons.size();++i){
        if(buttons.at(i)->_boardPos==start){
            if(dir)
                result+=buttons.at(i)->_boardPos.toLocal8Bit().constData();
            picUrl = buttons.at(i)->iconUrl;
            buttons.at(i)->lGtook.push(picUrl);

            figId = buttons.at(i)->figId;
            if(figId>=WP_FIRST&&figId<=WP_LAST&&start[1]=='2'&&st[1]=='4'&&start[0]==st[0]){
                lGEnpass.append(start[0]);lGEnpass.append('3');
                for(int i=0;i<buttons.size();++i){
                    if(buttons.at(i)->_boardPos==st){
                        enpassInd = i;
                    }
                }
                lasmovePawn = figId;
                isenpass = true;
            }
            else if(figId>=BP_FIRST&&figId<=BP_LAST&&start[1]=='7'&&st[1]=='5'){
                lGEnpass.append(start[0]);lGEnpass.append('6');
                for(int i=0;i<buttons.size();++i){
                    if(buttons.at(i)->_boardPos==st){
                        enpassInd = i;
                    }
                }
                lasmovePawn = figId;
                isenpass = true;
            }
            index = i;
            buttons.at(i)->setIconUrl("");
            changeBtnIcon(*buttons.at(i),"",icSize);
            buttons.at(i)->setFigId(NO_EDGE);
            break;
        }
    }

    for(int i=0;i<buttons.size();++i){
        if(buttons.at(i)->_boardPos==st){
            if(dir)
                result+=buttons.at(i)->_boardPos.toLocal8Bit().constData();

            QString pic = "";
            if((figId==WK&&st=="g1"&&start=="e1"&&dir)||(figId==WK&&st=="e1"&&start=="g1"&&!dir)){
                for(int i=0;i<buttons.size();++i){
                    if((buttons.at(i)->figId==WR2&&buttons.at(i)->_boardPos=="h1"&&dir)||
                        (buttons.at(i)->figId==WR2&&buttons.at(i)->_boardPos=="f1"&&!dir) ){
                        pic = buttons.at(i)->iconUrl;
                        buttons.at(i)->setIconUrl("");
                        changeBtnIcon(*buttons.at(i),"",icSize);
                        buttons.at(i)->setFigId(NO_EDGE);
                        break;
                    }
                }
                for(int i=0;i<buttons.size();++i){
                    if((buttons.at(i)->_boardPos=="f1"&&dir)||(buttons.at(i)->_boardPos=="h1"&&!dir)){
                        changeBtnIcon(*buttons.at(i),pic,icSize);
                        buttons.at(i)->setFigId(WR2);
                        pic = "";
                        break;
                    }
                }
            }
            else if((figId==WK&&st=="c1"&&start=="e1"&&dir)||(figId==WK&&st=="e1"&&start=="c1"&&!dir)){
                for(int i=0;i<buttons.size();++i){
                    if((buttons.at(i)->figId==WR1&&buttons.at(i)->_boardPos=="a1"&&dir)||
                        (buttons.at(i)->figId==WR1&&buttons.at(i)->_boardPos=="d1"&&!dir)){
                        pic = buttons.at(i)->iconUrl;
                        buttons.at(i)->setIconUrl("");
                        changeBtnIcon(*buttons.at(i),"",icSize);
                        buttons.at(i)->setFigId(NO_EDGE);
                        break;
                    }
                }
                for(int i=0;i<buttons.size();++i){
                    if((buttons.at(i)->_boardPos=="d1"&&dir)||(buttons.at(i)->_boardPos=="a1"&&!dir)){
                        changeBtnIcon(*buttons.at(i),pic,icSize);
                        buttons.at(i)->setFigId(WR1);
                        pic = "";
                        break;
                    }
                }
            }
            else if((figId==BK&&st=="g8"&&start=="e8"&&dir)||(figId==BK&&st=="e8"&&start=="g8"&&!dir)){
                for(int i=0;i<buttons.size();++i){
                    if((buttons.at(i)->figId==BR2&&buttons.at(i)->_boardPos=="h8"&&dir)||
                        (buttons.at(i)->figId==BR2&&buttons.at(i)->_boardPos=="f8"&&!dir)){
                        pic = buttons.at(i)->iconUrl;
                        buttons.at(i)->setIconUrl("");
                        changeBtnIcon(*buttons.at(i),"",icSize);
                        buttons.at(i)->setFigId(NO_EDGE);
                        break;
                    }
                }
                for(int i=0;i<buttons.size();++i){
                    if((buttons.at(i)->_boardPos=="f8"&&dir)||(buttons.at(i)->_boardPos=="h8"&&!dir)){
                        changeBtnIcon(*buttons.at(i),pic,icSize);
                        buttons.at(i)->setFigId(BR2);
                        pic = "";
                        break;
                    }
                }
            }
            else if((figId==BK&&st=="c8"&&start=="e8"&&dir)||(figId==BK&&st=="e8"&&start=="c8"&&!dir)){
                for(int i=0;i<buttons.size();++i){
                    if((buttons.at(i)->figId==BR1&&buttons.at(i)->_boardPos=="a8"&&dir)||
                        (buttons.at(i)->figId==BR1&&buttons.at(i)->_boardPos=="d8"&&!dir)){
                        pic = buttons.at(i)->iconUrl;
                        buttons.at(i)->setIconUrl("");
                        changeBtnIcon(*buttons.at(i),"",icSize);
                        buttons.at(i)->setFigId(NO_EDGE);
                        break;
                    }
                }
                for(int i=0;i<buttons.size();++i){
                    if((buttons.at(i)->_boardPos=="d8"&&dir)||(buttons.at(i)->_boardPos=="a8"&&!dir)){
                        changeBtnIcon(*buttons.at(i),pic,icSize);
                        buttons.at(i)->setFigId(BR1);
                        pic = "";
                        break;
                    }
                }
            }
            if(!dir&&buttons.at(i)->lGtook.top()!=""){
                picUrl = buttons.at(i)->lGtook.top();
                buttons.at(i)->lGtook.pop();
            }
            if(promo=='q'){
                if(picUrl=="assets:/images/bP.png"){
                    picUrl = "assets:/images/bQ.png";
                    figId=BQ;
                }
                else if(picUrl=="assets:/images/BlackPawn.png"){
                    picUrl = "assets:/images/BlackQueen.png";
                    figId=BQ;
                }
                else if(picUrl=="assets:/images/wP.png"){
                    picUrl = "assets:/images/wQ.png";
                    figId=WQ;
                }
                else if(picUrl=="assets:/images/WhitePawn.png"){
                    picUrl = "assets:/images/WhiteQueen.png";
                    figId=WQ;
                }
            }
            else if(promo=='r'){
                if(picUrl=="assets:/images/bP.png"){
                    picUrl = "assets:/images/bR.png";
                    figId=BR1;
                }
                else if(picUrl=="assets:/images/BlackPawn.png"){
                    picUrl = "assets:/images/BlackRook.png";
                    figId=BR1;
                }
                else if(picUrl=="assets:/images/wP.png"){
                    picUrl = "assets:/images/wR.png";
                    figId=WR1;
                }
                else if(picUrl=="assets:/images/WhitePawn.png"){
                    picUrl = "assets:/images/WhiteRook.png";
                    figId=WR1;
                }
            }
            else if(promo=='b'){
                if(picUrl=="assets:/images/bP.png"){
                    picUrl = "assets:/images/bB.png";
                    figId=BO1;
                }
                else if(picUrl=="assets:/images/BlackPawn.png"){
                    picUrl = "assets:/images/BlackOficer.png";
                    figId=BO1;
                }
                else if(picUrl=="assets:/images/wP.png"){
                    picUrl = "assets:/images/wB.png";
                    figId=WO1;
                }
                else if(picUrl=="assets:/images/WhitePawn.png"){
                    picUrl = "assets:/images/WhiteOficer.png";
                    figId=WO1;
                }
            }
            else if(promo=='n'){
                if(picUrl=="assets:/images/bP.png"){
                    picUrl = "assets:/images/bN.png";
                    figId=BH1;
                }
                else if(picUrl=="assets:/images/BlackPawn.png"){
                    picUrl = "assets:/images/BlackHorseRight.png";
                    figId=BH1;
                }
                else if(picUrl=="assets:/images/wP.png"){
                    picUrl = "assets:/images/wN.png";
                    figId=WH1;
                }
                else if(picUrl=="assets:/images/WhitePawn.png"){
                    picUrl = "assets:/images/WhiteHorseRight.png";
                    figId=WH1;
                }
            }
            changeBtnIcon(*buttons.at(i),picUrl,icSize);
            if(st==lGEnpass&&((figId>=WP_FIRST&&figId<=WP_LAST)||(figId>=BP_FIRST&&figId<=BP_LAST))&&start[0]!=st[0]&&enpassInd>=0){
                buttons.at(enpassInd)->setIconUrl("");
                buttons.at(enpassInd)->setFigId(-1);
                changeBtnIcon(*buttons.at(enpassInd),"",icSize);
            }
            if(!isenpass){
                enpassInd = -1;
                lGEnpass = "";
                lasmovePawn = -1;
            }
            buttons.at(i)->setFigId(figId);
        }
    }
}

void MainWindow::deleteGame()
{
    bool b=connection->deleteGame(currentLoadGameName);
    if(b){
        loadedGameNames.clear();
        connection->queryDBGames(loadedGameNames);
        if(!loadedGameNames.empty()){
            currentLoadGameName = loadedGameNames.at(0);
            if((currentLoadGameName.isEmpty()) || (currentLoadGameName == "")){
                QMessageBox::information(this, "Info", tr("There are No saved games"));
                currentLoadGameName = tr("No saved games");
            }
            loadGameStatusText->setText(currentLoadGameName);
        }
        else{
            currentLoadGameName=tr("No saved games");
            QMessageBox::information(this, "Info", tr("There are No saved games"));
            loadGameStatusText->setText(currentLoadGameName);
        }
    }
    else{
        QMessageBox::information(this, "Info", tr("There are No saved games"));
    }
}

void MainWindow::exportGame(bool isGreaterThan11)
{
    exportGameString = connection->queryDBGamesByName(currentLoadGameName);
    if(!exportGameString.isEmpty()){
        QChar color = exportGameString.mid(exportGameString.indexOf('|',Qt::CaseInsensitive),2).at(1).toLatin1();
        QString path = connection->exportFile(currentLoadGameName,color,playLevel,isGreaterThan11);

        if(!path.isEmpty()){
            QMessageBox::information(this, "Info", (tr("File exported in ")+path+"!"));
            gameEndResult=currentLoadGameName.toLocal8Bit().constData();
            gameEndResultTr = QString::fromStdString(gameEndResult);
        }
        else{
            QMessageBox::information(this, "Info", connection->dbExistsInfo.toLocal8Bit().constData());
        }
        increaseLoadedGames();
    }
    else{
        QMessageBox::information(this, "Info", connection->dbExistsInfo.toLocal8Bit().constData());
    }
}

void MainWindow::changeColors()
{
    if(theme=='s')
        theme = 'k';
    else if(theme=='k')
        theme = 'z';
    else if(theme=='z')
        theme = 's';

    if(theme=='s'){
        whitesColor = "rgb(215, 223, 234)";
        blacksColor = "rgb(131, 156, 196)";
        buttonsColor = "rgb(93, 109, 135)";
        chooseColor = "rgb(79, 93, 115)";
    }
    else if(theme=='k'){
        whitesColor = "rgb(238,211,124)";
        blacksColor = "rgb(124,96,5)";
        buttonsColor = "rgb(65,51,3)";
        chooseColor = "rgb(44, 34, 2)";
    }
    else if(theme=='z'){
        whitesColor = "rgb(189,252,182)";
        blacksColor = "rgb(93,190,80)";
        buttonsColor = "rgb(14, 87, 5);";//"rgb(18,112,7)";
        chooseColor = "rgb(15, 91, 6)";
    }
    changeWidgetsGeometry();
    flipGUIBoardChessAiColor();
    centralWidget()->setStyleSheet("background-color:"+buttonsColor+";");
    gameInfo->setStyleSheet("background-color:"+buttonsColor+";");
    if(colorWidget&&colorWidget!=nullptr)
        colorWidget->setStyleSheet("background-color:"+buttonsColor+";");
    if(sideBar&&sideBar!=nullptr)
        sideBar->setStyleSheet("background-color:"+whitesColor+";");
    if(hintWidget&&hintWidget!=nullptr)
        hintWidget->setStyleSheet("background-color:"+whitesColor+";");
    if(resignWidget&&resignWidget!=nullptr)
        resignWidget->setStyleSheet("background-color:"+whitesColor+";");
    if(newGameWidget&&newGameWidget!=nullptr)
        newGameWidget->setStyleSheet("background-color:"+whitesColor+";");
    if(pawnPromotion&&pawnPromotion!=nullptr)
        pawnPromotion->setStyleSheet("background-color:"+whitesColor+";");
    if(saveGameWidget&&saveGameWidget!=nullptr)
        saveGameWidget->setStyleSheet("background-color:"+whitesColor+";");
}

void MainWindow::rotateBoard()
{
    bool b = false;
    if(chessAiColor == ' ' || chessAiColor == 'b'){
        chessAiColor = 'w';
        emitSignalFlag = true;
        b = true;
    }
    if((!b) && chessAiColor == 'w'){
        chessAiColor = 'b';
        emitSignalFlag = false;
    }
    flipGUIBoardChessAiColor();
}

void MainWindow::gdprAgreed()
{
    if(gdprWidget&&gdprWidget!=nullptr){
        gdprWidget->hide();
    }
    if(isBanner){
        banner->setUnitId("", true);
        //banner->setUnitId("");
        banner->setSize(IQtAdMobBanner::AdaptiveBanner);//SmartBanner
        double levelGeomHeight = mobWidth*1.277777;
        double bannerOffset = ((checkHeight - mobWidth) <= 561 ? 0.07 : 0.08);
        bannerHAvNotRes = static_cast<int>(((levelGeomHeight+1)+(mobWidth * bannerOffset)));
        //QMessageBox::information(this, "Info", ("OFFSET CASE!"+QString::number((checkHeight - mobWidth))));
        if((checkHeight - mobWidth) >= 1045) {
           // QMessageBox::information(this, "Info", "OFFSET CASE!");
            double add = static_cast<double>(checkHeight * 0.035);
            bannerHAvNotRes += static_cast<int>(add);
        }
        QPoint bannerPos(0,bannerHAvNotRes);
        banner->setPosition(bannerPos);
    }
    if(banner&&banner!=nullptr){
        if(!bannerShut&&banner->isLoaded())
            banner->setVisible(true);
    }
    connection->updateGDPR();
}

void MainWindow::gdprPaidClicked()
{
    if(isUgrdButton == false){
        QMessageBox::information(this, "Info", tr("Sorry, but the paid version is not supported in your country or for your AndroidOS version!"));
        return;
    }
    QString link = "market://details?id=com.deepchess.deepchess";
    QDesktopServices::openUrl(QUrl(link));
}

void MainWindow::moveBackGui()
{
    if((isAnalyzisMode)||(chessAiColor==' ')){
        gameEndResult = "You can not Undo in this Mode!";
        gameEndResultTr = tr("You can not Undo in this Mode!");
        setGameInfoVis(gameInfo,gameText);
        return;
    }
    undoButtonClicked = true;
    if(isCompsTurn){
        gameEndResult = "Please wait its not Your turn!";
        gameEndResultTr = tr("Please wait its not Your turn!");
        setGameInfoVis(gameInfo,gameText);
    }
    else{

        if(backMoveData.empty()){
            gameEndResult = "No moves to undo!";
            gameEndResultTr = tr("No moves to undo!");
            setGameInfoVis(gameInfo,gameText);
            return;
        }

        if(backMoveData.size()<=2&&chessAiColor == 'w'){
            gameEndResult = "No moves to undo!";
            gameEndResultTr = tr("No moves to undo!");
            setGameInfoVis(gameInfo,gameText);
            return;
        }
        if(backMoveData.size()<=1&&chessAiColor == 'b'){
            return;
        }
        startBoard->moveBack();
        QSize icSize(static_cast<int>(icsize), static_cast<int>(icsize));
        std::vector<BackMove> backMove = backMoveData.top();

        resetAvMoves();
        for(int i=0;i<buttons.size();++i){
            buttons[i]->setFigId(backMove[i].figId);
            buttons[i]->setIconUrl(backMove[i].iconUrl);
            buttons[i]->setBorderColor(backMove[i].borderColor);
            buttons[i]->setBoardPos(backMove[i]._boardPos);
            buttons[i]->setIcon(QIcon(backMove[i].iconUrl));
            buttons[i]->setIconSize(icSize);
            buttons[i]->repaint();
            uciMoveCmd = backMove[i].uciMoveCmd;
            uciMoveCmdHint = backMove[i].uciMoveCmdHint;
            PGNString = backMove[i].PGNString;
            PGNMoveNum = backMove[i].PGNMoveNum;
        }
        widget->repaint();
        backMoveData.pop();

        if((chessAiColor=='b'&&startBoard->getBlackPlayer().isTurn())||(chessAiColor=='w'&&startBoard->getWhitePlayer().isTurn()))
            undoTurnChanged = true;
        else{
            undoTurnChanged = false;
        }
        if(startBoard->getWhitePlayer().checked()){
            gameEndResult = "White is Check!";
            gameEndResultTr = tr("White is Check!");
            setGameInfoVis(gameInfo,gameText);
        }
        else if(startBoard->getBlackPlayer().checked()){
            gameEndResult = "Black is Check!";
            gameEndResultTr = tr("Black is Check!");
            setGameInfoVis(gameInfo,gameText);
        }
        else{
            gameEndResult = "";
            gameEndResultTr = "";
            setGameInfoVis(gameInfo,gameText);
        }

        undoMoveClicked = true;
        if(backMoveData.size()<=1&&chessAiColor == 'b'){
            gameEndResult = "No moves to undo!";
            gameEndResultTr = tr("No moves to undo!");
            setGameInfoVis(gameInfo,gameText);
        }
    }

}
void MainWindow::RequestPermission(const QString permission, const QString purpose)
{
    const QString PermissionID(permission);

    if(QtAndroid::checkPermission(PermissionID) != QtAndroid::PermissionResult::Granted)
    {
        if(permission=="android.permission.READ_EXTERNAL_STORAGE")
            readPermAsk = true;
        else if(permission=="android.permission.WRITE_EXTERNAL_STORAGE")
            writePermAsk = true;
        if(QtAndroid::shouldShowRequestPermissionRationale(PermissionID))
        {
            if(permission=="android.permission.READ_EXTERNAL_STORAGE")
                QMessageBox::information(this, "Info", tr("Please, this permission is requested for reading Downloads folder"));
            else if(permission=="android.permission.WRITE_EXTERNAL_STORAGE")
                QMessageBox::information(this, "Info", tr("Please, this permission is requested for writing in Downloads folder"));
        }
        QtAndroid::requestPermissions(QStringList() << PermissionID, std::bind(&MainWindow::RequestPermissionsResults, this, std::placeholders::_1));
        return;
    }

    if(permission=="android.permission.READ_EXTERNAL_STORAGE"&&purpose=="OpenBook") {
        openBookFileDialog();
    }
    else if(permission=="android.permission.WRITE_EXTERNAL_STORAGE"&&purpose=="ExportPGN") {
        exportGame(false);
    }
}

void MainWindow::RequestPermissionsResults(const QtAndroid::PermissionResultMap &ResultMap)
{
    if(ResultMap["android.permission.READ_EXTERNAL_STORAGE"] == QtAndroid::PermissionResult::Granted && readPermAsk) {
        QMessageBox::information(this, "Info", tr("Read Permission granted!"));
        openBookFileDialog();
    }
    else if(ResultMap["android.permission.WRITE_EXTERNAL_STORAGE"] == QtAndroid::PermissionResult::Granted && writePermAsk) {
        QMessageBox::information(this, "Info", tr("Write Permission granted!"));
        exportGame(false);
    }
    else{
        QMessageBox::warning(this, "Error", tr("Permission denied!"));
        gameEndResult = "Permission denied!";
        gameEndResultTr = tr("Permission denied!");
        setGameInfoVis(gameInfo,gameText);
    }
}

void MainWindow::addToPGN()
{
    if(PGNMoveNum == 0||addPGNNum){
        ++PGNMoveNum;
        PGNString += " "; PGNString += QString::number(PGNMoveNum); PGNString += ". ";
        addPGNNum = false;
    }
    else {
        PGNString += " ";
        addPGNNum = true;
    }
    if((startBoard->castle=='o')||(startBoard->castle=='O')){
        if(startBoard->castle=='o')
            PGNString += "O-O";
        else if(startBoard->castle=='O')
            PGNString += "O-O-O";
    }
    else{
        if(PGNLastActiveFig==WR1||PGNLastActiveFig==WR2||PGNLastActiveFig==BR1||PGNLastActiveFig==BR2||
            ((PGNLastActiveFig % 100)==BR1)||((PGNLastActiveFig % 100)==WR1)){
            PGNString += "R";
        }
        else if(PGNLastActiveFig==WH1||PGNLastActiveFig==WH2||PGNLastActiveFig==BH1||PGNLastActiveFig==BH2||
                 ((PGNLastActiveFig % 100)==BH1)||((PGNLastActiveFig % 100)==WH1)){
            PGNString += "N";
        }
        else if(PGNLastActiveFig==WO1||PGNLastActiveFig==WO2||PGNLastActiveFig==BO1||PGNLastActiveFig==BO2||
                 ((PGNLastActiveFig % 100)==BO1)||((PGNLastActiveFig % 100)==WO1)){
            PGNString += "B";
        }
        else if(PGNLastActiveFig==WK||PGNLastActiveFig==BK){
            PGNString += "K";
        }
        else if(PGNLastActiveFig==WQ||PGNLastActiveFig==BQ||((PGNLastActiveFig % 100)==BQ)||((PGNLastActiveFig % 100)==WQ)){
            PGNString += "Q";
        }
        PGNString += char(tolower(chooseCol(buttons.at(PGNLastActiveFigId)->col)));
        PGNString += QString::number((BOARD_SIZE-1-buttons.at(PGNLastActiveFigId)->row)+1);

        if((PGNFigID != NO_EDGE) || (enpassantId != NO_EDGE)){
            PGNString += "x";
            PGNFigID = NO_EDGE;
        }

        PGNString += char(tolower(chooseCol(buttons.at(PGNMoveInd)->col)));
        PGNString += QString::number((BOARD_SIZE-1-buttons.at(PGNMoveInd)->row)+1);
        if(PGNPromoSymb != ' '&&((PGNLastActiveFig>=WP_FIRST&&PGNLastActiveFig<=WP_LAST)||(PGNLastActiveFig>=BP_FIRST&&PGNLastActiveFig<=BP_LAST))){
            PGNString += "=" ; PGNString += PGNPromoSymb;
            PGNPromoSymb = ' ';
        }
        if(startBoard->getWhitePlayer().stalemate()||startBoard->getBlackPlayer().stalemate()){
            PGNString += " 1/2-1/2";
            return;
        }
        else if(startBoard->getWhitePlayer().mate()||startBoard->getBlackPlayer().mate()){
            PGNString += "#";
            if(startBoard->getWhitePlayer().mate())
                PGNString += " 0-1";
            else if(startBoard->getBlackPlayer().mate())
                PGNString += " 1-0";
            return;
        }
        if(startBoard->getWhitePlayer().checked()||startBoard->getBlackPlayer().checked()){
            if((startBoard->getWhitePlayer().checked()||startBoard->getBlackPlayer().checked()))
                PGNString += "+";
        }
    }
    //gameEndResult = PGNString.toLocal8Bit().constData();
    //setGameInfoVis(gameInfo,gameText);
}

void MainWindow::openBookFileDialog(){
    if(QtAndroid::androidSdkVersion() == 30){//Android 11
        QMessageBox::information(this, "Info", tr("Please tap on 'Downloads' folder and write the full name of the book, which you have downloaded. For example: bookname.bin in the 'File name:' field and tap 'Open'"));
    }
    QFileDialog dialog;
    QList<QUrl> urls;
    if(QtAndroid::androidSdkVersion() <= 30) {
        urls << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::StandardLocation::DownloadLocation));
        urls << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::StandardLocation::DocumentsLocation));
    }

    urls << QUrl::fromLocalFile(QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).first());
    urls << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::StandardLocation::HomeLocation));
    urls << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::StandardLocation::DesktopLocation));
    urls << QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::StandardLocation::GenericDataLocation));
    dialog.setNameFilter("Polyglot Books (*.bin)");
    dialog.setViewMode(QFileDialog::Detail);
    dialog.setOption(QFileDialog::DontUseNativeDialog, true);
    dialog.setSidebarUrls(urls);
    QStringList fileNames;
    QUrl path;
    if(dialog.exec()){
        fileNames = dialog.selectedFiles();
        path = dialog.directoryUrl();
    }
    else{
        if(Search::bookSettings.path=="<empty>"){
            QMessageBox::information(this, "Info", tr("Book not found!"));
            addBookLabel->setText(tr("No Polyglot book added"));
        }
        return;
    }
    std::string s = fileNames[0].toLocal8Bit().constData();//gameEndResult;

    if(s.find(".bin")!=std::string::npos){
        Search::bookSettings.path = s;//"assets:/openingbooks/ProDeo.bin";
        Search::bookSettings.ownBook = true;
        s = s.substr(s.rfind("/")+1, s.size()-(s.rfind("/")+1)-s.rfind("."));
        s.replace(s.rfind("."),s.size(),"");
        QString str = "";
        try {
            str = QString::fromStdString(s);
        }
        catch (...) {
            str = "Book Added";
        }

        addBookLabel->setText(str);
        polybook.init(Search::bookSettings.path);
        connection->updateBooks(QString::fromStdString(Search::bookSettings.path));
        QMessageBox::information(this, "Info", tr("Book Added!"));
    }
    else{
        QMessageBox::information(this, "Info", tr("Book not found!"));
        addBookLabel->setText(tr("No Polyglot book added"));
    }
}

void MainWindow::filesButtonClicked()
{
    if(!bannerShut && banner && banner!=nullptr){
        QPoint bannerPos(0,bannerHNotAv);
        banner->setPosition(bannerPos);
        banner->setVisible(false);
    }

    if(!filesWidget)
        filesWidget = new QWidget(this);

    if(isResized) {
        filesWidget->setGeometry(0,0,gameInfoWidth,checkHeight);
        filesWidget->setStyleSheet(
            ".QWidget { "
            " background: " + resizedWood + ";background-size:cover;"
            "}");
    }
    else {
        filesWidget->setGeometry(0,0,mobWidth,checkHeight);
        filesWidget->setStyleSheet(
            ".QWidget { "
            " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
            "}");
    }
    filesWidget->adjustSize();

    if(!addBookLabel)
        addBookLabel = new QLabel(filesWidget);

    QString book = connection->checkBooks();
    if(book!="empty"){
        Search::bookSettings.ownBook=true;
        Search::bookSettings.path = book.toLocal8Bit().constData();
        std::string s = Search::bookSettings.path;
        s = s.substr(s.rfind("/")+1, s.size()-(s.rfind("/")+1)-s.rfind("."));
        s.replace(s.rfind("."),s.size(),"");
        QString str = "";
        try {
            str = QString::fromStdString(s);
        }
        catch (...) {
            str = "Book Added";
        }

        addBookLabel->setText(str);
    }
    else{
        addBookLabel->setText(tr("No Polyglot book added"));
        Search::bookSettings.ownBook=false;
        Search::bookSettings.path = "<empty>";
    }
    addBookLabel->setAlignment(Qt::AlignCenter);
    titleFont = mobWidth*0.0494444;//0.0694444
    addBookLabel->setStyleSheet("background:transparent;font-size:"+QString::number(static_cast<int>(titleFont)+5)+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");

    if(!addBookButton) {
        addBookButton = new QPushButton(tr("Add Book"),filesWidget);
        connect(addBookButton,&QPushButton::clicked,this,&MainWindow::addBookClicked);
    }

    addBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                 +QString::number(static_cast<int>(titleFont))+"px;");
    double geomWidth = (mobWidth-20) / 2;
    double geomButtonHeight = mobWidth*0.1388888;
    addBookButton->setMinimumWidth(static_cast<int>(geomWidth));
    addBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    addBookButton->adjustSize();

    if(!removeBookButton) {
        removeBookButton = new QPushButton(tr("Remove Book"),filesWidget);
        connect(removeBookButton,&QPushButton::clicked,this,&MainWindow::removeBookClicked);
    }

    removeBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    removeBookButton->setMinimumWidth(static_cast<int>(geomWidth));
    removeBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    removeBookButton->adjustSize();

    if(!closeFilesButton) {
        closeFilesButton = new QPushButton(tr("Close"),filesWidget);
        connect(closeFilesButton,&QPushButton::clicked,this,&MainWindow::filesClosedClicked);
    }

    closeFilesButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                    +QString::number(static_cast<int>(titleFont))+"px;");
    closeFilesButton->setMinimumWidth(static_cast<int>(geomWidth));
    closeFilesButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    closeFilesButton->adjustSize();

    //    importGameButton = new QPushButton("Files",filesWidget);
    //    importGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
    //    +QString::number(static_cast<int>(titleFont))+"px;");
    //      importGameButton->setMinimumWidth(static_cast<int>(geomWidth));
    //    importGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
    //    importGameButton->adjustSize();
    //    connect(importGameButton,&QPushButton::clicked,this,&MainWindow::importPGNClicked);

    if(!filesLayout_1) {
        filesLayout_1 = new QVBoxLayout(filesWidget);
        filesLayout_1->addWidget(addBookLabel,0,Qt::AlignCenter);
        filesLayout_1->addWidget(addBookButton,1,Qt::AlignCenter);
        filesLayout_1->addWidget(removeBookButton,1,Qt::AlignCenter);
        filesLayout_1->addWidget(closeFilesButton,1,Qt::AlignCenter);
    }

    if(isResized) {
        filesWidget->resize(gameInfoWidth,checkHeight);

        addBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                     +QString::number(static_cast<int>(titleFont))+"px;");
        geomWidth = (gameInfoWidth-40) / 2.0;
        geomButtonHeight = (gameInfoWidth*0.1388888)/3.0;
        addBookButton->setMinimumWidth(static_cast<int>(geomWidth));
        addBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        addBookButton->adjustSize();
        removeBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
        removeBookButton->setMinimumWidth(static_cast<int>(geomWidth));
        removeBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        removeBookButton->adjustSize();
        //            importGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
        //                                                +QString::number(static_cast<int>(titleFont))+"px;");
        //            importGameButton->setMinimumWidth(static_cast<int>(geomWidth));
        //            importGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        //            importGameButton->adjustSize();
        closeFilesButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
        closeFilesButton->setMinimumWidth(static_cast<int>(geomWidth));
        closeFilesButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        closeFilesButton->adjustSize();
    } else {
        titleFont = mobWidth*0.0494444;//0.0694444
        filesWidget->setGeometry(0,0,mobWidth,checkHeight);
        filesWidget->resize(mobWidth,checkHeight);
        filesWidget->setStyleSheet(
            ".QWidget { "
            " background: url(\"assets:/images/Wood.jpg\");background-size:cover;"
            "}");
        addBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                     +QString::number(static_cast<int>(titleFont))+"px;");
        geomWidth = (mobWidth-40) / 2.0;
        geomButtonHeight = mobWidth*0.1388888;
        addBookButton->setMinimumWidth(static_cast<int>(geomWidth));
        addBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        addBookButton->adjustSize();

        removeBookButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
        removeBookButton->setMinimumWidth(static_cast<int>(geomWidth));
        removeBookButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        removeBookButton->adjustSize();
        //            importGameButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
        //                                                +QString::number(static_cast<int>(titleFont))+"px;");
        //            importGameButton->setMinimumWidth(static_cast<int>(geomWidth));
        //            importGameButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        //            importGameButton->adjustSize();
        closeFilesButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                        +QString::number(static_cast<int>(titleFont))+"px;");
        closeFilesButton->setMinimumWidth(static_cast<int>(geomWidth));
        closeFilesButton->setMinimumHeight(static_cast<int>(geomButtonHeight));
        closeFilesButton->adjustSize();
    }
    filesWidget->repaint();

    if(settingsWidget&&settingsWidget!=nullptr)
        settingsWidget->hide();
    filesWidget->show();
}

void MainWindow::addBookClicked()
{
    if(QtAndroid::androidSdkVersion() >= 23 && QtAndroid::androidSdkVersion() <= 30 )
        RequestPermission("android.permission.READ_EXTERNAL_STORAGE", "OpenBook");
    else if(QtAndroid::androidSdkVersion() > 30)
        openBookFileDialog();
}

void MainWindow::removeBookClicked()
{
    Search::bookSettings.path = "<empty>";
    Search::bookSettings.ownBook = false;
    addBookLabel->setText(tr("No Polyglot book added"));
    polybook.init(Search::bookSettings.path);
    connection->updateBooks("empty");
    QMessageBox::information(this, "Info", tr("Book removed"));
}

void MainWindow::filesClosedClicked()
{
    if(settingsWidget&&settingsWidget!=nullptr)
        settingsWidget->show();
    if(filesWidget&&filesWidget!=nullptr){
        filesWidget->hide();
    }
}
void MainWindow::exportGameClicked()
{
    if(QtAndroid::androidSdkVersion() >= 23 && QtAndroid::androidSdkVersion() <= 30 )
        RequestPermission("android.permission.WRITE_EXTERNAL_STORAGE", "ExportPGN");
    else if(QtAndroid::androidSdkVersion() > 30)
        exportGame(true);
}

void MainWindow::loadGameCancelClicked()
{
    isLoadGame = false;
    loadGameButton->show();
    filesButton->show();
    titleInfoText->setAlignment(Qt::AlignLeft);
    removeAdsButton->show();
    levelInfoWidget->show();
    loadGameInfoWidget->hide();
    chooseStrengthWidget->show();
    starAnalysisWidget->show();
    colorInfoWidget->show();
    chooseColorWidget->show();
    loadGameWidget->hide();
    okOrCancelWidget->show();
    okOrCancelLoadGameWidget->hide();
    playAgainstComputer->show();
    prevMoveButton->hide();
    nextMoveButton->hide();
    if(hintWidget&&hintWidget!=nullptr)
        hintWidget->hide();
    deleteGameWidget->hide();
}

void MainWindow::flipGUIBoardChessAiColor() {
    isBoardFlipped.store(true);
    int whitePawnsCnt = WP_LAST;
    int blackPawnsCnt = BP_LAST;
    int wPosCnt = 0;
    int bPosCnt = 0;
    int row = 0;
    int col = 0;
    int rowAdd = 0;
    int colAdd = 0;
    int emptyCntRow = 0;
    int emptyCntCol = 0;

    QSize icSize(static_cast<int>(icsize), static_cast<int>(icsize));
    QString borderColor = "border:1px solid black;";
    int btnIndexCnt = NO_EDGE;
    QString coordUp; QString coordDown;

    if(chessAiColor=='w'||chessAiColor=='b'||chessAiColor==' '){
        clearAllButtons();
    }
    buttons.reserve(66);
    for(int i=BOARD_SIZE-1;i>=0;i--){
        for(int j=BOARD_SIZE-1;j>=0;j--){
            row = i;
            col = j;
            if(chessAiColor=='w'){
                rowAdd = (BOARD_SIZE-1)-row;
                colAdd = (BOARD_SIZE-1)-col;
            }
            else{
                rowAdd = row;
                colAdd = col;
            }
            if(row==0){
                if(col==0){
                    borderColor = "border:1px solid "+whitesColor+";";
                    QString pos = "a8";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "A";

                    }
                    else{
                        coordUp = "8";
                        coordDown = "";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, BR1, row, col, 'l', borderColor,fieldSize,pos,coordUp,coordDown));//Rook A-8
                    setBtnMode(*buttons.back(), false, (*buttons.back()).color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/bR.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(),"assets:/images/BlackRook.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==1){
                    borderColor = "border:1px solid "+blacksColor+";";
                    QString pos = "b8";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "B";

                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, BH1, row, col, 'd', borderColor,fieldSize,pos,coordUp,coordDown));//Knight B-8
                    setBtnMode(*buttons.back(), false, (*buttons.back()).color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/bN.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/BlackHorseLeft.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==2){
                    borderColor = "border:1px solid "+whitesColor+";";
                    QString pos = "c8";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "C";

                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, BO1, row, col, 'l', borderColor,fieldSize,pos,coordUp,coordDown));//Bishop C-8
                    setBtnMode(*buttons.back(), false, (*buttons.back()).color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/bB.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/BlackOficer.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==3){
                    borderColor = "border:1px solid "+blacksColor+";";
                    QString pos = "d8";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "D";

                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, BQ, row, col, 'd', borderColor,fieldSize,pos,coordUp,coordDown));//Queen D-8
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/bQ.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/BlackQueen.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==4){
                    borderColor = "border:1px solid "+whitesColor+";";
                    QString pos = "e8";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "E";

                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, BK, row, col, 'l', borderColor,fieldSize,pos,coordUp,coordDown));//King E-8
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/bK.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/BlackKing.png", icSize);
                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==5){
                    borderColor = "border:1px solid "+blacksColor+";";
                    QString pos = "f8";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "F";

                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, BO2, row, col, 'd', borderColor,fieldSize,pos,coordUp,coordDown));//Bishop F-8
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/bB.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/BlackOficer.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==6){
                    borderColor = "border:1px solid "+whitesColor+";";
                    QString pos = "g8";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "G";

                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, BH2, row, col, 'l', borderColor,fieldSize,pos,coordUp,coordDown));//Knight G-8
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/bN.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/BlackHorseRight.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==7){
                    borderColor = "border:1px solid "+blacksColor+";";
                    QString pos = "h8";
                    if(chessAiColor == 'w'){
                        coordUp = "8";
                        coordDown = "H";

                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, BR2, row, col, 'd', borderColor,fieldSize,pos,coordUp,coordDown));//Rook H-8
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/bR.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/BlackRook.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
            }
            else if(row==1){
                QChar color = ' ';
                if(col%2!=0){
                    color = 'l';
                    borderColor = "border:1px solid "+whitesColor+";";
                }
                else{
                    color = 'd';
                    borderColor = "border:1px solid "+blacksColor+";";
                }
                QString pos = "7";
                QChar c = 'a'+(BOARD_SIZE-bPosCnt-1);
                pos.prepend(c);
                if(chessAiColor == 'w'){
                    if(pos == "h7"){
                        coordUp = "7";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }

                }
                else{
                    if(pos == "a7"){
                        coordUp = "7";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                }
                buttons.push_back(new ChessFieldButton(nullptr, blackPawnsCnt, row, col, color, borderColor,fieldSize,pos,coordUp,coordDown));//Black Pawns 1-8
                setBtnMode(*buttons.back(), false, buttons.back()->color);
                if(newFiguresSelected)
                    changeBtnIcon(*buttons.back(),"assets:/images/bP.png", icSize);
                else
                    changeBtnIcon(*buttons.back(), "assets:/images/BlackPawn.png", icSize);

                ++btnIndexCnt;
                connect(buttons.back(), &QPushButton::clicked,
                        this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                grid->addWidget(buttons.back(), rowAdd, colAdd);
                blackPawnsCnt--;
                bPosCnt++;
            }
            else if(row==6){
                QChar color = ' ';
                if(col%2==0){
                    color = 'l';
                    borderColor = "border:1px solid "+whitesColor+";";
                }
                else{
                    color = 'd';
                    borderColor = "border:1px solid "+blacksColor+";";
                }
                QString pos = "2";
                QChar c = 'a'+(BOARD_SIZE-wPosCnt-1);
                pos.prepend(c);
                if(chessAiColor == 'w'){
                    if(pos == "h2"){
                        coordUp = "2";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                }
                else{
                    if(pos == "a2"){
                        coordUp = "2";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                }
                buttons.push_back(new ChessFieldButton(nullptr, whitePawnsCnt, row, col, color, borderColor,fieldSize,pos,coordUp,coordDown));//White Pawns 1-8
                setBtnMode(*buttons.back(), false, buttons.back()->color);
                if(newFiguresSelected)
                    changeBtnIcon(*buttons.back(),"assets:/images/wP.png", icSize);
                else
                    changeBtnIcon(*buttons.back(), "assets:/images/WhitePawn.png", icSize);

                ++btnIndexCnt;
                connect(buttons.back(), &QPushButton::clicked,
                        this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                grid->addWidget(buttons.back(), rowAdd, colAdd);
                whitePawnsCnt--;
                wPosCnt++;
            }
            else if(row==7){
                if(col==0){
                    borderColor = "border:1px solid "+blacksColor+";";
                    QString pos = "a1";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "";

                    }
                    else{
                        coordUp = "1";
                        coordDown = "A";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, WR1, row, col, 'd', borderColor,fieldSize,pos,coordUp,coordDown));//Rook A-1
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/wR.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/WhiteRook.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==1){
                    borderColor = "border:1px solid "+whitesColor+";";
                    QString pos = "b1";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "";

                    }
                    else{
                        coordUp = "";
                        coordDown = "B";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, WH1, row, col, 'l', borderColor,fieldSize,pos,coordUp,coordDown));//Knight B-1
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/wN.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/WhiteHorseLeft.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==2){
                    borderColor = "border:1px solid "+blacksColor+";";
                    QString pos = "c1";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "";

                    }
                    else{
                        coordUp = "";
                        coordDown = "C";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, WO1, row, col, 'd', borderColor,fieldSize,pos,coordUp,coordDown));//Bishop C-1
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/wB.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/WhiteOficer.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==3){
                    borderColor = "border:1px solid "+whitesColor+";";
                    QString pos = "d1";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "D";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, WQ, row, col, 'l', borderColor,fieldSize,pos,coordUp,coordDown));//Queen D-1
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/wQ.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/WhiteQueen.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==4){
                    borderColor = "border:1px solid "+blacksColor+";";
                    QString pos = "e1";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "E";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, WK, row, col, 'd', borderColor,fieldSize,pos,coordUp,coordDown));//King E-1
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/wK.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/WhiteKing.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==5){
                    borderColor = "border:1px solid "+whitesColor+";";
                    QString pos = "f1";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "F";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, WO2, row, col, 'l', borderColor,fieldSize,pos,coordUp,coordDown));//Bishop F-1
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/wB.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/WhiteOficer.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==6){
                    borderColor = "border:1px solid "+blacksColor+";";
                    QString pos = "g1";
                    if(chessAiColor == 'w'){
                        coordUp = "";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "G";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, WH2, row, col, 'd', borderColor,fieldSize,pos,coordUp,coordDown));//Knight G-1
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/wN.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/WhiteHorseRight.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
                else if(col==7){
                    borderColor = "border:1px solid "+whitesColor+";";
                    QString pos = "h1";
                    if(chessAiColor == 'w'){
                        coordUp = "1";
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "H";
                    }
                    buttons.push_back(new ChessFieldButton(nullptr, WR2, row, col, 'l', borderColor,fieldSize,pos,coordUp,coordDown));//Rook H-1
                    setBtnMode(*buttons.back(), false, buttons.back()->color);
                    if(newFiguresSelected)
                        changeBtnIcon(*buttons.back(),"assets:/images/wR.png", icSize);
                    else
                        changeBtnIcon(*buttons.back(), "assets:/images/WhiteRook.png", icSize);

                    ++btnIndexCnt;
                    connect(buttons.back(), &QPushButton::clicked,
                            this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                    grid->addWidget(buttons.back(), rowAdd, colAdd);
                }
            }
            else{
                QChar color = ' ';
                if(row%2==0){
                    if(col%2==0){
                        color = 'l';
                        borderColor = "border:1px solid "+whitesColor+";";
                    }
                    else{
                        color = 'd';
                        borderColor = "border:1px solid "+blacksColor+";";
                    }
                }
                else{
                    if(col%2!=0){
                        color = 'l';
                        borderColor = "border:1px solid "+whitesColor+";";
                    }
                    else{
                        color = 'd';
                        borderColor = "border:1px solid "+blacksColor+";";
                    }
                }
                QString pos = "";
                QChar r = '0'+(BOARD_SIZE-row);
                QChar c = 'a'+col;
                pos.prepend(c);pos.append(r);
                if(chessAiColor == 'w'){
                    if(col==7){
                        coordUp = QString::number((BOARD_SIZE-row));
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }

                }
                else{
                    if(col==0){
                        coordUp = QString::number((BOARD_SIZE-row));
                        coordDown = "";
                    }
                    else{
                        coordUp = "";
                        coordDown = "";
                    }
                }
                buttons.push_back(new ChessFieldButton(nullptr, NO_EDGE, row, col, color, borderColor,fieldSize,pos,coordUp,coordDown));
                setBtnMode(*buttons.back(), false, buttons.back()->color);
                ++btnIndexCnt;
                connect(buttons.back(), &QPushButton::clicked,
                        this, [this,btnIndexCnt](){ findAvailableMoves(btnIndexCnt); });
                grid->addWidget(buttons.back(), rowAdd, colAdd);
                emptyCntCol++;
                if(emptyCntCol==9)
                    emptyCntCol=0;
            }
        }
        emptyCntRow++;
    }
    widget->adjustSize();
    isBoardFlipped.store(false);
}
//PUZZLES
void MainWindow::initPuzzle()
{
    if(puzzleCnt >= HINT_PUZZLE_ACTIVE_COUNT){
        isPuzzleActivated = false;
        isPuzzleAct = false;
        gameEndResult="Please use Upgrade to unlock more puzzles";
        gameEndResultTr = tr("Please use Upgrade to unlock more puzzles");
        setGameInfoVis(gameInfo,gameText);
        connection->disablePuzzle(true);
        return;
    }
    nextMoveButton->setDisabled(true);
    resetAvMoves();

    if(connection->loadPuzzle(puzzlePos,puzzleMov)){//TESTPUZZ
        gameEndResult = puzzleMov[0].toLocal8Bit().constData();
        gameEndResult.append(" ");gameEndResult.append(puzzleMov[1].toLocal8Bit().constData());
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
    }
    else{
        gameEndResult = connection->dbExistsInfo.toLocal8Bit().constData();
        gameEndResultTr = QString::fromStdString(gameEndResult);
        setGameInfoVis(gameInfo,gameText);
    }
    puzzleColor = puzzlePos[0];
    puzzlePos.remove(0,1);
    if(puzzleColor=='w') {
        gameEndResult = "White to move! 4 moves";
        gameEndResultTr = tr("White to move! 4 moves");
    }
    else {
        gameEndResult = "Black to move! 4 moves";
        gameEndResultTr = tr("Black to move! 4 moves");
    }
    setGameInfoVis(gameInfo,gameText);
    setPuzzleBoard(puzzlePos);
}

void MainWindow::puzzleClicked()
{
    if(!isPuzzleActivated){
        QMessageBox::information(this, "Info", tr("Please use Upgrade button to unlock this feature!"));
    }
    else{
        puzzleMode = true;
        settingsWidget->hide();
        prevMoveButton->hide();
        nextMoveButton->setDisabled(true);
        if(colorWidget&&colorWidget!=nullptr){
            colorWidget->hide();
        }
        if(hintWidget&&hintWidget!=nullptr){
            undoMoveButton->hide();
            hintWidgetLayout->removeWidget(undoMoveButton);
            hintWidget->show();
            hintButton->setDisabled(false);
            hintWidgetLayout->removeWidget(hintButton);
            hintWidgetLayout->addWidget(hintButton,Qt::AlignHCenter);
        }
        initPuzzle();
    }
}

void MainWindow::setPuzzleBoard(const QString &pos)
{
    QSize icSize(static_cast<int>(icsize), static_cast<int>(icsize));
    QStringList setPos;
    setPos = pos.split("/",QString::SplitBehavior::SkipEmptyParts);
    for(int i=0;i<setPos.size();++i){
        for(int j=0;j<setPos[i].size();++j){
            if(setPos[i][j]=='*'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        changeBtnIcon(*(buttons[k]),"", icSize);
                        buttons[k]->setFigId(NO_EDGE);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='P'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/wP.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/WhitePawn.png", icSize);
                        buttons[k]->setFigId(WP_LAST);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='p'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/bP.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/BlackPawn.png", icSize);
                        buttons[k]->setFigId(BP_LAST);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='R'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/wR.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/WhiteRook.png", icSize);
                        buttons[k]->setFigId(WR1);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='r'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/bR.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/BlackRook.png", icSize);
                        buttons[k]->setFigId(BR1);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='N'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/wN.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/WhiteHorseRight.png", icSize);
                        buttons[k]->setFigId(WH1);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='n'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/bN.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/BlackHorseRight.png", icSize);
                        buttons[k]->setFigId(BH1);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='B'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/wB.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/WhiteOficer.png", icSize);
                        buttons[k]->setFigId(WO1);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='b'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/bB.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/BlackOficer.png", icSize);
                        buttons[k]->setFigId(BO1);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='K'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/wK.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/WhiteKing.png", icSize);
                        buttons[k]->setFigId(WK);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='k'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/bK.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/BlackKing.png", icSize);
                        buttons[k]->setFigId(BK);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='Q'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/wQ.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/WhiteQueen.png", icSize);
                        buttons[k]->setFigId(WQ);
                        break;
                    }
                }
            }
            else if(setPos[i][j]=='q'){
                for(int k=0;k<buttons.size();++k){
                    if(buttons[k]->row==i&&buttons[k]->col==j){
                        if(newFiguresSelected)
                            changeBtnIcon(*(buttons[k]),"assets:/images/bQ.png", icSize);
                        else
                            changeBtnIcon(*(buttons[k]), "assets:/images/BlackQueen.png", icSize);
                        buttons[k]->setFigId(BQ);
                        break;
                    }
                }
            }
        }
    }
    widget->repaint();
}

void MainWindow::puzzleBtnClick(int index)
{
    gameEndResult = "";
    gameEndResultTr = "";
    setGameInfoVis(gameInfo,gameText);
    if(movesInd>puzzleMov.size()-1&&(puzzleMov.size()-1)>-1){
        gameEndResult = "Correct!Puzzle Solved!";
        gameEndResultTr = tr("Correct!Puzzle Solved!");
        setGameInfoVis(gameInfo,gameText);
        connection->updatePuzzleStatus(puzzleCnt);
        if(puzzleCnt >= HINT_PUZZLE_ACTIVE_COUNT)
            connection->disablePuzzle(true);

        nextMoveButton->setDisabled(false);
        movesInd = 0;
        return;
    }
    QSize icSize(static_cast<int>(icsize), static_cast<int>(icsize));
    resetAvMoves();
    setAsActiveButton(index);
    bool isCastle = false;
    std::string str = puzzleMov[movesInd].toStdString();
    if(str=="O-O"||str=="o-o"||str=="O-O-O"||str=="o-o-o")
        isCastle=true;
    if(!isCastle){
        std::string st = chooseColLow(buttons[index]->col)+std::to_string(((BOARD_SIZE-1)-buttons[index]->row)+1);
        std::string start = str.substr(0,2);
        std::string end = str.substr(2,2);
        if(!toMove&&st==start){
            figURL = buttons[index]->iconUrl;
            figIndex = index;
            toMove=true;
        }
        else if(toMove&&st==end){
            if(str.size()>4){
                QString s = puzzleMov[movesInd].mid(4,2);
                if(s=="wq"){
                    if(newFiguresSelected)
                        figURL="assets:/images/wQ.png";
                    else
                        figURL="assets:/images/WhiteQueen.png";
                }
                else if(s=="bq"){
                    if(newFiguresSelected)
                        figURL="assets:/images/bQ.png";
                    else
                        figURL="assets:/images/BlackQueen.png";
                }
                else if(s=="wr"){
                    if(newFiguresSelected)
                        figURL="assets:/images/wR.png";
                    else
                        figURL="assets:/images/WhiteRook.png";
                }
                else if(s=="br"){
                    if(newFiguresSelected)
                        figURL="assets:/images/bR.png";
                    else
                        figURL="assets:/images/BlackRook.png";
                }
                else if(s=="wn"){
                    if(newFiguresSelected)
                        figURL="assets:/images/wN.png";
                    else
                        figURL="assets:/images/WhiteHorseRight.png";
                }
                else if(s=="bn"){
                    if(newFiguresSelected)
                        figURL="assets:/images/bN.png";
                    else
                        figURL="assets:/images/BlackHorseRight.png";
                }
                else if(s=="wb"){
                    if(newFiguresSelected)
                        figURL="assets:/images/wB.png";
                    else
                        figURL="assets:/images/WhiteOficer.png";
                }
                else if(s=="bb"){
                    if(newFiguresSelected)
                        figURL="assets:/images/bB.png";
                    else
                        figURL="assets:/images/BlackOficer.png";
                }
            }
            changeBtnIcon(*(buttons[index]), figURL, icSize);
            buttons[index]->setFigId(buttons[figIndex]->figId);
            buttons[index]->repaint();
            changeBtnIcon(*(buttons[figIndex]), "", icSize);
            buttons[figIndex]->setFigId(NO_EDGE);
            buttons[figIndex]->repaint();
            figIndex=NO_EDGE;
            figURL= "";
            toMove=false;
            if(movesInd<puzzleMov.size()-1){
                ++movesInd;
                bool iscastle = false;
                std::string str = puzzleMov[movesInd].toStdString();
                if(str=="O-O"||str=="o-o"||str=="O-O-O"||str=="o-o-o")
                    iscastle=true;
                if(!iscastle){
                    std::string st = "";
                    std::string start = str.substr(0,2);
                    std::string end = str.substr(2,2);
                    for(int i=0;i<buttons.size();++i){
                        st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                        if(!toMove&&st==start){
                            figURL= buttons[i]->iconUrl;
                            figIndex = i;
                            toMove = true;
                            break;
                        }
                    }
                    for(int i=0;i<buttons.size();++i){
                        st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                        if(toMove&&st==end){
                            if(str.size()>4){
                                QString s = puzzleMov[movesInd].mid(4,2);
                                if(s=="wq"){
                                    if(newFiguresSelected)
                                        figURL="assets:/images/wQ.png";
                                    else
                                        figURL="assets:/images/WhiteQueen.png";
                                }
                                else if(s=="bq"){
                                    if(newFiguresSelected)
                                        figURL="assets:/images/bQ.png";
                                    else
                                        figURL="assets:/images/BlackQueen.png";
                                }
                                else if(s=="wr"){
                                    if(newFiguresSelected)
                                        figURL="assets:/images/wR.png";
                                    else
                                        figURL="assets:/images/WhiteRook.png";
                                }
                                else if(s=="br"){
                                    if(newFiguresSelected)
                                        figURL="assets:/images/bR.png";
                                    else
                                        figURL="assets:/images/BlackRook.png";
                                }
                                else if(s=="wn"){
                                    if(newFiguresSelected)
                                        figURL="assets:/images/wN.png";
                                    else
                                        figURL="assets:/images/WhiteHorseRight.png";
                                }
                                else if(s=="bn"){
                                    if(newFiguresSelected)
                                        figURL="assets:/images/bN.png";
                                    else
                                        figURL="assets:/images/BlackHorseRight.png";
                                }
                                else if(s=="wb"){
                                    if(newFiguresSelected)
                                        figURL="assets:/images/wB.png";
                                    else
                                        figURL="assets:/images/WhiteOficer.png";
                                }
                                else if(s=="bb"){
                                    if(newFiguresSelected)
                                        figURL="assets:/images/bB.png";
                                    else
                                        figURL="assets:/images/BlackOficer.png";
                                }
                            }
                            changeBtnIcon(*(buttons[i]), figURL, icSize);
                            buttons[i]->setFigId(buttons[figIndex]->figId);
                            buttons[i]->repaint();
                            setAsActiveButton(i);
                            changeBtnIcon(*(buttons[figIndex]), "", icSize);
                            buttons[figIndex]->setFigId(NO_EDGE);
                            buttons[figIndex]->repaint();
                            figIndex=NO_EDGE;
                            figURL= "";
                            toMove = false;
                            break;
                        }
                    }
                }
                else{
                    //Castle Logic
                    std::string rook = "", king = "", st="";
                    if(str=="O-O"){
                        rook = "h1"; king = "e1";
                        for(int i=0;i<buttons.size();++i){
                            st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                            if(st==rook||st==king){
                                changeBtnIcon(*(buttons[i]), "", icSize);
                                buttons[i]->setFigId(NO_EDGE);
                                buttons[i]->repaint();
                            }
                            if(st=="g1"){
                                if(newFiguresSelected)
                                    figURL="assets:/images/wK.png";
                                else
                                    figURL="assets:/images/WhiteKing.png";
                                changeBtnIcon(*(buttons[i]), figURL, icSize);
                                buttons[i]->setFigId(WK);
                                buttons[i]->repaint();
                                setAsActiveButton(i);
                            }
                            if(st=="f1"){
                                if(newFiguresSelected)
                                    figURL="assets:/images/wR.png";
                                else
                                    figURL="assets:/images/WhiteRook.png";
                                changeBtnIcon(*(buttons[i]), figURL, icSize);
                                buttons[i]->setFigId(WR1);
                                buttons[i]->repaint();
                                setAsActiveButton(i);
                            }
                        }
                    }
                    else if(str=="o-o"){
                        rook = "h8"; king = "e8";
                        for(int i=0;i<buttons.size();++i){
                            st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                            if(st==rook||st==king){
                                changeBtnIcon(*(buttons[i]), "", icSize);
                                buttons[i]->setFigId(NO_EDGE);
                                buttons[i]->repaint();
                            }
                            if(st=="g8"){
                                if(newFiguresSelected)
                                    figURL="assets:/images/bK.png";
                                else
                                    figURL="assets:/images/BlackKing.png";
                                changeBtnIcon(*(buttons[i]), figURL, icSize);
                                buttons[i]->setFigId(BK);
                                buttons[i]->repaint();
                                setAsActiveButton(i);
                            }
                            if(st=="f8"){
                                if(newFiguresSelected)
                                    figURL="assets:/images/bR.png";
                                else
                                    figURL="assets:/images/BlackRook.png";
                                changeBtnIcon(*(buttons[i]), figURL, icSize);
                                buttons[i]->setFigId(BR1);
                                buttons[i]->repaint();
                                setAsActiveButton(i);
                            }
                        }
                    }
                    else if(str=="O-O-O"){
                        rook = "a1"; king = "e1";
                        for(int i=0;i<buttons.size();++i){
                            st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                            if(st==rook||st==king){
                                changeBtnIcon(*(buttons[i]), "", icSize);
                                buttons[i]->setFigId(NO_EDGE);
                                buttons[i]->repaint();
                            }
                            if(st=="c1"){
                                if(newFiguresSelected)
                                    figURL="assets:/images/wK.png";
                                else
                                    figURL="assets:/images/WhiteKing.png";
                                changeBtnIcon(*(buttons[i]), figURL, icSize);
                                buttons[i]->setFigId(WK);
                                buttons[i]->repaint();
                                setAsActiveButton(i);
                            }
                            if(st=="d1"){
                                if(newFiguresSelected)
                                    figURL="assets:/images/wR.png";
                                else
                                    figURL="assets:/images/WhiteRook.png";
                                changeBtnIcon(*(buttons[i]), figURL, icSize);
                                buttons[i]->setFigId(WR1);
                                buttons[i]->repaint();
                                setAsActiveButton(i);
                            }
                        }
                    }
                    else if(str=="o-o-o"){
                        rook = "a8"; king = "e8";
                        for(int i=0;i<buttons.size();++i){
                            st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                            if(st==rook||st==king){
                                changeBtnIcon(*(buttons[i]), "", icSize);
                                buttons[i]->setFigId(NO_EDGE);
                                buttons[i]->repaint();
                            }
                            if(st=="c8"){
                                if(newFiguresSelected)
                                    figURL="assets:/images/bK.png";
                                else
                                    figURL="assets:/images/BlackKing.png";
                                changeBtnIcon(*(buttons[i]), figURL, icSize);
                                buttons[i]->setFigId(BK);
                                buttons[i]->repaint();
                                setAsActiveButton(i);
                            }
                            if(st=="d8"){
                                if(newFiguresSelected)
                                    figURL="assets:/images/bR.png";
                                else
                                    figURL="assets:/images/BlackRook.png";
                                changeBtnIcon(*(buttons[i]), figURL, icSize);
                                buttons[i]->setFigId(BR1);
                                buttons[i]->repaint();
                                setAsActiveButton(i);
                            }
                        }
                    }
                }
            }
            ++movesInd;
            if(movesInd>puzzleMov.size()-1&&(puzzleMov.size()-1)>-1){
                gameEndResult = "Correct!Puzzle Solved!";
                gameEndResultTr = tr("Correct!Puzzle Solved!");
                setGameInfoVis(gameInfo,gameText);
                nextMoveButton->setDisabled(false);
                connection->updatePuzzleStatus(puzzleCnt);
                if(puzzleCnt >= HINT_PUZZLE_ACTIVE_COUNT)
                    connection->disablePuzzle(true);

                movesInd = 0;
                return;
            }
        }
        else{
            gameEndResult = "Incorrect!Please try again";
            gameEndResultTr = tr("Incorrect!Please try again");
            setGameInfoVis(gameInfo,gameText);
        }
    }
    else{
        //Castle Logic
        std::string rook = "", king = "", st="";
        if(str=="O-O"){
            rook = "h1"; king = "e1";
            for(int i=0;i<buttons.size();++i){
                st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                if(st==rook||st==king){
                    changeBtnIcon(*(buttons[i]), "", icSize);
                    buttons[i]->setFigId(NO_EDGE);
                    buttons[i]->repaint();
                }
                if(st=="g1"){
                    if(newFiguresSelected)
                        figURL="assets:/images/wK.png";
                    else
                        figURL="assets:/images/WhiteKing.png";
                    changeBtnIcon(*(buttons[i]), figURL, icSize);
                    buttons[i]->setFigId(WK);
                    buttons[i]->repaint();
                    setAsActiveButton(i);
                }
                if(st=="f1"){
                    if(newFiguresSelected)
                        figURL="assets:/images/wR.png";
                    else
                        figURL="assets:/images/WhiteRook.png";
                    changeBtnIcon(*(buttons[i]), figURL, icSize);
                    buttons[i]->setFigId(WR1);
                    buttons[i]->repaint();
                    setAsActiveButton(i);
                }
            }
        }
        else if(str=="o-o"){
            rook = "h8"; king = "e8";
            for(int i=0;i<buttons.size();++i){
                st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                if(st==rook||st==king){
                    changeBtnIcon(*(buttons[i]), "", icSize);
                    buttons[i]->setFigId(NO_EDGE);
                    buttons[i]->repaint();
                }
                if(st=="g8"){
                    if(newFiguresSelected)
                        figURL="assets:/images/bK.png";
                    else
                        figURL="assets:/images/BlackKing.png";
                    changeBtnIcon(*(buttons[i]), figURL, icSize);
                    buttons[i]->setFigId(BK);
                    buttons[i]->repaint();
                    setAsActiveButton(i);
                }
                if(st=="f8"){
                    if(newFiguresSelected)
                        figURL="assets:/images/bR.png";
                    else
                        figURL="assets:/images/BlackRook.png";
                    changeBtnIcon(*(buttons[i]), figURL, icSize);
                    buttons[i]->setFigId(BR1);
                    buttons[i]->repaint();
                    setAsActiveButton(i);
                }
            }
        }
        else if(str=="O-O-O"){
            rook = "a1"; king = "e1";
            for(int i=0;i<buttons.size();++i){
                st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                if(st==rook||st==king){
                    changeBtnIcon(*(buttons[i]), "", icSize);
                    buttons[i]->setFigId(NO_EDGE);
                    buttons[i]->repaint();
                }
                if(st=="c1"){
                    if(newFiguresSelected)
                        figURL="assets:/images/wK.png";
                    else
                        figURL="assets:/images/WhiteKing.png";
                    changeBtnIcon(*(buttons[i]), figURL, icSize);
                    buttons[i]->setFigId(WK);
                    buttons[i]->repaint();
                    setAsActiveButton(i);
                }
                if(st=="d1"){
                    if(newFiguresSelected)
                        figURL="assets:/images/wR.png";
                    else
                        figURL="assets:/images/WhiteRook.png";
                    changeBtnIcon(*(buttons[i]), figURL, icSize);
                    buttons[i]->setFigId(WR1);
                    buttons[i]->repaint();
                    setAsActiveButton(i);
                }
            }
        }
        else if(str=="o-o-o"){
            rook = "a8"; king = "e8";
            for(int i=0;i<buttons.size();++i){
                st = chooseColLow(buttons[i]->col)+std::to_string(((BOARD_SIZE-1)-buttons[i]->row)+1);
                if(st==rook||st==king){
                    changeBtnIcon(*(buttons[i]), "", icSize);
                    buttons[i]->setFigId(NO_EDGE);
                    buttons[i]->repaint();
                }
                if(st=="c8"){
                    if(newFiguresSelected)
                        figURL="assets:/images/bK.png";
                    else
                        figURL="assets:/images/BlackKing.png";
                    changeBtnIcon(*(buttons[i]), figURL, icSize);
                    buttons[i]->setFigId(BK);
                    buttons[i]->repaint();
                    setAsActiveButton(i);
                }
                if(st=="d8"){
                    if(newFiguresSelected)
                        figURL="assets:/images/bR.png";
                    else
                        figURL="assets:/images/BlackRook.png";
                    changeBtnIcon(*(buttons[i]), figURL, icSize);
                    buttons[i]->setFigId(BR1);
                    buttons[i]->repaint();
                    setAsActiveButton(i);
                }
            }
        }
        ++movesInd;
        if(movesInd>puzzleMov.size()-1){
            gameEndResult = "Correct!Puzzle Solved!";
            gameEndResultTr = tr("Correct!Puzzle Solved!");
            setGameInfoVis(gameInfo,gameText);
            connection->updatePuzzleStatus(puzzleCnt);
            if(puzzleCnt >= HINT_PUZZLE_ACTIVE_COUNT)
                connection->disablePuzzle(true);

            movesInd = 0;
            return;
        }
    }
}

void MainWindow::gameResigned()
{
    gameEnd.store(true);
    if(playerColor=='w'){
        if(isAnalyzisMode == true){
            gameEndResult = "Game Aborted!";
            gameEndResultTr = tr("Game Aborted!");
        }
        else{
            gameEndResult = "White Resigned! Black Wins!";
            gameEndResultTr = tr("White Resigned! Black Wins!");
        }

        startBoard->getBlackPlayer().setWin(true);
        PGNString += " 0-1";
    }
    else if(playerColor=='b'){
        if(isAnalyzisMode == true){
            gameEndResult = "Game Aborted!";
            gameEndResultTr = tr("Game Aborted!");
        }
        else{
            gameEndResult = "Black Resigned! White Wins!";
            gameEndResultTr = tr("Black Resigned! White Wins!");
        }
        startBoard->getWhitePlayer().setWin(true);
        PGNString += " 1-0";
    }
    setGameInfoVis(gameInfo,gameText);
    disableAllButtons();
    if(resignWidget&&resignWidget!=nullptr){
        resignWidget->hide();
    }

    if(hintWidget&&hintWidget!=nullptr){
        hintWidget->hide();
    }
    newGameWidget->show();
    saveGameWidget->show();
    return;
}

void MainWindow::proposeDraw()
{
    //DRAW
    if((isAnalyzisMode)||(chessAiColor==' ')){
        gameEndResult = "No Draw! Please play move!";
        gameEndResultTr = tr("No Draw! Please play move!");
        setGameInfoVis(gameInfo,gameText);
        return;
    }

    int score = Search::positionScore.load();
    if(drawMovesCnt<30&&((playerColor == 'w' && chessAiColor == 'b')||(playerColor == 'b' && chessAiColor == 'w'))){
        if(score<-1500){
            gameEnd.store(true);
            if(resignWidget&&resignWidget!=nullptr){
                resignWidget->hide();
            }

            if(hintWidget&&hintWidget!=nullptr){
                hintWidget->hide();
            }

            newGameWidget->show();
            saveGameWidget->show();

            gameEndResult = "Draw offer accepted!Game Draw!";
            gameEndResultTr = tr("Draw offer accepted!Game Draw!");
            setGameInfoVis(gameInfo,gameText);
            disableAllButtons();
        }
        else{
            gameEndResult = "No Draw! Please play move!";
            gameEndResultTr = tr("No Draw! Please play move!");
            setGameInfoVis(gameInfo,gameText);
        }
        return;
    }
    if(drawMovesCnt>=30&&((playerColor == 'w' && chessAiColor == 'b')||(playerColor == 'b' && chessAiColor == 'w'))){
        if((score>-40&&score<40)||score<-1500){
            gameEnd.store(true);
            if(resignWidget&&resignWidget!=nullptr){
                resignWidget->hide();
            }

            if(hintWidget&&hintWidget!=nullptr){
                hintWidget->hide();
            }

            newGameWidget->show();
            saveGameWidget->show();

            gameEndResult = "Draw offer accepted!Game Draw!";
            gameEndResultTr = tr("Draw offer accepted!Game Draw!");
            setGameInfoVis(gameInfo,gameText);
            disableAllButtons();
        }
        else{
            gameEndResult = "No Draw! Please play move!";
            gameEndResultTr = tr("No Draw! Please play move!");
            setGameInfoVis(gameInfo,gameText);
        }
    }
}

void MainWindow::repaintMainWindow()
{
    if(repaintMainWindowCalled.load())
        return;
    repaintMainWindowCalled.store(true);

    if(saveGameWidget)
        saveGameWidget->hide();

    gameEnd.store(true);
    if(lastChessAi)
        lastChessAi->stopMoveTh.store(true);

    if(banner && banner != nullptr) {
        //banner->setVisible(false);
        banner->closeBannerAd();
        if(bannerShut)
            banner->shutdownBanner();
        banner->disconnect();
        banner->deleteLater();
    }

    if((m_Interstitial) && (m_Interstitial != nullptr)) {
        m_Interstitial->closeInterstitial();
        m_Interstitial->disconnect();
        m_Interstitial->deleteLater();
    }

    clearAllButtons();

    if(threadsAreUp)
        Search::clear();

    Search::bestMoveForGuiDone.store(true);
    Search::variantsCntDone.store(true);

    restartGame = true;
    emit restartGameChanged();
}

void MainWindow::disposeMainWindow()
{
    gameEnd.store(true);
    if(lastChessAi)
        lastChessAi->stopMoveTh.store(true);

    if(banner && banner != nullptr) {
        banner->setVisible(false);
        banner->closeBannerAd();
        banner->disconnect();
        banner->deleteLater();
    }

    if((m_Interstitial) && (m_Interstitial != nullptr)) {
        m_Interstitial->disconnect();
        m_Interstitial->deleteLater();
    }

    clearAllButtons();

    if(threadsAreUp)
        Search::clear();

    Search::bestMoveForGuiDone.store(true);
    Search::variantsCntDone.store(true);
}

void MainWindow::analyzisModeNotAvailable()
{
    QMessageBox::information(this, "Info", tr("Please setup a position to use this feature!"));
}

void MainWindow::startAnalyzisMode()
{
    if(isAnalyzisMode==false){
        isAnalyzisMode = true;
        emitSignalFlag = true;
        starAnalyzisButton->setStyleSheet("background-color:rgba(235, 244, 66,0.7);color:rgba(204, 163, 0,0.5);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    }
    else if(isAnalyzisMode==true){
        isAnalyzisMode = false;
        starAnalyzisButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    }
}

void MainWindow::startChessAI()
{
    uciMoveCommandSetUp = "uci";
    isComputer = true;
    if(gamesCount==0) {
        UCI::init(Options);
        PSQT::init();
        Bitboards::init();
        PositionAI::init();
        Bitbases::init();
        Search::init();
        Pawns::init();
        Tablebases::init(/*Options["SyzygyPath"]*/"<empty>");
//        if(banner&&banner!=nullptr){
//            Search::hashMem = banner->getMemory();
//            if(Search::hashMem>2000)
//                Search::hashMem = 2;
//        }
//        else{
//            Search::hashMem = 1;
//        }
        Search::hashMem = 1;
        TT.resize(static_cast<size_t>(Search::hashMem));
        int coresCount = QThread::idealThreadCount();
        if(coresCount > 4){
            coresCount = 4;
        }
        Threads.set(static_cast<size_t>(coresCount));
        //POLYBOOK
        QString book = connection->checkBooks();
        if(book.size()>0&&book!="empty"){
            Search::bookSettings.ownBook=true;
            Search::bookSettings.path = book.toLocal8Bit().constData();
        }
        else{
            Search::bookSettings.ownBook=false;
            Search::bookSettings.path = "<empty>";
        }
        polybook.init(Search::bookSettings.path);
        threadsAreUp = true;
        Search::clear(); // After threads are up

        uiThread = new Thread(0);
        const char* StartFEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        Search::getStartPos().set(StartFEN, false, &Search::states->back(), uiThread);

        uciMoveCommandSetUp = "isready";
        UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
    }
    uciMoveCommandSetUp = "position startpos";
    UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
    uciMoveCommandSetUp = "isready";
    UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
    connect(this, SIGNAL(uciMoveCmdChanged()), this, SLOT(startChessAICalculation()));

    if(isNotStartPosition && gamesCount==0 &&!islGActive.load()){
        UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
        uciMoveCommandSetUp = computerPlayLevel;
        emit uciMoveCmdChanged();
    }
    else if(isNotStartPosition && gamesCount>0 &&!islGActive.load()){
        UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
        uciMoveCommandSetUp = computerPlayLevel;
        emit uciMoveCmdChanged();
    }
    if(chessAiColor == 'w' && gamesCount==0&&!isNotStartPosition &&!islGActive.load()){
        uciMoveCommandSetUp = "position startpos";
        UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
        uciMoveCommandSetUp = "isready";
        UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
        uciMoveCommandSetUp  = computerPlayLevel;
        emit uciMoveCmdChanged();
    }
    else if(chessAiColor == 'w' && gamesCount>0&&!isNotStartPosition &&!islGActive.load()){
        uciMoveCommandSetUp = "quit";
        UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
        uciMoveCommandSetUp = "position startpos";
        UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
        uciMoveCommandSetUp = "isready";
        UCI::loop(&uciMoveCommandSetUp,Search::startPos,Search::states);
        uciMoveCommandSetUp  = computerPlayLevel;
        emit uciMoveCmdChanged();
    }
}

void MainWindow::chooseChessAIPlayingStrength(QString command_text)
{
    computerPlayLevel = chooseCommandByLevel(command_text).toLocal8Bit().constData();
    levelForDB = command_text;
    winsOnLevelCounter = connection->queryDBAchievemnts(levelForDB);

    if(winsOnLevelCounter>=3&&winsOnLevelCounter<5&&!isNotStartPosition){
        achievementsLabelPath = "assets:/images/bronzestar.png";
        resignImageLabel->setIcon(QIcon(achievementsLabelPath));
        resignImageLabel->setIconSize(QSize(resignImageLabel->width(),resignImageLabel->height()));
        resignImageLabel->setStyleSheet("background-color:"+whitesColor+";border:none;outline:none");
        resignImageLabel->show();
    }
    else if(winsOnLevelCounter>=5&&winsOnLevelCounter<7&&!isNotStartPosition){
        achievementsLabelPath = "assets:/images/silverstar.png";
        resignImageLabel->setIcon(QIcon(achievementsLabelPath));
        resignImageLabel->setIconSize(QSize(resignImageLabel->width(),resignImageLabel->height()));
        resignImageLabel->setStyleSheet("background-color:"+whitesColor+";border:none;outline:none");
        resignImageLabel->show();
    }
    else if(winsOnLevelCounter>=7&&!isNotStartPosition){
        achievementsLabelPath = "assets:/images/goldstar.png";
        resignImageLabel->setIcon(QIcon(achievementsLabelPath));
        resignImageLabel->setIconSize(QSize(resignImageLabel->width(),resignImageLabel->height()));
        resignImageLabel->setStyleSheet("background-color:"+whitesColor+";border:none;outline:none");
        resignImageLabel->show();
    }

    hintDepth = chooseHintMoveLevel(command_text);
    if((computerPlayLevel != "") && (computerPlayLevel != "Level")){
        if((isNotStartPosition) || (chessAiColor != ' ')){
            if(chessAiColor == ' '){
                if(playerColor == 'w'){
                    colorFromNotStartPos = 'b';
                }
                else if(playerColor == 'b'){
                    colorFromNotStartPos = 'w';
                }
            }
            sideBar->hide();
            resignWidget->show();
            if(isAnalyzisMode == true){
                resignButton->setText(tr("Abort"));
            }
            if(isHintActivatedInMainWindow){
                if((!hintWidget) || (hintWidget == nullptr)){
                    double geomWidth = mobWidth*0.375;
                    double geomHeight = mobWidth*0.1388888;
                    double levelGeomHeight = mobWidth*1.277777;
                    double ratio = mobWidth*0.18055555;
                    hintWidget = new QWidget(this);
                    hintWidget->setGeometry(0,static_cast<int>(levelGeomHeight+1),mobWidth,static_cast<int>(ratio+1));
                    hintWidget->setStyleSheet("background-color:"+whitesColor+";");
                    hintWidgetLayout = new QHBoxLayout(hintWidget);
                    hintButton = new QPushButton(tr("Hint"),hintWidget);
                    hintButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                              +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
                    hintButton->setMinimumWidth(static_cast<int>(geomWidth+1));
                    hintButton->setMinimumHeight(static_cast<int>(geomHeight-(mobWidth*0.01388888)));
                    hintButton->adjustSize();
                    hintButton->setDisabled(true);
                    //Move Back
                    undoMoveButton = new QPushButton(tr("Undo"),hintWidget);
                    undoMoveButton->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                  +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
                    undoMoveButton->setMinimumWidth(static_cast<int>(geomWidth+1));
                    undoMoveButton->setMinimumHeight(static_cast<int>(geomHeight-(mobWidth*0.01388888)));
                    undoMoveButton->adjustSize();
                    hintWidgetLayout->addWidget(hintButton,1,Qt::AlignLeft);
                    hintWidgetLayout->addWidget(undoMoveButton,1,Qt::AlignRight);
                    connect(hintButton, &QPushButton::clicked, this, &MainWindow::startSearchHintMove);
                    connect(this, &MainWindow::hintMoveFound, this, &MainWindow::showHintMove);
                    connect(undoMoveButton, &QPushButton::clicked, this, &MainWindow::moveBackGui);
                    hintWidget->show();
                }
                else {
                    hintWidget->show();
                }
            }
            if(isPuzzleAct)
                isPuzzleActivated = true;
            if(isNotStartPosition){
                isStartedFromPosition = true;
            }
            startChessAI();
        }
    }
}

void MainWindow::chooseChessAIPlayingStrengthShow()
{
    isBannerVisibleCond = false;
    wrongBanerPos = false;

    if(saveGameWidget->isVisible()){//TEST
        saveGameWidget->hide();
    }
    if(!bannerShut&&banner&&banner!=nullptr){
        QPoint bannerPos(0,bannerHNotAv);
        banner->setPosition(bannerPos);
        banner->setVisible(false);
    }

    if(settingsWidget&&settingsWidget!=nullptr)
        settingsWidget->show();

    if(!bannerShut&&banner&&banner!=nullptr){
        QPoint bannerPos(0,bannerHNotAv);
        banner->setPosition(bannerPos);
        banner->setVisible(false);
    }
}

void MainWindow::changeEmitFlag()
{
    if(emitSignalFlag==true){
        emitSignalFlag = false;
    }
    else{
        emitSignalFlag = true;
    }
}

void MainWindow::chessAiErr(QString err)
{
    qDebug()<<tr("Error in ChessAI: ")<<err;
    try {
        if((isAnalyzisMode) || (chessAiColor == ' ')){
            repaintMainWindow();
            gameEndResult = "Sorry,Error occured!";
            gameEndResultTr = tr("Sorry,Error occured!");
            setGameInfoVis(gameInfo,gameText);
        }
        else{
            moveBackGui();
            gameEndResult = "Error occured!Please try again!";
            gameEndResultTr = tr("Error occured!Please try again!");
            setGameInfoVis(gameInfo,gameText);
        }
    }
    catch (...) {
        repaintMainWindow();
        gameEndResult = "Sorry,Error occured!";
        gameEndResultTr = tr("Sorry,Error occured!");
        setGameInfoVis(gameInfo,gameText);
    }
}

void MainWindow::getChessAiMove()
{
    if(gameEnd.load()){
        return;
    }
    std::string str = "none";
    if(islGActive.load()){
        while(!(Search::bestMoveForGuiDone.load())) { continue; }
        //assert(Search::bestMoveForGui != "no move");
        if((Search::variantsCntDone.load()) && (Search::variantsForLG!="(none)") && (Search::variantsForLG!="<none>") &&
            (Search::variantsForLG!="none")&&(Search::variantsForLG!="")&&(Search::variantsForLG!=" "))
            isPonder = true;
        else
            isPonder = false;
    }
    else{
        while(!(Search::bestMoveForGuiDone.load())){ continue; }
        //assert(Search::bestMoveForGui != "no move");
    }


    //Protects from crashes//TEST
    if((Search::bestMoveForGui=="(none)"||Search::bestMoveForGui=="<none>"||Search::bestMoveForGui=="none"||
         Search::bestMoveForGui.find(str)!=std::string::npos||Search::bestMoveForGui==""||Search::bestMoveForGui==" ") && !gameEnd.load()){
        gameEnd.store(true);
        if(playerColor=='w'){
            gameEndResult = "Checkmate!Black Wins!";
            gameEndResultTr = tr("Checkmate!Black Wins!");
            startBoard->getBlackPlayer().setWin(true);
        }
        else if(playerColor=='b'){
            gameEndResult = "Checkmate!White Wins!";
            gameEndResultTr = tr("Checkmate!White Wins!");
            startBoard->getWhitePlayer().setWin(true);
        }
        setGameInfoVis(gameInfo,gameText);
        wonCongratulations();
        if(resignWidget&&resignWidget!=nullptr){
            resignWidget->hide();
        }

        if(hintWidget&&hintWidget!=nullptr){
            hintWidget->hide();
        }
        windowCrashed = true;

        newGameWidget->show();
        saveGameWidget->show();
        return;
    }
    if(!isMoveHintActive.load()){
        uciMove = Search::bestMoveForGui;
        Search::bestMoveForGui = "no move";
        if(uciMove!="no move"){
            if(uciMove.size()==5){
                promotionSymbChessAi = uciMove[4];
                std::string st = uciMove.substr(0, (uciMove.size()-1));
                uciMove = st;
            }
            if(!gameEnd.load()) {
                moveGuiFigureUci();
            }
        }
    }
    else{
        uciMoveHint = Search::bestMoveForGui;
        Search::bestMoveForGui = "no move";
        if(uciMoveHint!="no move"){
            if(uciMoveHint.size()==5){
                std::string st = uciMoveHint.substr(0, (uciMoveHint.size()-1));
                uciMoveHint = st;
            }
        }
        hintMoveStr = uciMoveHint;

        if(!gameEnd.load()){
            emit hintMoveFound();
        }
    }
}

void MainWindow::setUciMoveCmd(const std::string &cmd)
{
    treefoldRepetition.push_back(cmd);
    ++movesCounterDraw;
    if(movesCounterDraw==10){
        std::string m1="",m2="",m5="",m6="",m9="",m10="";
        for(int i=0;i<treefoldRepetition.size();i++){
            if(i==0)
                m1 = treefoldRepetition.at(i);
            if(i==1)
                m2 = treefoldRepetition.at(i);
            if(i==4)
                m5 = treefoldRepetition.at(i);
            if(i==5)
                m6 = treefoldRepetition.at(i);
            if(i==8)
                m9 = treefoldRepetition.at(i);
            if(i==9)
                m10 = treefoldRepetition.at(i);
        }
        if(m1==m5&&m5==m9&&m2==m6&&m6==m10&&m1!=""&&m2!=""&&m5!=""&&m6!=""&&m9!=""&&m10!=""){
            gameEnd.store(true);
            isThreefoldRepetition = true;
            gameEndResult="Game Draw!Threefold repetition!";
            gameEndResultTr = tr("Game Draw!Threefold repetition!");
            movesCounterDraw = 0;
            treefoldRepetition.clear();
        }
        else{
            movesCounterDraw = 0;
            treefoldRepetition.clear();
        }
    }
    uciMoveCmd += cmd;
    setUciMoveCommand();
}

std::string &MainWindow::getUciMoveCmd()
{
    return uciMoveCmd;
}

void MainWindow::startChessAICalculation()
{
    if(gameEnd.load()){
        return;
    }
    uciMoveCommandSetUp2 = "";

    //HINT
    if(isMoveHintActive.load()){
        int skillL = (Search::skillLevelPlay + 7);
        Search::skillLevel = (skillL >= 20) ? 20 : skillL;
        UCI::loop(&uciMoveCommandHint,Search::startPos,Search::states);
        uciMoveCommandSetUp2 = hintDepth;
    }
    else{
        Search::skillLevel = Search::skillLevelPlay;
        UCI::loop(&uciMoveCommand,Search::startPos,Search::states);
        uciMoveCommandSetUp2  = computerPlayLevel;
    }
    //HINT

    QThread *thread = new QThread;
    ChessAI *chessAi = new ChessAI();
    lastChessAi = chessAi;
    chessAi->setUciMoveCommand(uciMoveCommandSetUp2);
    chessAi->setTime(time);
    chessAi->moveToThread(thread);
    connect(chessAi, SIGNAL(error(QString)), this, SLOT(chessAiErr(QString)));
    connect(thread, SIGNAL(started()), chessAi, SLOT(process()));
    connect(chessAi, SIGNAL(uciMoveChanged()), this, SLOT(getChessAiMove()));
    connect(this, SIGNAL(uciMoveChanged()), chessAi, SLOT(stop()));
    connect(chessAi, SIGNAL(finished()), thread, SLOT(quit()));
    // free resources used by thread and chessAi
    connect(chessAi, SIGNAL(finished()), chessAi, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
}

void MainWindow::setUciMoveCommand()
{
    uciMoveCommand = getUciMoveCmd();
}

void MainWindow::setUciMoveCommandHint()
{
    uciMoveCommandHint = uciMoveCmdHint;
}

MainWindow::~MainWindow()
{
}

void MainWindow::resetAvMoves()
{
    avMoves.clear();
    bool b = false;
    for(int i=0;i<buttons.size();i++){
        buttons.at(i)->setAvMove(b);
        if(((buttons.at(i)->color)=='d')||((buttons.at(i)->color)=='D')){
            buttons.at(i)->setBorderColor("border:1px solid "+blacksColor+";");
        }
        else{
            buttons.at(i)->setBorderColor("border:1px solid "+whitesColor+";");
        }
        setBtnMode((*buttons.at(i)),false,(buttons.at(i)->color));
    }
}

void MainWindow::findAvailableMoves(int index) {
    if(puzzleMode){
        puzzleBtnClick(index);
        return;
    }
    if(isButtonsDisabled)
        return;
    if(isMoveHintActive.load())
        return;
    if(pawnPromotion&&pawnPromotion!=nullptr){
        if(pawnPromotion->isVisible())
            return;
    }
    if(islGActive.load())
        return;
    //Move Back
    if(undoTurnChanged)
        return;

    if(isChessAISearchActive.load())
        return;

    if(colorWidget&&colorWidget!=nullptr) {
        colorWidget->hide();
    }

    if((gameEnd.load()) || (isBoardFlipped.load()))
        return;

    try {
        if(index>=0){
            if(isComputer&&chessAiColor=='b'&&playerColor=='b'&&!isStartedFromPosition&&colorFromNotStartPos==' '){
                if((buttons.at(index)->figId>=BR1&&buttons.at(index)->figId<=BO2)||
                    (buttons.at(index)->figId>=BP_TRANSF_FIRST&&buttons.at(index)->figId<=BP_TRANSF_LAST)){
                    return;
                }
            }
            else if(isComputer&&chessAiColor=='w'&&playerColor=='w'&&!isStartedFromPosition&&colorFromNotStartPos==' '){
                if((buttons.at(index)->figId>=WR1&&buttons.at(index)->figId<=WO2)||
                    (buttons.at(index)->figId>=WP_TRANSF_FIRST&&buttons.at(index)->figId<=WP_TRANSF_LAST)){
                    return;
                }
            }
            else if(isComputer&&chessAiColor==' '&&playerColor=='w'&&isStartedFromPosition&&colorFromNotStartPos=='b'){
                if((buttons.at(index)->figId>=WR1&&buttons.at(index)->figId<=WO2)||
                    (buttons.at(index)->figId>=WP_TRANSF_FIRST&&buttons.at(index)->figId<=WP_TRANSF_LAST)){
                    return;
                }
            }
            else if(isComputer&&chessAiColor==' '&&playerColor=='b'&&isStartedFromPosition&&colorFromNotStartPos=='w'){
                if((buttons.at(index)->figId>=BR1&&buttons.at(index)->figId<=BO2)||
                    (buttons.at(index)->figId>=BP_TRANSF_FIRST&&buttons.at(index)->figId<=BP_TRANSF_LAST)){
                    return;
                }
            }
            else if(isComputer&&isAnalyzisMode){
                if((buttons.at(index)->figId>=BR1&&buttons.at(index)->figId<=BO2)||
                    (buttons.at(index)->figId>=BP_TRANSF_FIRST&&buttons.at(index)->figId<=BP_TRANSF_LAST)){
                    return;
                }
                if((buttons.at(index)->figId>=WR1&&buttons.at(index)->figId<=WO2)||
                    (buttons.at(index)->figId>=WP_TRANSF_FIRST&&buttons.at(index)->figId<=WP_TRANSF_LAST)){
                    return;
                }
            }

            if(!buttons.at(index)->isAvMov){
                resetAvMoves();
                startBoard->findFigureAvailableMoves(avMoves,buttons.at(index)->figId,playerColor);

                if(avMoves.size()>0){
                    activeFigurePicUrl = (buttons.at(index)->iconUrl);
                    setActiveFigureIndex(index);
                    setBtnMode(*buttons.at(index),true,buttons.at(index)->color);
                    //Workaround for BUG removing unavailable moves
                    char color = ' ';
                    if((buttons.at(index)->figId>=WR1&&buttons.at(index)->figId<=WO2)||
                        (buttons.at(index)->figId>=WP_TRANSF_FIRST&&buttons.at(index)->figId<=WP_TRANSF_LAST))
                        color = 'w';
                    else if((buttons.at(index)->figId>=BR1&&buttons.at(index)->figId<=BO2)||
                             (buttons.at(index)->figId>=BP_TRANSF_FIRST&&buttons.at(index)->figId<=BP_TRANSF_LAST))
                        color = 'b';
                    if(color=='w'){
                        for(int i=0;i<buttons.size();i++){
                            for(int j=0;j<avMoves.size();j++){
                                if(((BOARD_SIZE-1)-buttons.at(i)->row==avMoves.at(j).row)&&
                                    (buttons.at(i)->col==avMoves.at(j).col)&&
                                    !((avMoves.at(j).figId>=BR1&&avMoves.at(j).figId<=BO2)||
                                      (avMoves.at(j).figId>=BP_TRANSF_FIRST&&avMoves.at(j).figId<=BP_TRANSF_LAST))){

                                    if(avMoves.at(j).enpId!=-1){
                                        enpassantId = avMoves.at(j).enpId;
                                    }
                                    if(!((buttons.at(i)->figId>=WR1&&buttons.at(i)->figId<=WO2)||
                                          (buttons.at(i)->figId>=WP_TRANSF_FIRST&&buttons.at(i)->figId<=WP_TRANSF_LAST)))
                                        setAsActiveButton(i);
                                }
                            }
                        }
                    }
                    else if(color=='b'){
                        for(int i=0;i<buttons.size();i++){
                            for(int j=0;j<avMoves.size();j++){
                                if(((BOARD_SIZE-1)-buttons.at(i)->row==avMoves.at(j).row)&&
                                    (buttons.at(i)->col==avMoves.at(j).col)){

                                    if(avMoves.at(j).enpId!=-1){
                                        enpassantId = avMoves.at(j).enpId;
                                    }
                                    if(!((buttons.at(i)->figId>=BR1&&buttons.at(i)->figId<=BO2)||
                                          (buttons.at(i)->figId>=BP_TRANSF_FIRST&&buttons.at(i)->figId<=BP_TRANSF_LAST)))
                                        setAsActiveButton(i);
                                }
                            }
                        }
                    }
                }
            }
            else if(buttons.at(index)->isAvMov&&index!=activeFigureIndex){
                if(((buttons.at(activeFigureIndex)->figId>=WP_FIRST)&&(buttons.at(activeFigureIndex)->figId<=WP_LAST)&&
                     ((BOARD_SIZE-1)-buttons.at(index)->row==7)&&(!(isComputer)||(promotionSymbChessAi==' '&&chessAiColor=='b')))||
                    ((buttons.at(activeFigureIndex)->figId>=WP_FIRST)&&(buttons.at(activeFigureIndex)->figId<=WP_LAST)&&
                     ((BOARD_SIZE-1)-buttons.at(index)->row==7)&&(!(isComputer)||
                                                                          (promotionSymbChessAi==' '&&chessAiColor==' '&&isStartedFromPosition&&colorFromNotStartPos=='w')))||
                    (((buttons.at(activeFigureIndex)->figId>=BP_FIRST)&&(buttons.at(activeFigureIndex)->figId<=BP_LAST)&&
                      ((BOARD_SIZE-1)-buttons.at(index)->row==0))&&(!(isComputer)||(promotionSymbChessAi==' '&&chessAiColor=='w')))||
                    (((buttons.at(activeFigureIndex)->figId>=BP_FIRST)&&(buttons.at(activeFigureIndex)->figId<=BP_LAST)&&
                      ((BOARD_SIZE-1)-buttons.at(index)->row==0))&&(!(isComputer)||
                         (promotionSymbChessAi==' '&&chessAiColor==' '&&isStartedFromPosition&&colorFromNotStartPos=='b')))){
                    setPawnPromoVis();
                    pawnPromoIndex = index;
                }
                else{

                    if(!isComputer&&moveMadeWithoutComputer==false){
                        if(colorWidget&&colorWidget!=nullptr){
                            colorWidget->hide();
                        }
                        isNotStartPosition = true;
                        emitSignalFlag = true;
                        moveMadeWithoutComputer = true;
                        starAnalyzisButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                          +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
                        disconnect(starAnalyzisButton,&QPushButton::clicked,this,&MainWindow::analyzisModeNotAvailable);
                        connect(starAnalyzisButton,&QPushButton::clicked,this,&MainWindow::startAnalyzisMode);
                    }
                    for(int j=0;j<avMoves.size();j++){
                        if(((BOARD_SIZE-1)-buttons.at(index)->row==avMoves.at(j).row)&&
                            (buttons.at(index)->col==avMoves.at(j).col)){
                            if(avMoves.at(j).enpId==NO_EDGE){
                                enpassantId = NO_EDGE;
                                break;
                            }
                        }
                    }

                    moveGuiFigure(index);

                    if(hintWidget&&hintWidget!=nullptr){
                        hintButton->setDisabled(true);
                        isCompsTurn = true;
                    }
                }
            }
        }
    } catch(...) {}
}

void MainWindow::findAvailableMoves(int index, bool isComp)
{
    if(gameEnd.load())
        return;

    if(isComp) {
        if(isMoveHintActive.load()){
            return;
        }

        try {
            if((index >= 0) && (index < buttons.size())){
                if(buttons.at(index)){
                    if(!(buttons.at(index)->isAvMov)){
                        resetAvMoves();
                        startBoard->findFigureAvailableMoves(avMoves,(buttons.at(index)->figId),playerColor);

                        if(avMoves.size() > 0){
                            activeFigurePicUrl = (buttons.at(index)->iconUrl);
                            setActiveFigureIndex(index);
                            setBtnMode((*buttons.at(index)),true,(buttons.at(index)->color));
                            for(int i=0;i<buttons.size();i++){
                                for(int j=0;j<avMoves.size();j++){
                                    if((((BOARD_SIZE-1)-buttons.at(i)->row) == (avMoves.at(j).row)) &&
                                        ((buttons.at(i)->col) == (avMoves.at(j).col))){
                                        if((avMoves.at(j).enpId) != -1){
                                            enpassantId = (avMoves.at(j).enpId);
                                        }
                                        setAsActiveButton(i);
                                    }
                                }
                            }
                        }
                    }
                    else if((buttons.at(index)->isAvMov) && (index != activeFigureIndex)){
                        if((((buttons.at(activeFigureIndex)->figId) >= WP_FIRST)&&((buttons.at(activeFigureIndex)->figId) <= WP_LAST)&&
                             (((BOARD_SIZE-1)-buttons.at(index)->row) ==7)&&((!(isComputer))||(((promotionSymbChessAi)==' ')&&(chessAiColor=='b'))))||
                            ((((buttons.at(activeFigureIndex)->figId) >=BP_FIRST)&&((buttons.at(activeFigureIndex)->figId) <=BP_LAST)&&
                              (((BOARD_SIZE-1)-buttons.at(index)->row) ==0))&&((!(isComputer))||(((promotionSymbChessAi)==' ')&&(chessAiColor=='w'))))){
                            setPawnPromoVis();
                            pawnPromoIndex = index;
                        }
                        else{

                            if((!(isComputer)) && ((moveMadeWithoutComputer)==false)){
                                if(colorWidget&&colorWidget!=nullptr){
                                    colorWidget->hide();
                                }
                                isNotStartPosition = true;
                                emitSignalFlag = true;
                                (moveMadeWithoutComputer) = true;
                                starAnalyzisButton->setStyleSheet("background-color:rgba(2, 53, 104,0.7);color:rgba(204, 163, 0,1);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                                                                  +QString::number(static_cast<int>(btnFontSiz+1)+comboBoxFontInc)+"px;");
                                disconnect(starAnalyzisButton,&QPushButton::clicked,this,&MainWindow::analyzisModeNotAvailable);
                                connect(starAnalyzisButton,&QPushButton::clicked,this,&MainWindow::startAnalyzisMode);
                            }
                            for(int j=0;j<avMoves.size();j++){
                                if((((BOARD_SIZE-1)-buttons.at(index)->row) == (avMoves.at(j).row)) &&
                                    ((buttons.at(index)->col) == (avMoves.at(j).col))){
                                    if((avMoves.at(j).enpId) == NO_EDGE){
                                        enpassantId = NO_EDGE;
                                        break;
                                    }
                                }
                            }

                            moveGuiFigure(index);

                            if((hintWidget) && (hintWidget!=nullptr))
                                hintButton->setDisabled(false);
                        }
                    }
                }
                else{
                    gameEnd.store(true);
                    if((playerColor)=='w'){
                        (gameEndResult) = "Checkmate!Black Wins!";
                        gameEndResultTr = tr("Checkmate!Black Wins!");
                        startBoard->getBlackPlayer().setWin(true);
                    }
                    else if((playerColor) =='b'){
                        (gameEndResult) = "Checkmate!White Wins!";
                        gameEndResultTr = tr("Checkmate!White Wins!");
                        startBoard->getWhitePlayer().setWin(true);
                    }
                    setGameInfoVis(gameInfo,gameText);
                    wonCongratulations();
                    if((resignWidget) && (resignWidget!=nullptr)){
                        resignWidget->hide();
                    }

                    if((hintWidget) && (hintWidget!=nullptr)){
                        hintWidget->hide();
                    }
                    windowCrashed = true;

                    newGameWidget->show();
                    saveGameWidget->show();
                    repaintMainWindow();
                    return;
                }
            }
            else{
                gameEnd.store(true);
                if((playerColor) =='w'){
                    (gameEndResult) = "Checkmate!Black Wins!";
                    gameEndResultTr = tr("Checkmate!Black Wins!");
                    startBoard->getBlackPlayer().setWin(true);
                }
                else if((playerColor) =='b'){
                    (gameEndResult) = "Checkmate!White Wins!";
                    gameEndResultTr = tr("Checkmate!White Wins!");
                    startBoard->getWhitePlayer().setWin(true);
                }
                setGameInfoVis(gameInfo,gameText);
                wonCongratulations();
                if((resignWidget) && (resignWidget!=nullptr)){
                    resignWidget->hide();
                }

                if((hintWidget) && (hintWidget!=nullptr)){
                    hintWidget->hide();
                }
                windowCrashed = true;

                newGameWidget->show();
                saveGameWidget->show();
                repaintMainWindow();
                return;
            }
        } catch(...) {}
    }
}

void MainWindow::setAsActiveButton(int index)
{
    bool b = true;
    if((avMoves.size()>0)||(puzzleMode)){
        buttons.at(index)->setAvMove(b);
        buttons.at(index)->setBorderColor("border:2px solid yellow;");
        setBtnMode((*buttons.at(index)),true,(buttons.at(index)->color));
    }
}

void MainWindow::highliteLastMove(int start, int dest)
{
    if(buttons.at(start)->color=='d'||buttons.at(start)->color=='D'){
        buttons.at(start)->setStyleSheet("background-color:lightblue;border:2px solid yellow;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
    }
    if(buttons.at(start)->color=='l'||buttons.at(start)->color=='L'){
        buttons.at(start)->setStyleSheet("background-color:lightblue;border:2px solid yellow;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
    }
    if(buttons.at(dest)->color=='l'||buttons.at(dest)->color=='L'){
        buttons.at(dest)->setStyleSheet("background-color:lightblue;border:2px solid yellow;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
    }
    if(buttons.at(dest)->color=='d'||buttons.at(dest)->color=='D'){
        buttons.at(dest)->setStyleSheet("background-color:lightblue;border:2px solid yellow;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
    }
}

void MainWindow::highliteLastMove( int start, int dest, int pStart, int pDest)
{
    if(buttons.at(start)->color=='d'||buttons.at(start)->color=='D'){
        buttons.at(start)->setStyleSheet("background-color:lightblue;border:2px solid red;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
    }
    if(buttons.at(start)->color=='l'||buttons.at(start)->color=='L'){
        buttons.at(start)->setStyleSheet("background-color:lightblue;border:2px solid red;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
    }
    if(buttons.at(dest)->color=='l'||buttons.at(dest)->color=='L'){
        buttons.at(dest)->setStyleSheet("background-color:lightblue;border:2px solid red;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
    }
    if(buttons.at(dest)->color=='d'||buttons.at(dest)->color=='D'){
        buttons.at(dest)->setStyleSheet("background-color:lightblue;border:2px solid red;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
    }
    if(isPonder&&pStart>=0&&pDest>=0){
        if(buttons.at(pStart)->color=='d'||buttons.at(pStart)->color=='D'){
            buttons.at(pStart)->setStyleSheet("background-color:lightblue;border:2px solid green;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
        }
        if(buttons.at(pStart)->color=='l'||buttons.at(pStart)->color=='L'){
            buttons.at(pStart)->setStyleSheet("background-color:lightblue;border:2px solid green;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
        }
        if(buttons.at(pDest)->color=='l'||buttons.at(pDest)->color=='L'){
            buttons.at(pDest)->setStyleSheet("background-color:lightblue;border:2px solid green;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
        }
        if(buttons.at(pDest)->color=='d'||buttons.at(pDest)->color=='D'){
            buttons.at(pDest)->setStyleSheet("background-color:lightblue;border:2px solid green;color:red;font-size:"+QString::number((btnNumFontS))+"px;");
        }
    }
}

void MainWindow::clearAllButtons()
{
    if(!grid)
        return;

    try {
        for(unsigned int i=0; i<buttons.size(); ++i) {
            if(!buttons.at(i))
                continue;
            buttons.at(i)->disconnect();
            buttons.at(i)->hide();
            grid->removeWidget(grid->itemAtPosition(buttons.at(i)->row, buttons.at(i)->col)->widget());
            buttons.at(i)->deleteLater();
        }
    } catch(...) { }

    if(buttons.size() > 0)
        buttons.clear();
}

void MainWindow::moveGuiFigure(int index)
{
    try {
        PGNMoveInd = index;
        PGNFigID = (buttons.at(index)->figId);
        PGNLastActiveFig = (buttons.at(activeFigureIndex)->figId);
        ++drawMovesCnt;
        //Move Back
        std::vector<BackMove> backMoveV;
        for(int i=0;i<buttons.size();++i){
            BackMove backMove;
            backMove.figId = buttons[i]->figId;
            backMove.iconUrl = buttons[i]->iconUrl;
            backMove.borderColor = buttons[i]->borderColor;
            backMove._boardPos = buttons[i]->_boardPos;
            backMove.uciMoveCmd = uciMoveCmd;
            backMove.uciMoveCmdHint = uciMoveCmdHint;
            backMove.PGNString = PGNString;
            backMove.PGNMoveNum = PGNMoveNum;
            backMoveV.push_back(backMove);
        }
        backMoveData.push(backMoveV);

        if(!undoMoveClicked)
            undoMoveClicked = false;
        QSize icSize(static_cast<int>(icsize), static_cast<int>(icsize));
        int activeFigIndexChessAi = 0;
        int pt = -1;
        if((isComputer) && ((promotionSymbChessAi) !=' ')){
            fig = toupper(promotionSymbChessAi);
            PGNPromoSymb = fig;
            startBoard->gameInit(pawnTransfer,gameEnd,gameEndResult,gameEndResultTr,playerColor,(buttons.at(activeFigureIndex)->row),
                                 (buttons.at(activeFigureIndex)->col),(buttons.at(index)->row),(buttons.at(index)->col),isKCastle,isQCastle,fig);
            if(!(isComputer)){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol((buttons.at(activeFigureIndex)->col));
                ss<<((buttons.at(activeFigureIndex)->row)+1);
                ss<<chooseCol((buttons.at(index)->col));
                ss<<((buttons.at(index)->row)+1);
                ss<<fig;
                std::string s = ss.str();
                std::transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);

            }
            fig = ' ';
        }
        else{
            startBoard->gameInit(pawnTransfer,gameEnd,gameEndResult,gameEndResultTr,playerColor,(buttons.at(activeFigureIndex)->row),
                                 (buttons.at(activeFigureIndex)->col),(buttons.at(index)->row),(buttons.at(index)->col),isKCastle,isQCastle,fig);
            if(!(isComputer)){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol((buttons.at(activeFigureIndex)->col));
                ss<<(((BOARD_SIZE-1)-(buttons.at(activeFigureIndex)->row))+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-(buttons.at(index)->row))+1);
                if((pawnTransfer)!=NO_EDGE){
                    int p = ((pawnTransfer)%100);
                    if(p==WQ||p==BQ){
                        ss<<"q";PGNPromoSymb = 'Q';
                    }
                    else if(p==WR1||p==BR1){
                        ss<<"r";PGNPromoSymb = 'R';
                    }
                    else if(p==WH1||p==BH1){
                        ss<<"n";PGNPromoSymb = 'N';
                    }
                    else if(p==WO1||p==BO1){
                        ss<<"b";PGNPromoSymb = 'B';
                    }
                }
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);
            }
        }

        if((!(isComputer))||((promotionSymbChessAi)==' ')){
            pt = ((pawnTransfer)%100);
        }
        else{
            if(!isAnalyzisMode){
                if(chessAiColor=='b'||(chessAiColor==' '&&isStartedFromPosition&&colorFromNotStartPos=='w')){
                    if(promotionSymbChessAi=='q'){
                        pt = BQ; PGNPromoSymb = 'Q';
                    }
                    else if(promotionSymbChessAi=='r'){
                        pt = BR1; PGNPromoSymb = 'R';
                    }
                    else if(promotionSymbChessAi=='n'){
                        pt = BH1; PGNPromoSymb = 'N';
                    }
                    else if(promotionSymbChessAi=='b'){
                        pt = BO1; PGNPromoSymb = 'B';
                    }
                }
                else if(chessAiColor=='w'||(chessAiColor==' '&&isStartedFromPosition&&colorFromNotStartPos=='b')){
                    if(promotionSymbChessAi=='q'){
                        pt = WQ; PGNPromoSymb = 'Q';
                    }
                    else if(promotionSymbChessAi=='r'){
                        pt = WR1; PGNPromoSymb = 'R';
                    }
                    else if(promotionSymbChessAi=='n'){
                        pt = WH1; PGNPromoSymb = 'N';
                    }
                    else if(promotionSymbChessAi=='b'){
                        pt = WO1; PGNPromoSymb = 'B';
                    }
                }
                promotionSymbChessAi = ' ';
            }else{
                if(playerColor=='w'){
                    if(promotionSymbChessAi=='q'){
                        pt = BQ; PGNPromoSymb = 'Q';
                    }
                    else if(promotionSymbChessAi=='r'){
                        pt = BR1; PGNPromoSymb = 'R';
                    }
                    else if(promotionSymbChessAi=='n'){
                        pt = BH1; PGNPromoSymb = 'N';
                    }
                    else if(promotionSymbChessAi=='b'){
                        pt = BO1; PGNPromoSymb = 'B';
                    }
                }
                else if(playerColor=='b'){
                    if(promotionSymbChessAi=='q'){
                        pt = WQ; PGNPromoSymb = 'Q';
                    }
                    else if(promotionSymbChessAi=='r'){
                        pt = WR1; PGNPromoSymb = 'R';
                    }
                    else if(promotionSymbChessAi=='n'){
                        pt = WH1; PGNPromoSymb = 'N';
                    }
                    else if(promotionSymbChessAi=='b'){
                        pt = WO1; PGNPromoSymb = 'B';
                    }
                }
                promotionSymbChessAi = ' ';
            }
        }
        if(pt==WQ){
            if(newFiguresSelected)
                buttons.at(index)->setIconUrl("assets:/images/wQ.png");
            else
                buttons.at(index)->setIconUrl("assets:/images/WhiteQueen.png");

            changeBtnIcon(*buttons.at(index),buttons.at(index)->iconUrl,icSize);
            buttons.at(index)->setFigId(pawnTransfer);
            buttons.at(activeFigureIndex)->setIconUrl("");
            buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
            activeFigurePicUrl = "";
            changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
            activeFigIndexChessAi = activeFigureIndex;
            setActiveFigureIndex(NO_EDGE);
            pawnTransfer = NO_EDGE;
            //EXPORT TEST
            PGNPromoSymb = 'Q';
            addToPGN();
            resetAvMoves();
            highliteLastMove(activeFigIndexChessAi, index);
            if(isThreefoldRepetition){
                gameEndResult="GameDraw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            if(gameEnd.load()){
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }
                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
            if(isComputer){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
                ss<<"q";
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);

                if(!isAnalyzisMode){
                    changeEmitFlag();
                }
                if(emitSignalFlag&&(!(gameEnd.load()))){
                    emit uciMoveCmdChanged();
                }
            }
            return;
        }
        else if(pt==WR1){
            if(newFiguresSelected)
                buttons.at(index)->setIconUrl("assets:/images/wR.png");
            else
                buttons.at(index)->setIconUrl("assets:/images/WhiteRook.png");

            changeBtnIcon(*buttons.at(index),buttons.at(index)->iconUrl,icSize);
            buttons.at(index)->setFigId(pawnTransfer);
            buttons.at(activeFigureIndex)->setIconUrl("");
            buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
            activeFigurePicUrl = "";
            changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
            activeFigIndexChessAi = activeFigureIndex;
            setActiveFigureIndex(NO_EDGE);
            pawnTransfer = NO_EDGE;
            //EXPORT TEST
            PGNPromoSymb = 'R';
            addToPGN();
            resetAvMoves();
            highliteLastMove(activeFigIndexChessAi, index);
            if(isThreefoldRepetition){
                gameEndResult="GameDraw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            if(gameEnd.load()){
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }
                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
            if(isComputer){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
                ss<<"r";
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);
                if(!isAnalyzisMode){
                    changeEmitFlag();
                }
                if(emitSignalFlag&&!gameEnd.load()){
                    emit uciMoveCmdChanged();
                }
                else if(gameEnd.load()){
                    if(isThreefoldRepetition){
                        gameEndResult="GameDraw!Threefold repetition!";
                        gameEndResultTr = tr("GameDraw!Threefold repetition!");
                    }
                    setGameInfoVis(gameInfo,gameText);
                    wonCongratulations();
                    if(resignWidget&&resignWidget!=nullptr){
                        resignWidget->hide();
                    }

                    if(hintWidget&&hintWidget!=nullptr){
                        hintWidget->hide();
                    }

                    newGameWidget->show();
                    saveGameWidget->show();
                    disableAllButtons();
                    return;
                }
            }
            return;
        }
        else if(pt==WH1){
            if(newFiguresSelected)
                buttons.at(index)->setIconUrl("assets:/images/wN.png");
            else
                buttons.at(index)->setIconUrl("assets:/images/WhiteHorseLeft.png");

            changeBtnIcon(*buttons.at(index),buttons.at(index)->iconUrl,icSize);
            buttons.at(index)->setFigId(pawnTransfer);
            buttons.at(activeFigureIndex)->setIconUrl("");
            buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
            activeFigurePicUrl = "";
            changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
            activeFigIndexChessAi = activeFigureIndex;
            setActiveFigureIndex(NO_EDGE);
            pawnTransfer = NO_EDGE;
            //EXPORT TEST
            PGNPromoSymb = 'N';
            addToPGN();
            resetAvMoves();
            highliteLastMove(activeFigIndexChessAi, index);
            if(isThreefoldRepetition){
                gameEndResult="GameDraw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            if(gameEnd.load()){
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }

                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
            if(isComputer){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
                ss<<"n";
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);
                if(!isAnalyzisMode){
                    changeEmitFlag();
                }
                if(emitSignalFlag&&!gameEnd.load()){
                    emit uciMoveCmdChanged();
                }
                else if(gameEnd.load()){
                    if(isThreefoldRepetition){
                        gameEndResult="GameDraw!Threefold repetition!";
                        gameEndResultTr = tr("GameDraw!Threefold repetition!");
                    }
                    setGameInfoVis(gameInfo,gameText);
                    wonCongratulations();
                    if(resignWidget&&resignWidget!=nullptr){
                        resignWidget->hide();
                    }

                    if(hintWidget&&hintWidget!=nullptr){
                        hintWidget->hide();
                    }

                    newGameWidget->show();
                    saveGameWidget->show();

                    disableAllButtons();
                    return;
                }
            }
            return;
        }
        else if(pt==WO1){
            if(newFiguresSelected)
                buttons.at(index)->setIconUrl("assets:/images/wB.png");
            else
                buttons.at(index)->setIconUrl("assets:/images/WhiteOficer.png");

            changeBtnIcon(*buttons.at(index),buttons.at(index)->iconUrl,icSize);
            buttons.at(index)->setFigId(pawnTransfer);
            buttons.at(activeFigureIndex)->setIconUrl("");
            buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
            activeFigurePicUrl = "";
            changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
            activeFigIndexChessAi = activeFigureIndex;
            setActiveFigureIndex(NO_EDGE);
            pawnTransfer = NO_EDGE;
            //EXPORT TEST
            PGNPromoSymb = 'B';
            addToPGN();
            resetAvMoves();
            highliteLastMove(activeFigIndexChessAi, index);
            if(isThreefoldRepetition){
                gameEndResult="GameDraw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            if(gameEnd.load()){
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }

                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
            if(isComputer){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
                ss<<"b";
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);
                if(!isAnalyzisMode){
                    changeEmitFlag();
                }
                if(emitSignalFlag&&!gameEnd.load()){
                    emit uciMoveCmdChanged();
                }
                else if(gameEnd.load()){
                    if(isThreefoldRepetition){
                        gameEndResult="GameDraw!Threefold repetition!";
                        gameEndResultTr = tr("GameDraw!Threefold repetition!");
                    }
                    setGameInfoVis(gameInfo,gameText);
                    wonCongratulations();
                    if(resignWidget&&resignWidget!=nullptr){
                        resignWidget->hide();
                    }

                    if(hintWidget&&hintWidget!=nullptr){
                        hintWidget->hide();
                    }
                    newGameWidget->show();
                    saveGameWidget->show();
                    disableAllButtons();
                    return;
                }
            }
            return;
        }
        else if(pt==BQ){
            if(newFiguresSelected)
                buttons.at(index)->setIconUrl("assets:/images/bQ.png");
            else
                buttons.at(index)->setIconUrl("assets:/images/BlackQueen.png");

            changeBtnIcon(*buttons.at(index),buttons.at(index)->iconUrl,icSize);
            buttons.at(index)->setFigId(pawnTransfer);
            buttons.at(activeFigureIndex)->setIconUrl("");
            buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
            activeFigurePicUrl = "";
            changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
            activeFigIndexChessAi = activeFigureIndex;
            setActiveFigureIndex(NO_EDGE);
            pawnTransfer = NO_EDGE;
            //EXPORT TEST
            PGNPromoSymb = 'Q';
            addToPGN();
            resetAvMoves();
            highliteLastMove(activeFigIndexChessAi, index);
            if(isThreefoldRepetition){
                gameEndResult="GameDraw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            if(gameEnd.load()){
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }
                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
            if(isComputer){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
                ss<<"q";
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);
                if(!isAnalyzisMode){
                    changeEmitFlag();
                }
                if(emitSignalFlag&&!gameEnd.load()){
                    emit uciMoveCmdChanged();
                }
                else if(gameEnd.load()){
                    if(isThreefoldRepetition){
                        gameEndResult="GameDraw!Threefold repetition!";
                        gameEndResultTr = tr("GameDraw!Threefold repetition!");
                    }
                    setGameInfoVis(gameInfo,gameText);
                    wonCongratulations();
                    if(resignWidget&&resignWidget!=nullptr){
                        resignWidget->hide();
                    }

                    if(hintWidget&&hintWidget!=nullptr){
                        hintWidget->hide();
                    }
                    newGameWidget->show();
                    saveGameWidget->show();
                    disableAllButtons();
                    return;
                }
            }
            return;
        }
        else if(pt==BR1){
            if(newFiguresSelected)
                buttons.at(index)->setIconUrl("assets:/images/bR.png");
            else
                buttons.at(index)->setIconUrl("assets:/images/BlackRook.png");

            changeBtnIcon(*buttons.at(index),(*buttons.at(index)).iconUrl,icSize);
            buttons.at(index)->setFigId(pawnTransfer);
            buttons.at(activeFigureIndex)->setIconUrl("");
            buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
            activeFigurePicUrl = "";
            changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
            activeFigIndexChessAi = activeFigureIndex;
            setActiveFigureIndex(NO_EDGE);
            pawnTransfer = NO_EDGE;
            //EXPORT TEST
            PGNPromoSymb = 'R';
            addToPGN();
            resetAvMoves();
            highliteLastMove(activeFigIndexChessAi, index);
            if(isThreefoldRepetition){
                gameEndResult="GameDraw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            if(gameEnd.load()){
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }
                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
            if(isComputer){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
                ss<<"r";
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);
                if(!isAnalyzisMode){
                    changeEmitFlag();
                }
                if(emitSignalFlag&&!gameEnd.load()){
                    emit uciMoveCmdChanged();
                }
                else if(gameEnd.load()){
                    if(isThreefoldRepetition){
                        gameEndResult="GameDraw!Threefold repetition!";
                        gameEndResultTr = tr("GameDraw!Threefold repetition!");
                    }
                    setGameInfoVis(gameInfo,gameText);
                    wonCongratulations();
                    if(resignWidget&&resignWidget!=nullptr){
                        resignWidget->hide();
                    }

                    if(hintWidget&&hintWidget!=nullptr){
                        hintWidget->hide();
                    }
                    newGameWidget->show();
                    saveGameWidget->show();
                    disableAllButtons();
                    return;
                }
            }
            return;
        }
        else if(pt==BH1){
            if(newFiguresSelected)
                buttons.at(index)->setIconUrl("assets:/images/bN.png");
            else
                buttons.at(index)->setIconUrl("assets:/images/BlackHorseLeft.png");

            changeBtnIcon(*buttons.at(index),(*buttons.at(index)).iconUrl,icSize);
            buttons.at(index)->setFigId(pawnTransfer);
            buttons.at(activeFigureIndex)->setIconUrl("");
            buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
            activeFigurePicUrl = "";
            changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
            activeFigIndexChessAi = activeFigureIndex;
            setActiveFigureIndex(NO_EDGE);
            pawnTransfer = NO_EDGE;
            //EXPORT TEST
            PGNPromoSymb = 'N';
            addToPGN();
            resetAvMoves();
            highliteLastMove(activeFigIndexChessAi, index);
            if(isThreefoldRepetition){
                gameEndResult="GameDraw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            if(gameEnd.load()){
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }
                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
            if(isComputer){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
                ss<<"n";
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);
                if(!isAnalyzisMode){
                    changeEmitFlag();
                }
                if(emitSignalFlag&&!gameEnd.load()){
                    emit uciMoveCmdChanged();
                }
                else if(gameEnd.load()){
                    if(isThreefoldRepetition){
                        gameEndResult="GameDraw!Threefold repetition!";
                        gameEndResultTr = tr("GameDraw!Threefold repetition!");
                    }
                    setGameInfoVis(gameInfo,gameText);
                    wonCongratulations();
                    if(resignWidget&&resignWidget!=nullptr){
                        resignWidget->hide();
                    }

                    if(hintWidget&&hintWidget!=nullptr){
                        hintWidget->hide();
                    }
                    newGameWidget->show();
                    saveGameWidget->show();
                    disableAllButtons();
                    return;
                }
            }
            return;
        }
        else if(pt==BO1){
            if(newFiguresSelected)
                buttons.at(index)->setIconUrl("assets:/images/bB.png");
            else
                buttons.at(index)->setIconUrl("assets:/images/BlackOficer.png");

            changeBtnIcon(*buttons.at(index),buttons.at(index)->iconUrl,icSize);
            buttons.at(index)->setFigId(pawnTransfer);
            buttons.at(activeFigureIndex)->setIconUrl("");
            buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
            activeFigurePicUrl = "";
            changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
            activeFigIndexChessAi = activeFigureIndex;
            setActiveFigureIndex(NO_EDGE);
            pawnTransfer = NO_EDGE;
            //EXPORT TEST
            PGNPromoSymb = 'B';
            addToPGN();
            resetAvMoves();
            highliteLastMove(activeFigIndexChessAi, index);
            if(isThreefoldRepetition){
                gameEndResult="Game Draw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            if(gameEnd.load()){
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }
                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
            if(isComputer){
                std::stringstream ss;
                ss<<" ";
                ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
                ss<<chooseCol(buttons.at(index)->col);
                ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
                ss<<"b";
                std::string s = ss.str();
                transform(s.begin(), s.end(), s.begin(), ::tolower);
                setUciMoveCmd(s);
                if(!isAnalyzisMode){
                    changeEmitFlag();
                }
                if(emitSignalFlag&&!gameEnd.load()){
                    emit uciMoveCmdChanged();
                }
                else if(gameEnd.load()){
                    if(isThreefoldRepetition){
                        gameEndResult="Game Draw!Threefold repetition!";
                        gameEndResultTr = tr("GameDraw!Threefold repetition!");
                    }
                    setGameInfoVis(gameInfo,gameText);
                    wonCongratulations();
                    if(resignWidget&&resignWidget!=nullptr){
                        resignWidget->hide();
                    }

                    if(hintWidget&&hintWidget!=nullptr){
                        hintWidget->hide();
                    }
                    newGameWidget->show();
                    saveGameWidget->show();
                    disableAllButtons();
                    return;
                }
            }
            return;
        }
        buttons.at(index)->setIconUrl(activeFigurePicUrl);
        changeBtnIcon(*buttons.at(index),activeFigurePicUrl,icSize);
        buttons.at(index)->setFigId(buttons.at(activeFigureIndex)->figId);

        buttons.at(activeFigureIndex)->setIconUrl("");
        buttons.at(activeFigureIndex)->setFigId(NO_EDGE);
        activeFigurePicUrl = "";
        changeBtnIcon(*buttons.at(activeFigureIndex),activeFigurePicUrl,icSize);
        activeFigIndexChessAi = activeFigureIndex;
        setActiveFigureIndex(NO_EDGE);
        //EXPORT TEST
        addToPGN();
        if(enpassantId!=-1){
            for(int i=0;i<buttons.size();i++){
                if(buttons.at(i)->figId==enpassantId){
                    buttons.at(i)->setIconUrl("");
                    buttons.at(i)->setFigId(NO_EDGE);
                    changeBtnIcon(*buttons.at(i),"",icSize);
                }
            }
            enpassantId = -1;
        }
        setGameInfoVis(gameInfo,gameText);

        if(isKCastle&&playerColor=='b'){
            for(int i=0;i<buttons.size();i++){
                if((((BOARD_SIZE-1)-buttons.at(i)->row)==0)&&(buttons.at(i)->col==5)){
                    if(newFiguresSelected){
                        buttons.at(i)->setIconUrl("assets:/images/wR.png");
                        changeBtnIcon(*buttons.at(i),"assets:/images/wR.png",icSize);
                        buttons.at(i)->setFigId(WR2);
                    }
                    else{
                        buttons.at(i)->setIconUrl("assets:/images/WhiteRook.png");
                        changeBtnIcon(*buttons.at(i),"assets:/images/WhiteRook.png",icSize);
                        buttons.at(i)->setFigId(WR2);
                    }
                }
                if(((BOARD_SIZE-1)-buttons.at(i)->row)==0&&(buttons.at(i)->col==(BOARD_SIZE-1))){
                    buttons.at(i)->setIconUrl("");
                    changeBtnIcon(*buttons.at(i),"",icSize);
                    buttons.at(i)->setFigId(NO_EDGE);
                }
            }
            isKCastle = false;
        }
        else if(isKCastle&&playerColor=='w'){
            for(int i=0;i<buttons.size();i++){
                if((((BOARD_SIZE-1)-buttons.at(i)->row)==(BOARD_SIZE-1))&&buttons.at(i)->col==5){
                    if(newFiguresSelected){
                        buttons.at(i)->setIconUrl("assets:/images/bR.png");
                        changeBtnIcon(*buttons.at(i),"assets:/images/bR.png",icSize);
                        buttons.at(i)->setFigId(BR2);
                    }
                    else{
                        buttons.at(i)->setIconUrl("assets:/images/BlackRook.png");
                        changeBtnIcon(*buttons.at(i),"assets:/images/BlackRook.png",icSize);
                        buttons.at(i)->setFigId(BR2);
                    }
                }
                if((((BOARD_SIZE-1)-buttons.at(i)->row)==(BOARD_SIZE-1))&&((buttons.at(i)->col)==(BOARD_SIZE-1))){
                    buttons.at(i)->setIconUrl("");
                    changeBtnIcon(*buttons.at(i),"",icSize);
                    buttons.at(i)->setFigId(NO_EDGE);
                }
            }
            isKCastle = false;
        }
        else if(isQCastle&&playerColor=='b'){
            for(int i=0;i<buttons.size();i++){
                if(((BOARD_SIZE-1)-buttons.at(i)->row)==0&&buttons.at(i)->col==3){
                    if(newFiguresSelected){
                        buttons.at(i)->setIconUrl("assets:/images/wR.png");
                        changeBtnIcon(*buttons.at(i),"assets:/images/wR.png",icSize);
                        buttons.at(i)->setFigId(WR1);
                    }
                    else{
                        buttons.at(i)->setIconUrl("assets:/images/WhiteRook.png");
                        changeBtnIcon(*buttons.at(i),"assets:/images/WhiteRook.png",icSize);
                        buttons.at(i)->setFigId(WR1);
                    }
                }
                if(((BOARD_SIZE-1)-buttons.at(i)->row)==0&&buttons.at(i)->col==0){
                    buttons.at(i)->setIconUrl("");
                    changeBtnIcon(*buttons.at(i),"",icSize);
                    buttons.at(i)->setFigId(NO_EDGE);
                }
            }
            isQCastle = false;
        }
        else if((isQCastle)&&playerColor=='w'){
            for(int i=0;i<buttons.size();i++){
                if((((BOARD_SIZE-1)-buttons.at(i)->row)==(BOARD_SIZE-1))&&buttons.at(i)->col==3){
                    if(newFiguresSelected){
                        buttons.at(i)->setIconUrl("assets:/images/bR.png");
                        changeBtnIcon(*buttons.at(i),"assets:/images/bR.png",icSize);
                        buttons.at(i)->setFigId(BR1);
                    }
                    else{
                        buttons.at(i)->setIconUrl("assets:/images/BlackRook.png");
                        changeBtnIcon(*buttons.at(i),"assets:/images/BlackRook.png",icSize);
                        buttons.at(i)->setFigId(BR1);
                    }
                }
                if((((BOARD_SIZE-1)-buttons.at(i)->row)==(BOARD_SIZE-1))&&buttons.at(i)->col==0){
                    buttons.at(i)->setIconUrl("");
                    changeBtnIcon(*buttons.at(i),"",icSize);
                    buttons.at(i)->setFigId(NO_EDGE);
                }
            }
            isQCastle = false;
        }
        if(gameEnd.load()){
            if(isThreefoldRepetition){
                gameEndResult="GameDraw!Threefold repetition!";
                gameEndResultTr = tr("GameDraw!Threefold repetition!");
            }
            setGameInfoVis(gameInfo,gameText);
            wonCongratulations();
            if(resignWidget&&resignWidget!=nullptr){
                resignWidget->hide();
            }

            if(hintWidget&&hintWidget!=nullptr){
                hintWidget->hide();
            }
            newGameWidget->show();
            saveGameWidget->show();
            disableAllButtons();
            return;
        }
        if(isComputer){
            std::stringstream ss;
            ss<<" ";
            ss<<chooseCol(buttons.at(activeFigIndexChessAi)->col);
            ss<<(((BOARD_SIZE-1)-buttons.at(activeFigIndexChessAi)->row)+1);
            ss<<chooseCol(buttons.at(index)->col);
            ss<<(((BOARD_SIZE-1)-buttons.at(index)->row)+1);
            std::string s = ss.str();
            transform(s.begin(), s.end(), s.begin(), ::tolower);
            setUciMoveCmd(s);
            if(!isAnalyzisMode){
                changeEmitFlag();
            }

            if(emitSignalFlag&&!(gameEnd.load())){
                emit uciMoveCmdChanged();
            }
            else if(gameEnd.load()){
                if(isThreefoldRepetition){
                    gameEndResult="GameDraw!Threefold repetition!";
                    gameEndResultTr = tr("GameDraw!Threefold repetition!");
                }
                setGameInfoVis(gameInfo,gameText);
                wonCongratulations();
                if((resignWidget)&&(resignWidget!=nullptr)){
                    resignWidget->hide();
                }

                if((hintWidget) && (hintWidget!=nullptr)){
                    hintWidget->hide();
                }
                newGameWidget->show();
                saveGameWidget->show();
                disableAllButtons();
                return;
            }
        }

        resetAvMoves();
        highliteLastMove(activeFigIndexChessAi, index);

    } catch(...) {
        QMessageBox::information(this, "Error", tr("Error occured!Please try again!"));
        qApp->exit(0);
    }

}
void MainWindow::moveGuiFigureUci()
{
    //HINT
    if(isMoveHintActive.load()){
        return;
    }

    isChessAISearchActive.store(true);

    try {
        std::string str = uciMove;
        transform(str.begin(), str.end(), str.begin(), ::toupper);
        int startCol = chooseNumLetter(str[0]);
        int destCol = chooseNumLetter(str[2]);
        int startRow = ((BOARD_SIZE)-(str[1]-'0'));
        int destRow = ((BOARD_SIZE)-(str[3]-'0'));
        int startIndex = 0;
        int destIndex = 0;
        for(int i=0;i<buttons.size();i++){
            if((buttons[i]->row)==startRow && buttons[i]->col==startCol){
                startIndex = i;
                break;
            }
        }
        for(int i=0;i<buttons.size();i++){
            if((buttons[i]->row)==destRow && buttons[i]->col==destCol){
                destIndex = i;
                break;
            }
        }

        findAvailableMoves(startIndex,true);
        bool b = true;
        if(destIndex >= 0&&buttons.size()>0&&destIndex<buttons.size()&&!gameEnd.load()){
            try{
                buttons.at(destIndex)->setAvMove(b);
            }
            catch(std::exception &e){
                gameEnd.store(true);
                if(playerColor=='w'){
                    gameEndResult = "White resigned!Black Wins!";
                    gameEndResultTr = tr("White resigned!Black Wins!");
                    startBoard->getBlackPlayer().setWin(true);
                }
                else if(playerColor=='b'){
                    gameEndResult = "Black resigned!White wins!";
                    gameEndResultTr = tr("Black resigned!White Wins!");
                    startBoard->getWhitePlayer().setWin(true);
                }
                setGameInfoVis(gameInfo,gameText);
                wonCongratulations();
                if(resignWidget&&resignWidget!=nullptr){
                    resignWidget->hide();
                }

                if(hintWidget&&hintWidget!=nullptr){
                    hintWidget->hide();
                }
                windowCrashed = true;

                newGameWidget->show();
                saveGameWidget->show();
                isChessAISearchActive.store(false);
                repaintMainWindow();
                return;
            }
        }
        else{
            gameEnd.store(true);
            if(playerColor=='w'){
                gameEndResult = "Checkmate!Black Wins!";
                gameEndResultTr = tr("Checkmate!Black Wins!");
                startBoard->getBlackPlayer().setWin(true);
            }
            else if(playerColor=='b'){
                gameEndResult = "Checkmate!White Wins!";
                gameEndResultTr = tr("Checkmate!White Wins!");
                startBoard->getWhitePlayer().setWin(true);
            }
            setGameInfoVis(gameInfo,gameText);
            wonCongratulations();
            if(resignWidget && resignWidget!=nullptr){
                resignWidget->hide();
            }

            if(hintWidget && hintWidget!=nullptr){
                hintWidget->hide();
            }
            windowCrashed = true;

            newGameWidget->show();
            saveGameWidget->show();
            isChessAISearchActive.store(false);
            repaintMainWindow();
            return;
        }
        findAvailableMoves(destIndex,true);
        isCompsTurn = false;
    }
    catch (...) {
        try {
            if(isAnalyzisMode||chessAiColor==' '){
                isChessAISearchActive.store(false);
                repaintMainWindow();
                gameEndResult = "Sorry,Error occured!";
                gameEndResultTr = tr("Sorry,Error occured!");
                setGameInfoVis(gameInfo,gameText);
            }
            else{
                moveBackGui();
                gameEndResult = "Error occured!Please try again!";
                gameEndResultTr = tr("Error occured!Please try again!");
                setGameInfoVis(gameInfo,gameText);
            }
        }
        catch (...) {
            isChessAISearchActive.store(false);
            repaintMainWindow();
            gameEndResult = "Sorry,Error occured!";
            gameEndResultTr = tr("Sorry,Error occured!");
            setGameInfoVis(gameInfo,gameText);
        }
    }
    isChessAISearchActive.store(false);
}

void MainWindow::disableAllButtons()
{
    if(lastChessAi)
        lastChessAi->stopMoveTh.store(true);

    Search::bestMoveForGui = "no move";
    uciMove = "no move";
    Search::variantsForLG = "no move";
    uciMoveHint = "no move";
    Search::bestMoveForGuiDone.store(true);
    Search::variantsCntDone.store(true);

    isButtonsDisabled = true;
    avMoves.clear();
    bool isWhite = startBoard->getWhitePlayer().win();
    bool isBlack = startBoard->getBlackPlayer().win();
    bool b = false;
    for(int i=0;i<buttons.size();i++){
        buttons.at(i)->setAvMove(b);

        if(isWhite&&buttons.at(i)->figId==BK)
            buttons.at(i)->setBorderColor("border:2px solid red;");
        else if(isBlack&&buttons.at(i)->figId==WK)
            buttons.at(i)->setBorderColor("border:2px solid red;");
        else
            buttons.at(i)->setBorderColor("border:none;");

        setBtnMode(*buttons.at(i),false,buttons.at(i)->color);
    }
    if(gameEndResult == "Draw offer accepted!Game Draw!"||gameEndResult=="GameDraw!Threefold repetition!")
        PGNString += " 1/2-1/2";
}

void MainWindow::keyPressEventQ()
{
    fig = 'Q';
    PGNPromoSymb = 'Q';
    moveGuiFigure(pawnPromoIndex);
    fig = ' ';
    pawnPromoIndex = NO_EDGE;
    setPawnPromoHidden();
}

void MainWindow::keyPressEventR(){
    fig = 'R';
    PGNPromoSymb = 'R';
    moveGuiFigure(pawnPromoIndex);
    fig = ' ';
    pawnPromoIndex = NO_EDGE;
    setPawnPromoHidden();
}
void MainWindow::keyPressEventK(){
    fig = 'N';
    PGNPromoSymb = 'N';
    moveGuiFigure(pawnPromoIndex);
    fig = ' ';
    pawnPromoIndex = NO_EDGE;
    setPawnPromoHidden();
}
void MainWindow::keyPressEventB(){
    fig = 'B';
    PGNPromoSymb = 'B';
    moveGuiFigure(pawnPromoIndex);
    fig = ' ';
    pawnPromoIndex = NO_EDGE;
    setPawnPromoHidden();
}

void MainWindow::setGameInfoVis(QWidget *widget, QLabel *label)
{
    //double fontsize = mobWidth*0.04;
    //QString str = QString::fromUtf8(gameEndResult.c_str());
    //    if(!isPGN){
    //        str = QString::fromUtf8(gameEndResult->c_str());
    //        label->setStyleSheet("font-size:"+QString::number(static_cast<int>(fontsize))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    //        label->setWordWrap(false);
    //    }
    //    else{
    //        if(isResized && gameEndResult == ""){
    //            str = PGNString;
    //            label->setStyleSheet("font-size:"+QString::number(static_cast<int>(fontsize-(mobWidth*0.014)))+"px;color:rgb(204, 163, 0);font-weight:bold;text-align:center;");
    //            label->setWordWrap(true);
    //        }
    //    }
    label->setText(gameEndResultTr);
    //label->setWordWrap(true);
    widget->show();
    label->show();
}

void MainWindow::setGameInfoHidden(QWidget *widget, QLabel *label)
{
    label->setText("");
    widget->hide();
    label->hide();
}
void MainWindow::setPawnPromoVis()
{
    double ratio = mobWidth*0.11111;
    int sqW = static_cast<int>(ratio+1);
    int sqH = static_cast<int>(ratio+1);
    pawnPromotion = new QWidget(this);
    ratio = mobWidth*0.236111;
    pawnPromotion->setGeometry(0,0,mobWidth,static_cast<int>(ratio+1));
    pawnPromotion->setStyleSheet("background-color:"+whitesColor+";");
    pawnPromoQ = new QPushButton("Q",pawnPromotion);
    pawnPromoQ->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                              +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    pawnPromoQ->setMinimumWidth(sqW);
    pawnPromoQ->setMinimumHeight(sqH);
    pawnPromoQ->adjustSize();
    connect(pawnPromoQ, SIGNAL(clicked()), this, SLOT(keyPressEventQ()));
    pawnPromoR = new QPushButton("R",pawnPromotion);
    pawnPromoR->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                              +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    pawnPromoR->setMinimumWidth(sqW);
    pawnPromoR->setMinimumHeight(sqH);
    pawnPromoR->adjustSize();
    connect(pawnPromoR, SIGNAL(clicked()), this, SLOT(keyPressEventR()));
    pawnPromoK = new QPushButton("N",pawnPromotion);
    pawnPromoK->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                              +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    pawnPromoK->setMinimumWidth(sqW);
    pawnPromoK->setMinimumHeight(sqH);
    pawnPromoK->adjustSize();
    connect(pawnPromoK, SIGNAL(clicked()), this, SLOT(keyPressEventK()));
    pawnPromoB = new QPushButton("B",pawnPromotion);
    pawnPromoB->setStyleSheet("background-color:"+buttonsColor+";color:rgb(204, 163, 0);border-radius:20px;Text-align:center;border:1px solid rgb(2, 53, 104);font-weight:bold;font-size:"
                              +QString::number(static_cast<int>(btnFontSiz+1))+"px;");
    pawnPromoB->setMinimumWidth(sqW);
    pawnPromoB->setMinimumHeight(sqH);
    pawnPromoB->adjustSize();
    connect(pawnPromoB, SIGNAL(clicked()), this, SLOT(keyPressEventB()));
    QHBoxLayout *pawnPromo = new QHBoxLayout(pawnPromotion);
    pawnPromo->addWidget(pawnPromoQ);
    pawnPromo->addWidget(pawnPromoR);
    pawnPromo->addWidget(pawnPromoK);
    pawnPromo->addWidget(pawnPromoB);
    pawnPromotion->show();
    pawnPromoQ->show();
    pawnPromoR->show();
    pawnPromoK->show();
    pawnPromoB->show();
}

void MainWindow::setPawnPromoHidden()
{
    if(pawnPromotion&&pawnPromotion!=nullptr){
        pawnPromotion->hide();
        pawnPromoQ->hide();
        pawnPromoR->hide();
        pawnPromoK->hide();
        pawnPromoB->hide();
    }
}

void MainWindow::changeBtnIcon(ChessFieldButton& btn,const QString &newPath, const QSize &size)
{
    std::lock_guard<std::mutex> lg(mMtx);
    btn.setIconUrl(newPath);
    btn.setIcon(QIcon(btn.iconUrl));
    btn.setIconSize(size);
}

void MainWindow::setBtnMode(ChessFieldButton& btn, bool isMove, QChar color)
{
    std::lock_guard<std::mutex> lg(mMtx);
    QSize icSize(static_cast<int>(icsize), static_cast<int>(icsize));
    btn.setMinimumHeight(static_cast<int>(fieldSize));
    btn.setMaximumHeight(static_cast<int>(fieldSize));
    btn.setMinimumWidth(static_cast<int>(fieldSize));
    btn.setMaximumWidth(static_cast<int>(fieldSize));
    btn.setIconSize(icSize);
    bool b = isMove;
    btn.setAvMove(b);
    if(btn.isAvMove()){
        if(color=='d'||color=='D'){
            btn.setStyleSheet("background-color:"+blacksColor+";"+btn.borderColor+"color:red;font-size:"+QString::number((btnNumFontS))+"px;");
        }
        else if(color=='l'||color=='L'){
            btn.setStyleSheet("background-color:"+whitesColor+";"+btn.borderColor+"color:red;font-size:"+QString::number((btnNumFontS))+"px;");
        }
    }
    else{
        if(color=='d'||color=='D'){
            btn.setStyleSheet("background-color:"+blacksColor+";"+btn.borderColor+"color:red;font-size:"+QString::number((btnNumFontS))+"px;");
        }
        else if(color=='l'||color=='L'){
            btn.setStyleSheet("background-color:"+whitesColor+";"+btn.borderColor+"color:red;font-size:"+QString::number((btnNumFontS))+"px;");
        }
    }
    btn.adjustSize();
}

void MainWindow::setActiveFigureIndex(int index)
{
    std::lock_guard<std::mutex> lg(mMtx);
    PGNLastActiveFigId = (activeFigureIndex==NO_EDGE)?index:activeFigureIndex;
    activeFigureIndex = index;
}


#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <typeinfo>
#include <string>
#include <sstream>

#include "AdjacencyMatrixGraph.hpp"
#include "functions.hpp"
#include "Figures.hpp"
#include "Rook.hpp"
#include "Bishop.hpp"
#include "Queen.hpp"
#include "Knight.hpp"
#include "Pawn.hpp"
#include "King.hpp"
#define NO_EDGE -1
#define BOARD_SIZE 8

namespace LastMoveInfo{
volatile int lastMoveFigure = NO_EDGE;
}

//Graph
void StartPosGraph::setNumVertices(int numVert){
    numVertices = numVert;
}
int StartPosGraph::getNumVertices(){
    return numVertices;
}

void StartPosGraph::setLastMovFig(int lastMoveId){
    prevLastMovFig = lastMovFig;
    lastMovFig = lastMoveId;
    LastMoveInfo::lastMoveFigure = lastMovFig;
}
int StartPosGraph::getLastMovFig()const{
    return lastMovFig;
}
void StartPosGraph::removeFigure(char color,int ID){
    if(ID>=0){
        if(color=='w'){
            for(unsigned int i=0;i<whiteFigs.size();i++){
                if(whiteFigs[i]->getId()==ID){
                    whiteFigs.erase(whiteFigs.begin()+i);
                }
            }
        }
        else if(color=='b'){
            for(unsigned int i=0;i<blackFigs.size();i++){
                if(blackFigs[i]->getId()==ID){
                    blackFigs.erase(blackFigs.begin()+i);
                }
            }
        }
        fiftyMovesTake = true;
        return;
    }
}
void StartPosGraph::checkForDrawPosition(){
    if(getWhiteFigs().size()==1&&getBlackFigs().size()==1){
        whitePlayer.setStalemate(true);
        blackPlayer.setStalemate(true);
    }
    else if(getWhiteFigs().size()==2&&getBlackFigs().size()==1){
        for(unsigned int i=0;i<getWhiteFigs().size();i++){
            if((getWhiteFigs().at(i)->getId()==WH1)||
                    (getWhiteFigs().at(i)->getId()==WH2)||
                    ((getWhiteFigs().at(i)->getId()%100)==WH1)){
                whitePlayer.setStalemate(true);
                blackPlayer.setStalemate(true);
                break;
            }
            else if((getWhiteFigs().at(i)->getId()==WO1)||
                    (getWhiteFigs().at(i)->getId()==WO2)||
                    ((getWhiteFigs().at(i)->getId()%100)==WO1)){
                whitePlayer.setStalemate(true);
                blackPlayer.setStalemate(true);
                break;
            }
        }
    }
    else if(getWhiteFigs().size()==1&&getBlackFigs().size()==2){
        for(unsigned int i=0;i<getBlackFigs().size();i++){
            if((getBlackFigs().at(i)->getId()==BH1)||
                    (getBlackFigs().at(i)->getId()==BH2)||
                    ((getBlackFigs().at(i)->getId()%100)==BH1)){
                whitePlayer.setStalemate(true);
                blackPlayer.setStalemate(true);
                break;
            }
            else if((getBlackFigs().at(i)->getId()==BO1)||
                    (getBlackFigs().at(i)->getId()==BO2)||
                    ((getBlackFigs().at(i)->getId()%100)==BO1)){
                whitePlayer.setStalemate(true);
                blackPlayer.setStalemate(true);
                break;
            }
        }
    }
    else if(getWhiteFigs().size()==2&&getBlackFigs().size()==2){
        bool black = false, white = false;
        for(unsigned int i=0;i<getWhiteFigs().size();i++){
            if(((getWhiteFigs().at(i)->getId()==WH1)||(getWhiteFigs().at(i)->getId()==WH2)||((getWhiteFigs().at(i)->getId()%100)==WH1))||
                    ((getWhiteFigs().at(i)->getId()==WO1)||(getWhiteFigs().at(i)->getId()==WO2)||((getWhiteFigs().at(i)->getId()%100)==WO1))){
                white = true;
                break;
            }
        }
        for(unsigned int i=0;i<getBlackFigs().size();i++){
            if(((getBlackFigs().at(i)->getId()==BH1)||(getBlackFigs().at(i)->getId()==BH2)||((getBlackFigs().at(i)->getId()%100)==BH1))||
                    ((getBlackFigs().at(i)->getId()==BO1)||(getBlackFigs().at(i)->getId()==BO2)||((getBlackFigs().at(i)->getId()%100)==BO1))){
                black = true;
                break;
            }
        }
        if(white&&black){
            whitePlayer.setStalemate(true);
            blackPlayer.setStalemate(true);
        }
    }
    else if(getWhiteFigs().size()>=2&&getBlackFigs().size()==1){
        ++counterForFiftyMovesDraw;
        if(counterForFiftyMovesDraw>=99){
            whitePlayer.setStalemate(true);
            blackPlayer.setStalemate(true);
        }
    }
    else if(getBlackFigs().size()>=2&&getWhiteFigs().size()==1){
        ++counterForFiftyMovesDraw;
        if(counterForFiftyMovesDraw>=99){
            whitePlayer.setStalemate(true);
            blackPlayer.setStalemate(true);
        }
    }
    if(blackFigs.size()>=2&&whiteFigs.size()>=2){//50 moves draw with pawns
        if(!(lastMovFig>=WP_FIRST&&lastMovFig<=WP_LAST)&&!(lastMovFig>=BP_FIRST&&lastMovFig<=BP_LAST)&&!fiftyMovesTake)
            ++counterForFiftyMovesDrawPawns;
        else
            counterForFiftyMovesDrawPawns = 0;
        if(counterForFiftyMovesDrawPawns>=99){
            whitePlayer.setStalemate(true);
            blackPlayer.setStalemate(true);
        }
    }
}
StartPosGraph::StartPosGraph():openCheckFig(NO_EDGE), lastMovFig(NO_EDGE){
    LastMoveInfo::lastMoveFigure = NO_EDGE;
    setNumVertices(BOARD_SIZE);

    int whitePawnsCnt = WP_LAST;
    int blackPawnsCnt = BP_LAST;
    for(int i=getNumVertices()-1;i>=0;i--){
        for(int j=getNumVertices()-1;j>=0;j--){
            if(i==0){
                if(j==0){
                    adjMatrix[i][j] = WR1;//Rook A-1
                }
                else if(j==1){
                    adjMatrix[i][j] = WH1;//Knight B-1
                }
                else if(j==2){
                    adjMatrix[i][j] = WO1;//Bishop C-1
                }
                else if(j==3){
                    adjMatrix[i][j] = WQ;//Queen D-1
                }
                else if(j==4){
                    adjMatrix[i][j] = WK;//King E-1
                }
                else if(j==5){
                    adjMatrix[i][j] = WO2;//Bishop F-1
                }
                else if(j==6){
                    adjMatrix[i][j] = WH2;//Knight G-1
                }
                else if(j==7){
                    adjMatrix[i][j] = WR2;//Rook H-1
                }
            }
            else if(i==1){
                adjMatrix[i][j] = whitePawnsCnt;//White Pawns 1-8
                whitePawnsCnt--;
            }
            else if(i==6){
                adjMatrix[i][j] = blackPawnsCnt;//Black Pawns 1-8
                blackPawnsCnt--;
            }
            else if(i==7){
                if(j==0){
                    adjMatrix[i][j] = BR1;//Rook A-8
                }
                else if(j==1){
                    adjMatrix[i][j] = BH1;//Knight B-8
                }
                else if(j==2){
                    adjMatrix[i][j] = BO1;//Bishop C-8
                }
                else if(j==3){
                    adjMatrix[i][j] = BQ;//Queen D-8
                }
                else if(j==4){
                    adjMatrix[i][j] = BK;//King E-8
                }
                else if(j==5){
                    adjMatrix[i][j] = BO2;//Bishop F-8
                }
                else if(j==6){
                    adjMatrix[i][j] = BH2;//Knight G-8
                }
                else if(j==7){
                    adjMatrix[i][j] = BR2;//Rook H-8
                }
            }
            else{
                adjMatrix[i][j] = NO_EDGE;
            }
        }
    }
    //Black Rooks
    Position p1;
    p1.setP(7,'A',BR1);
    std::shared_ptr<Rook> r1 = std::make_shared< Rook>("b_rook_A",p1,0,BR1,'b');
    r1->addAvailableMoves(adjMatrix,r1->getColor());
    Position p2;
    p2.setP(7,'H',BR2);
    std::shared_ptr<Rook> r2 = std::make_shared< Rook>("b_rook_H",p2,0,BR2,'b');
    r2->addAvailableMoves(adjMatrix,r2->getColor());
    blackFigs.push_back(r1);
    blackFigs.push_back(r2);
    //White Rooks
    Position p3;
    p3.setP(0,'A',WR1);
    std::shared_ptr<Rook> r3 = std::make_shared< Rook>("w_rook_A",p3,0,WR1,'w');
    r3->addAvailableMoves(adjMatrix,r3->getColor());
    Position p4;
    p4.setP(0,'H',WR2);
    std::shared_ptr<Rook> r4 = std::make_shared< Rook>("w_rook_H",p4,0,WR2,'w');
    r4->addAvailableMoves(adjMatrix,r4->getColor());
    whiteFigs.push_back(r3);
    whiteFigs.push_back(r4);
    //White officers
    Position p5;p5.setP(0,'C',WO1);
    std::shared_ptr<Bishop> o1 = std::make_shared< Bishop>("w_officer_C",p5,0,WO1,'w');
    o1->addAvailableMoves(adjMatrix,o1->getColor());
    Position p6;p6.setP(0,'F',WO2);
    std::shared_ptr<Bishop> o2 = std::make_shared< Bishop>("w_officer_F",p6,0,WO2,'w');
    o2->addAvailableMoves(adjMatrix,o2->getColor());
    whiteFigs.push_back(o1);
    whiteFigs.push_back(o2);
    //Black officers
    Position p7;p7.setP(7,'C',BO1);
    std::shared_ptr<Bishop> o3 = std::make_shared< Bishop>("b_officer_C",p7,0,BO1,'b');
    o3->addAvailableMoves(adjMatrix,o3->getColor());
    Position p8;p8.setP(7,'F',BO2);
    std::shared_ptr<Bishop> o4 = std::make_shared< Bishop>("b_officer_F",p8,0,BO2,'b');
    o4->addAvailableMoves(adjMatrix,o4->getColor());
    blackFigs.push_back(o3);
    blackFigs.push_back(o4);
    //White Queen
    Position p9;p9.setP(0,'D',WQ);
    std::shared_ptr<Queen> q = std::make_shared< Queen>("w_Queen",p9,0,WQ,'w');
    q->addAvailableMoves(adjMatrix,q->getColor());
    whiteFigs.push_back(q);
    //Black Queen
    Position p10;p10.setP(7,'D',BQ);
    std::shared_ptr<Queen> q1 = std::make_shared< Queen>("b_Queen",p10,0,BQ,'b');
    q1->addAvailableMoves(adjMatrix,q1->getColor());
    blackFigs.push_back(q1);
    //White Horses
    Position p11;p11.setP(0,'B',WH1);
    std::shared_ptr<Knight> h1 = std::make_shared< Knight>("w_Horse_B",p11,0,WH1,'w');
    h1->addAvailableMoves(adjMatrix,h1->getColor());
    whiteFigs.push_back(h1);
    Position p12;p12.setP(0,'G',WH2);
    std::shared_ptr<Knight> h2 = std::make_shared< Knight>("w_Horse_G",p12,0,WH2,'w');
    h2->addAvailableMoves(adjMatrix,h2->getColor());
    whiteFigs.push_back(h2);
    //Black Horses
    Position p13;p13.setP(7,'B',BH1);
    std::shared_ptr<Knight> h3 = std::make_shared< Knight>("b_Horse_B",p13,0,BH1,'b');
    h3->addAvailableMoves(adjMatrix,h3->getColor());
    blackFigs.push_back(h3);
    Position p14;p14.setP(7,'G',BH2);
    std::shared_ptr<Knight> h4 = std::make_shared< Knight>("b_Horse_G",p14,0,BH2,'b');
    h4->addAvailableMoves(adjMatrix,h4->getColor());
    blackFigs.push_back(h4);
    //White Pawns
    int wpCnt = -1;
    for(int i=WP_FIRST;i<=WP_LAST;i++){
        Position p15;p15.setP(1,chooseCol(++wpCnt),i);
        stringstream ss;
        ss<<wpCnt;
        string s=ss.str();
        std::shared_ptr<Pawn> pw = std::make_shared< Pawn>("w_Pawn_"+s,p15,0,i,'w');
        pw->addAvailableMoves(adjMatrix,pw->getColor());
        whiteFigs.push_back(pw);
    }
    //Black Pawns
    int bpCnt = -1;
    for(int i=BP_FIRST;i<=BP_LAST;i++){
        Position p16;p16.setP(6,chooseCol(++bpCnt),i);
        stringstream ss;
        ss<<bpCnt;
        string s=ss.str();
        std::shared_ptr<Pawn> pb = std::make_shared< Pawn>("b_Pawn_"+s,p16,0,i,'b');
        pb->addAvailableMoves(adjMatrix,pb->getColor());
        blackFigs.push_back(pb);
    }
    //White King
    Position p17;p17.setP(0,'E',WK);
    std::shared_ptr<King> kingW = std::make_shared< King>("w_King",p17,0,WK,'w');
    kingW->addAvailableMoves(adjMatrix,kingW->getColor());
    whiteFigs.push_back(kingW);
    //Black King
    Position p18;p18.setP(7,'E',BK);
    std::shared_ptr<King> kingB = std::make_shared< King>("b_King",p18,0,BK,'b');
    kingB->addAvailableMoves(adjMatrix,kingB->getColor());
    blackFigs.push_back(kingB);

    //Move Back
    BackMoveAdj back;
    for(unsigned int i=0;i<whiteFigs.size();++i){
        back.whiteFigsBack.push_back(std::make_shared< Figure>());
    }
    for(unsigned int i=0;i<blackFigs.size();++i){
        back.blackFigsBack.push_back(std::make_shared< Figure>());
    }
    for(int i=0;i<BOARD_SIZE;++i){
        for(int j=0;j<BOARD_SIZE;++j){
            back.adjMatrixBack[i][j] = adjMatrix[i][j];
        }
    }
    for(unsigned int i=0;i<whiteFigs.size();++i){
        *(back.whiteFigsBack[i]) = *(whiteFigs[i]);
    }
    for(unsigned int i=0;i<blackFigs.size();++i){
        *(back.blackFigsBack[i]) = *(blackFigs[i]);
    }
    back.lastMovFig=lastMovFig;
    back.prevLastMovFig=prevLastMovFig;
    back.openCheckFig=openCheckFig;
    backMoveData.push(back);
}
bool StartPosGraph::checkForStalemate(char color){
    if(color=='w'){
        bool isMove = false;
        for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
            if((whitePlayer.getAvMoves()[i].posId!=WK)&&(whitePlayer.getAvMoves()[i].prot==-1)&&
                    (!whitePlayer.getAvMoves()[i].pawnHit)&&(!whitePlayer.getAvMoves()[i].opensCh)){
                isMove = true;
                return isMove;
            }
        }
        return isMove;
    }
    else if(color=='b'){
        bool isMove = false;
        for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
            if((blackPlayer.getAvMoves()[i].posId!=BK)&&(blackPlayer.getAvMoves()[i].prot==-1)&&
                    (!blackPlayer.getAvMoves()[i].pawnHit)&&(!blackPlayer.getAvMoves()[i].opensCh)){
                isMove = true;
                return isMove;
            }
        }
        return isMove;
    }
    return false;
}
void StartPosGraph::init(){
    whitePlayer.eraseAvMoves();
    whitePlayer.eraseCheckAvMoves();
    eraseOpenChMoves();
    for(unsigned int i=0;i<getWhiteFigs().size();i++){
        if(!getWhiteFigs()[i])
            continue;
        //White Pawns
        if(getWhiteFigs()[i]->getId()>=WP_FIRST&&getWhiteFigs()[i]->getId()<=WP_LAST){
            Pawn pawn = static_cast<Pawn>(*(getWhiteFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(pawn.getAvMovesCnt());j++){
                Position p;
                if(pawn.getAvMoves()[j].enPassant==NO_EDGE){
                    p.setP(pawn.getAvMoves()[j].row,pawn.getAvMoves()[j].col,pawn.getAvMoves()[j].posId,pawn.getAvMoves()[j].isCheck,
                           pawn.getAvMoves()[j].isTake,pawn.getAvMoves()[j].isPawnTransf,pawn.getAvMoves()[j].dirId,pawn.getAvMoves()[j].prot);
                    p.pawnHit = pawn.getAvMoves()[j].pawnHit;
                    whitePlayer.setAvMoves(p);
                }
                else if((getLastMovFig()==pawn.getAvMoves()[j].enPassant)&&(pawn.getAvMoves()[j].enPassant!=NO_EDGE)){
                    p.setP(pawn.getAvMoves()[j].row,pawn.getAvMoves()[j].col,pawn.getAvMoves()[j].posId,pawn.getAvMoves()[j].isCheck,
                           pawn.getAvMoves()[j].isTake,pawn.getAvMoves()[j].isPawnTransf,pawn.getAvMoves()[j].dirId,pawn.getAvMoves()[j].prot);
                    p.pawnHit = pawn.getAvMoves()[j].pawnHit;
                    p.enPassant = pawn.getAvMoves()[j].enPassant;
                    whitePlayer.setAvMoves(p);
                }
            }
        }
        //White Rooks
        else if((getWhiteFigs()[i]->getId()==WR1)||(getWhiteFigs()[i]->getId()==WR2)||((getWhiteFigs()[i]->getId()%100)==WR1)){
            Rook rook = static_cast<Rook>(*(getWhiteFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(rook.getAvMovesCnt());j++){
                Position p;p.setP(rook.getAvMoves()[j].row,rook.getAvMoves()[j].col,rook.getAvMoves()[j].posId,rook.getAvMoves()[j].isCheck,
                                  rook.getAvMoves()[j].isTake,rook.getAvMoves()[j].dirId,rook.getAvMoves()[j].prot);
                p.opensCh = rook.getAvMoves()[j].opensCh;
                whitePlayer.setAvMoves(p);
            }
        }
        //White Officers
        else if((getWhiteFigs()[i]->getId()==WO1)||(getWhiteFigs()[i]->getId()==WO2)||((getWhiteFigs()[i]->getId()%100)==WO1)){
            Bishop oficer = static_cast<Bishop>(*(getWhiteFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(oficer.getAvMovesCnt());j++){
                Position p;p.setP(oficer.getAvMoves()[j].row,oficer.getAvMoves()[j].col,oficer.getAvMoves()[j].posId,oficer.getAvMoves()[j].isCheck,
                                  oficer.getAvMoves()[j].isTake,oficer.getAvMoves()[j].dirId,oficer.getAvMoves()[j].prot);
                p.opensCh = oficer.getAvMoves()[j].opensCh;
                whitePlayer.setAvMoves(p);
            }
        }
        //White Queen
        else if((getWhiteFigs()[i]->getId()==WQ)||((getWhiteFigs()[i]->getId()%100)==WQ)){
            Queen queen = static_cast<Queen>(*(getWhiteFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(queen.getAvMovesCnt());j++){
                Position p;p.setP(queen.getAvMoves()[j].row,queen.getAvMoves()[j].col,queen.getAvMoves()[j].posId,queen.getAvMoves()[j].isCheck,
                                  queen.getAvMoves()[j].isTake,queen.getAvMoves()[j].dirId,queen.getAvMoves()[j].prot);
                p.opensCh = queen.getAvMoves()[j].opensCh;
                whitePlayer.setAvMoves(p);
            }
        }
        //White Knight
        else if((getWhiteFigs()[i]->getId()==WH1)||(getWhiteFigs()[i]->getId()==WH2)||((getWhiteFigs()[i]->getId()%100)==WH1)){
            Knight horse = static_cast<Knight>(*(getWhiteFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(horse.getAvMovesCnt());j++){
                Position p;p.setP(horse.getAvMoves()[j].row,horse.getAvMoves()[j].col,horse.getAvMoves()[j].posId,horse.getAvMoves()[j].isCheck,
                                  horse.getAvMoves()[j].isTake,horse.getAvMoves()[j].dirId,horse.getAvMoves()[j].prot);
                whitePlayer.setAvMoves(p);
            }
        }
        //White King
        else if(getWhiteFigs()[i]->getId()==WK){
            King king = static_cast<King>(*(getWhiteFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(king.getAvMovesCnt());j++){
                Position p;p.setP(king.getAvMoves()[j].row,king.getAvMoves()[j].col,king.getAvMoves()[j].posId,king.getAvMoves()[j].isCheck,
                                  king.getAvMoves()[j].isTake,-1,king.getAvMoves()[j].prot);
                whitePlayer.setAvMoves(p);
            }
        }
    }
    //Black Figures
    blackPlayer.eraseAvMoves();
    blackPlayer.eraseCheckAvMoves();
    for(unsigned int i=0;i<getBlackFigs().size();i++){
        if(!getBlackFigs()[i])
            continue;
        //Black Pawns
        if(getBlackFigs()[i]->getId()>=BP_FIRST&&getBlackFigs()[i]->getId()<=BP_LAST){
            Pawn pawn = static_cast<Pawn>(*(getBlackFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(pawn.getAvMovesCnt());j++){
                Position p;
                //CHECK HERE FOR PAWN PROBLEM
                if(pawn.getAvMoves()[j].enPassant==NO_EDGE){
                    p.setP(pawn.getAvMoves()[j].row,pawn.getAvMoves()[j].col,pawn.getAvMoves()[j].posId,pawn.getAvMoves()[j].isCheck,
                           pawn.getAvMoves()[j].isTake,pawn.getAvMoves()[j].isPawnTransf,pawn.getAvMoves()[j].dirId,pawn.getAvMoves()[j].prot);
                    p.pawnHit = pawn.getAvMoves()[j].pawnHit;
                    blackPlayer.setAvMoves(p);
                }
                else if((getLastMovFig()==pawn.getAvMoves()[j].enPassant)&&(pawn.getAvMoves()[j].enPassant!=NO_EDGE)){
                    p.setP(pawn.getAvMoves()[j].row,pawn.getAvMoves()[j].col,pawn.getAvMoves()[j].posId,pawn.getAvMoves()[j].isCheck,
                           pawn.getAvMoves()[j].isTake,pawn.getAvMoves()[j].isPawnTransf,pawn.getAvMoves()[j].dirId,pawn.getAvMoves()[j].prot);
                    p.pawnHit = pawn.getAvMoves()[j].pawnHit;
                    p.enPassant = pawn.getAvMoves()[j].enPassant;
                    blackPlayer.setAvMoves(p);
                }
            }
        }
        //Black Rooks
        else if((getBlackFigs()[i]->getId()==BR1)||(getBlackFigs()[i]->getId()==BR2)||((getBlackFigs()[i]->getId()%100)==BR1)){
            Rook rook = static_cast<Rook>(*(getBlackFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(rook.getAvMovesCnt());j++){
                Position p;p.setP(rook.getAvMoves()[j].row,rook.getAvMoves()[j].col,rook.getAvMoves()[j].posId,rook.getAvMoves()[j].isCheck,
                                  rook.getAvMoves()[j].isTake,rook.getAvMoves()[j].dirId,rook.getAvMoves()[j].prot);
                p.opensCh = rook.getAvMoves()[j].opensCh;
                blackPlayer.setAvMoves(p);
            }
        }
        //Black Officers
        else if((getBlackFigs()[i]->getId()==BO1)||(getBlackFigs()[i]->getId()==BO2)||((getBlackFigs()[i]->getId()%100)==BO1)){
            Bishop oficer= static_cast<Bishop>(*(getBlackFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(oficer.getAvMovesCnt());j++){
                Position p;p.setP(oficer.getAvMoves()[j].row,oficer.getAvMoves()[j].col,oficer.getAvMoves()[j].posId,oficer.getAvMoves()[j].isCheck,
                                  oficer.getAvMoves()[j].isTake,oficer.getAvMoves()[j].dirId,oficer.getAvMoves()[j].prot);
                p.opensCh = oficer.getAvMoves()[j].opensCh;
                blackPlayer.setAvMoves(p);
            }
        }
        //Black Queen
        else if((getBlackFigs()[i]->getId()==BQ)||((getBlackFigs()[i]->getId()%100)==BQ)){
            Queen queen = static_cast<Queen>(*(getBlackFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(queen.getAvMovesCnt());j++){
                Position p;p.setP(queen.getAvMoves()[j].row,queen.getAvMoves()[j].col,queen.getAvMoves()[j].posId,queen.getAvMoves()[j].isCheck,
                                  queen.getAvMoves()[j].isTake,queen.getAvMoves()[j].dirId,queen.getAvMoves()[j].prot);
                p.opensCh = queen.getAvMoves()[j].opensCh;
                blackPlayer.setAvMoves(p);
            }
        }
        //Black Knight
        else if((getBlackFigs()[i]->getId()==BH1)||(getBlackFigs()[i]->getId()==BH2)||((getBlackFigs()[i]->getId()%100)==BH1)){
            Knight horse = static_cast<Knight>(*(getBlackFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(horse.getAvMovesCnt());j++){
                Position p;p.setP(horse.getAvMoves()[j].row,horse.getAvMoves()[j].col,horse.getAvMoves()[j].posId,horse.getAvMoves()[j].isCheck,
                                  horse.getAvMoves()[j].isTake,horse.getAvMoves()[j].dirId,horse.getAvMoves()[j].prot);
                blackPlayer.setAvMoves(p);
            }
        }
        //Black King
        else if(getBlackFigs()[i]->getId()==BK){
            King king = static_cast<King>(*(getBlackFigs()[i]));
            for(size_t j=0;j<static_cast<size_t>(king.getAvMovesCnt());j++){
                Position p;p.setP(king.getAvMoves()[j].row,king.getAvMoves()[j].col,king.getAvMoves()[j].posId,king.getAvMoves()[j].isCheck,
                                  king.getAvMoves()[j].isTake,-1,king.getAvMoves()[j].prot);
                blackPlayer.setAvMoves(p);
            }
        }
    }
    //Removes open check moves
    //White
    for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
        if(whitePlayer.getAvMoves()[i].opensCh){
            for(unsigned int j=0;j<blackPlayer.getAvMoves().size();j++){
                int res = (blackPlayer.getAvMoves()[j].dirId-(blackPlayer.getAvMoves()[j].dirId%10))/10;
                int res1 = (whitePlayer.getAvMoves()[i].dirId-(whitePlayer.getAvMoves()[i].dirId%10))/10;
                if((blackPlayer.getAvMoves()[j].posId==whitePlayer.getAvMoves()[i].posId)&&((res*res)!=(res1*res1))){
                    blackPlayer.removeAvMove(static_cast<int>(j));
                }
            }
            addOpenChMove(whitePlayer.getAvMoves()[i]);
        }
    }
    //Black
    for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
        if(blackPlayer.getAvMoves()[i].opensCh){
            for(unsigned int j=0;j<whitePlayer.getAvMoves().size();j++){
                int res = (whitePlayer.getAvMoves()[j].dirId-(whitePlayer.getAvMoves()[j].dirId%10))/10;
                int res1 = (blackPlayer.getAvMoves()[i].dirId-(blackPlayer.getAvMoves()[i].dirId%10))/10;
                if((whitePlayer.getAvMoves()[j].posId==blackPlayer.getAvMoves()[i].posId)&&((res*res)!=(res1*res1))){
                    whitePlayer.removeAvMove(static_cast<int>(j));
                }
            }
            addOpenChMove(blackPlayer.getAvMoves()[i]);
        }
    }
    for(size_t i=0;i<openChMoves.size();i++){//TEST OPENCHECK
        if(openChMoves.at(i).row==-1&&openChMoves.at(i).col==' '&&openChMoves.at(i).removedRow!=-1&&openChMoves.at(i).removedCol!=' ')
            openChMoves.at(i).isRemoved = true;
    }
    findCheckMoves('w');
    findCheckMoves('b');
}

void StartPosGraph::moveBack()
{ 
    if(backMoveData.empty()){
        setAllFigsAvMoves();
        init();
        return;
    }
    BackMoveAdj back = backMoveData.top();
    for(int i=0;i<BOARD_SIZE;++i){
        for(int j=0;j<BOARD_SIZE;++j){
            adjMatrix[i][j] = back.adjMatrixBack[i][j];
        }
    }
    //White Figs
    if(back.whiteFigsBack.size()==whiteFigs.size()){
        for(unsigned int i=0;i<back.whiteFigsBack.size();++i){
            *(whiteFigs[i]) = *(back.whiteFigsBack[i]);
        }
    }
    else if(back.whiteFigsBack.size()>whiteFigs.size()){
        unsigned int diff = back.whiteFigsBack.size()-whiteFigs.size();
        for(unsigned int i=0;i<diff;++i){
            whiteFigs.push_back(std::make_shared< Figure>());
        }
        for(unsigned int i=0;i<back.whiteFigsBack.size();++i){
            *(whiteFigs[i]) = *(back.whiteFigsBack[i]);
        }
    }
    else if(back.whiteFigsBack.size()<whiteFigs.size()){
        whiteFigs.clear();
        for(unsigned int i=0;i<back.whiteFigsBack.size();++i){
            whiteFigs.push_back(std::make_shared< Figure>());
        }
        for(unsigned int i=0;i<back.whiteFigsBack.size();++i){
            *(whiteFigs[i]) = *(back.whiteFigsBack[i]);
        }
    }
    //Black Figs
    if(back.blackFigsBack.size()==blackFigs.size()){
        for(unsigned int i=0;i<back.blackFigsBack.size();++i){
            *(blackFigs[i]) = *(back.blackFigsBack[i]);
        }
    }
    else if(back.blackFigsBack.size()>blackFigs.size()){
        unsigned int diff = back.blackFigsBack.size()-blackFigs.size();
        for(unsigned int i=0;i<diff;++i){
            blackFigs.push_back(std::make_shared< Figure>());
        }
        for(unsigned int i=0;i<back.blackFigsBack.size();++i){
            *(blackFigs[i]) = *(back.blackFigsBack[i]);
        }
    }
    else if(back.blackFigsBack.size()<blackFigs.size()){
        blackFigs.clear();
        for(unsigned int i=0;i<back.blackFigsBack.size();++i){
            blackFigs.push_back(std::make_shared< Figure>());
        }
        for(unsigned int i=0;i<back.blackFigsBack.size();++i){
            *(blackFigs[i]) = *(back.blackFigsBack[i]);
        }
    }

    lastMovFig = back.lastMovFig;
    prevLastMovFig = back.prevLastMovFig;
    openCheckFig = back.openCheckFig;
    if(back.whiteChecked){
        whitePlayer.setCheck(true);
        blackPlayer.setCheck(false);
    }
    else if(back.blackChecked){
        blackPlayer.setCheck(true);
        whitePlayer.setCheck(false);
    }
    else{
        blackPlayer.setCheck(false);
        whitePlayer.setCheck(false);
    }
    LastMoveInfo::lastMoveFigure = lastMovFig;//TO CHECK
    if(whitePlayer.isTurn()){
        whitePlayer.setTurn(false);
        blackPlayer.setTurn(true);
        backMoveData.pop();
        setAllFigsAvMoves();
        init();
        return;
    }
    if(blackPlayer.isTurn()){
        whitePlayer.setTurn(true);
        blackPlayer.setTurn(false);
        backMoveData.pop();
        setAllFigsAvMoves();
        init();
        return;
    }
}

void StartPosGraph::addOpenChMove(Position& p){
    openChMoves.push_back(p);
}

void StartPosGraph::eraseOpenChMoves(){
    openChMoves.clear();
}
void StartPosGraph::findCheckMoves(char color){
    if(color=='w'){
        int dir = -1;
        int dir2 = -1;
        Position pCh;
        Position pOpen;
        for(int i=0;i<BOARD_SIZE;i++){
            for(int j=0;j<BOARD_SIZE;j++){
                if(getLastMovFig()!=-1&&getTable()[i][j]==getLastMovFig()){
                    pCh.setP(i,chooseCol(j),getTable()[i][j]);
                }
                if(getOpenCheckFig()!=-1&&getTable()[i][j]==getOpenCheckFig()){
                    pOpen.setP(i,chooseCol(j));
                }
            }
        }
        for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
            if(blackPlayer.getAvMoves()[i].isCheck&&(blackPlayer.getAvMoves()[i].posId==getLastMovFig())){
                dir = blackPlayer.getAvMoves()[i].dirId;
            }
            if((blackPlayer.getAvMoves()[i].posId==getOpenCheckFig())&&(blackPlayer.getAvMoves()[i].isCheck)){
                dir2 = blackPlayer.getAvMoves()[i].dirId;
            }
        }
        for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
            Position p;
            if(dir!=-1&&dir2==-1&&whitePlayer.getAvMoves()[i].posId!=WK&&whitePlayer.getAvMoves()[i].prot==-1&&
                    !whitePlayer.getAvMoves()[i].pawnHit){
                for(unsigned int j=0;j<blackPlayer.getAvMoves().size();j++){
                    if(((blackPlayer.getAvMoves()[j].row==whitePlayer.getAvMoves()[i].row)&&
                        (blackPlayer.getAvMoves()[j].col==whitePlayer.getAvMoves()[i].col)&&
                        (blackPlayer.getAvMoves()[j].dirId==dir&&blackPlayer.getAvMoves()[j].dirId!=H_DIR)&&
                        (blackPlayer.getAvMoves()[j].prot==-1)&&blackPlayer.getAvMoves()[j].posId==getLastMovFig()&&
                        blackPlayer.getAvMoves()[j].isCheck==0)||
                            ((blackPlayer.getAvMoves()[j].removedRow==whitePlayer.getAvMoves()[i].row)&&
                             (blackPlayer.getAvMoves()[j].removedRow!=-1)&&
                             (blackPlayer.getAvMoves()[j].removedCol==whitePlayer.getAvMoves()[i].col)&&
                             (blackPlayer.getAvMoves()[j].removedCol!=' ')&&
                             (blackPlayer.getAvMoves()[j].dirId==dir&&blackPlayer.getAvMoves()[j].dirId!=H_DIR)&&
                             (blackPlayer.getAvMoves()[j].prot==-1)&&blackPlayer.getAvMoves()[j].posId==getLastMovFig()&&
                             blackPlayer.getAvMoves()[j].isCheck==0)){
                        p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                        whitePlayer.setCheckAvMoves(p);
                    }
                }
            }
            else if(dir==-1&&dir2!=-1&&whitePlayer.getAvMoves()[i].posId!=WK&&whitePlayer.getAvMoves()[i].prot==-1&&
                    !whitePlayer.getAvMoves()[i].pawnHit){
                for(unsigned int j=0;j<blackPlayer.getAvMoves().size();j++){
                    if(((blackPlayer.getAvMoves()[j].row==whitePlayer.getAvMoves()[i].row)&&
                        (blackPlayer.getAvMoves()[j].col==whitePlayer.getAvMoves()[i].col)&&
                        (blackPlayer.getAvMoves()[j].dirId==dir2&&blackPlayer.getAvMoves()[j].dirId!=H_DIR)&&
                        (blackPlayer.getAvMoves()[j].prot==-1)&&(blackPlayer.getAvMoves()[j].posId==getOpenCheckFig())&&
                        blackPlayer.getAvMoves()[j].isCheck==0)||
                            ((blackPlayer.getAvMoves()[j].removedRow==whitePlayer.getAvMoves()[i].row)&&
                             (blackPlayer.getAvMoves()[j].removedRow!=-1)&&
                             (blackPlayer.getAvMoves()[j].removedCol==whitePlayer.getAvMoves()[i].col)&&
                             (blackPlayer.getAvMoves()[j].removedCol!=' ')&&
                             (blackPlayer.getAvMoves()[j].dirId==dir2&&blackPlayer.getAvMoves()[j].dirId!=H_DIR)&&
                             (blackPlayer.getAvMoves()[j].prot==-1)&&(blackPlayer.getAvMoves()[j].posId==getOpenCheckFig())&&
                             blackPlayer.getAvMoves()[j].isCheck==0)){
                        p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                        whitePlayer.setCheckAvMoves(p);
                    }
                }
            }
            else if(dir!=-1&&dir2!=-1&&whitePlayer.getAvMoves()[i].posId!=WK&&whitePlayer.getAvMoves()[i].prot==-1&&
                    !whitePlayer.getAvMoves()[i].pawnHit){
                for(unsigned int j=0;j<blackPlayer.getAvMoves().size();j++){
                    if((((blackPlayer.getAvMoves()[j].row==whitePlayer.getAvMoves()[i].row)&&
                         (blackPlayer.getAvMoves()[j].col==whitePlayer.getAvMoves()[i].col)&&
                         (blackPlayer.getAvMoves()[j].dirId==dir&&blackPlayer.getAvMoves()[j].dirId!=H_DIR)&&
                         (blackPlayer.getAvMoves()[j].prot==-1)&&(blackPlayer.getAvMoves()[j].posId==getLastMovFig())&&
                         blackPlayer.getAvMoves()[j].isCheck==0)&&
                        ((blackPlayer.getAvMoves()[j].row==whitePlayer.getAvMoves()[i].row)&&
                         (blackPlayer.getAvMoves()[j].col==whitePlayer.getAvMoves()[i].col)&&
                         (blackPlayer.getAvMoves()[j].dirId==dir2&&blackPlayer.getAvMoves()[j].dirId!=H_DIR)&&
                         (blackPlayer.getAvMoves()[j].prot==-1)&&(blackPlayer.getAvMoves()[j].posId==getOpenCheckFig())&&
                         blackPlayer.getAvMoves()[j].isCheck==0))){
                        p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                        whitePlayer.setCheckAvMoves(p);
                    }
                }
            }
            if(dir!=-1&&dir2==-1&&(whitePlayer.getAvMoves()[i].posId!=WK)&&(((whitePlayer.getAvMoves()[i].row==pCh.row)&&
                                                                             (whitePlayer.getAvMoves()[i].col==pCh.col))||(whitePlayer.getAvMoves()[i].enPassant==pCh.posId))){
                p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                p.enPassant = whitePlayer.getAvMoves()[i].enPassant;
                whitePlayer.setCheckAvMoves(p);
            }
            else if(dir==-1&&dir2!=-1&&(whitePlayer.getAvMoves()[i].posId!=WK)&&(whitePlayer.getAvMoves()[i].row==pOpen.row)&&
                    (whitePlayer.getAvMoves()[i].col==pOpen.col)){
                p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                whitePlayer.setCheckAvMoves(p);
            }
            else if(dir!=-1&&dir2!=-1&&(whitePlayer.getAvMoves()[i].posId!=WK)&&(whitePlayer.getAvMoves()[i].row==pOpen.row)&&
                    (whitePlayer.getAvMoves()[i].col==pOpen.col)&&(whitePlayer.getAvMoves()[i].row==pCh.row)&&
                    (whitePlayer.getAvMoves()[i].col==pCh.col)){
                p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                whitePlayer.setCheckAvMoves(p);
            }
        }
        bool rookMovedK = false;
        bool rookMovedQ = false;
        for(unsigned int k=0;k<getWhiteFigs().size();k++){
            if(getWhiteFigs()[k]->getId()==WR2){
                Rook r = static_cast<Rook>(*(getWhiteFigs()[k]));
                if(r.hasmoved()){rookMovedK = true;}
            }
            else if(getWhiteFigs()[k]->getId()==WR1){
                Rook r = static_cast<Rook>(*(getWhiteFigs()[k]));
                if(r.hasmoved()){rookMovedQ = true;}
            }
        }
        for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
            if((whitePlayer.getAvMoves()[i].posId==WK)&&(!whitePlayer.getAvMoves()[i].isTake)&&(whitePlayer.getAvMoves()[i].prot==-1)){
                bool isUnderHit = false;
                for(unsigned int j=0;j<blackPlayer.getAvMoves().size();j++){
                    if(getTable()[0][chooseNumLetter('E')]==WK&&whitePlayer.getAvMoves()[i].row==0&&whitePlayer.getAvMoves()[i].col=='G'){
                        if((blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='F'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='E'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='G'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='F'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='E'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='G'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (rookMovedK)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='F'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='E'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='G'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='F'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='E'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='G'&&blackPlayer.getAvMoves()[j].pawnHit)){
                            isUnderHit = true;
                            break;
                        }
                    }
                    else if(getTable()[0][chooseNumLetter('E')]==WK&&whitePlayer.getAvMoves()[i].row==0&&whitePlayer.getAvMoves()[i].col=='C'){
                        if((blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='D'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='E'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='C'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='D'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='E'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='C'&&blackPlayer.getAvMoves()[j].dirId!=P_UP)||
                                (rookMovedQ)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='D'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='E'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].row==0&&blackPlayer.getAvMoves()[j].col=='C'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='D'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='E'&&blackPlayer.getAvMoves()[j].pawnHit)||
                                (blackPlayer.getAvMoves()[j].removedRow==0&&blackPlayer.getAvMoves()[j].removedCol=='C'&&blackPlayer.getAvMoves()[j].pawnHit)){
                            isUnderHit = true;
                            break;
                        }
                    }
                    else if(((blackPlayer.getAvMoves()[j].dirId!=P_UP)&&(whitePlayer.getAvMoves()[i].row==blackPlayer.getAvMoves()[j].row)&&
                             (whitePlayer.getAvMoves()[i].col==blackPlayer.getAvMoves()[j].col))||
                            ((blackPlayer.getAvMoves()[j].dirId!=P_UP)&&(whitePlayer.getAvMoves()[i].row==blackPlayer.getAvMoves()[j].removedRow)&&
                             (whitePlayer.getAvMoves()[i].col==blackPlayer.getAvMoves()[j].removedCol)&&
                             blackPlayer.getAvMoves()[j].removedRow!=-1&&blackPlayer.getAvMoves()[j].removedCol!=' ')){
                        isUnderHit = true;
                        break;
                    }
                }
                if(isUnderHit==false){
                    Position p;p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                    whitePlayer.setCheckAvMoves(p);
                }
            }
        }
        for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
            bool protect = false;
            if(whitePlayer.getAvMoves()[i].posId==WK&&whitePlayer.getAvMoves()[i].isTake&&whitePlayer.getAvMoves()[i].prot==-1){
                int id = getTable()[whitePlayer.getAvMoves()[i].row!=-1?whitePlayer.getAvMoves()[i].row:whitePlayer.getAvMoves()[i].removedRow][chooseNumLetter(whitePlayer.getAvMoves()[i].col!=' '?whitePlayer.getAvMoves()[i].col:whitePlayer.getAvMoves()[i].removedCol)];
                for(unsigned int k=0;k<blackPlayer.getAvMoves().size();k++){
                    if(blackPlayer.getAvMoves()[k].prot==id){
                        protect = true;
                        break;
                    }
                    if(((whitePlayer.getAvMoves()[i].row==blackPlayer.getAvMoves()[k].row)&&
                        (whitePlayer.getAvMoves()[i].col==blackPlayer.getAvMoves()[k].col)&&blackPlayer.getAvMoves()[k].isCheck==1)||
                            ((whitePlayer.getAvMoves()[i].row==blackPlayer.getAvMoves()[k].removedRow)&&
                             blackPlayer.getAvMoves()[k].removedRow!=-1&&
                             blackPlayer.getAvMoves()[k].removedCol!=' '&&
                             (whitePlayer.getAvMoves()[i].col==blackPlayer.getAvMoves()[k].removedCol)&&blackPlayer.getAvMoves()[k].isCheck==1)){
                        protect = true;
                        break;
                    }
                }
                if(protect==false){
                    Position p;p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                    whitePlayer.setCheckAvMoves(p);
                }
            }
        }
        for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
            if(whitePlayer.getAvMoves()[i].posId!=WK&&whitePlayer.getAvMoves()[i].isTake&&whitePlayer.getAvMoves()[i].prot==-1){
                for(unsigned int k=0;k<blackPlayer.getAvMoves().size();k++){
                    int row = blackPlayer.getAvMoves()[k].row!=-1?blackPlayer.getAvMoves()[k].row:blackPlayer.getAvMoves()[k].removedRow;
                    char col = blackPlayer.getAvMoves()[k].col!=' '?blackPlayer.getAvMoves()[k].col:blackPlayer.getAvMoves()[k].removedCol;
                    if((blackPlayer.getAvMoves().at(k).posId==getLastMovFig()&&blackPlayer.getAvMoves().at(k).isCheck==1
                        &&whitePlayer.getAvMoves()[i].row==blackPlayer.getAvMoves().at(k).row&&
                        whitePlayer.getAvMoves()[i].col==blackPlayer.getAvMoves().at(k).col&&
                        getTable()[row][chooseNumLetter(col)]==getLastMovFig())||
                            (blackPlayer.getAvMoves().at(k).posId==getLastMovFig()&&blackPlayer.getAvMoves().at(k).isCheck==1
                             &&whitePlayer.getAvMoves()[i].row==blackPlayer.getAvMoves().at(k).removedRow&&
                             blackPlayer.getAvMoves().at(k).removedRow!=-1&&
                             whitePlayer.getAvMoves()[i].col==blackPlayer.getAvMoves().at(k).removedCol&&
                             blackPlayer.getAvMoves().at(k).removedCol!=' '&&
                             getTable()[row][chooseNumLetter(col)]==getLastMovFig())){
                        Position p;p.setP(whitePlayer.getAvMoves()[i].row,whitePlayer.getAvMoves()[i].col,whitePlayer.getAvMoves()[i].posId);
                        whitePlayer.setCheckAvMoves(p);
                    }
                }
            }
        }
    }
    else if(color=='b'){
        int dir = -1;
        int dir2 = -1;
        Position pCh;
        Position pOpen;
        for(int i=0;i<BOARD_SIZE;i++){
            for(int j=0;j<BOARD_SIZE;j++){
                if(getLastMovFig()!=-1&&getTable()[i][j]==getLastMovFig()){
                    pCh.setP(i,chooseCol(j),getTable()[i][j]);
                }
                if(getOpenCheckFig()!=-1&&getTable()[i][j]==getOpenCheckFig()){
                    pOpen.setP(i,chooseCol(j));
                }
            }
        }
        for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
            if(whitePlayer.getAvMoves()[i].isCheck&&(whitePlayer.getAvMoves()[i].posId==getLastMovFig())){
                dir = whitePlayer.getAvMoves()[i].dirId;
            }
            if((whitePlayer.getAvMoves()[i].posId==getOpenCheckFig())&&(whitePlayer.getAvMoves()[i].isCheck)){
                dir2 = whitePlayer.getAvMoves()[i].dirId;
            }
        }
        for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
            Position p;
            if(dir!=-1&&dir2==-1&&blackPlayer.getAvMoves()[i].posId!=BK&&blackPlayer.getAvMoves()[i].prot==-1&&
                    !blackPlayer.getAvMoves()[i].pawnHit){
                for(unsigned int j=0;j<whitePlayer.getAvMoves().size();j++){
                    if(((whitePlayer.getAvMoves()[j].row==blackPlayer.getAvMoves()[i].row)&&
                        (whitePlayer.getAvMoves()[j].col==blackPlayer.getAvMoves()[i].col)&&
                        (whitePlayer.getAvMoves()[j].dirId==dir&&whitePlayer.getAvMoves()[j].dirId!=H_DIR)&&
                        (whitePlayer.getAvMoves()[j].prot==-1)&&whitePlayer.getAvMoves()[j].posId==getLastMovFig()&&
                        whitePlayer.getAvMoves()[j].isCheck==0)||
                            ((whitePlayer.getAvMoves()[j].removedRow==blackPlayer.getAvMoves()[i].row)&&
                             whitePlayer.getAvMoves()[j].removedRow!=-1&&
                             (whitePlayer.getAvMoves()[j].removedCol==blackPlayer.getAvMoves()[i].col)&&
                             whitePlayer.getAvMoves()[j].removedCol!=' '&&
                             (whitePlayer.getAvMoves()[j].dirId==dir&&whitePlayer.getAvMoves()[j].dirId!=H_DIR)&&
                             (whitePlayer.getAvMoves()[j].prot==-1)&&whitePlayer.getAvMoves()[j].posId==getLastMovFig()&&
                             whitePlayer.getAvMoves()[j].isCheck==0)){
                        p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                        blackPlayer.setCheckAvMoves(p);
                    }
                }
            }
            else if(dir==-1&&dir2!=-1&&blackPlayer.getAvMoves()[i].posId!=BK&&blackPlayer.getAvMoves()[i].prot==-1&&
                    !blackPlayer.getAvMoves()[i].pawnHit){
                for(unsigned int j=0;j<whitePlayer.getAvMoves().size();j++){
                    if(((whitePlayer.getAvMoves()[j].row==blackPlayer.getAvMoves()[i].row)&&
                        (whitePlayer.getAvMoves()[j].col==blackPlayer.getAvMoves()[i].col)&&
                        (whitePlayer.getAvMoves()[j].dirId==dir2&&whitePlayer.getAvMoves()[j].dirId!=H_DIR)&&
                        (whitePlayer.getAvMoves()[j].prot==-1)&&(whitePlayer.getAvMoves()[j].posId==getOpenCheckFig())&&
                        whitePlayer.getAvMoves()[j].isCheck==0)||
                            ((whitePlayer.getAvMoves()[j].removedRow==blackPlayer.getAvMoves()[i].row)&&
                             whitePlayer.getAvMoves()[j].removedRow!=-1&&
                             (whitePlayer.getAvMoves()[j].removedCol==blackPlayer.getAvMoves()[i].col)&&
                             whitePlayer.getAvMoves()[j].removedCol!=' '&&
                             (whitePlayer.getAvMoves()[j].dirId==dir2&&whitePlayer.getAvMoves()[j].dirId!=H_DIR)&&
                             (whitePlayer.getAvMoves()[j].prot==-1)&&(whitePlayer.getAvMoves()[j].posId==getOpenCheckFig())&&
                             whitePlayer.getAvMoves()[j].isCheck==0)){
                        p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                        blackPlayer.setCheckAvMoves(p);
                    }
                }
            }
            else if(dir!=-1&&dir2!=-1&&blackPlayer.getAvMoves()[i].posId!=BK&&blackPlayer.getAvMoves()[i].prot==-1&&
                    !blackPlayer.getAvMoves()[i].pawnHit){
                for(unsigned int j=0;j<whitePlayer.getAvMoves().size();j++){
                    if((((whitePlayer.getAvMoves()[j].row==blackPlayer.getAvMoves()[i].row)&&
                         (whitePlayer.getAvMoves()[j].col==blackPlayer.getAvMoves()[i].col)&&
                         (whitePlayer.getAvMoves()[j].dirId==dir&&whitePlayer.getAvMoves()[j].dirId!=H_DIR)&&
                         (whitePlayer.getAvMoves()[j].prot==-1)&&(whitePlayer.getAvMoves()[j].posId==getLastMovFig())&&
                         whitePlayer.getAvMoves()[j].isCheck==0)&&
                        ((whitePlayer.getAvMoves()[j].row==blackPlayer.getAvMoves()[i].row)&&
                         (whitePlayer.getAvMoves()[j].col==blackPlayer.getAvMoves()[i].col)&&
                         (whitePlayer.getAvMoves()[j].dirId==dir2&&whitePlayer.getAvMoves()[j].dirId!=H_DIR)&&
                         (whitePlayer.getAvMoves()[j].prot==-1)&&(whitePlayer.getAvMoves()[j].posId==getOpenCheckFig())&&
                         whitePlayer.getAvMoves()[j].isCheck==0))){
                        p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                        blackPlayer.setCheckAvMoves(p);
                    }
                }
            }
            if(dir!=-1&&dir2==-1&&(blackPlayer.getAvMoves()[i].posId!=BK)&&(((blackPlayer.getAvMoves()[i].row==pCh.row)&&
                                                                             (blackPlayer.getAvMoves()[i].col==pCh.col))||(blackPlayer.getAvMoves()[i].enPassant==pCh.posId))){
                p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                p.enPassant = blackPlayer.getAvMoves()[i].enPassant;
                blackPlayer.setCheckAvMoves(p);
            }
            else if(dir==-1&&dir2!=-1&&(blackPlayer.getAvMoves()[i].posId!=BK)&&(blackPlayer.getAvMoves()[i].row==pOpen.row)&&
                    (blackPlayer.getAvMoves()[i].col==pOpen.col)){
                p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                blackPlayer.setCheckAvMoves(p);
            }
            else if(dir!=-1&&dir2!=-1&&(blackPlayer.getAvMoves()[i].posId!=BK)&&(blackPlayer.getAvMoves()[i].row==pOpen.row)&&
                    (blackPlayer.getAvMoves()[i].col==pOpen.col)&&(blackPlayer.getAvMoves()[i].row==pCh.row)&&
                    (blackPlayer.getAvMoves()[i].col==pCh.col)){
                p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                blackPlayer.setCheckAvMoves(p);
            }
        }
        bool rookMovedK = false;
        bool rookMovedQ = false;
        for(unsigned int k=0;k<getBlackFigs().size();k++){
            if(getBlackFigs()[k]->getId()==BR2){
                Rook r = static_cast<Rook>(*(getBlackFigs()[k]));
                if(r.hasmoved()){rookMovedK = true;}
            }
            else if(getBlackFigs()[k]->getId()==BR1){
                Rook r = static_cast<Rook>(*(getBlackFigs()[k]));
                if(r.hasmoved()){rookMovedQ = true;}
            }
        }
        for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
            if((blackPlayer.getAvMoves()[i].posId==BK)&&(!blackPlayer.getAvMoves()[i].isTake)&&blackPlayer.getAvMoves()[i].prot==-1){
                bool isUnderHit = false;
                for(unsigned int j=0;j<whitePlayer.getAvMoves().size();j++){
                    if(getTable()[7][chooseNumLetter('E')]==BK&&blackPlayer.getAvMoves()[i].row==7&&blackPlayer.getAvMoves()[i].col=='G'){
                        if((whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='F'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='E'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='G'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='F'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='E'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='G'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (rookMovedK)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='F'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='E'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='G'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='F'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='E'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='G'&&whitePlayer.getAvMoves()[j].pawnHit)){
                            isUnderHit = true;
                            break;
                        }
                    }
                    else if(getTable()[7][chooseNumLetter('E')]==BK&&blackPlayer.getAvMoves()[i].row==7&&blackPlayer.getAvMoves()[i].col=='C'){
                        if((whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='D'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='E'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='C'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='D'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='E'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='C'&&whitePlayer.getAvMoves()[j].dirId!=P_UP)||
                                (rookMovedQ)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='D'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='E'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].row==7&&whitePlayer.getAvMoves()[j].col=='C'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='D'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='E'&&whitePlayer.getAvMoves()[j].pawnHit)||
                                (whitePlayer.getAvMoves()[j].removedRow==7&&whitePlayer.getAvMoves()[j].removedCol=='C'&&whitePlayer.getAvMoves()[j].pawnHit)){
                            isUnderHit = true;
                            break;
                        }
                    }
                    else if(((whitePlayer.getAvMoves()[j].dirId!=P_UP)&&(blackPlayer.getAvMoves()[i].row==whitePlayer.getAvMoves()[j].row)&&
                             (blackPlayer.getAvMoves()[i].col==whitePlayer.getAvMoves()[j].col))||
                            ((whitePlayer.getAvMoves()[j].dirId!=P_UP)&&(blackPlayer.getAvMoves()[i].row==whitePlayer.getAvMoves()[j].removedRow)&&
                             (blackPlayer.getAvMoves()[i].col==whitePlayer.getAvMoves()[j].removedCol)&&
                             whitePlayer.getAvMoves()[j].removedRow!=-1&&whitePlayer.getAvMoves()[j].removedCol!=' ')){
                        isUnderHit = true;
                        break;
                    }
                }
                if(isUnderHit==false){
                    Position p;p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                    blackPlayer.setCheckAvMoves(p);
                }
            }
        }
        for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
            bool protect = false;
            if(blackPlayer.getAvMoves()[i].posId==BK&&blackPlayer.getAvMoves()[i].isTake&&blackPlayer.getAvMoves()[i].prot==-1){
                int id = getTable()[blackPlayer.getAvMoves()[i].row!=-1?blackPlayer.getAvMoves()[i].row:blackPlayer.getAvMoves()[i].removedRow][chooseNumLetter(blackPlayer.getAvMoves()[i].col!=' '?blackPlayer.getAvMoves()[i].col:blackPlayer.getAvMoves()[i].removedCol)];
                for(unsigned int k=0;k<whitePlayer.getAvMoves().size();k++){
                    if(whitePlayer.getAvMoves()[k].prot==id){
                        protect = true;
                        break;
                    }
                    if(((blackPlayer.getAvMoves()[i].row==whitePlayer.getAvMoves()[k].row)&&
                        (blackPlayer.getAvMoves()[i].col==whitePlayer.getAvMoves()[k].col)&&whitePlayer.getAvMoves()[k].isCheck==1)||
                            ((blackPlayer.getAvMoves()[i].row==whitePlayer.getAvMoves()[k].removedRow)&&
                             (blackPlayer.getAvMoves()[i].col==whitePlayer.getAvMoves()[k].removedCol)&&whitePlayer.getAvMoves()[k].isCheck==1&&
                             whitePlayer.getAvMoves()[k].removedRow!=-1&&whitePlayer.getAvMoves()[k].removedCol!=' ')){
                        protect = true;
                        break;
                    }
                }
                if(protect==false){
                    Position p;p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                    blackPlayer.setCheckAvMoves(p);
                }
            }
        }
        for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
            if(blackPlayer.getAvMoves()[i].posId!=BK&&blackPlayer.getAvMoves()[i].isTake&&blackPlayer.getAvMoves()[i].prot==-1){
                for(unsigned int k=0;k<whitePlayer.getAvMoves().size();k++){
                    int row = whitePlayer.getAvMoves()[k].row!=-1?whitePlayer.getAvMoves()[k].row:whitePlayer.getAvMoves()[k].removedRow;
                    char col = whitePlayer.getAvMoves()[k].col!=' '?whitePlayer.getAvMoves()[k].col:whitePlayer.getAvMoves()[k].removedCol;
                    if((whitePlayer.getAvMoves().at(k).posId==getLastMovFig()&&whitePlayer.getAvMoves().at(k).isCheck==1
                        &&blackPlayer.getAvMoves()[i].row==whitePlayer.getAvMoves().at(k).row&&
                        blackPlayer.getAvMoves()[i].col==whitePlayer.getAvMoves().at(k).col&&
                        getTable()[row][chooseNumLetter(col)]==getLastMovFig())||
                            (whitePlayer.getAvMoves().at(k).posId==getLastMovFig()&&whitePlayer.getAvMoves().at(k).isCheck==1
                             &&blackPlayer.getAvMoves()[i].row==whitePlayer.getAvMoves().at(k).removedRow&&
                             whitePlayer.getAvMoves().at(k).removedRow!=-1&&
                             blackPlayer.getAvMoves()[i].col==whitePlayer.getAvMoves().at(k).removedCol&&
                             whitePlayer.getAvMoves().at(k).removedCol!=' '&&
                             getTable()[row][chooseNumLetter(col)]==getLastMovFig())){
                        Position p;p.setP(blackPlayer.getAvMoves()[i].row,blackPlayer.getAvMoves()[i].col,blackPlayer.getAvMoves()[i].posId);
                        blackPlayer.setCheckAvMoves(p);
                    }
                }
            }
        }
    }
}

bool StartPosGraph::move(Position& start, Position& dest, char color, int &pawnTransfer, bool &isKCastle, bool &isQCastle, char &fig){
    castle = ' ';
    fiftyMovesTake = false;
    //    //Move Back
    BackMoveAdj back;
    for(unsigned int i=0;i<whiteFigs.size();++i){
        back.whiteFigsBack.push_back(std::make_shared< Figure>());
    }
    for(unsigned int i=0;i<blackFigs.size();++i){
        back.blackFigsBack.push_back(std::make_shared< Figure>());
    }
    for(int i=0;i<BOARD_SIZE;++i){
        for(int j=0;j<BOARD_SIZE;++j){
            back.adjMatrixBack[i][j] = adjMatrix[i][j];
        }
    }
    for(unsigned int i=0;i<whiteFigs.size();++i){
        *(back.whiteFigsBack[i]) = *(whiteFigs[i]);
    }
    for(unsigned int i=0;i<blackFigs.size();++i){
        *(back.blackFigsBack[i]) = *(blackFigs[i]);
    }
    back.lastMovFig=lastMovFig;
    back.prevLastMovFig=prevLastMovFig;
    back.openCheckFig=openCheckFig;
    if(whitePlayer.checked())
        back.whiteChecked = true;
    else if(blackPlayer.checked())
        back.blackChecked = true;
    backMoveData.push(back);

    int destId=-1;
    int startId = -1;
    bool b = false;
    for(int i=0;i<BOARD_SIZE;i++){
        for(int j=0;j<BOARD_SIZE;j++){
            if(dest.row==i&&chooseNumLetter(dest.col)==j){
                destId = getTable()[i][j];
                b = true;
                break;
            }
        }
        if(b)
            break;
    }
    b = false;
    for(int i=0;i<BOARD_SIZE;i++){
        for(int j=0;j<BOARD_SIZE;j++){
            if(start.row==i&&chooseNumLetter(start.col)==j){
                startId = getTable()[i][j];
                b = true;
                break;
            }
        }
        if(b)
            break;
    }
    for(unsigned int i=0;i<getWhiteFigs().size();i++){
        //White Pawns
        int enpassantCheckFigure = -1;
        int figId = getWhiteFigs()[i]->getId();
        if(figId>=WP_FIRST&&figId<=WP_LAST&&figId==startId){
            Pawn pawn = static_cast<Pawn>(*(getWhiteFigs()[i]));
            char moveResult = moveFig(start, dest, color, pawn, destId, startId, enpassantCheckFigure);
            if(moveResult=='Y'||moveResult=='T'||moveResult=='E'){
                std::shared_ptr<Pawn> p = std::make_shared< Pawn>(pawn.getTitle(),dest,0,pawn.getId(),pawn.getColor());
                if(enpassantCheckFigure==-1)
                    setLastMovFig(p->getId());
                else
                    setLastMovFig(prevLastMovFig);

                p->setStartPosWhite(false);
                *(getWhiteFigs().at(i)) = *(p);
                setAllFigsAvMoves();
                return true;
            }
            else if(moveResult=='P'||moveResult=='F'){
                if(fig=='Q'){
                    std::shared_ptr<Queen> q = std::make_shared< Queen>("w_Queen_new",dest,0,((pawn.getId()*100)+WQ),pawn.getColor());
                    getTable()[dest.row][chooseNumLetter(dest.col)] = (pawn.getId()*100)+WQ;
                    setLastMovFig(q->getId());
                    *(getWhiteFigs().at(i)) = *(q);
                    pawnTransfer = (pawn.getId()*100)+WQ;
                    setAllFigsAvMoves();
                    return true;
                }
                else if(fig=='R'){
                    std::shared_ptr<Rook> r = std::make_shared< Rook>("w_Rook_new",dest,0,((pawn.getId()*100)+WR1),pawn.getColor());
                    getTable()[dest.row][chooseNumLetter(dest.col)] = (pawn.getId()*100)+WR1;
                    r->setMoved(true);
                    setLastMovFig(r->getId());
                    *(getWhiteFigs().at(i)) = *(r);
                    pawnTransfer = (pawn.getId()*100)+WR1;
                    setAllFigsAvMoves();
                    return true;
                }
                else if(fig=='B'){
                    std::shared_ptr<Bishop> o = std::make_shared< Bishop>("w_Officer_new",dest,0,((pawn.getId()*100)+WO1),pawn.getColor());
                    getTable()[dest.row][chooseNumLetter(dest.col)] = (pawn.getId()*100)+WO1;
                    setLastMovFig(o->getId());
                    *(getWhiteFigs().at(i)) = *(o);
                    pawnTransfer = (pawn.getId()*100)+WO1;
                    setAllFigsAvMoves();
                    return true;
                }
                else if(fig=='N'){
                    std::shared_ptr<Knight> h = std::make_shared< Knight>("w_Horse_new",dest,0,((pawn.getId()*100)+WH1),pawn.getColor());
                    getTable()[dest.row][chooseNumLetter(dest.col)] = (pawn.getId()*100)+WH1;
                    setLastMovFig(h->getId());
                    *(getWhiteFigs().at(i)) = *(h);
                    pawnTransfer = (pawn.getId()*100)+WH1;
                    setAllFigsAvMoves();
                    return true;
                }
            }
            return false;
        }
        //White Rooks
        else if(((figId==WR1||figId==WR2)&&figId==startId)||((figId%100)==WR1&&figId==startId)){
            Rook rook = static_cast<Rook>(*(getWhiteFigs()[i]));
            if(moveFigure(start, dest, color, rook, destId,isKCastle,isQCastle)){
                std::shared_ptr<Rook> r = std::make_shared< Rook>(rook.getTitle(),dest,0,rook.getId(),rook.getColor());
                setLastMovFig(r->getId());
                r->setMoved(true);
                *(getWhiteFigs().at(i)) = *(r);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
        //White Officers
        else if(((figId==WO1||figId==WO2)&&figId==startId)||((figId%100)==WO1&&figId==startId)){
            Bishop oficer = static_cast<Bishop>(*(getWhiteFigs()[i]));
            if(moveFigure(start, dest, color, oficer, destId,isKCastle,isQCastle)){
                std::shared_ptr<Bishop> o = std::make_shared< Bishop>(oficer.getTitle(),dest,0,oficer.getId(),oficer.getColor());
                setLastMovFig(o->getId());
                *(getWhiteFigs().at(i)) = *(o);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
        //White Queen
        else if((figId==WQ&&figId==startId)||((figId%100)==WQ&&figId==startId)){
            Queen queen = static_cast<Queen>(*(getWhiteFigs()[i]));
            if(moveFigure(start, dest, color, queen, destId,isKCastle,isQCastle)){
                std::shared_ptr<Queen> q = std::make_shared< Queen>(queen.getTitle(),dest,0,queen.getId(),queen.getColor());
                setLastMovFig(q->getId());
                *(getWhiteFigs().at(i)) = *(q);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
        //White Knight
        else if(((figId==WH1||figId==WH2)&&figId==startId)||((figId%100)==WH1&&figId==startId)){
            Knight horse = static_cast<Knight>(*(getWhiteFigs()[i]));
            if(moveFigure(start, dest, color, horse, destId,isKCastle,isQCastle)){
                std::shared_ptr<Knight> h = std::make_shared< Knight>(horse.getTitle(),dest,0,horse.getId(),horse.getColor());
                setLastMovFig(h->getId());
                *(getWhiteFigs().at(i)) = *(h);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
        //White King
        else if(figId==WK&&figId==startId){
            King king = static_cast<King>(*(getWhiteFigs()[i]));
            if(moveFigure(start, dest, color, king, destId,isKCastle,isQCastle)){
                king.setMoved(true);
                std::shared_ptr<King> k = std::make_shared< King>(king.getTitle(),dest,0,king.getId(),king.getColor());
                if(start.row!=0&&start.col!='E'&&dest.row!=0&&dest.col!='C'&&dest.col!='G'){
                    setLastMovFig(k->getId());
                }
                k->setChecked(king.check());
                k->setKingsCastle(king.isKingCastle());
                k->setQueensCastle(king.isQueenCastle());
                k->setHasAvMoves(king.hasAvMoves());
                k->setMoved(true);
                *(getWhiteFigs().at(i)) = *(k);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
    }
    for(unsigned int i=0;i<getBlackFigs().size();i++){
        int enpassantCheckFigure = -1;
        int figId = getBlackFigs()[i]->getId();
        //Black Pawns
        if(figId>=BP_FIRST&&figId<=BP_LAST&&figId==startId){
            Pawn pawn = static_cast<Pawn>(*(getBlackFigs()[i]));
            char moveResult = moveFig(start, dest, color, pawn, destId, startId, enpassantCheckFigure);
            if(moveResult=='Y'||moveResult=='T'||moveResult=='E'){
                std::shared_ptr<Pawn> p = std::make_shared< Pawn>(pawn.getTitle(),dest,0,pawn.getId(),pawn.getColor());
                if(enpassantCheckFigure==-1)
                    setLastMovFig(p->getId());
                else
                    setLastMovFig(prevLastMovFig);

                p->setStartPosBlack(false);
                *(getBlackFigs().at(i)) = *(p);
                setAllFigsAvMoves();
                return true;
            }
            else if(moveResult=='P'||moveResult=='F'){
                if(fig=='Q'){
                    std::shared_ptr<Queen> q = std::make_shared< Queen>("b_Queen_new",dest,0,((pawn.getId()*100)+BQ),pawn.getColor());
                    getTable()[dest.row][chooseNumLetter(dest.col)] = (pawn.getId()*100)+BQ;
                    setLastMovFig(q->getId());
                    *(getBlackFigs().at(i)) = *(q);
                    pawnTransfer = (pawn.getId()*100)+BQ;
                    setAllFigsAvMoves();
                    return true;
                }
                else if(fig=='R'){
                    std::shared_ptr<Rook> r = std::make_shared< Rook>("b_Rook_new",dest,0,((pawn.getId()*100)+BR1),pawn.getColor());
                    getTable()[dest.row][chooseNumLetter(dest.col)] = (pawn.getId()*100)+BR1;
                    r->setMoved(true);
                    setLastMovFig(r->getId());
                    *(getBlackFigs().at(i)) = *(r);
                    pawnTransfer = (pawn.getId()*100)+BR1;
                    setAllFigsAvMoves();
                    return true;
                }
                else if(fig=='B'){
                    std::shared_ptr<Bishop> o = std::make_shared< Bishop>("b_Officer_new",dest,0,((pawn.getId()*100)+BO1),pawn.getColor());
                    getTable()[dest.row][chooseNumLetter(dest.col)] = (pawn.getId()*100)+BO1;
                    setLastMovFig(o->getId());
                    *(getBlackFigs().at(i)) = *(o);
                    pawnTransfer = (pawn.getId()*100)+BO1;
                    setAllFigsAvMoves();
                    return true;
                }
                else if(fig=='N'){
                    std::shared_ptr<Knight> h = std::make_shared< Knight>("b_Horse_new",dest,0,((pawn.getId()*100)+BH1),pawn.getColor());
                    getTable()[dest.row][chooseNumLetter(dest.col)] = (pawn.getId()*100)+BH1;
                    setLastMovFig(h->getId());
                    *(getBlackFigs().at(i)) = *(h);
                    pawnTransfer = (pawn.getId()*100)+BH1;
                    setAllFigsAvMoves();
                    return true;
                }
            }
            return false;
        }
        //Black Rooks
        else if(((figId==BR1||figId==BR2)&&figId==startId)||((figId%100)==BR1&&figId==startId)){
            Rook rook = static_cast<Rook>(*(getBlackFigs()[i]));
            if(moveFigure(start, dest, color, rook, destId,isKCastle,isQCastle)){
                std::shared_ptr<Rook> r = std::make_shared< Rook>(rook.getTitle(),dest,0,rook.getId(),rook.getColor());
                setLastMovFig(r->getId());
                r->setMoved(true);
                *(getBlackFigs().at(i)) = *(r);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
        //Black Officers
        else if(((figId==BO1||figId==BO2)&&figId==startId)||((figId%100)==BO1&&figId==startId)){
            Bishop oficer= static_cast<Bishop>(*(getBlackFigs()[i]));
            if(moveFigure(start, dest, color, oficer, destId,isKCastle,isQCastle)){
                std::shared_ptr<Bishop> o = std::make_shared< Bishop>(oficer.getTitle(),dest,0,oficer.getId(),oficer.getColor());
                setLastMovFig(o->getId());
                *(getBlackFigs().at(i)) = *(o);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
        //Black Queen
        else if((figId==BQ&&figId==startId)||((figId%100)==BQ&&figId==startId)){
            Queen queen = static_cast<Queen>(*(getBlackFigs()[i]));
            if(moveFigure(start, dest, color, queen, destId,isKCastle,isQCastle)){
                std::shared_ptr<Queen> q = std::make_shared< Queen>(queen.getTitle(),dest,0,queen.getId(),queen.getColor());
                setLastMovFig(q->getId());
                *(getBlackFigs().at(i)) = *(q);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
        //Black Knight
        else if(((figId==BH1||figId==BH2)&&figId==startId)||((figId%100)==BH1&&figId==startId)){
            Knight horse = static_cast<Knight>(*(getBlackFigs()[i]));
            if(moveFigure(start, dest, color, horse, destId,isKCastle,isQCastle)){
                std::shared_ptr<Knight> h = std::make_shared< Knight>(horse.getTitle(),dest,0,horse.getId(),horse.getColor());
                setLastMovFig(h->getId());
                *(getBlackFigs().at(i)) = *(h);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
        //Black King
        else if(figId==BK&&figId==startId){
            King king = static_cast<King>(*(getBlackFigs()[i]));
            if(moveFigure(start, dest, color, king, destId,isKCastle,isQCastle)){
                std::shared_ptr<King> k = std::make_shared< King>(king.getTitle(),dest,0,king.getId(),king.getColor());
                if(start.row!=7&&start.col!='E'&&dest.row!=7&&dest.col!='C'&&dest.col!='G'){
                    setLastMovFig(k->getId());
                }
                k->setChecked(king.check());
                k->setKingsCastle(king.isKingCastle());
                k->setQueensCastle(king.isQueenCastle());
                k->setHasAvMoves(king.hasAvMoves());
                k->setMoved(true);
                *(getBlackFigs().at(i)) = *(k);
                setAllFigsAvMoves();
                return true;
            }
            return false;
        }
    }
    return false;
}
void StartPosGraph::findFigureAvailableMoves(std::vector<AvMove> &avMoves, int figId, char &playerColor){
    if(playerColor=='w'){
        if(((figId>=WR1&&figId<=WO2)||(figId>=WP_TRANSF_FIRST&&figId<=WP_TRANSF_LAST))&&(!whitePlayer.checked())&&(figId!=-1)){
            for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
                AvMove avMove;
                if((whitePlayer.getAvMoves().at(i).posId!=WK)&&(whitePlayer.getAvMoves().at(i).posId==figId)&&(whitePlayer.getAvMoves().at(i).prot==-1)&&
                        (!whitePlayer.getAvMoves().at(i).pawnHit)){
                    avMove.figId = figId;
                    avMove.row = whitePlayer.getAvMoves().at(i).row;
                    avMove.col = chooseNumLetter(whitePlayer.getAvMoves().at(i).col);
                    if(whitePlayer.getAvMoves().at(i).enPassant!=NO_EDGE){
                        avMove.enpId = whitePlayer.getAvMoves().at(i).enPassant;
                    }
                    else{
                        avMove.enpId = NO_EDGE;
                    }
                    avMoves.push_back(avMove);
                }
                else if(figId==WK&&whitePlayer.getAvMoves().at(i).posId==WK){
                    for(unsigned int j=0;j<whitePlayer.getCheckAvMoves().size();j++){
                        if(whitePlayer.getCheckAvMoves().at(j).posId==WK){
                            avMove.figId = figId;
                            avMove.row = whitePlayer.getCheckAvMoves().at(j).row;
                            avMove.col = chooseNumLetter(whitePlayer.getCheckAvMoves().at(j).col);
                            avMove.enpId = NO_EDGE;
                            avMoves.push_back(avMove);
                        }
                    }
                }
            }
        }
        else if(((figId>=WR1&&figId<=WO2)||(figId>=WP_TRANSF_FIRST&&figId<=WP_TRANSF_LAST))&&whitePlayer.checked()){
            for(unsigned int i=0;i<whitePlayer.getCheckAvMoves().size();i++){
                AvMove avMove;
                if(figId==whitePlayer.getCheckAvMoves().at(i).posId){
                    avMove.figId = figId;
                    avMove.row = whitePlayer.getCheckAvMoves().at(i).row;
                    avMove.col = chooseNumLetter(whitePlayer.getCheckAvMoves().at(i).col);
                    if(whitePlayer.getCheckAvMoves().at(i).enPassant!=NO_EDGE){
                        avMove.enpId = whitePlayer.getCheckAvMoves().at(i).enPassant;
                    }
                    else{
                        avMove.enpId = NO_EDGE;
                    }
                    avMoves.push_back(avMove);
                }
            }
        }
    }
    else if(playerColor=='b'){
        if(((figId>=BR1&&figId<=BO2)||(figId>=BP_TRANSF_FIRST&&figId<=BP_TRANSF_LAST))&&(!blackPlayer.checked())&&(figId!=-1)){
            for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
                AvMove avMove;
                if((blackPlayer.getAvMoves().at(i).posId!=BK)&&(blackPlayer.getAvMoves().at(i).posId==figId)&&(blackPlayer.getAvMoves().at(i).prot==-1)&&
                        (!blackPlayer.getAvMoves().at(i).pawnHit)){
                    avMove.figId = figId;
                    avMove.row = blackPlayer.getAvMoves().at(i).row;
                    avMove.col = chooseNumLetter(blackPlayer.getAvMoves().at(i).col);
                    if(blackPlayer.getAvMoves().at(i).enPassant!=NO_EDGE){
                        avMove.enpId = blackPlayer.getAvMoves().at(i).enPassant;
                    }
                    else{
                        avMove.enpId = NO_EDGE;
                    }
                    avMoves.push_back(avMove);
                }
                else if(figId==BK&&blackPlayer.getAvMoves().at(i).posId==BK){
                    for(unsigned int j=0;j<blackPlayer.getCheckAvMoves().size();j++){
                        if(blackPlayer.getCheckAvMoves().at(j).posId==BK){
                            avMove.figId = figId;
                            avMove.row = blackPlayer.getCheckAvMoves().at(j).row;
                            avMove.col = chooseNumLetter(blackPlayer.getCheckAvMoves().at(j).col);
                            avMove.enpId = NO_EDGE;
                            avMoves.push_back(avMove);
                        }
                    }
                }
            }
        }
        else if(((figId>=BR1&&figId<=BO2)||(figId>=BP_TRANSF_FIRST&&figId<=BP_TRANSF_LAST))&&blackPlayer.checked()){
            for(unsigned int i=0;i<blackPlayer.getCheckAvMoves().size();i++){
                AvMove avMove;
                if(figId==blackPlayer.getCheckAvMoves().at(i).posId){
                    avMove.figId = figId;
                    avMove.row = blackPlayer.getCheckAvMoves().at(i).row;
                    avMove.col = chooseNumLetter(blackPlayer.getCheckAvMoves().at(i).col);
                    if(blackPlayer.getCheckAvMoves().at(i).enPassant!=NO_EDGE){
                        avMove.enpId = blackPlayer.getCheckAvMoves().at(i).enPassant;
                    }
                    else{
                        avMove.enpId = NO_EDGE;
                    }
                    avMoves.push_back(avMove);
                }
            }
        }
    }
}
void StartPosGraph::gameInit(int &pawnTransfer, std::atomic<bool> &gameEnd, string &gameEndResult, QString &gameEndResultTr, char &playerColor, int sRow, int sCol, int dRow,
                             int dCol, bool &isKCastle, bool &isQCastle,char &fig){
    Position start;
    Position dest;
    //init();
    //printGraph();
    if(whitePlayer.isTurn()){
        start.col = chooseCol(sCol);
        start.row = (BOARD_SIZE-1)-sRow;
        if((start.row>=0&&start.row<=(BOARD_SIZE-1))&&(chooseNumLetter(start.col)>=0&&chooseNumLetter(start.col)<=(BOARD_SIZE-1))){
            for(unsigned int i=0;i<getOpenChMoves().size();i++){
                if(getTable()[start.row][chooseNumLetter(start.col)]!=-1&&
                        (getOpenChMoves()[i].posId==getTable()[start.row][chooseNumLetter(start.col)])&&getOpenChMoves()[i].row>=0){
                    setOpenCheckFig(getTable()[getOpenChMoves()[i].row][chooseNumLetter(getOpenChMoves()[i].col)]);
                }
            }
        }
        dest.col = chooseCol(dCol);
        dest.row = (BOARD_SIZE-1)-dRow;
        startGame(gameEndResult,gameEndResultTr,pawnTransfer,start,dest,playerColor,isKCastle,isQCastle,fig);
        checkForDrawPosition();
        if(blackPlayer.stalemate()||whitePlayer.stalemate()){
            if(counterForFiftyMovesDraw>=98||counterForFiftyMovesDrawPawns>=98){
                gameEndResult = "50 moves rule!Game Draw!";
                gameEndResultTr = QObject::tr("50 moves rule!Game Draw!");
            }
            else{
                gameEndResult = "Draw Position!Game Draw!";
                gameEndResultTr = QObject::tr("Draw Position!Game Draw!");
            }
            return;
        }
        if(blackPlayer.checked()){
            gameEndResult = "Black is Check!";
            gameEndResultTr = QObject::tr("Black is Check!");
        }
        if(blackPlayer.checked()&&blackPlayer.checkAvMovesCnt()==0){
            blackPlayer.setMate(true);
            whitePlayer.setWin(true);
            gameEndResult = "Checkmate!White Wins!";
            gameEndResultTr = QObject::tr("Checkmate!White Wins!");
            gameEnd.store(true);
            return;
        }
        else if(!blackPlayer.checked()&&blackPlayer.checkAvMovesCnt()==0&&!checkForStalemate('b')){
            whitePlayer.setStalemate(true);
            gameEndResult = "Stalemate!Game Draw!";
            gameEndResultTr = QObject::tr("Stalemate!Game Draw!");
            gameEnd.store(true);
            return;
        }
    }
    else if(blackPlayer.isTurn()){
        start.col = chooseCol(sCol);
        start.row = (BOARD_SIZE-1)-sRow;
        if((start.row>=0&&start.row<=(BOARD_SIZE-1))&&(chooseNumLetter(start.col)>=0&&chooseNumLetter(start.col)<=(BOARD_SIZE-1))){
            for(unsigned int i=0;i<getOpenChMoves().size();i++){
                if(getTable()[start.row][chooseNumLetter(start.col)]!=-1&&
                        (getOpenChMoves()[i].posId==getTable()[start.row][chooseNumLetter(start.col)])&&getOpenChMoves()[i].row>=0){
                    setOpenCheckFig(getTable()[getOpenChMoves()[i].row][chooseNumLetter(getOpenChMoves()[i].col)]);
                }
            }
        }
        dest.col = chooseCol(dCol);
        dest.row = (BOARD_SIZE-1)-dRow;
        startGame(gameEndResult,gameEndResultTr,pawnTransfer,start,dest,playerColor,isKCastle,isQCastle,fig);
        checkForDrawPosition();
        if(blackPlayer.stalemate()||whitePlayer.stalemate()){
            if(counterForFiftyMovesDraw>=98||counterForFiftyMovesDrawPawns>=98){
                gameEndResult = "50 moves rule!Game Draw!";
                gameEndResultTr = QObject::tr("50 moves rule!Game Draw!");
            }
            else{
                gameEndResult = "Draw Position!Game Draw!";
                gameEndResultTr = QObject::tr("Draw Position!Game Draw!");
            }
            gameEnd.store(true);
            return;
        }
        if(whitePlayer.checked()){
            gameEndResult = "White is Check!";
            gameEndResultTr = QObject::tr("White is Check!");
        }
        if(whitePlayer.checked()&&whitePlayer.checkAvMovesCnt()==0){
            whitePlayer.setMate(true);
            blackPlayer.setWin(true);
            gameEndResult = "Checkmate!Black Wins!";
            gameEndResultTr = QObject::tr("Checkmate!Black Wins!");
            gameEnd.store(true);
            return;
        }
        else if(!whitePlayer.checked()&&whitePlayer.checkAvMovesCnt()==0&&!checkForStalemate('w')){
            whitePlayer.setStalemate(true);
            gameEndResult = "Stalemate!Game Draw!";
            gameEndResultTr = QObject::tr("Stalemate!Game Draw!");
            gameEnd.store(true);
            return;
        }
    }
}

void StartPosGraph::startGame(string &gameEndResult, QString &gameEndResultTr, int &pawnTransfer, Position &start, Position &dest, char &playerColor, bool &isKCastle, bool &isQCastle, char &fig){
    if(playerColor=='w'){
        if(!move(start,dest,playerColor,pawnTransfer,isKCastle,isQCastle,fig)){
            gameEndResult = "Not available move while White is Check!";
            gameEndResultTr = QObject::tr("Not available move while White is Check!");
            return;
        }
        whitePlayer.setTurn(false);
        blackPlayer.setTurn(true);
        gameEndResult = "";
        gameEndResultTr = "";
        playerColor = 'b';
        init();
        sayCheck('w');
    }
    else if(playerColor=='b'){
        if(!move(start,dest,playerColor,pawnTransfer,isKCastle,isQCastle,fig)){
            gameEndResult = "Not available move while Black is Check!";
            gameEndResultTr = QObject::tr("Not available move while Black is Check!");
            return;
        }
        blackPlayer.setTurn(false);
        whitePlayer.setTurn(true);
        gameEndResult = "";
        gameEndResultTr = "";
        playerColor = 'w';
        init();
        sayCheck('b');
    }
}
void StartPosGraph::setOpenCheckFig(int openCh){
    openCheckFig = openCh;
}
int StartPosGraph::getOpenCheckFig()const{
    return openCheckFig;
}
void StartPosGraph::sayCheck(char color){
    if(color=='w'){
        int cnt = 0;
        for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
            if(whitePlayer.getAvMoves()[i].posId==getLastMovFig()&&whitePlayer.getAvMoves()[i].isCheck){
                blackPlayer.setCheck(true);
                cnt++;
            }
            if(whitePlayer.getAvMoves()[i].posId==getOpenCheckFig()&&whitePlayer.getAvMoves()[i].isCheck){
                blackPlayer.setCheck(true);
                cnt++;
            }
        }
        if(cnt==2){
            blackPlayer.setDoubleCheck(true);
        }
    }
    else if(color=='b'){
        int cnt = 0;
        for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
            if(blackPlayer.getAvMoves()[i].posId==getLastMovFig()&&blackPlayer.getAvMoves()[i].isCheck){
                whitePlayer.setCheck(true);
                cnt++;
            }
            if(blackPlayer.getAvMoves()[i].posId==getOpenCheckFig()&&blackPlayer.getAvMoves()[i].isCheck){
                whitePlayer.setCheck(true);
                cnt++;
            }
        }
        if(cnt==2){
            whitePlayer.setDoubleCheck(true);
        }
    }
}
template<typename T> bool StartPosGraph::moveFigure(Position &start, Position &dest, char color, T& figure, int destId, bool &isKCastle, bool &isQCastle){
    if(color=='w'){
        if(whitePlayer.checked()){
            if(whitePlayer.isCheckAvMove(dest)){
                for(size_t i=0;i<openChMoves.size();i++){
                    if(openChMoves.at(i).posId==figure.getId()&&!openChMoves.at(i).isRemoved){//TEST OPENCHECK
                        isOpenCheck = true;
                        break;
                    }
                }
                char moveRes = figure.move(dest,getTable(),figure.getColor());
                if(moveRes=='N'){
                    return false;
                }
                if(moveRes=='T'){
                    removeFigure('b',destId);
                }
                else if(moveRes=='C'){
                }
                whitePlayer.setCheck(false);

                if(isOpenCheck){//TEST OPENCHECK
                    blackPlayer.setCheck(true);
                    isOpenCheck = false;
                }
                else {
                    if(getOpenCheckFig()!=-1){
                        setOpenCheckFig(-1);
                    }
                }

            }
            return true;
        }
        else{
            if(figure.getTitle()=="w_King"&&!figure.hasmoved()&&!figure.check()){
                if(dest.row==0&&dest.col=='G'){
                    bool isUnderHit = false;
                    for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
                        if(((blackPlayer.getAvMoves()[i].row==dest.row)&&(blackPlayer.getAvMoves()[i].col==dest.col))||
                                ((blackPlayer.getAvMoves()[i].row==0)&&(blackPlayer.getAvMoves()[i].col=='F'))){
                            isUnderHit = true;
                            break;
                        }
                    }
                    if(figure.isKingCastle()&&!isUnderHit){
                        for(unsigned int i=0;i<getWhiteFigs().size();i++){
                            if(getWhiteFigs()[i]->getId()==WR2){
                                Rook rook = static_cast<Rook>(*(getWhiteFigs()[i]));
                                if(!rook.hasmoved()){
                                    rook.moveCastle(adjMatrix,'w','R');
                                    isKCastle = true;
                                    char moveRes = figure.move(dest,getTable(),figure.getColor());
                                    if(moveRes=='N'){
                                        return false;
                                    }
                                }
                                std::unique_ptr<Rook> r = std::make_unique< Rook>(rook.getTitle(),rook.getPosition(),0,rook.getId(),rook.getColor());
                                setLastMovFig(r->getId());
                                *(getWhiteFigs().at(i)) = *(r);
                                break;
                            }
                        }
                        castle='o';
                    }
                    return true;
                }
                else if(dest.row==0&&dest.col=='C'){
                    bool isUnderHit = false;
                    for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
                        if(((blackPlayer.getAvMoves()[i].row==dest.row)&&(blackPlayer.getAvMoves()[i].col==dest.col))||
                                ((blackPlayer.getAvMoves()[i].row==0)&&(blackPlayer.getAvMoves()[i].col=='D'))){
                            isUnderHit = true;
                            break;
                        }
                    }
                    if(figure.isQueenCastle()&&!isUnderHit){
                        for(unsigned int i=0;i<getWhiteFigs().size();i++){
                            if(getWhiteFigs()[i]->getId()==WR1){
                                Rook rook = static_cast<Rook>(*(getWhiteFigs()[i]));
                                if(!rook.hasmoved()){
                                    rook.moveCastle(adjMatrix,'w','L');
                                    isQCastle = true;
                                    char moveRes = figure.move(dest,getTable(),figure.getColor());
                                    if(moveRes=='N'){
                                        return false;
                                    }
                                }
                                std::unique_ptr<Rook> r = std::make_unique< Rook>(rook.getTitle(),rook.getPosition(),0,rook.getId(),rook.getColor());
                                setLastMovFig(r->getId());
                                *(getWhiteFigs().at(i)) = *(r);
                                break;
                            }
                        }
                        castle='O';
                    }
                    return true;
                }
                else{
                    char moveRes = figure.move(dest,getTable(),figure.getColor());
                    if(moveRes=='N'){
                        return false;
                    }
                    if(moveRes=='T'){
                        removeFigure('b',destId);
                    }
                    return true;
                }
                return true;
            }
            else if(whitePlayer.getAvMoveId(dest)!=-1){
                char moveRes = figure.move(dest,getTable(),figure.getColor());
                if(moveRes=='N'){
                    return false;
                }
                if(moveRes=='T'){
                    removeFigure('b',destId);
                }
                else if(moveRes=='C'){
                }
                return true;
            }
            return true;
        }
        return true;
    }
    else if(color=='b'){
        if(blackPlayer.checked()){
            if(blackPlayer.isCheckAvMove(dest)){
                for(size_t i=0;i<openChMoves.size();i++){
                    if(openChMoves.at(i).posId==figure.getId()&&!openChMoves.at(i).isRemoved){//TEST OPENCHECK
                        isOpenCheck = true;
                        break;
                    }
                }
                char moveRes = figure.move(dest,getTable(),figure.getColor());
                if(moveRes=='N'){
                    return false;
                }
                if(moveRes=='T'){
                    removeFigure('w',destId);
                }
                else if(moveRes=='C'){
                }
                blackPlayer.setCheck(false);
                if(isOpenCheck){//TEST OPENCHECK
                    whitePlayer.setCheck(true);
                    isOpenCheck = false;
                }
                else {
                    if(getOpenCheckFig()!=-1){
                        setOpenCheckFig(-1);
                    }
                }

            }
            return true;
        }
        else{
            if(figure.getTitle()=="b_King"&&!figure.hasmoved()&&!figure.check()){
                if(dest.row==7&&dest.col=='G'){
                    bool isUnderHit = false;
                    for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
                        if(((whitePlayer.getAvMoves()[i].row==dest.row)&&(whitePlayer.getAvMoves()[i].col==dest.col))||
                                ((whitePlayer.getAvMoves()[i].row==7)&&(whitePlayer.getAvMoves()[i].col=='F'))){
                            isUnderHit = true;
                            break;
                        }
                    }
                    if(figure.isKingCastle()&&!isUnderHit){
                        for(unsigned int i=0;i<getBlackFigs().size();i++){
                            if(getBlackFigs()[i]->getId()==BR2){
                                Rook rook = static_cast<Rook>(*(getBlackFigs()[i]));
                                if(!rook.hasmoved()){
                                    rook.moveCastle(adjMatrix,'b','R');
                                    isKCastle = true;
                                    char moveRes = figure.move(dest,getTable(),figure.getColor());
                                    if(moveRes=='N'){
                                        return false;
                                    }
                                }
                                std::unique_ptr<Rook> r = std::make_unique< Rook>(rook.getTitle(),rook.getPosition(),0,rook.getId(),rook.getColor());
                                setLastMovFig(r->getId());
                                *(getBlackFigs().at(i)) = *(r);
                                break;
                            }
                        }
                        castle='o';
                    }
                    return true;
                }
                else if(dest.row==7&&dest.col=='C'){
                    bool isUnderHit = false;
                    for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
                        if(((whitePlayer.getAvMoves()[i].row==dest.row)&&(whitePlayer.getAvMoves()[i].col==dest.col))||
                                ((whitePlayer.getAvMoves()[i].row==7)&&(whitePlayer.getAvMoves()[i].col=='D'))){
                            isUnderHit = true;
                            break;
                        }
                    }
                    if(figure.isQueenCastle()&&!isUnderHit){
                        for(unsigned int i=0;i<getBlackFigs().size();i++){
                            if(getBlackFigs()[i]->getId()==BR1){
                                Rook rook = static_cast<Rook>(*(getBlackFigs()[i]));
                                if(!rook.hasmoved()){
                                    rook.moveCastle(adjMatrix,'b','L');
                                    isQCastle = true;
                                    char moveRes = figure.move(dest,getTable(),figure.getColor());
                                    if(moveRes=='N'){
                                        return false;
                                    }
                                }
                                std::unique_ptr<Rook> r = std::make_unique< Rook>(rook.getTitle(),rook.getPosition(),0,rook.getId(),rook.getColor());
                                setLastMovFig(r->getId());
                                *(getBlackFigs().at(i)) = *(r);
                                break;
                            }
                        }
                        castle='O';
                    }
                    return true;
                }
                else{
                    char moveRes = figure.move(dest,getTable(),figure.getColor());
                    if(moveRes=='N'){
                        return false;
                    }
                    if(moveRes=='T'){
                        removeFigure('w',destId);
                    }
                    return true;
                }
                return true;
            }
            else if(blackPlayer.getAvMoveId(dest)!=-1){
                char moveRes = figure.move(dest,getTable(),figure.getColor());
                if(moveRes=='N'){
                    return false;
                }
                if(moveRes=='T'){
                    removeFigure('w',destId);
                }
                else if(moveRes=='C'){
                }
                return true;
            }
            return true;
        }
        return true;
    }
    return true;
}
template bool StartPosGraph::moveFigure<Pawn>(Position&, Position&, char, Pawn&, int,bool &isKCastle,bool &isQCastle);
template bool StartPosGraph::moveFigure<Queen>(Position&, Position&, char, Queen&, int,bool &isKCastle,bool &isQCastle);
template bool StartPosGraph::moveFigure<King>(Position&, Position&, char, King&, int,bool &isKCastle,bool &isQCastle);
template bool StartPosGraph::moveFigure<Bishop>(Position&, Position&, char, Bishop&, int,bool &isKCastle,bool &isQCastle);
template bool StartPosGraph::moveFigure<Knight>(Position&, Position&, char, Knight&, int,bool &isKCastle,bool &isQCastle);

template<typename T> char StartPosGraph::moveFig(Position& start, Position& dest, char color, T &figure, int destId, int startId, int &enpasOpensCheck){
    if(color=='w'){
        if(whitePlayer.checked()){
            if(whitePlayer.isCheckAvMove(dest)){
                for(size_t i=0;i<openChMoves.size();i++){
                    if(openChMoves.at(i).posId==figure.getId()&&!openChMoves.at(i).isRemoved){//TEST OPENCHECK
                        isOpenCheck = true;
                        break;
                    }
                }
                char moveRes = figure.move(dest,getTable(),figure.getColor());
                if(moveRes=='N'){
                    return moveRes;
                }
                if(moveRes=='T'){
                    removeFigure('b',destId);
                    whitePlayer.setCheck(false);
                    if(isOpenCheck){//TEST OPENCHECK
                        blackPlayer.setCheck(true);
                        isOpenCheck = false;
                    }
                    else {
                        if(getOpenCheckFig()!=-1){
                            setOpenCheckFig(-1);
                        }
                    }

                    return moveRes;
                }
                else if(moveRes=='E'){
                    removeFigure('b',(destId*NO_EDGE));
                    whitePlayer.setCheck(false);
                    if(isOpenCheck){//TEST OPENCHECK
                        blackPlayer.setCheck(true);
                        isOpenCheck = false;
                    }
                    else {
                        if(getOpenCheckFig()!=-1){
                            setOpenCheckFig(-1);
                        }
                    }

                    return moveRes;
                }
                else if(moveRes=='C'){
                    return 'N';
                }
                whitePlayer.setCheck(false);
                if(isOpenCheck){//TEST OPENCHECK
                    blackPlayer.setCheck(true);
                    isOpenCheck = false;
                }
                else {
                    if(getOpenCheckFig()!=-1){
                        setOpenCheckFig(-1);
                    }
                }

                return moveRes;
            }
            else{
                return 'N';
            }
        }
        else{
            char moveRes = figure.move(dest,getTable(),figure.getColor());

            if(moveRes=='N'){
                return moveRes;
            }
            else if(moveRes=='T'){
                removeFigure('b',destId);
                return moveRes;
            }
            else if(moveRes=='E'){
                bool isch = false;
                for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
                    if((blackPlayer.getAvMoves().at(i).opensCh)&&(blackPlayer.getAvMoves().at(i).posId==(destId*NO_EDGE))&&
                            (blackPlayer.getAvMoves().at(i).dirId!=BP_LEFT) &&(blackPlayer.getAvMoves().at(i).dirId!=BP_RIGHT)){
                        blackPlayer.setCheck(true);
                        enpasOpensCheck = getLastMovFig();
                        isch = true;
                        break;
                    }
                }

                if(isch==false){
                    bool isKing = false, isEnpChecked = false;
                    int kRow = 0, kCol = 0, opensChFig = 0;
                    for(int i=start.row;i<=start.row;++i){
                        for(int j=0;j<BOARD_SIZE;++j){
                            if(adjMatrix[i][j]==BK){
                                isKing = true;
                                kRow = i;
                                kCol = j;
                                break;
                            }
                        }
                    }
                    if(isKing==true){
                        for(int i=kRow;i<=kRow;++i){
                            for(int j=(kCol+1);j<BOARD_SIZE;++j){//check right
                                if(adjMatrix[i][j] > NO_EDGE){
                                    if((adjMatrix[i][j]==WQ)||(adjMatrix[i][j]==WR1)||(adjMatrix[i][j]==WR2)||
                                            ((adjMatrix[i][j] % 100)==WR1)||((adjMatrix[i][j] % 100)==WQ)){//TODO test this last two
                                        isEnpChecked = true;
                                        opensChFig = adjMatrix[i][j];
                                        break;
                                    }
                                    else
                                        break;
                                }
                            }
                        }
                        if(isEnpChecked==false){
                            for(int i=kRow;i<=kRow;++i){
                                for(int j=(kCol-1);j>=0;--j){//check left
                                    if(adjMatrix[i][j] > NO_EDGE){
                                        if((adjMatrix[i][j]==WQ)||(adjMatrix[i][j]==WR1)||(adjMatrix[i][j]==WR2)||
                                            ((adjMatrix[i][j] % 100)==WR1)||((adjMatrix[i][j] % 100)==WQ)){//TODO test this last two
                                            isEnpChecked = true;
                                            opensChFig = adjMatrix[i][j];
                                            break;
                                        }
                                        else
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    if((isKing==true)&&(isEnpChecked==true)){
                        blackPlayer.setCheck(true);
                        setOpenCheckFig(opensChFig);
                        enpasOpensCheck = getLastMovFig();
                        isch = true;
                    }
                }
                if(isch==false){
                    for(unsigned int i=0;i<blackPlayer.getAvMoves().size();i++){
                        if((blackPlayer.getAvMoves().at(i).opensCh)&&(blackPlayer.getAvMoves().at(i).posId==(startId*NO_EDGE))){
                            blackPlayer.setCheck(true);
                            enpasOpensCheck = getLastMovFig();
                            break;
                        }
                    }
                }

                removeFigure('b',(destId*NO_EDGE));
                return moveRes;
            }
            else if(moveRes=='P'){
                moveRes = 'P';
                return moveRes;
            }
            else if(moveRes == 'F'){
                removeFigure('b',destId);
                moveRes = 'F';
                return moveRes;
            }
            else if(moveRes=='C'){
            }
            return moveRes;
        }
    }
    else if(color=='b'){
        if(blackPlayer.checked()){
            if(blackPlayer.isCheckAvMove(dest)){
                for(size_t i=0;i<openChMoves.size();i++){
                    if(openChMoves.at(i).posId==figure.getId()&&!openChMoves.at(i).isRemoved){//TEST OPENCHECK
                        isOpenCheck = true;
                        break;
                    }
                }
                char moveRes = figure.move(dest,getTable(),figure.getColor());
                if(moveRes=='N'){
                    return moveRes;
                }
                if(moveRes=='T'){
                    removeFigure('w',destId);
                    blackPlayer.setCheck(false);
                    if(isOpenCheck){//TEST OPENCHECK
                        whitePlayer.setCheck(true);
                        isOpenCheck = false;
                    }
                    else {
                        if(getOpenCheckFig()!=-1){
                            setOpenCheckFig(-1);
                        }
                    }

                    return moveRes;
                }
                else if(moveRes=='E'){
                    removeFigure('w',(destId*NO_EDGE));
                    blackPlayer.setCheck(false);
                    if(isOpenCheck){//TEST OPENCHECK
                        whitePlayer.setCheck(true);
                        isOpenCheck = false;
                    }
                    else {
                        if(getOpenCheckFig()!=-1){
                            setOpenCheckFig(-1);
                        }
                    }
                    return moveRes;
                }
                else if(moveRes=='C'){
                }
                blackPlayer.setCheck(false);
                if(isOpenCheck){//TEST OPENCHECK
                    whitePlayer.setCheck(true);
                    isOpenCheck = false;
                }
                else {
                    if(getOpenCheckFig()!=-1){
                        setOpenCheckFig(-1);
                    }
                }
                return moveRes;
            }
            else{
                return 'N';
            }
        }
        else{
            char moveRes = figure.move(dest,getTable(),figure.getColor());
            if(moveRes=='N'){
                return moveRes;
            }
            else if(moveRes=='T'){
                removeFigure('w',destId);
                return moveRes;
            }
            else if(moveRes=='E'){
                bool isch = false;
                for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
                    if((whitePlayer.getAvMoves().at(i).opensCh)&&(whitePlayer.getAvMoves().at(i).posId==(destId*NO_EDGE))&&
                            (whitePlayer.getAvMoves().at(i).dirId!=WP_LEFT) &&(whitePlayer.getAvMoves().at(i).dirId!=WP_RIGHT)){
                        whitePlayer.setCheck(true);
                        enpasOpensCheck = getLastMovFig();
                        isch = true;
                        break;
                    }
                }

                if(isch==false){
                    bool isKing = false, isEnpChecked = false;
                    int kRow = 0, kCol = 0, opensChFig = 0;
                    for(int i=start.row;i<=start.row;++i){
                        for(int j=0;j<BOARD_SIZE;++j){
                            if(adjMatrix[i][j]==WK){
                                isKing = true;
                                kRow = i;
                                kCol = j;
                                break;
                            }
                        }
                    }
                    if(isKing==true){
                        for(int i=kRow;i<=kRow;++i){
                            for(int j=(kCol+1);j<BOARD_SIZE;++j){//check right
                                if(adjMatrix[i][j] > NO_EDGE){
                                    if((adjMatrix[i][j]==BQ)||(adjMatrix[i][j]==BR1)||(adjMatrix[i][j]==BR2)||
                                            ((adjMatrix[i][j] % 100)==BR1)||((adjMatrix[i][j] % 100)==BQ)){//TODO test this last two
                                        isEnpChecked = true;
                                        opensChFig = adjMatrix[i][j];
                                        break;
                                    }
                                    else
                                        break;
                                }
                            }
                        }
                        if(isEnpChecked==false){
                            for(int i=kRow;i<=kRow;++i){
                                for(int j=(kCol-1);j>=0;--j){//check left
                                    if(adjMatrix[i][j] > NO_EDGE){
                                        if((adjMatrix[i][j]==BQ)||(adjMatrix[i][j]==BR1)||(adjMatrix[i][j]==BR2)||
                                                ((adjMatrix[i][j] % 100)==BR1)||((adjMatrix[i][j] % 100)==BQ)){//TODO test this last two
                                            isEnpChecked = true;
                                            opensChFig = adjMatrix[i][j];
                                            break;
                                        }
                                        else
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    if((isKing==true)&&(isEnpChecked==true)){
                        whitePlayer.setCheck(true);
                        setOpenCheckFig(opensChFig);
                        enpasOpensCheck = getLastMovFig();
                        isch = true;
                    }
                }
                if(isch==false){
                    for(unsigned int i=0;i<whitePlayer.getAvMoves().size();i++){
                        if((whitePlayer.getAvMoves().at(i).opensCh)&&(whitePlayer.getAvMoves().at(i).posId==(startId*NO_EDGE))){
                            whitePlayer.setCheck(true);
                            enpasOpensCheck = getLastMovFig();
                            break;
                        }
                    }
                }

                removeFigure('w',(destId*NO_EDGE));
                return moveRes;
            }
            else if(moveRes=='P'){
                moveRes = 'P';
                return moveRes;
            }
            else if(moveRes == 'F'){
                removeFigure('w',destId);
                moveRes = 'F';
                return moveRes;
            }
            else if(moveRes=='C'){
            }
            return moveRes;
        }
    }
    return 'N';
}
template char StartPosGraph::moveFig<Pawn>(Position&, Position&, char, Pawn&, int, int, int&);
template char StartPosGraph::moveFig<Queen>(Position&, Position&, char, Queen&, int, int, int&);
template char StartPosGraph::moveFig<King>(Position&, Position&, char, King&, int, int, int&);
template char StartPosGraph::moveFig<Bishop>(Position&, Position&, char, Bishop&, int, int, int&);
template char StartPosGraph::moveFig<Knight>(Position&, Position&, char, Knight&, int, int, int&);

void StartPosGraph::setAllFigsAvMoves(){
    for(unsigned int i=0;i<getWhiteFigs().size();i++){
        //White Pawns
        if(getWhiteFigs()[i]->getId()>=WP_FIRST&&getWhiteFigs()[i]->getId()<=WP_LAST){
            Pawn pawn = static_cast<Pawn>(*(getWhiteFigs()[i]));
            std::unique_ptr<Pawn> p = std::make_unique< Pawn>(pawn.getTitle(),pawn.getPosition(),pawn.getAvMovesCnt(),pawn.getId(),pawn.getColor());
            p->setStartPosWhite(pawn.isStPosWhite());
            p->addAvailableMoves(adjMatrix,'w');
            *(getWhiteFigs()[i]) = *p;
        }
        //White Rooks
        else if((getWhiteFigs()[i]->getId()==WR1)||((getWhiteFigs()[i]->getId()%100)==WR1)||getWhiteFigs()[i]->getId()==WR2){
            Rook rook = static_cast<Rook>(*(getWhiteFigs()[i]));
            std::unique_ptr<Rook> r = std::make_unique< Rook>(rook.getTitle(),rook.getPosition(),rook.getAvMovesCnt(),rook.getId(),rook.getColor());
            r->setMoved(rook.hasmoved());
            r->addAvailableMoves(adjMatrix,'w');
            *(getWhiteFigs()[i]) = *r;
        }
        //White Officers
        else if((getWhiteFigs()[i]->getId()==WO1)||((getWhiteFigs()[i]->getId()%100)==WO1)||(getWhiteFigs()[i]->getId()==WO2)){
            Bishop oficer = static_cast<Bishop>(*(getWhiteFigs()[i]));
            std::unique_ptr<Bishop> o = std::make_unique< Bishop>(oficer.getTitle(),oficer.getPosition(),oficer.getAvMovesCnt(),oficer.getId(),oficer.getColor());
            o->addAvailableMoves(adjMatrix,'w');
            *(getWhiteFigs()[i]) = *o;
        }
        //White Queen
        else if((getWhiteFigs()[i]->getId()==WQ)||((getWhiteFigs()[i]->getId()%100)==WQ)){
            Queen queen = static_cast<Queen>(*(getWhiteFigs()[i]));
            std::unique_ptr<Queen> q = std::make_unique< Queen>(queen.getTitle(),queen.getPosition(),queen.getAvMovesCnt(),queen.getId(),queen.getColor());
            q->addAvailableMoves(adjMatrix,'w');
            *(getWhiteFigs()[i]) = *q;
        }
        //White Knight
        else if((getWhiteFigs()[i]->getId()==WH1)||((getWhiteFigs()[i]->getId()%100)==WH1)||(getWhiteFigs()[i]->getId()==WH2)){
            Knight horse = static_cast<Knight>(*(getWhiteFigs()[i]));
            std::unique_ptr<Knight> h = std::make_unique< Knight>(horse.getTitle(),horse.getPosition(),horse.getAvMovesCnt(),horse.getId(),horse.getColor());
            h->addAvailableMoves(adjMatrix,'w');
            *(getWhiteFigs()[i]) = *h;
        }
        //White King
        else if(getWhiteFigs()[i]->getId()==WK){
            King king = static_cast<King>(*(getWhiteFigs()[i]));
            std::unique_ptr<King> k = std::make_unique< King>(king.getTitle(),king.getPosition(),king.getAvMovesCnt(),king.getId(),king.getColor());
            k->setChecked(king.check());
            k->setKingsCastle(king.isKingCastle());
            k->setQueensCastle(king.isQueenCastle());
            k->setHasAvMoves(king.hasAvMoves());
            k->setMoved(king.hasmoved());
            k->addAvailableMoves(adjMatrix,'w');
            *(getWhiteFigs()[i]) = *k;
        }
    }
    //Black Figures
    for(unsigned int i=0;i<getBlackFigs().size();i++){
        //Black Pawns
        if(getBlackFigs()[i]->getId()>=BP_FIRST&&getBlackFigs()[i]->getId()<=BP_LAST){
            Pawn pawn = static_cast<Pawn>(*(getBlackFigs()[i]));
            std::unique_ptr<Pawn> p = std::make_unique< Pawn>(pawn.getTitle(),pawn.getPosition(),pawn.getAvMovesCnt(),pawn.getId(),pawn.getColor());
            p->setStartPosBlack(pawn.isStPosBlack());
            p->addAvailableMoves(adjMatrix,'b');
            *(getBlackFigs()[i]) = *p;
        }
        //Black Rooks
        else if((getBlackFigs()[i]->getId()==BR1)||((getBlackFigs()[i]->getId()%100)==BR1)||(getBlackFigs()[i]->getId()==BR2)){
            Rook rook = static_cast<Rook>(*(getBlackFigs()[i]));
            std::unique_ptr<Rook> r = std::make_unique< Rook>(rook.getTitle(),rook.getPosition(),rook.getAvMovesCnt(),rook.getId(),rook.getColor());
            r->setMoved(rook.hasmoved());
            r->addAvailableMoves(adjMatrix,'b');
            *(getBlackFigs()[i]) = *r;
        }
        //Black Officers
        else if((getBlackFigs()[i]->getId()==BO1)||((getBlackFigs()[i]->getId()%100)==BO1)||(getBlackFigs()[i]->getId()==BO2)){
            Bishop oficer= static_cast<Bishop>(*(getBlackFigs()[i]));
            std::unique_ptr<Bishop> o = std::make_unique< Bishop>(oficer.getTitle(),oficer.getPosition(),oficer.getAvMovesCnt(),oficer.getId(),oficer.getColor());
            o->addAvailableMoves(adjMatrix,'b');
            *(getBlackFigs()[i]) = *o;
        }
        //Black Queen
        else if((getBlackFigs()[i]->getId()==BQ)||((getBlackFigs()[i]->getId()%100)==BQ)){
            Queen queen = static_cast<Queen>(*(getBlackFigs()[i]));
            std::unique_ptr<Queen> q = std::make_unique< Queen>(queen.getTitle(),queen.getPosition(),queen.getAvMovesCnt(),queen.getId(),queen.getColor());
            q->addAvailableMoves(adjMatrix,'b');
            *(getBlackFigs()[i]) = *q;
        }
        //Black Knight
        else if((getBlackFigs()[i]->getId()==BH1)||((getBlackFigs()[i]->getId()%100)==BH1)||(getBlackFigs()[i]->getId()==BH2)){
            Knight horse = static_cast<Knight>(*(getBlackFigs()[i]));
            std::unique_ptr<Knight> h = std::make_unique< Knight>(horse.getTitle(),horse.getPosition(),horse.getAvMovesCnt(),horse.getId(),horse.getColor());
            h->addAvailableMoves(adjMatrix,'b');
            *(getBlackFigs()[i]) = *h;
        }
        //Black King
        else if(getBlackFigs()[i]->getId()==BK){
            King king = static_cast<King>(*(getBlackFigs()[i]));
            std::unique_ptr<King> k = std::make_unique< King>(king.getTitle(),king.getPosition(),king.getAvMovesCnt(),king.getId(),king.getColor());
            k->setChecked(king.check());
            k->setKingsCastle(king.isKingCastle());
            k->setQueensCastle(king.isQueenCastle());
            k->setHasAvMoves(king.hasAvMoves());
            k->setMoved(king.hasmoved());
            k->addAvailableMoves(adjMatrix,'b');
            *(getBlackFigs()[i]) = *k;
        }
    }
}
StartPosGraph::StartPosGraph(int numVert){
    setNumVertices(numVert);
}
StartPosGraph::StartPosGraph(const StartPosGraph &old_graph){
    numVertices = old_graph.numVertices;
}
StartPosGraph::~StartPosGraph(){
}

std::vector<std::vector<int>> &StartPosGraph::getTable(){
    return adjMatrix;
}


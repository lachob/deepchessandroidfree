#ifndef HORSE_HPP_INCLUDED
#define HORSE_HPP_INCLUDED
#include "Figures.hpp"

class Knight : public Figure{
    private:
        char color;
        void setColor(char);
        char ch{'N'};
        char chMov{'N'};
    public:
        Knight();
        Knight(std::string,Position&, int, int, char);
        Knight(const Knight &);
        Knight(const Figure &);
        ~Knight()override;
        char getColor()const override;
        void setPosition(Position&);
        Position& getPosition();
        void setAvailableMoves(int, Position&)override;
        void setAvailableMovesCnt(int) override;
        std::vector<Position> getAvMoves()const override;
        int getAvMovesCnt()const override;
        Knight &operator=(const Knight &);
        int getId()const;
        void addAvailableMoves(std::vector<std::vector<int>> &adjMatrix,char)override;
        char isAvailableMove(Position&);
        char move(Position&,std::vector<std::vector<int>> &adjMatrix, char color)override;
};
#endif // HORSE_HPP_INCLUDED

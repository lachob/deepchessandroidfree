#include <algorithm>
#include <cassert>
#include <ostream>
#include <QThread>

#include "misc.h"
#include "search.h"
#include "thread.h"
#include "tt.h"
#include "uci.h"
#include "syzygy/tbprobe.h"
#include "polybook.h"

#ifndef NDEBUG
#define NDEBUG
#endif
using std::string;

UCI::OptionsMap Options; // Global object

namespace UCI {

/// 'On change' actions, triggered by an option's value change
void on_clear_hash(const Option&) { Search::clear(); }
void on_hash_size(const Option& o) { TT.resize(static_cast<size_t>(o)); }
void on_logger(const Option& o) { start_logger(o); }
void on_threads(const Option& o) { Threads.set(static_cast<size_t>(o)); }
void on_tb_path(const Option& o) { Tablebases::init(o); }
//POLYBOOK
void on_book_file(const Option& o) { polybook.init(o); }
void on_best_book_move(const Option& o) { polybook.set_best_book_move(o); }
void on_book_depth(const Option& o) { polybook.set_book_depth(o); }


/// Our case insensitive less() function as required by UCI protocol
bool CaseInsensitiveLess::operator() (const string& s1, const string& s2) const {

  return std::lexicographical_compare(s1.begin(), s1.end(), s2.begin(), s2.end(),
         [](char c1, char c2) { return tolower(c1) < tolower(c2); });
}


/// init() initializes the UCI options to their hard-coded default values

void init(OptionsMap& o) {

  // at most 2^32 clusters.
  constexpr int MaxHashMB = Is64Bit ? 131072 : 2048;

    int coresCount = QThread::idealThreadCount();

    if(coresCount > 4){
        coresCount = 4;
    }

  o["Debug Log File"]        << Option("", on_logger);
  o["Contempt"]              << Option(60, -100, 100);
  o["Analysis Contempt"]     << Option("Both var Off var White var Black var Both", "Both");
  o["Threads"]               << Option(coresCount, 1, 512, on_threads);
  o["Hash"]                  << Option(1, 1, MaxHashMB, on_hash_size);
  o["Clear Hash"]            << Option(on_clear_hash);
  o["Ponder"]                << Option(false);
  o["MultiPV"]               << Option(1, 1, 500);
  o["Skill Level"]           << Option(Search::skillLevel, 0, 20);
  o["Move Overhead"]         << Option(5000, 0, 5000);
  o["Minimum Thinking Time"] << Option(0, 0, 5000);
  o["Slow Mover"]            << Option(10, 10, 1000);//84
  o["nodestime"]             << Option(0, 0, 10000);
  o["UCI_Chess960"]          << Option(false);
  o["UCI_AnalyseMode"]       << Option(false);
  o["SyzygyPath"]            << Option("<empty>", on_tb_path);
  o["SyzygyProbeDepth"]      << Option(1, 1, 100);
  o["Syzygy50MoveRule"]      << Option(true);
  o["SyzygyProbeLimit"]      << Option(7, 0, 7);
  //POLYBOOK
  o["BookFile"]              << Option(/*"<empty>"*/Search::bookSettings.path.c_str(), on_book_file);//"assets:/openingbooks/ProDeo.bin"
  o["BestBookMove"]          << Option(Search::bookSettings.bestBookMove, on_best_book_move);
  o["BookDepth"]             << Option(Search::bookSettings.depth, 1, 255, on_book_depth);
  o["OwnBook"]               << Option(Search::bookSettings.ownBook);
}


/// operator<<() is used to print all the options default values in chronological
/// insertion order (the idx field) and in the format defined by the UCI protocol.

std::ostream& operator<<(std::ostream& os, const OptionsMap& om) {

  for (size_t idx = 0; idx < om.size(); ++idx)
      for (const auto& it : om)
          if (it.second.idx == idx)
          {
              const Option& o = it.second;
              os << "\noption name " << it.first << " type " << o.type;

              if (o.type == "string" || o.type == "check" || o.type == "combo")
                  os << " default " << o.defaultValue;

              if (o.type == "spin")
                  os << " default " << static_cast<int>(stof(o.defaultValue))
                     << " min "     << o.min
                     << " max "     << o.max;

              break;
          }

  return os;
}


/// Option class constructors and conversion operators

Option::Option(const char* v, OnChange f) : type("string"), min(0), max(0), on_change(f)
{ defaultValue = currentValue = v; }

Option::Option(bool v, OnChange f) : type("check"), min(0), max(0), on_change(f)
{ defaultValue = currentValue = (v ? "true" : "false"); }

Option::Option(OnChange f) : type("button"), min(0), max(0), on_change(f)
{}

Option::Option(double v, int minv, int maxv, OnChange f) : type("spin"), min(minv), max(maxv), on_change(f)
{ defaultValue = currentValue = std::to_string(v); }

Option::Option(const char* v, const char* cur, OnChange f) : type("combo"), min(0), max(0), on_change(f)
{ defaultValue = v; currentValue = cur; }

Option::operator double() const {
  //assert(type == "check" || type == "spin");
  return (type == "spin" ? stod(currentValue) : 1.0000000);//currentValue == "true");
}

Option::operator std::string() const {
  //assert(type == "string");
  return currentValue;
}

bool Option::operator==(const char* s) const {
  //assert(type == "combo");
  return    !CaseInsensitiveLess()(currentValue, s)
         && !CaseInsensitiveLess()(s, currentValue);
}


/// operator<<() inits options and assigns idx in the correct printing order

void Option::operator<<(const Option& o) {

  static size_t insert_order = 0;

  *this = o;
  idx = insert_order++;
}


/// operator=() updates currentValue and triggers on_change() action. It's up to
/// the GUI to check for option's limits, but we could receive the new value from
/// the user by console window, so let's check the bounds anyway.

Option& Option::operator=(const string& v) {

  //assert(!type.empty());

  if (   (type != "button" && v.empty())
      || (type == "check" && v != "true" && v != "false")
      || (type == "spin" && (stof(v) < min || stof(v) > max)))
      return *this;

  if (type != "button")
      currentValue = v;

  if (on_change)
      on_change(*this);

  return *this;
}

} // namespace UCI

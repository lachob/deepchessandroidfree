#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <stdexcept>

#include "Figures.hpp"
#include "functions.hpp"
#define NO_EDGE -1
#define BOARD_SIZE 8

Figure::Figure():id(-1),title(""),availableMovesCnt(0){
    currPos.setP(NO_EDGE,'N',-1);
}
Figure::Figure(std::string title, Position& p , int avMovesCnt, int id){
    this->title = title;
    currPos = p;
    availableMovesCnt = avMovesCnt;
    setId(id);
}
Figure::Figure(const Figure &old_fig){
    title = old_fig.title;
    currPos = old_fig.currPos;
    availableMovesCnt = old_fig.availableMovesCnt;
    id = old_fig.id;
    availableMoves = old_fig.availableMoves;
    isStartPosWhite = old_fig.isStartPosWhite;
    isStartPosBlack = old_fig.isStartPosBlack;
    hasAvailableMoves = old_fig.hasAvailableMoves;
    hasMoved = old_fig.hasMoved;
    isKingsCastle = old_fig.isKingsCastle;
    isQueensCastle = old_fig.isQueensCastle;
    checked = old_fig.checked;
}
Figure::~Figure(){}

void Figure::setId(int id){
    this->id = id;
}
int Figure::getId()const{
    return id;
}
void Figure::setTitle(std::string title){
    this->title = title;
}
std::string Figure::getTitle()const{
    return title;
}
void Figure::setPosition(Position &newPosition){
    currPos = newPosition;
}
Position& Figure::getPosition(){
    return currPos;
}
int Figure::incAvMovesCnt(){
    ++availableMovesCnt;
    return availableMovesCnt;
}
//Virtual methods
void Figure::checkMoves(std::vector<std::vector<int>> &adjMatrix,int i,int j,int flag,char color,int dir){}
void Figure::setChecked(bool c){}
bool Figure::check()const{return 0;}
void Figure::setQueensCastle(bool q){}
bool Figure::isQueenCastle()const{return 0;}
void Figure::setKingsCastle(bool k){}
bool Figure::isKingCastle()const{return 0;}
void Figure::setMoved(bool m){}
bool Figure::hasmoved()const{return 0;}
void Figure::setHasAvMoves(bool m){}
bool Figure::hasAvMoves()const{return 0;}
void Figure::setStartPosWhite(bool b){}
bool Figure::isStPosWhite()const{return 0;}
void Figure::setStartPosBlack(bool b){}
bool Figure::isStPosBlack()const{return 0;}
void Figure::setAvailableMoves(int index, Position& p){}
void Figure::addAvailableMoves(std::vector<std::vector<int>> &adjMatrix, char color){}
std::vector<Position> Figure::getAvMoves()const{}
void Figure::setAvailableMovesCnt(int newCount){}
int Figure::getAvMovesCnt()const{return 0;}
char Figure::move(Position &p, std::vector<std::vector<int>> &a, char c){return 'F';}
char Figure::getColor()const{return 'F';}
Figure &Figure::operator=(const Figure &other){
    title = other.title;
    currPos = other.currPos;
    id = other.id;
    availableMovesCnt = other.availableMovesCnt;
    availableMoves = other.availableMoves;
    isStartPosWhite = other.isStartPosWhite;
    isStartPosBlack = other.isStartPosBlack;
    hasAvailableMoves = other.hasAvailableMoves;
    hasMoved = other.hasMoved;
    isKingsCastle = other.isKingsCastle;
    isQueensCastle = other.isQueensCastle;
    checked = other.checked;
    return *this;
}


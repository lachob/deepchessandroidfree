#ifndef FUNCTIONS_HPP_INCLUDED
#define FUNCTIONS_HPP_INCLUDED
#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include <QString>

char chooseCol(int);
char chooseColLow(int);
int chooseNumLetter(char);
QString chooseCommandByLevel(const QString& command_text);
std::string chooseHintMoveLevel(const QString &command_text);

#endif // FUNCTIONS_HPP_INCLUDED

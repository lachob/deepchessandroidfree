#include "deepchess.h"


DeepChess::DeepChess()
{
    bannerCnt = 0;
    theme = 's';
    MainWindow *mWindow = nullptr;
    try {
        mWindow = new MainWindow(false,false,true,bannerCnt,theme,nullptr,true,0);
        if(!mWindow || !mWindow->isCorrectInitialization) {
            QMessageBox::information(this, "ERROR", tr("Seems there is not enough available memory left in the device! Please close some apps and try again!"));
            std::exit(0);
        }
    } catch(std::exception &ex) {
        QMessageBox::information(this, "ERROR", tr("Seems there is not enough available memory left in the device! Please close some apps and try again!"));
        std::exit(0);
    }
    gamesWithHintCount = mWindow->connection->checkGameCnt();
    bool isActive = mWindow->connection->checkPuzzle();
    if(isActive){
        mWindow->isHintActivatedInMainWindow = true;
        mWindow->isPuzzleAct = true;
    }
    else{
        mWindow->isHintActivatedInMainWindow = false;
        mWindow->isPuzzleAct = false;
    }
    if(mWindow->isSoundsAval){
        QSound::play("assets:sounds/startgame.wav");
    }

    uiThread = mWindow->uiThread;
    mWindows.push(mWindow);
    connect(mWindows.front(),&MainWindow::restartGameChanged,this,&DeepChess::startNewGame);
    mWindows.front()->show();
}

DeepChess::~DeepChess()
{
    this->disconnect();
    UCI::loop(&stopChessAI,Search::startPos,Search::states);

    while(!mWindows.empty()) {
        if(mWindows.front()) {
            mWindows.front()->hide();
            mWindows.front()->disconnect();
            mWindows.front()->disposeMainWindow();
            mWindows.front()->deleteLater();
        }
        mWindows.pop();
    }

    if(uiThread) {
        delete uiThread; uiThread = nullptr;
    }
}

void DeepChess::setMainWindow()
{
    connect(mWindows.front(),&MainWindow::restartGameChanged,this,&DeepChess::startNewGame);
    mWindows.front()->show();
}

void DeepChess::startNewGame()
{
    theme = mWindows.front()->theme;
    bool newFiguresChoosed = mWindows.front()->newFiguresSelected;
    isHintActivated = mWindows.front()->isHintActivatedInMainWindow;
    isPuzzleActiv = mWindows.front()->isPuzzleAct;
    gamesWithPuzz = mWindows.front()->puzzleCnt;
    isGameResseted = mWindows.front()->isGameResetted;
    if((isHintActivated) && (!isGameResseted)){
        ++gamesWithHintCount;
        if(gamesWithHintCount == HINT_PUZZLE_ACTIVE_COUNT) {
            isHintActivated = false;
            gamesWithHintCount = 0;
            mWindows.front()->connection->disablePuzzle(true);
        }
        mWindows.front()->connection->updateGameCnt(gamesWithHintCount);
    }
    if(gamesWithPuzz >= HINT_PUZZLE_ACTIVE_COUNT)
        isPuzzleActiv = false;
    if(isPuzzleActiv && gamesWithPuzz < HINT_PUZZLE_ACTIVE_COUNT) {
        ++gamesWithPuzz;
        if(gamesWithPuzz >= HINT_PUZZLE_ACTIVE_COUNT)
            isPuzzleActiv = false;
        else
            isPuzzleActiv = true;
    }

    ++bannerCnt;
    int cnt = mWindows.front()->gamesCount;
    if(!isGameResseted)
        ++cnt;

    mWindows.front()->hide();
    disconnect(mWindows.front(),&MainWindow::restartGameChanged,this,&DeepChess::startNewGame);
    mWindows.front()->deleteLater();
    mWindows.pop();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    MainWindow *mWindow = nullptr;
    try {
        mWindow = new MainWindow(isHintActivated,isPuzzleActiv,newFiguresChoosed,bannerCnt,theme,nullptr,false,cnt);
        if(!mWindow || !mWindow->isCorrectInitialization) {
            QMessageBox::information(this, "ERROR", tr("Seems there is not enough available memory left in the device! Please close some apps and try again!"));
            std::exit(0);
        }
    } catch(std::exception &ex) {
        QMessageBox::information(this, "ERROR", tr("Seems there is not enough available memory left in the device! Please close some apps and try again!"));
        std::exit(0);
    }

    mWindows.push(mWindow);
    setMainWindow();
    if(mWindow->isSoundsAval){
        QSound::play("assets:sounds/startgame.wav");
    }
}


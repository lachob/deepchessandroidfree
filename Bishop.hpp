#ifndef OFICER_HPP_INCLUDED
#define OFICER_HPP_INCLUDED
#include "Figures.hpp"

class Bishop : public Figure{
    private:
        char color;
        void setColor(char);
        char ch{'N'};
        char chMov{'N'};
    public:
        Bishop();
        Bishop(std::string,Position&, int, int, char);
        Bishop(const Bishop &);
        Bishop(const Figure &);
        ~Bishop()override;
        char getColor()const override;
        void setTitle(std::string);
        std::string getTitle()const;
        void setPosition(Position&);
        int incAvMovesCnt();
        Position& getPosition();
        void setAvailableMoves(int, Position&)override;
        void setAvailableMovesCnt(int)override;
        std::vector<Position> getAvMoves()const override;
        int getAvMovesCnt()const override;
        Bishop &operator=(const Bishop &);
        void setId(int);
        int getId()const;
        void addAvailableMoves(std::vector<std::vector<int>> &adjMatrix,char)override;
        char isAvailableMove(Position&);
        char move(Position&,std::vector<std::vector<int>> &adjMatrix, char color)override;
};
#endif // OFICER_HPP_INCLUDED

#ifndef ROOK_HPP_INCLUDED
#define ROOK_HPP_INCLUDED
#include "Figures.hpp"

class Rook : public Figure{
    private:
        char color;
        void setColor(char);
        char ch{'N'};
        char chMov{'N'};
    public:
        Rook();
        Rook(std::string,Position&, int, int, char);
        Rook(const Rook &);
        Rook(const Figure &);
        ~Rook() override;
        void setMoved(bool) override;
        bool hasmoved()const override;
        char getColor()const override;
        std::string getTitle()const;
        void setPosition(Position&);
        Position& getPosition();
        void setAvailableMoves(int, Position&) override;
        void setAvailableMovesCnt(int) override;
        std::vector<Position> getAvMoves()const override;
        int getAvMovesCnt()const override;
        Rook &operator=(const Rook &);
        int getId()const;
        void addAvailableMoves(std::vector<std::vector<int>> &adjMatrix,char) override;
        char isAvailableMove(Position&);
        char move(Position&,std::vector<std::vector<int>> &, char) override;
        void moveCastle(std::vector<std::vector<int>> &,char,char);
};

#endif // ROOK_HPP_INCLUDED

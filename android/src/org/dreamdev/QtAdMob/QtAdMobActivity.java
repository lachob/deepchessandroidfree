package org.dreamdev.QtAdMob;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.LoadAdError;
import androidx.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.ViewGroup;
import android.util.Log;
import android.graphics.Rect;
import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.bindings.QtApplication;
import java.util.ArrayList;
import android.widget.FrameLayout;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.telephony.TelephonyManager;
import java.util.Locale;
import java.util.Arrays;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.Application;
import android.content.ComponentCallbacks2;
import java.io.File;
import android.view.KeyEvent;
import androidx.annotation.Nullable;
import android.os.Build.VERSION_CODES;
import android.util.DisplayMetrics;
import android.view.Display;

import com.google.android.ump.ConsentInformation;
import com.google.android.ump.ConsentRequestParameters;
import com.google.android.ump.ConsentDebugSettings;
import com.google.android.ump.FormError;
import com.google.android.ump.UserMessagingPlatform;
import com.google.android.ump.ConsentInformation.PrivacyOptionsRequirementStatus;
import java.util.concurrent.atomic.AtomicBoolean;
// import org.deepchess.deepchess.R;

public class QtAdMobActivity extends QtActivity {
    private ViewGroup m_ViewGroup = null;
    private AdView m_AdBannerView = null;
    private InterstitialAd m_AdInterstitial = null;
    private RewardedInterstitialAd m_RewardedInterstitialAd = null;
    private final AtomicBoolean m_IsAdBannerShowed = new AtomicBoolean(false);
    private final AtomicBoolean m_IsAdBannerLoaded = new AtomicBoolean(false);
    private final AtomicBoolean m_IsAdInterstitialLoaded = new AtomicBoolean(false);
    private final AtomicBoolean m_IsAdRewardedLoaded = new AtomicBoolean(false);
    private final AtomicBoolean mAreAdsBlockedBecauseOfLowMemory = new AtomicBoolean(false);
    private ArrayList < String > m_TestDevices = new ArrayList < String > ();
    private int m_AdBannerWidth = 0;
    private int m_AdBannerHeight = 0;
    private int m_StatusBarHeight = 0;
    private int m_ReadyToRequest = 0x00;
    private int m_MaxMemoryAvailable = 0;
    private int isEuUser = 0;
    private long mAvailMemDiff = 104857600; // should be 52428800(50 Mb) 104857600(100 Mb) 209715200(200 Mb)
    private int mAdsDeletionCounter = 0;
    private int mAdsRestoreCounter = 0;
    private final String AD_ID = "";
    private final String mRewardedAdID = "";
    private ConsentInformation consentInformation;
    private final AtomicBoolean isMobileAdsInitializeCalled = new AtomicBoolean(false);
    private final AtomicBoolean isMobileAdsInitialized = new AtomicBoolean(false);
    private String mAdId;

    //To get available memory
    private ActivityManager.MemoryInfo getAvailableMemory() {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo;
    }

    public int isEUUser(Context context) {
        int retVal = 0;
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String country = tm != null ? tm.getSimCountryIso() : null;
        country = country != null ? country : Locale.getDefault().getCountry();
        /*String[] euCountries = {
        "BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR",
        "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL", "UK",
        "CH", "NO", "IS", "LI", "GR", "US", "BR"
        };*/
        String[] euCountries = {
            "IR",
            "CU",
            "SD"
        };
        boolean isUser = Arrays.asList(euCountries).contains(country.toUpperCase());
        boolean androidOSVersion = false;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) { //Marshmallow
            androidOSVersion = true;
        } else {
            androidOSVersion = false;
        }
        if (isUser == true) {
            retVal = 1;
        } else {
            if (androidOSVersion == false) {
                retVal = 1;
            } else {
                retVal = 0;
            }
        }
        return retVal;
    }
    public int checkEuUser() {
        return isEuUser;
    }

    private int GetStatusBarHeight() {
        Rect rectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = contentViewTop - statusBarHeight;
        return titleBarHeight;
    }

    public void SetAdBannerUnitId(final String adId) {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        if (m_AdBannerView == null) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_AdBannerView == null) {
                    return;
                }
                try {
                    m_AdBannerView.setAdUnitId(adId);
                    m_ReadyToRequest |= 0x01;
                    if (m_ReadyToRequest == 0x03 && !IsAdBannerLoaded()) {
                        RequestBanner();
                    }
                } catch (Exception ex) {
                    return;
                }
            }
        });
    }

    private AdSize getAdSize() {
        //Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int)(widthPixels / density);

        //Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    public void SetAdBannerSize(final int size) {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        if (m_AdBannerView == null) {
            return;
        }
        //final QtAdMobActivity self = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_AdBannerView == null) {
                    return;
                }
                AdSize adSize = AdSize.BANNER;
                switch (size) {
                    case 0:
                        adSize = AdSize.BANNER;
                        break;
                    case 1:
                        adSize = AdSize.FULL_BANNER;
                        break;
                    case 2:
                        adSize = AdSize.LARGE_BANNER;
                        break;
                    case 3:
                        adSize = AdSize.MEDIUM_RECTANGLE;
                        break;
                    case 4:
                        adSize = AdSize.SMART_BANNER;
                        break;
                    case 5:
                        adSize = AdSize.WIDE_SKYSCRAPER;
                        break;
                    case 6:
                        adSize = getAdSize();
                        break;
                };
                try {
                    m_AdBannerView.setAdSize(adSize);
                    m_AdBannerWidth = adSize.getWidthInPixels(getApplicationContext());
                    m_AdBannerHeight = adSize.getHeightInPixels(getApplicationContext());

                    m_ReadyToRequest |= 0x02;
                    if (m_ReadyToRequest == 0x03 && !IsAdBannerLoaded()) {
                        RequestBanner();
                    }
                } catch (Exception ex) {
                    return;
                }
            }
        });
    }

    public void SetAdBannerPosition(final int x, final int y) {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        if (m_AdBannerView == null) {
            return;
        }

        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (memoryInfo.lowMemory || ((memoryInfo.availMem <= mAvailMemDiff) || ((memoryInfo.availMem - mAvailMemDiff) <= memoryInfo.threshold)) || mAreAdsBlockedBecauseOfLowMemory.get()) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_AdBannerView == null) {
                    return;
                }
                try {
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT);
                    m_AdBannerView.setLayoutParams(layoutParams);
                    m_AdBannerView.setX(x);
                    m_AdBannerView.setY(y + m_StatusBarHeight);
                } catch (Exception ex) {
                    return;
                }
            }
        });
    }

    public void AddAdTestDevice(final String deviceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_TestDevices.add(deviceId);
            }
        });
    }

    public boolean IsAdBannerShowed() {
        return (m_IsAdBannerShowed.get() && m_IsAdBannerLoaded.get());
    }

    public boolean IsAdBannerLoaded() {
        return m_IsAdBannerLoaded.get();
    }

    public int GetAdBannerWidth() {
        return m_AdBannerWidth;
    }

    public int GetAdBannerHeight() {
        return m_AdBannerHeight;
    }

    public void ShowAdBanner() {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (memoryInfo.lowMemory || ((memoryInfo.availMem <= mAvailMemDiff) || ((memoryInfo.availMem - mAvailMemDiff) <= memoryInfo.threshold)) || mAreAdsBlockedBecauseOfLowMemory.get()) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_AdBannerView == null || IsAdBannerShowed()) {
                    return;
                }
                try {
                    if (m_ReadyToRequest == 0x03 && !IsAdBannerLoaded()) {
                        RequestBanner();
                    }
                    if (m_AdBannerView != null) {
                        m_AdBannerView.resume();
                        m_AdBannerView.setVisibility(View.VISIBLE);
                        m_IsAdBannerShowed.set(true);
                    }
                } catch (Exception ex) {
                    m_IsAdBannerShowed.set(false);
                    return;
                }
            }
        });
    }

    private void RequestBanner() {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (memoryInfo.lowMemory || ((memoryInfo.availMem <= mAvailMemDiff) || ((memoryInfo.availMem - mAvailMemDiff) <= memoryInfo.threshold)) || mAreAdsBlockedBecauseOfLowMemory.get()) {
            return;
        }

        if (m_AdBannerView == null) {
             return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_AdBannerView == null) {
                    return;
                }
                ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
                if (!(memoryInfo.lowMemory) && ((memoryInfo.availMem > mAvailMemDiff) && ((memoryInfo.availMem - mAvailMemDiff) > memoryInfo.threshold)) && !mAreAdsBlockedBecauseOfLowMemory.get() && m_AdBannerView != null) {
                    onBannerLoading();
                    AdRequest.Builder adRequest = new AdRequest.Builder();
                    try {
                        if(adRequest != null) {
                            m_AdBannerView.loadAd(adRequest.build());
                        }
                    } catch (Exception ignored) {
                        m_IsAdBannerLoaded.set(false);
                        return;
                    }
                } else {
                    m_IsAdBannerLoaded.set(false);
                }
            }
        });
    }

    public void HideAdBanner() {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_AdBannerView == null) // || !IsAdBannerShowed()
                {
                    return;
                }
                try {
                    m_AdBannerView.pause();
                    m_AdBannerView.setVisibility(View.INVISIBLE);//GONE
                    m_IsAdBannerShowed.set(false);
                } catch (Exception ex) {
                    m_IsAdBannerShowed.set(false);
                    return;
                }
            }
        });
    }

    public void InitializeAdBanner() {
        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (memoryInfo.lowMemory || ((memoryInfo.availMem <= mAvailMemDiff) || ((memoryInfo.availMem - mAvailMemDiff) <= memoryInfo.threshold)) || mAreAdsBlockedBecauseOfLowMemory.get()) {
            return;
        }

        if(!isMobileAdsInitialized.get()) {
           return;
        }

        //final QtAdMobActivity self = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_AdBannerView != null) {
                    return;
                }

                try {
                    m_StatusBarHeight = GetStatusBarHeight();

                    m_AdBannerView = new AdView(getApplicationContext());
                    m_AdBannerView.setVisibility(View.INVISIBLE);//GONE

                    View view = getWindow().getDecorView().getRootView();
                    //view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    //view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                    if (view instanceof ViewGroup) {
                        m_ViewGroup = (ViewGroup) view;

                        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.WRAP_CONTENT);
                        m_AdBannerView.setLayoutParams(layoutParams);
                        m_AdBannerView.setX(0);
                        m_AdBannerView.setY(m_StatusBarHeight);
                        m_ViewGroup.addView(m_AdBannerView);

                        m_AdBannerView.setAdListener(new AdListener() {
                            public void onAdLoaded() {
                                m_IsAdBannerLoaded.set(true);
                                onBannerLoaded();
                            }

                            public void onAdClosed() {
                                onBannerClosed();
                            }

                            public void onAdLeftApplication() {
                                onBannerClicked();
                            }

                            public void onAdClicked() {
                                onBannerClicked();
                            }

                        });
                        m_AdBannerView.setVisibility(View.INVISIBLE);//GONE
                    }
                } catch (Exception ex) {
                    m_AdBannerView = null;
                    return;
                }
            }
        });
    }

    public void ShutdownAdBanner() {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_AdBannerView == null) {
                    return;
                }
                try {
                    m_IsAdBannerShowed.set(false);
                    m_IsAdInterstitialLoaded.set(false);
                    m_IsAdRewardedLoaded.set(false);
                    if(m_AdInterstitial != null) {
                        m_AdInterstitial.setFullScreenContentCallback(null);
                    }
                    m_AdInterstitial = null;
                    if(m_RewardedInterstitialAd != null) {
                        m_RewardedInterstitialAd.setFullScreenContentCallback(null);
                    }
                    m_RewardedInterstitialAd = null;
                    m_AdBannerView.setVisibility(View.INVISIBLE);//GONE
                    m_ViewGroup.removeView(m_AdBannerView);
                    m_AdBannerView.destroy();
                    m_AdBannerView = null;
                } catch (Exception ex) {
                    return;
                }
            }
        });
    }

    public void LoadAdInterstitialWithUnitId(final String adId) //INITIALIZE HERE
    {
        if(!isMobileAdsInitialized.get()) {
            return;
        }
        //final QtAdMobActivity self = this;
        mAdId = adId;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
                if ( !(memoryInfo.lowMemory) && ((memoryInfo.availMem > mAvailMemDiff) && ((memoryInfo.availMem - mAvailMemDiff) > memoryInfo.threshold)) && !mAreAdsBlockedBecauseOfLowMemory.get()) {

                  if (m_AdInterstitial == null) {
                      m_IsAdInterstitialLoaded.set(false);
                      AdRequest adRequest = new AdRequest.Builder().build();

                      InterstitialAd.load(/*QtAdMobActivity.this*/ getApplicationContext(), adId, adRequest,
                          new InterstitialAdLoadCallback() {
                              @Override
                              public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                                  // The mInterstitialAd reference will be null until
                                  // an ad is loaded.
                                  m_AdInterstitial = interstitialAd;
                                  try {
                                      m_AdInterstitial.setFullScreenContentCallback(new FullScreenContentCallback(){
                                        @Override
                                        public void onAdClicked() {
                                          // Called when a click is recorded for an ad.
                                          onInterstitialClicked();
                                        }

                                        @Override
                                        public void onAdDismissedFullScreenContent() {
                                          // Called when ad is dismissed.
                                          // Set the ad reference to null so you don't show the ad a second time.
//                                            Log.d("Interstitial ad", "Ad dismissed fullscreen content.");
//                                            m_IsAdInterstitialLoaded.set(false);
//                                            if(m_AdInterstitial != null) {
//                                                m_AdInterstitial.setFullScreenContentCallback(null);
//                                            }
//                                            m_AdInterstitial = null;
                                            onInterstitialClosed();
                                        }

                                        @Override
                                        public void onAdFailedToShowFullScreenContent(AdError adError) {
                                          // Called when ad fails to show.
                                          //Log.e(TAG, "Ad failed to show fullscreen content.");
                                          m_IsAdInterstitialLoaded.set(false);
                                          m_AdInterstitial = null;
                                        }

                                        @Override
                                        public void onAdImpression() {
                                          // Called when an impression is recorded for an ad.
                                          //Log.d(TAG, "Ad recorded an impression.");
                                        }

                                        @Override
                                        public void onAdShowedFullScreenContent() {
                                          // Called when ad is shown.
                                          //Log.d(TAG, "Ad showed fullscreen content.");
                                        }
                                      });
                                      m_IsAdInterstitialLoaded.set(true);
                                      onInterstitialLoaded();
                                  } catch (Exception ex) {
                                      m_IsAdInterstitialLoaded.set(false);
                                      m_AdInterstitial = null;
                                      return;
                                  }
                              }

                              @Override
                              public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                  // Handle the error
                                  m_AdInterstitial = null;
                                  m_IsAdInterstitialLoaded.set(false);
                              }
                        });

                }
            }
                //RequestNewInterstitial(adId);
            }
        });
    }

    public void LoadAdRewarded() {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
                if ( !(memoryInfo.lowMemory) && ((memoryInfo.availMem > mAvailMemDiff) && ((memoryInfo.availMem - mAvailMemDiff) > memoryInfo.threshold))) {

                    if (m_RewardedInterstitialAd == null) {
                       m_IsAdRewardedLoaded.set(false);
                       AdRequest adRequest = new AdRequest.Builder().build();
                       RewardedInterstitialAd.load(getApplicationContext(), mRewardedAdID, adRequest, new RewardedInterstitialAdLoadCallback() {
                           @Override
                           public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                             // Handle the error.
                             //Log.d(TAG, loadAdError.toString());
                             //Log.d("ADMOB activity", ("REWARDEDINTERSTITIAL ERROR: " + loadAdError.toString()));
                             m_RewardedInterstitialAd = null;
                             m_IsAdRewardedLoaded.set(false);
                           }

                           @Override
                           public void onAdLoaded(@NonNull RewardedInterstitialAd ad) {
                              m_RewardedInterstitialAd = ad;
                              //Log.d("ADMOB activity", "REWARDEDINTERSTITIAL Ad was loaded.");
                              try {
                                  m_RewardedInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                                    @Override
                                    public void onAdClicked() {
                                      // Called when a click is recorded for an ad.
                                      //Log.d(TAG, "Ad was clicked.");
                                      onInterstitialClicked();
                                    }

                                     @Override
                                    public void onAdDismissedFullScreenContent() {
                                      // Called when ad is dismissed.
                                      // Set the ad reference to null so you don't show the ad a second time.
                                      //Log.d(TAG, "Ad dismissed fullscreen content.");
                                      m_RewardedInterstitialAd = null;
                                      m_IsAdRewardedLoaded.set(false);
                                      onInterstitialClosed();
                                    }

                                    @Override
                                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                                      // Called when ad fails to show.
                                      //Log.e(TAG, "Ad failed to show fullscreen content.");
                                      m_RewardedInterstitialAd = null;
                                      m_IsAdRewardedLoaded.set(false);
                                    }

                                    @Override
                                    public void onAdImpression() {
                                      // Called when an impression is recorded for an ad.
                                      //Log.d(TAG, "Ad recorded an impression.");
                                    }

                                    @Override
                                    public void onAdShowedFullScreenContent() {
                                      // Called when ad is shown.
                                      //Log.d(TAG, "Ad showed fullscreen content.");
                                    }
                                  });
                                  m_IsAdRewardedLoaded.set(true);
                                  //onInterstitialLoaded();
                              } catch(Exception ex) {
                                  m_IsAdRewardedLoaded.set(false);
                                  m_RewardedInterstitialAd = null;
                                  return;
                              }
                           }
                       });
                    }
                }
            }
        });
    }

     public void CloseInterstitial() {

         if(!isMobileAdsInitialized.get()) {
             return;
         }

         runOnUiThread(new Runnable() {
             @Override
             public void run() {
                 try {
                    // m_IsAdBannerShowed.set(false);
                     m_IsAdInterstitialLoaded.set(false);
                     m_IsAdRewardedLoaded.set(false);

                     if(m_AdInterstitial != null) {
                         m_AdInterstitial.setFullScreenContentCallback(null);
                     }
                     m_AdInterstitial = null;

                     if(m_RewardedInterstitialAd != null) {
                         m_RewardedInterstitialAd.setFullScreenContentCallback(null);
                     }
                     m_RewardedInterstitialAd = null;

                     //boolean isAdviewDestroyed = false;
                     ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
                     double totalRam = (double)memoryInfo.totalMem / (1024.0 * 1024.0);
                     int counterThrldStop = 0;
                     //int counterThrldRestore = 0;
                     if(totalRam <= 1000.0) { // RAM <= 2.5GB
                         counterThrldStop = 10;
                         //counterThrldRestore = 4;
                     } else if(totalRam > 1000.0 && totalRam <= 2000.0) { // RAM GB
                         counterThrldStop = 30;
                         //counterThrldRestore = 4;
                     } else if(totalRam > 2000.0 && totalRam <= 2500.0) {
                         counterThrldStop = 40;
                         //counterThrldRestore = 4;
                     } else if(totalRam > 2500.0 && totalRam <= 3500.0) {
                         counterThrldStop = 50;
                         //counterThrldRestore = 5;
                     } else if(totalRam > 3500.0 && totalRam <= 4500.0) {
                         counterThrldStop = 60;
                         //counterThrldRestore = 5;
                     } else if(totalRam > 4500.0 && totalRam <= 6500.0) {
                         counterThrldStop = 70;
                         //counterThrldRestore = 6;
                     }  else if(totalRam > 6500.0 && totalRam <= 8500.0) {
                         counterThrldStop = 80;
                         //counterThrldRestore = 6;
                     } else if(totalRam > 8500.0 && totalRam <= 12500.0) {
                         counterThrldStop = 90;
                         //counterThrldRestore = 6;
                     } else {
                         counterThrldStop = 100;
                     }

                     if(!mAreAdsBlockedBecauseOfLowMemory.get() && mAdsDeletionCounter < counterThrldStop)
                         ++mAdsDeletionCounter;
                     else if(mAdsDeletionCounter == counterThrldStop) {
                         mAreAdsBlockedBecauseOfLowMemory.set(true);
                         //mAdsDeletionCounter = 0;
                     }

//                     if(mAreAdsBlockedBecauseOfLowMemory && mAdsRestoreCounter < counterThrldRestore)
//                         ++mAdsRestoreCounter;
//                     else if(mAdsRestoreCounter == counterThrldRestore) {
//                         mAreAdsBlockedBecauseOfLowMemory = false;
//                         mAdsRestoreCounter = 0;
//                     }

                     if (memoryInfo.lowMemory || (memoryInfo.availMem <= mAvailMemDiff) || ((memoryInfo.availMem - mAvailMemDiff) <= memoryInfo.threshold) || mAreAdsBlockedBecauseOfLowMemory.get()) {
                         if(m_AdBannerView != null) {
                             m_AdBannerView.setVisibility(View.INVISIBLE);//GONE
                             m_AdBannerView.setAdListener(null);
                             m_ViewGroup.removeView(m_AdBannerView);
                             m_AdBannerView.destroy();
                             m_AdBannerView = null;
                            // isAdviewDestroyed = true;
                         }
                     }

//                     if(isAdviewDestroyed) {
//                         return;
//                     }

//                     if(m_AdBannerView != null) {
//                        m_AdBannerView.setVisibility(View.INVISIBLE);//GONE
//                        m_AdBannerView.setAdListener(null);
//                     }
//                     m_AdBannerView = null;

                 } catch (Exception ex) {
                     return;
                 }
             }
         });

        //RequestNewInterstitial(mAdId);
    }

    private void RequestNewInterstitial(final String adId) {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (!(memoryInfo.lowMemory) && ((memoryInfo.availMem > mAvailMemDiff) && ((memoryInfo.availMem - mAvailMemDiff) > memoryInfo.threshold)) && !mAreAdsBlockedBecauseOfLowMemory.get()) {
            onInterstitialLoading();
            AdRequest adRequest = new AdRequest.Builder().build();

            InterstitialAd.load(getApplicationContext(), adId, adRequest,

                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        m_AdInterstitial = interstitialAd;
                        try {
                            m_IsAdInterstitialLoaded.set(true);
                            onInterstitialLoaded();
                        } catch (Exception ex) {
                            m_IsAdInterstitialLoaded.set(false);
                            m_AdInterstitial = null;
                            return;
                        }
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
                        m_AdInterstitial = null;
                        m_IsAdInterstitialLoaded.set(false);
                    }
                });
        }
    }

    public boolean IsAdInterstitialLoaded() {
        return m_IsAdInterstitialLoaded.get();
    }

    public boolean IsAdRewardedLoaded() {
        return m_IsAdRewardedLoaded.get();
    }

    public boolean IsAdsSDKInitialized() {
        if(!mAreAdsBlockedBecauseOfLowMemory.get()) {
            return isMobileAdsInitialized.get();
        }
        return true;
    }

    public void ShowAdInterstitial()
    {
        if(!isMobileAdsInitialized.get()) {
            return;
        }

        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (memoryInfo.lowMemory || ((memoryInfo.availMem <= mAvailMemDiff) || ((memoryInfo.availMem - mAvailMemDiff) <= memoryInfo.threshold)) || mAreAdsBlockedBecauseOfLowMemory.get()) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_IsAdInterstitialLoaded.get()) {
                    try {
                        if (m_AdInterstitial != null) {
                            m_AdInterstitial.show(QtAdMobActivity.this);
                            m_IsAdInterstitialLoaded.set(false); // Ad might be presented only once, need reload
                        }
                    } catch (Exception ignored) {
                        return;
                    }
                }
            }
        });
    }

    public void ShowAdRewarded() {
        if(!isMobileAdsInitialized.get()) {
           return;
        }

        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (memoryInfo.lowMemory || ((memoryInfo.availMem <= mAvailMemDiff) || ((memoryInfo.availMem - mAvailMemDiff) <= memoryInfo.threshold))) {
            return;
        }

        runOnUiThread(new Runnable() {
           @Override
           public void run() {
               if (m_IsAdRewardedLoaded.get()) {
                   try {
                       if (m_RewardedInterstitialAd != null) {
                           m_RewardedInterstitialAd.show(QtAdMobActivity.this, new OnUserEarnedRewardListener() {
                               @Override
                               public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                                   // Handle the reward.
                                   //Log.d("ADMOB activity", "REWARDEDINTERSTITIAL: The user earned the reward.");
                                   onInterstitialClicked();
//                                 int rewardAmount = rewardItem.getAmount();
//                                 String rewardType = rewardItem.getType();
                               }
                             });
                           m_IsAdRewardedLoaded.set(false); // Ad might be presented only once, need reload
                       }
                   } catch (Exception ignored) {
                       return;
                   }
                }
            }
        });
    }

    //TESt MEMORY
    public int GetRuntimeMemoryAvailable() {
        m_MaxMemoryAvailable = (int)(Runtime.getRuntime().maxMemory() / (1024 * 1024));
        return m_MaxMemoryAvailable;
    }

    //Fix for Android 10 back button crash
    @Override
    public void onBackPressed() {
        try {
            finishAffinity();
            System.exit(0);
        } catch (Exception exc) {
            return;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            try {
                finishAffinity();
                System.exit(0);
            } catch (Exception exc) {
                return false;
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initializeMobileAdsSdk() {
       if (isMobileAdsInitializeCalled.get()) {
          return;
       }
       isMobileAdsInitializeCalled.set(true);

       // Initialize the Google Mobile Ads SDK.
       MobileAds.initialize(this, new OnInitializationCompleteListener() {
           @Override
           public void onInitializationComplete(InitializationStatus initializationStatus) {}
       });
       isMobileAdsInitialized.set(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_layout);

//        // Create an instance of a manager.
//        standardIntegrityManager = IntegrityManagerFactory.createStandard(this);
//        // Prepare integrity token. Can be called once in a while to keep internal
//        // state fresh.
//        standardIntegrityManager.prepareIntegrityToken(
//            PrepareIntegrityTokenRequest.builder()
//                .setCloudProjectNumber(cloudProjectNumber)
//                .build())
//            .addOnSuccessListener(tokenProvider -> {
//                integrityTokenProvider = tokenProvider;
//            })
//            .addOnFailureListener(exception -> handleError(exception));

//        Task<StandardIntegrityToken> integrityTokenResponse =
//                integrityTokenProvider.request(
//                    StandardIntegrityTokenRequest.builder()
//                        .setRequestHash(requestHash)
//                        .build());
//                integrityTokenResponse
//                .addOnSuccessListener(response -> sendToServer(response.token()))
//                .addOnFailureListener(exception -> handleError(exception));

        // MobileAds.initialize(this,
        //"ca-app-pub-3502266329582579~8809138636");
        isEuUser = isEUUser(this);

        //m_MaxMemoryAvailable = (int)(Runtime.getRuntime().maxMemory() / (1024*1024));
        ConsentDebugSettings debugSettings = new ConsentDebugSettings.Builder(this)
            .addTestDeviceHashedId("TEST-DEVICE-HASHED-ID")
            .build();

        // Set tag for under age of consent. false means users are not under age of consent.
            ConsentRequestParameters params = new ConsentRequestParameters
                .Builder()
                .setTagForUnderAgeOfConsent(false)
                .build();

            consentInformation = UserMessagingPlatform.getConsentInformation(this);
            consentInformation.requestConsentInfoUpdate(
                    this,
                    params,
                    () -> {
                      UserMessagingPlatform.loadAndShowConsentFormIfRequired(
                        this,
                        loadAndShowError -> {
                          if (loadAndShowError != null) {
                            // Consent gathering failed.
                            // Log.w(TAG, String.format("%s: %s",
                            // loadAndShowError.getErrorCode(),
                            // loadAndShowError.getMessage()));
                          }

                          // Consent has been gathered.
                          if (consentInformation.canRequestAds()) {
                              ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
                              if (!memoryInfo.lowMemory && (memoryInfo.availMem > mAvailMemDiff) && ((memoryInfo.availMem - mAvailMemDiff) > memoryInfo.threshold)) {
                                  initializeMobileAdsSdk();
                              }
                          }
                        }
                      );
                    },
                    requestConsentError -> {
                      // Consent gathering failed.
                      // Log.w(TAG, String.format("%s: %s",
                      // requestConsentError.getErrorCode(),
                      // requestConsentError.getMessage()));
                    });

                if (consentInformation.getPrivacyOptionsRequirementStatus() == PrivacyOptionsRequirementStatus.REQUIRED) {
                    // Regenerate the options menu to include a privacy setting.
                    UserMessagingPlatform.showPrivacyOptionsForm(
                         this,
                         formError -> {
                            if (formError != null) {}

                            // Consent has been gathered.
                            if (consentInformation.canRequestAds()) {
                                ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
                                if (!memoryInfo.lowMemory && (memoryInfo.availMem > mAvailMemDiff) && ((memoryInfo.availMem - mAvailMemDiff) > memoryInfo.threshold)) {
                                    initializeMobileAdsSdk();
                                }
                            }
                    });
                 }

                if (consentInformation.canRequestAds()) {
                    ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
                    if (!memoryInfo.lowMemory && (memoryInfo.availMem > mAvailMemDiff) && ((memoryInfo.availMem - mAvailMemDiff) > memoryInfo.threshold)) {
                        initializeMobileAdsSdk();
                    }
                }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level >= ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW || level >= ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            try {
                if(level >= ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           if(isMobileAdsInitialized.get() && m_AdInterstitial != null) {
                              m_IsAdInterstitialLoaded.set(false);
                              m_AdInterstitial.setFullScreenContentCallback(null);
                              m_AdInterstitial = null;
                           }
                        }
                    });
                }
                File dir = getApplicationContext().getCacheDir();
                deleteDir(dir);
            } catch (Exception e) {
                return;
            }
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            return;
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public void onPause() {
        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (memoryInfo.lowMemory) {
            finishAffinity();
            System.exit(0);
        }

        if(isMobileAdsInitialized.get()) {
            try {
                runOnUiThread(new Runnable() {
                     @Override
                     public void run() {
                         if (m_AdBannerView != null) {
                            m_AdBannerView.pause();
                         }
                     }
                });
            } catch (Exception ex) {}
        }

        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
        if (memoryInfo.lowMemory) {
            finishAffinity();
            System.exit(0);
        }

        if(isMobileAdsInitialized.get()) {
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       if (m_AdBannerView != null) {
                           m_AdBannerView.resume();
                       }
                    }
                });
            } catch (Exception ex) {
                return;
            }
        }
    }

    @Override
    public void onDestroy() {
        try {
            File dir = getApplicationContext().getCacheDir();
            if(dir != null) {
                deleteDir(dir);
            }

        } catch (Exception e) {}

        if(isMobileAdsInitialized.get()) {
            ShutdownAdBanner();
        }

        super.onDestroy();
    }

    private static native void onBannerLoaded();
    private static native void onBannerLoading();
    private static native void onBannerClosed();
    private static native void onBannerClicked();

    private static native void onInterstitialLoaded();
    private static native void onInterstitialLoading();
    private static native void onInterstitialWillPresent();
    private static native void onInterstitialClosed();
    private static native void onInterstitialClicked();
}

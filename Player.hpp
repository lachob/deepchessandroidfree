#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <vector>
#include <string>

#include "Figures.hpp"

#define NO_EDGE -1
#define BOARD_SIZE 8

class Player{
    private:
        bool turn {true};
        bool isCheck {false};
        bool isDoubleCheck {false};
        bool isWinning {false};
        bool isMate {false};
        bool isStalemate {false};
        std::vector<Position> availableMoves;
        std::vector<Position> checkAvailableMoves;
    public:
        explicit Player();
        explicit Player(Position&);
        Player(const Player &);
        ~Player();
        void setTurn(bool);
        bool isTurn()const;
        void setCheck(bool);
        bool checked()const;
        void setDoubleCheck(bool);
        void setWin(bool);
        bool win()const;
        void setMate(bool);
        bool mate()const;
        void setStalemate(bool);
        bool stalemate()const;
        void setAvMoves(Position&);
        void removeAvMove(int);
        void eraseAvMoves();
        void eraseCheckAvMoves();
        inline std::vector<Position> &getAvMoves(){return availableMoves;}
        void setCheckAvMoves(Position&);
        inline std::vector<Position> &getCheckAvMoves(){return checkAvailableMoves;}
        int getAvMoveId(Position&);
        bool isCheckAvMove(Position&);
        int checkAvMovesCnt()const;
};
#endif  // PLAYER_HPP_INCLUDED

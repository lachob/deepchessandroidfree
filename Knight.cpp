#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <stdexcept>

#include "Knight.hpp"
#include "functions.hpp"
#define NO_EDGE -1
#define BOARD_SIZE 8

Knight::Knight():Figure(){
    setColor(' ');
}
Knight::Knight(std::string title, Position& p, int avMovesCnt, int id, char color):Figure(title,p,avMovesCnt,id){
    setColor(color);
}
Knight::Knight(const Knight &other):Figure(other){
    color = other.color;
}
Knight::Knight(const Figure &other):Figure(other){
    if(other.getTitle()[0]=='b'){
        color = 'b';
    }
    else{
        color = 'w';
    }
}
Knight::~Knight(){}

int Knight::getId()const{
    return Figure::getId();
}
void Knight::setColor(char color){
    this->color = color;
}

char Knight::getColor()const{
    return color;
}
void Knight::setPosition(Position &p){
    Figure::setPosition(p);
}
Position& Knight::getPosition(){
    return Figure::getPosition();
}

void Knight::setAvailableMoves(int index, Position& p){
    availableMoves.push_back(p);
}
void Knight::setAvailableMovesCnt(int newCount){
    availableMovesCnt = newCount+1;
}
std::vector<Position> Knight::getAvMoves()const{
    return availableMoves;
}
int Knight::getAvMovesCnt()const{
   return availableMovesCnt;
}
void Knight::addAvailableMoves(std::vector<std::vector<int>> &adjMatrix, char color){
    int row = getPosition().row;
    int col = chooseNumLetter(getPosition().col);
    int a=0;
    a=col-1<8&&col-1>-1?adjMatrix[row][col-1]:-2;
    int b=0;
    b=col+1<8&&col+1>-1?adjMatrix[row][col+1]:-2;
    int c=0;
    c=row-1<8&&row-1>-1?adjMatrix[row-1][col]:-2;
    int d=0;
    d=row+1<8&&row+1>-1?adjMatrix[row+1][col]:-2;

    if(a>=-1||b>=-1||c>=-1||d>=-1){
       int counter = -1;
        //White Horses
       if(color=='w'){
        //Check two rows up left
         for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=WK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check two rows up right
         for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=WK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check one row up left
         for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col-2;j<=col-2&&j>=0;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=WK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check one row up right
         for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=WK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check two rows down left
         for(int i=row-2;i<=row-2&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=WK){
                   p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check two rows down right
         for(int i=row-2;i<=row-2&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=WK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check one row down left
         for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col-2;j<=col-2&&j>=0;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=WK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check one row down right
         for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=WK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
       }
        //Black Horses
       else if(color=='b'){
         //Check two rows up left
         for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=BK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check two rows up right
         for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=BK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check one row up left
         for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col-2;j<=col-2&&j>=0;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
            else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=BK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check one row up right
         for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=BK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check two rows down left
         for(int i=row-2;i<=row-2&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=BK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check two rows down right
         for(int i=row-2;i<=row-2&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=BK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check one row down left
         for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col-2;j<=col-2&&j>=0;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=BK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check one row down right
         for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
             if((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST)){
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                if(adjMatrix[i][j]!=BK){
                    p.setP(i,chooseCol(j),getId(),H_DIR,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                }
                else{
                    p.isTake = true;
                }
                p.setP(i,chooseCol(j),getId());
                p.dirId = H_DIR;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        }

        setAvailableMovesCnt(counter);
    }
    else{
        return;
    }
}
char Knight::isAvailableMove(Position &p){
    ch = 'N';
    for(size_t i=0;i<getAvMoves().size();i++){
        if(getAvMoves()[i].col==p.col&&getAvMoves()[i].row==p.row&&getAvMoves()[i].prot==-1){
            if(getAvMoves()[i].isCheck){
                ch = 'C';
                break;
            }
            else if(getAvMoves()[i].isTake){
                ch = 'T';
                break;
            }
            ch = 'Y';
        }
    }
    return ch;
}

char Knight::move(Position& dest,std::vector<std::vector<int>> &adjMatrix, char color){
    chMov = isAvailableMove(dest);
    Position p;
    addAvailableMoves(adjMatrix,color);
    //cout<<"Is av move: "<<ch<<endl;
    if(chMov!='N'){
        if(chMov=='C'){
            return chMov;
        }
        p.setP(getPosition().row,getPosition().col,getId());
        adjMatrix[p.row][chooseNumLetter(p.col)] = NO_EDGE;
        adjMatrix[dest.row][chooseNumLetter(dest.col)] = getId();
        setPosition(dest);
        addAvailableMoves(adjMatrix,color);
        if(chMov=='T'){
            return chMov;
        }
        else if(chMov=='Y'){
            return chMov;
        }
    }
    else{
       return chMov;
    }
    return chMov;
}
Knight &Knight::operator=(const Knight &other){
    Figure::operator=(other);
    color = other.color;
    return *this;
}

#ifndef PAWN_HPP_INCLUDED
#define PAWN_HPP_INCLUDED
#include "Figures.hpp"

class Pawn : public Figure{
    private:
        char color;
        void setColor(char);
        char ch{'N'};
        char chMov{'N'};
    public:
        Pawn();
        Pawn(std::string,Position&, int, int, char);
        Pawn(const Pawn &);
        Pawn(const Figure &);
        ~Pawn() override;
        void setStartPosWhite(bool) override;
        bool isStPosWhite()const override;
        void setStartPosBlack(bool) override;
        bool isStPosBlack()const override;
        char getColor()const override;
        void setPosition(Position&);
        Position& getPosition();
        void setAvailableMoves(int, Position&) override;
        void setAvailableMovesCnt(int) override;
        std::vector<Position> getAvMoves()const override;
        int getAvMovesCnt()const override;
        Pawn &operator=(const Pawn &);
        Pawn &operator=(const Figure &);
        int getId()const;
        void addAvailableMoves(std::vector<std::vector<int>> &adjMatrix,char) override;
        char isAvailableMove(Position&);
        char move(Position&,std::vector<std::vector<int>> &adjMatrix, char color)override;
};
#endif // PAWN_HPP_INCLUDED

#include "Player.hpp"

using namespace std;

Player::Player():turn(true),isCheck(false),isWinning(false),isMate(false),isStalemate(false){}
Player::Player(Position& p):turn(true),isCheck(false),isWinning(false),isMate(false),isStalemate(false){
    availableMoves.push_back(p);
}
Player::Player(const Player &other){
    setTurn(other.turn);
    setCheck(other.isCheck);
    setWin(other.isWinning);
    setMate(other.isMate);
    setStalemate(other.isStalemate);
    availableMoves = other.availableMoves;
    checkAvailableMoves = other.checkAvailableMoves;
}
Player::~Player(){}
void Player::setTurn(bool turn){
    this->turn = turn;
}
bool Player::isTurn()const{
    return turn;
}
void Player::setCheck(bool check){
    isCheck = check;
}
bool Player::checked()const{
    return isCheck;
}
void Player::setDoubleCheck(bool dCh){
    isDoubleCheck = dCh;
}

void Player::setWin(bool win){
    isWinning = win;
}
bool Player::win()const{
    return isWinning;
}
void Player::setMate(bool mate){
    isMate = mate;
}
bool Player::mate()const{
    return isMate;
}
void Player::setStalemate(bool draw){
    isStalemate = draw;
}
bool Player::stalemate()const{
    return isStalemate;
}
void Player::eraseAvMoves(){
    availableMoves.clear();
}
void Player::eraseCheckAvMoves(){
    checkAvailableMoves.clear();
}
void Player::setAvMoves(Position &p){
    availableMoves.push_back(p);
}
void Player::removeAvMove(int index){
    availableMoves.at(static_cast<size_t>(index)).opensCh = true;
    availableMoves.at(static_cast<size_t>(index)).removedRow = availableMoves.at(static_cast<size_t>(index)).row;
    availableMoves.at(static_cast<size_t>(index)).removedCol = availableMoves.at(static_cast<size_t>(index)).col;
    availableMoves.at(static_cast<size_t>(index)).row = -1;
    availableMoves.at(static_cast<size_t>(index)).col = ' ';

}

void Player::setCheckAvMoves(Position& p){
    checkAvailableMoves.push_back(p);
}

int Player::checkAvMovesCnt()const{
    return static_cast<int>(checkAvailableMoves.size());
}

int Player::getAvMoveId(Position& p){
    int result = -1;
    for(unsigned int i=0;i<getAvMoves().size();i++){
        if(getAvMoves()[i].row==p.row&&getAvMoves()[i].col==p.col){
            result = getAvMoves()[i].posId;
            break;
        }
    }
    return result;
}

bool Player::isCheckAvMove(Position& p){
    for(unsigned int i=0;i<getCheckAvMoves().size();i++){
        if(p.row==getCheckAvMoves()[i].row&&p.col==getCheckAvMoves()[i].col){
            return true;
        }
    }
    return false;
}


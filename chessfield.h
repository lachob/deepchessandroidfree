#ifndef CHESSFIELD_H
#define CHESSFIELD_H

#include <QObject>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include <memory>
#include <stack>

struct AvMove{
    int figId{0};
    int row{0};
    int col{0};
    int enpId{-1};
};
class ChessFieldButton : public QPushButton
{
    Q_OBJECT

public:
    QString iconUrl;
    int row{-1};
    int col{-1};
    int figId{-1};
    bool isAvMov{false};
    QChar color{' '};
    QString borderColor;
    QString _boardPos;
    //Move Back
    ChessFieldButton()=default;
    explicit ChessFieldButton(QWidget *parent, int _figId, int _row, int _col, const QChar &color, const QString &bordColor,
                               double fieldSize, const QString &boardPos, const QString &_textUp = "", const QString &_textDown = "");
    ~ChessFieldButton();
    inline void setFigId(int _figId){figId = _figId;}
    inline bool &isAvMove(){return  isAvMov;}
    inline void setBoardPos(const QString &pos){_boardPos = pos;}
    bool isConnected{false};
    std::stack<QString> lGtook;
    QVBoxLayout *textL = nullptr;
    QLabel *textUp = nullptr;
    QLabel *textDown = nullptr;
signals:
    void avMoveChanged(bool value);
    void borderColorChanged(QString &newBC);
    void iconUrlChanged(QString &newIconUrl);
public slots:
    inline void setAvMove(bool isMove){isAvMov = isMove;}
    inline void setBorderColor(const QString &str){borderColor = str;}
    inline void setIconUrl(const QString &_iconUrl){iconUrl = _iconUrl;}
};

#endif // CHESSFIELD_H

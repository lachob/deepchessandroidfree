#include "chessai.h"

#include <QTimer>

ChessAI::ChessAI(QObject *parent) : QObject(parent)
{
    uciMove = "";
    uciMoveCommand = "";
}

ChessAI::~ChessAI()
{

}

std::string ChessAI::getUciMove()
{
    return uciMove;
}

void ChessAI::setUciMoveCommand(const std::string &cmd)
{
    uciMoveCommand = cmd;
}

void ChessAI::setTime(int t)
{
    time = t;
}

int ChessAI::getTime() const
{
    return time;
}

void ChessAI::process()
{
    QTimer::singleShot(getTime(), this, SLOT(waitForChessAi()));
}

void ChessAI::stop()
{
    emit finished();
}

void ChessAI::waitForChessAi()
{
    if(stopMoveTh.load()) {
        stop();
        return;
    }

    Search::bestMoveForGui = "no move";
    Search::bestMoveForGuiDone.store(false);
    uciMove = "no move";
    UCI::loop(&uciMoveCommand,Search::getStartPos(),Search::getStates());

    while(!Search::bestMoveForGuiDone.load()){ continue; }
    uciMove = Search::bestMoveForGui;

    if(!stopMoveTh.load())
        emit uciMoveChanged();
    else
        stop();
}

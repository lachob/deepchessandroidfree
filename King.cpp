#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <stdexcept>

#include "King.hpp"
#include "functions.hpp"
#define NO_EDGE -1
#define BOARD_SIZE 8

King::King():Figure(){
    setColor(' ');
}
King::King(string title, Position& p, int avMovesCnt, int id, char color):Figure(title,p,avMovesCnt,id){
    setColor(color);
}
King::King(const King &other):Figure(other){
    color = other.color;
}
King::King(const Figure &other):Figure(other){
    if(other.getTitle()[0]=='b'){
        color = 'b';
    }
    else{
        color = 'w';
    }
}
King::~King(){}

int King::getId()const{
    return Figure::getId();
}
void King::setColor(char color){
    this->color = color;
}

string King::getTitle()const{
    return Figure::getTitle();
}
char King::getColor()const{
    return color;
}
void King::setPosition(Position& p){
    Figure::setPosition(p);
}
Position& King::getPosition(){
    return Figure::getPosition();
}

void King::setAvailableMoves(int index, Position& p){
    availableMoves.push_back(p);
}
void King::setAvailableMovesCnt(int newCount){
    availableMovesCnt = newCount+1;
}
vector<Position> King::getAvMoves()const{
    return availableMoves;
}
int King::getAvMovesCnt()const{
   return availableMovesCnt;
}
void King::setHasAvMoves(bool hasMoves){
    hasAvailableMoves = hasMoves;
}
bool King::hasAvMoves()const{
    return hasAvailableMoves;
}
void King::setMoved(bool ismoved){
    hasMoved = ismoved;
}
bool King::hasmoved()const{
    return hasMoved;
}
void King::setQueensCastle(bool isCas){
    isQueensCastle = isCas;
}
bool King::isQueenCastle()const{
    return isQueensCastle;
}
void King::setKingsCastle(bool isCas){
    isKingsCastle = isCas;
}
bool King::isKingCastle()const{
    return isKingsCastle;
}
void King::setChecked(bool check){
    checked = check;
}
bool King::check()const{
    return checked;
}
void King::addAvailableMoves(std::vector<std::vector<int>> &adjMatrix, char color){
    int row = getPosition().row;
    int col = chooseNumLetter(getPosition().col);
    int a=0;
    a=col-1<8&&col-1>-1?adjMatrix[row][col-1]:-2;
    int b=0;
    b=col+1<8&&col+1>-1?adjMatrix[row][col+1]:-2;
    int c=0;
    c=row-1<8&&row-1>-1?adjMatrix[row-1][col]:-2;
    int d=0;
    d=row+1<8&&row+1>-1?adjMatrix[row+1][col]:-2;

    if(a>=-1||b>=-1||c>=-1||d>=-1){
       int counter = -1;
        //White King
       if(color=='w'){
        //Check for castle availability
        if(!hasmoved()){
          //Left Castle
          int lCount = 0;
          for(int i=row;i<=row&&i==0;i++){
           for(int j=col-1;j>0;j--){
             if(adjMatrix[i][j]==-1){
                ++lCount;
             }
           }
         }
         if(lCount==3&&adjMatrix[row][0]==WR1){
            Position p;
            p.setP(row,chooseCol(col-2),getId());
            setAvailableMoves(++counter, p);
            setQueensCastle(true);
         }
         //Right Castle
         int rCount = 0;
          for(int i=row;i<=row&&i==0;i++){
           for(int j=col+1;j<BOARD_SIZE-1;j++){
             if(adjMatrix[i][j]==-1){
                ++rCount;
             }
           }
         }
         if(rCount==2&&adjMatrix[row][BOARD_SIZE-1]==WR2){
            Position p1;
            p1.setP(row,chooseCol(col+2),getId());
            setAvailableMoves(++counter, p1);
            setKingsCastle(true);
         }
        }
        //Check moves up
        int isBlackKing = 0;
        for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
             for(int j=col-1;j<=col+1&&j>=0&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col;j<=col;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isBlackKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     isBlackKing==0&&adjMatrix[i][j]!=BK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check moves up-left
        isBlackKing = 0;
        for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
             for(int j=col-2;j<=col&&j>=0;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row+2;i>=row&&i<BOARD_SIZE;i--){
             for(int j=col-2;j<=col-2&&j>=0;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isBlackKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))
                     &&isBlackKing==0&&adjMatrix[i][j]!=BK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check moves up-right
        isBlackKing = 0;
        for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
             for(int j=col;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row+2;i>=row&&i<BOARD_SIZE;i--){
             for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isBlackKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     isBlackKing==0&&adjMatrix[i][j]!=BK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves down
        isBlackKing = 0;
        for(int i=row-2;i>=row-2&&i>=0;i--){
             for(int j=col-1;j<=col+1&&j>=0&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col;j<=col;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isBlackKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     isBlackKing==0&&adjMatrix[i][j]!=BK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves down-left
        isBlackKing = 0;
        for(int i=row-2;i<=row-2&&i>=0;i++){
             for(int j=col-2;j<=col&&j>=0;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row;i>=row-2&&i>=0;i--){
             for(int j=col-2;j<=col-2&&j>=0;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isBlackKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     isBlackKing==0&&adjMatrix[i][j]!=BK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves down-right
        isBlackKing = 0;
        for(int i=row-2;i<=row-2&&i>=0;i++){
             for(int j=col;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row;i>=row-2&&i>=0;i--){
             for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isBlackKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     isBlackKing==0&&adjMatrix[i][j]!=BK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves left
        isBlackKing = 0;
        for(int i=row+1;i>=row-1&&i>=0&&i<BOARD_SIZE;i--){
             for(int j=col-2;j<=col-2&&j>=0;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row;i<=row;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isBlackKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     isBlackKing==0&&adjMatrix[i][j]!=BK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves right
        isBlackKing = 0;
        for(int i=row+1;i>=row-1&&i>=0&&i<BOARD_SIZE;i--){
             for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==BK){
                    ++isBlackKing;
                }
             }
        }
        for(int i=row;i<=row;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isBlackKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     isBlackKing==0&&adjMatrix[i][j]!=BK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
       }
        //Black King
       else if(color=='b'){
          //Check for castle availability
        if(!hasmoved()){
          //Left Castle
          int lCount = 0;
          for(int i=row;i<=row&&i==7;i++){
           for(int j=col-1;j>0;j--){
             if(adjMatrix[i][j]==NO_EDGE){
                ++lCount;
             }
           }
         }
         if(lCount==3&&adjMatrix[row][0]==BR1){
            Position p;
            p.setP(row,chooseCol(col-2),getId());
            setAvailableMoves(++counter, p);
            setQueensCastle(true);
         }
         //Right Castle
         int rCount = 0;
          for(int i=row;i<=row&&i==7;i++){
           for(int j=col+1;j<BOARD_SIZE-1;j++){
             if(adjMatrix[i][j]==NO_EDGE){
                ++rCount;
             }
           }
         }
         if(rCount==2&&adjMatrix[row][BOARD_SIZE-1]==BR2){
            Position p1;
            p1.setP(row,chooseCol(col+2),getId());
            setAvailableMoves(++counter, p1);
            setKingsCastle(true);
         }
        }
        //Check moves up
        int isWhiteKing = 0;
        for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
             for(int j=col-1;j<=col+1&&j>=0&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col;j<=col;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isWhiteKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     isWhiteKing==0&&adjMatrix[i][j]!=WK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check moves up-left
        isWhiteKing = 0;
        for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
             for(int j=col-2;j<=col&&j>=0;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row+2;i>=row&&i<BOARD_SIZE;i--){
             for(int j=col-2;j<=col-2&&j>=0;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isWhiteKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     isWhiteKing==0&&adjMatrix[i][j]!=WK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
         //Check moves up-right
        isWhiteKing = 0;
        for(int i=row+2;i<=row+2&&i<BOARD_SIZE;i++){
             for(int j=col;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row+2;i>=row&&i<BOARD_SIZE;i--){
             for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row+1;i<=row+1&&i<BOARD_SIZE;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isWhiteKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     isWhiteKing==0&&adjMatrix[i][j]!=WK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves down
        isWhiteKing = 0;
        for(int i=row-2;i>=row-2&&i>=0;i--){
             for(int j=col-1;j<=col+1&&j>=0&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col;j<=col;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isWhiteKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     isWhiteKing==0&&adjMatrix[i][j]!=WK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves down-left
        isWhiteKing = 0;
        for(int i=row-2;i<=row-2&&i>=0;i++){
             for(int j=col-2;j<=col&&j>=0;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row;i>=row-2&&i>=0;i--){
             for(int j=col-2;j<=col-2&&j>=0;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isWhiteKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     isWhiteKing==0&&adjMatrix[i][j]!=WK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves down-right
        isWhiteKing = 0;
        for(int i=row-2;i<=row-2&&i>=0;i++){
             for(int j=col;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row;i>=row-2&&i>=0;i--){
             for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row-1;i<=row-1&&i>=0;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isWhiteKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     isWhiteKing==0&&adjMatrix[i][j]!=WK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves left
        isWhiteKing = 0;
        for(int i=row+1;i>=row-1&&i>=0&&i<BOARD_SIZE;i--){
             for(int j=col-2;j<=col-2&&j>=0;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row;i<=row;i++){
           Position p;
           int flag = 0;
           for(int j=col-1;j<=col-1&&j>=0;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isWhiteKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     isWhiteKing==0&&adjMatrix[i][j]!=WK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
        //Check moves right
        isWhiteKing = 0;
        for(int i=row+1;i>=row-1&&i>=0&&i<BOARD_SIZE;i--){
             for(int j=col+2;j<=col+2&&j<BOARD_SIZE;j++){
                if(adjMatrix[i][j]==WK){
                    ++isWhiteKing;
                }
             }
        }
        for(int i=row;i<=row;i++){
           Position p;
           int flag = 0;
           for(int j=col+1;j<=col+1&&j<BOARD_SIZE;j++){
             if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&isWhiteKing==0){
                p.setP(i,chooseCol(j),getId());
                setAvailableMoves(++counter, p);
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                p.setP(i,chooseCol(j),getId(),-1,adjMatrix[i][j]);
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     isWhiteKing==0&&adjMatrix[i][j]!=WK){
                p.setP(i,chooseCol(j),getId());
                p.isTake = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
            }
           if(flag==1){break;}
         }
       }
        setAvailableMovesCnt(counter);
        if(counter>0){
            setHasAvMoves(true);
        }
        else{
            setHasAvMoves(false);
        }
    }
    else{
        return;
    }
}
char King::isAvailableMove(Position &p){
    ch = 'N';
    for(size_t i=0;i<getAvMoves().size();i++){
        if(getAvMoves()[i].col==p.col&&getAvMoves()[i].row==p.row&&getAvMoves()[i].prot==-1){
            if(getAvMoves()[i].isTake){
                ch = 'T';
                break;
            }
            ch = 'Y';
        }
    }
    return ch;
}
char King::move(Position& dest,std::vector<std::vector<int>> &adjMatrix, char color){
    chMov = isAvailableMove(dest);
    Position p;
    //cout<<"Is av move: "<<ch<<endl;
    if(chMov!='N'){
        p.setP(getPosition().row,getPosition().col,getId());
        adjMatrix[p.row][chooseNumLetter(p.col)] = NO_EDGE;
        adjMatrix[dest.row][chooseNumLetter(dest.col)] = getId();
        setPosition(dest);
        if(chMov=='T'){
            return chMov;
        }
        else if(chMov=='Y'){
            return chMov;
        }
        setMoved(true);
    }
    else{
       return chMov;
    }
    return chMov;
}
King &King::operator=(const King &other){
    Figure::operator=(other);
    color = other.color;
    return *this;
}


#ifndef KING_HPP_INCLUDED
#define KING_HPP_INCLUDED
#include "Figures.hpp"
#include "Rook.hpp"
using namespace std;

class King : public Figure{
    private:
        char color;
        void setColor(char);
        char ch{'N'};
        char chMov{'N'};
    public:
        King();
        King(string, Position &, int, int, char);
        King(const King &);
        King(const Figure &);
        ~King()override;
        void setChecked(bool)override;
        bool check()const override;
        void setQueensCastle(bool)override;
        bool isQueenCastle()const override;
        void setKingsCastle(bool)override;
        bool isKingCastle()const override;
        void setMoved(bool) override;
        bool hasmoved()const override;
        void setHasAvMoves(bool) override;
        bool hasAvMoves()const override;
        char getColor()const override;
        string getTitle()const;
        void setPosition(Position &);
        Position& getPosition();
        void setAvailableMoves(int, Position&)override;
        void setAvailableMovesCnt(int) override;
        vector<Position> getAvMoves()const override;
        int getAvMovesCnt()const override;
        King &operator=(const King &);
        int getId()const;
        void addAvailableMoves(std::vector<std::vector<int>> &,char) override;
        char isAvailableMove(Position&);
        char move(Position &, std::vector<std::vector<int>> &adjMatrix, char color) override;
};
#endif // KING_HPP_INCLUDED

#ifndef SEARCH_H_INCLUDED
#define SEARCH_H_INCLUDED

#include <QString>
#include <vector>
#include <mutex>
#include <string>
#include <atomic>

#include "misc.h"
#include "movepick.h"
#include "types.h"

#ifndef NDEBUG
#define NDEBUG
#endif

class PositionAI;
//POLYBOOK
struct BookSettings{
    std::string path{"<empty>"};
    int depth{255};
    bool ownBook{false};
	bool bestBookMove{false};
};
namespace Search {

/// Threshold used for countermoves based pruning
const int CounterMovePruneThreshold = 0;

extern std::atomic<bool> bestMoveForGuiDone;
extern std::string bestMoveForGui;
extern std::string variantsForLG;
extern int variantsCnt;
extern std::atomic<bool> variantsCntDone;
extern std::mutex mBestMoveForGui;
extern StateListPtr states;
extern PositionAI startPos;
extern volatile int skillLevel;
extern volatile int skillLevelPlay;
extern int hashMem;
//DRAW
extern std::atomic<int> positionScore;
//POLYBOOK
extern BookSettings bookSettings;
/// Stack struct keeps track of the information we need to remember from nodes
/// shallower and deeper in the tree during the search. Each search thread has
/// its own array of Stack objects, indexed by the current ply.

struct Stack {
    Move* pv;
    PieceToHistory* contHistory;
    int ply;
    Move currentMove;
    Move excludedMove;
    Move killers[2];
    Value staticEval;
    int statScore;
    int moveCount;
};


/// RootMove struct is used for moves at the root of the tree. For each root move
/// we store a score and a PV (really a refutation in the case of moves which
/// fail low). Score is normally set at -VALUE_INFINITE for all non-pv moves.

struct RootMove {

    explicit RootMove(Move m) : pv(1, m) {}
    bool extract_ponder_from_tt(PositionAI& pos);
    bool operator==(const Move& m) const { return pv[0] == m; }
    bool operator<(const RootMove& m) const { // Sort in descending order
        return m.score != score ? m.score < score
                                : m.previousScore < previousScore;
    }

    Value score = -VALUE_INFINITE;
    Value previousScore = -VALUE_INFINITE;
    int selDepth = 0;
    std::vector<Move> pv;
};

typedef std::vector<RootMove> RootMoves;


/// LimitsType struct stores information sent by GUI about available time to
/// search the current move, maximum depth/time, or if we are in analysis mode.

struct LimitsType {

    LimitsType() { // Init explicitly due to broken value-initialization of non POD in MSVC
        nodes = time[WHITE] = time[BLACK] = inc[WHITE] = inc[BLACK] =
                npmsec = movestogo = depth = movetime = mate = perft = infinite = 0;
    }

    bool use_time_management() const {
        return !(mate | movetime | depth | nodes | perft | infinite);
    }

    std::vector<Move> searchmoves;
    int time[COLOR_NB], inc[COLOR_NB], npmsec, movestogo, depth,
    movetime, mate, perft, infinite;
    int64_t nodes;
    TimePoint startTime;
};

extern LimitsType Limits;

void init();
void clear();
//std::string &getBestMoveForGui();
//void setBestMoveForGui(std::string str);
StateListPtr &getStates();
PositionAI &getStartPos();
} // namespace Search

#endif // #ifndef SEARCH_H_INCLUDED

#ifndef CHESSAI_H
#define CHESSAI_H

#include "search.h"
#include "uci.h"

#include <QObject>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <string>

class ChessAI : public QObject
{
    Q_OBJECT

public:
    explicit ChessAI(QObject *parent = nullptr);
    ~ChessAI();

    void setUciMoveCommand(const std::string &cmd);
    void setTime( int t);
    int getTime()const;
    std::atomic<bool> stopMoveTh{false};
signals:
    void finished();
    void error(QString err);
    void uciMoveChanged();
public slots:
    std::string getUciMove();
    void process();
    void stop();
    void waitForChessAi();
private:
    std::string uciMove;
    std::string uciMoveCommand;
    int time{0};
};

#endif // CHESSAI_H

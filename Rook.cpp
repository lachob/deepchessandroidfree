#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <exception>
#include <stdexcept>

#include "Rook.hpp"
#include "functions.hpp"
#define NO_EDGE -1
#define BOARD_SIZE 8

//Rook
Rook::Rook():Figure(){
    setColor(' ');
}
Rook::Rook(std::string title, Position& p, int avMovesCnt, int id, char color):Figure(title,p,avMovesCnt,id){
    setColor(color);
}
Rook::Rook(const Rook &other):Figure(other){
    color = other.color;
}
Rook::Rook(const Figure &other):Figure(other){
    if(other.getTitle()[0]=='b'){
        color = 'b';
    }
    else{
        color = 'w';
    }
}
Rook::~Rook(){}

int Rook::getId()const{
    return Figure::getId();
}
void Rook::setColor(char color){
    this->color = color;
}

std::string Rook::getTitle()const{
    return Figure::getTitle();
}
char Rook::getColor()const{
    return color;
}
void Rook::setPosition(Position &p){
    Figure::setPosition(p);
}
Position& Rook::getPosition(){
    return Figure::getPosition();
}

void Rook::setAvailableMoves(int index, Position& p){
    availableMoves.push_back(p);
}
void Rook::setAvailableMovesCnt(int newCount){
    availableMovesCnt = newCount+1;
}
std::vector<Position> Rook::getAvMoves()const{
    return availableMoves;
}
int Rook::getAvMovesCnt()const{
   return availableMovesCnt;
}
void Rook::setMoved(bool moved){
    hasMoved = moved;
}
bool Rook::hasmoved()const{
    return hasMoved;
}
void Rook::addAvailableMoves(std::vector<std::vector<int>> &adjMatrix, char color){
    int row = getPosition().row;
    int col = chooseNumLetter(getPosition().col);
    int a=0;
    a=col-1<8&&col-1>-1?adjMatrix[row][col-1]:-2;
    int b=0;
    b=col+1<8&&col+1>-1?adjMatrix[row][col+1]:-2;
    int c=0;
    c=row-1<8&&row-1>-1?adjMatrix[row-1][col]:-2;
    int d=0;
    d=row+1<8&&row+1>-1?adjMatrix[row+1][col]:-2;

    if(a>=-1||b>=-1||c>=-1||d>=-1){
       int counter = -1;
        //White Rooks
       if(color=='w'){
        //Check rows up
        int flag = 0;
        int openChPr = 0;
        int openChMe = 0;
        int protFigCnt = 0;
         for(int i=row+1;i<BOARD_SIZE;i++){
           Position p;
           Position p1;
           for(int j=col;j<=col;j++){
             if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),R_UP);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==BK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,R_UP);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=BK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==BK){
                p.setP(row,chooseCol(col),openChMe,R_UP);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),R_UP);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),R_UP,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),R_UP);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i+1<BOARD_SIZE)&&((adjMatrix[i+1][j]==NO_EDGE)||(adjMatrix[i+1][j]>=WR1&&adjMatrix[i+1][j]<=WO2)||(adjMatrix[i+1][j]>=WP_TRANSF_FIRST&&adjMatrix[i+1][j]<=WP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i+1,chooseCol(j),getId(),R_UP);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),R_UP);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check rows down
        flag = 0;
        openChPr = 0;
        openChMe = 0;
        protFigCnt = 0;
         for(int i=row-1;i>=0;i--){
           Position p;
           Position p1;
           for(int j=col;j<=col;j++){
             if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),R_DOWN);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==BK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,R_DOWN);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=BK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==BK){
                p.setP(row,chooseCol(col),openChMe,R_DOWN);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),R_DOWN);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),R_DOWN,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),R_DOWN);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i-1>NO_EDGE)&&((adjMatrix[i-1][j]==NO_EDGE)||(adjMatrix[i-1][j]>=WR1&&adjMatrix[i-1][j]<=WO2)||(adjMatrix[i-1][j]>=WP_TRANSF_FIRST&&adjMatrix[i-1][j]<=WP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i-1,chooseCol(j),getId(),R_DOWN);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),R_DOWN);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check cols right
         flag = 0;
        openChPr = 0;
        openChMe = 0;
        protFigCnt = 0;
         for(int i=row;i<=row;i++){
           Position p;
           Position p1;
           for(int j=col+1;j<BOARD_SIZE;j++){
             if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),R_RIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==BK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,R_RIGHT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=BK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==BK){
                p.setP(row,chooseCol(col),openChMe,R_RIGHT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),R_RIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),R_RIGHT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),R_RIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((j+1<BOARD_SIZE)&&((adjMatrix[i][j+1]==NO_EDGE)||(adjMatrix[i][j+1]>=WR1&&adjMatrix[i][j+1]<=WO2)||(adjMatrix[i][j+1]>=WP_TRANSF_FIRST&&adjMatrix[i][j+1]<=WP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i,chooseCol(j+1),getId(),R_RIGHT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),R_RIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check cols left
         flag = 0;
        openChPr = 0;
        openChMe = 0;
        protFigCnt = 0;
         for(int i=row;i<=row;i++){
           Position p;
           Position p1;
           for(int j=col-1;j>=0;j--){
             if(adjMatrix[i][j]==getId()){continue;}
             else if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),R_LEFT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==BK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,R_LEFT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=BK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==BK){
                p.setP(row,chooseCol(col),openChMe,R_LEFT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),R_LEFT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),R_LEFT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==BK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),R_LEFT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((j-1>NO_EDGE)&&((adjMatrix[i][j-1]==NO_EDGE)||(adjMatrix[i][j-1]>=WR1&&adjMatrix[i][j-1]<=WO2)||(adjMatrix[i][j-1]>=WP_TRANSF_FIRST&&adjMatrix[i][j-1]<=WP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i,chooseCol(j-1),getId(),R_LEFT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),R_LEFT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
       }
        //Black rooks
       else if(color=='b'){
        int flag = 0;
        int openChPr = 0;
        int openChMe = 0;
        int protFigCnt = 0;
        //Check rows up
         for(int i=row+1;i<BOARD_SIZE;i++){
           Position p;
           Position p1;
           for(int j=col;j<=col;j++){
             if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),R_UP);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==WK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,R_UP);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=WK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==WK){
                p.setP(row,chooseCol(col),openChMe,R_UP);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),R_UP);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),R_UP,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),R_UP);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i+1<BOARD_SIZE)&&((adjMatrix[i+1][j]>=BR1&&adjMatrix[i+1][j]<=BO2)||(adjMatrix[i+1][j]>=BP_TRANSF_FIRST&&adjMatrix[i+1][j]<=BP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i+1,chooseCol(j),getId(),R_UP);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),R_UP);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check rows down
         flag = 0;
        openChPr = 0;
        openChMe = 0;
        protFigCnt = 0;
         for(int i=row-1;i>=0;i--){
           Position p;
           Position p1;
           for(int j=col;j<=col;j++){
             if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),R_DOWN);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==WK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,R_DOWN);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=WK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==WK){
                p.setP(row,chooseCol(col),openChMe,R_DOWN);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3&&flag!=4){
                p.setP(i,chooseCol(j),getId(),R_DOWN);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),R_DOWN,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),R_DOWN);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((i-1>NO_EDGE)&&((adjMatrix[i-1][j]>=BR1&&adjMatrix[i-1][j]<=BO2)||(adjMatrix[i-1][j]>=BP_TRANSF_FIRST&&adjMatrix[i-1][j]<=BP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i-1,chooseCol(j),getId(),R_DOWN);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),R_DOWN);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check cols right
         flag = 0;
        openChPr = 0;
        openChMe = 0;
        protFigCnt = 0;
         for(int i=row;i<=row;i++){
           Position p;
           Position p1;
           for(int j=col+1;j<BOARD_SIZE;j++){
             if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),R_RIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==WK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,R_RIGHT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=WK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==WK){
                p.setP(row,chooseCol(col),openChMe,R_RIGHT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),R_RIGHT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),R_RIGHT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),R_RIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((j+1<BOARD_SIZE)&&((adjMatrix[i][j+1]>=BR1&&adjMatrix[i][j+1]<=BO2)||(adjMatrix[i][j+1]>=BP_TRANSF_FIRST&&adjMatrix[i][j+1]<=BP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i,chooseCol(j+1),getId(),R_RIGHT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),R_RIGHT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
         //Check cols left
         flag = 0;
        openChPr = 0;
        openChMe = 0;
        protFigCnt = 0;
         for(int i=row;i<=row;i++){
           Position p;
           Position p1;
           for(int j=col-1;j>=0;j--){
             if(flag==4&&((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))){
                p.isCheck = true;
                p.setP(i,chooseCol(j),getId(),R_LEFT);
                setAvailableMoves(++counter, p);
             }
             else if(flag==2&&adjMatrix[i][j]==WK){
                if(protFigCnt==1){
                    p.setP(row,chooseCol(col),openChPr,R_LEFT);
                    p.opensCh = true;
                    setAvailableMoves(++counter, p);
                }
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST))&&
                     adjMatrix[i][j]!=WK){
                flag = 1;
                break;
             }
             else if(flag>=2&&((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))){
                flag = 1;
                break;
             }
             else if(flag==3&&adjMatrix[i][j]==WK){
                p.setP(row,chooseCol(col),openChMe,R_LEFT);
                p.opensCh = true;
                setAvailableMoves(++counter, p);
                flag = 1;
                break;
             }
             else if(((adjMatrix[i][j]==NO_EDGE)||(adjMatrix[i][j]>=WP_ENPASS_LAST&&adjMatrix[i][j]<=BP_ENPASS_FIRST))&&flag!=2&&flag!=3){
                p.setP(i,chooseCol(j),getId(),R_LEFT);
                setAvailableMoves(++counter, p);
             }
             else if(((adjMatrix[i][j]>=BR1&&adjMatrix[i][j]<=BO2)||(adjMatrix[i][j]>=BP_TRANSF_FIRST&&adjMatrix[i][j]<=BP_TRANSF_LAST))&&flag!=3){
                    p.setP(i,chooseCol(j),getId(),R_LEFT,adjMatrix[i][j]);
                    setAvailableMoves(++counter, p);
                    flag = 3;
                    openChMe = adjMatrix[i][j];
             }
             else if((adjMatrix[i][j]>=WR1&&adjMatrix[i][j]<=WO2)||(adjMatrix[i][j]>=WP_TRANSF_FIRST&&adjMatrix[i][j]<=WP_TRANSF_LAST)){
                protFigCnt++;
                if(adjMatrix[i][j]==WK){
                    p.isCheck = true;
                    p.setP(i,chooseCol(j),getId(),R_LEFT);
                    setAvailableMoves(++counter, p);
                    flag = 4;
                    if((j-1>NO_EDGE)&&((adjMatrix[i][j-1]>=BR1&&adjMatrix[i][j-1]<=BO2)||(adjMatrix[i][j-1]>=BP_TRANSF_FIRST&&adjMatrix[i][j-1]<=BP_TRANSF_LAST))){
                        p1.isCheck = true;
                        p1.setP(i,chooseCol(j-1),getId(),R_LEFT);
                        setAvailableMoves(++counter, p1);
                    }
                }
                else if(flag!=3){
                    p.isTake = true;
                    p.setP(i,chooseCol(j),getId(),R_LEFT);
                    setAvailableMoves(++counter, p);
                    flag = 2;
                    openChPr = adjMatrix[i][j];
                }
                if(flag ==1){break;}
             }
            }
           if(flag==1){break;}
         }
        }
        setAvailableMovesCnt(counter);
    }
    else{
        return;
    }
}
char Rook::isAvailableMove(Position &p){
    ch = 'N';
    for(size_t i=0;i<getAvMoves().size();i++){
        if(getAvMoves()[i].col==p.col&&getAvMoves()[i].row==p.row&&getAvMoves()[i].prot==NO_EDGE){
            if(getAvMoves()[i].isCheck){
                ch = 'C';
                break;
            }
            else if(getAvMoves()[i].isTake){
                ch = 'T';
                break;
            }
            ch = 'Y';
        }
    }
    return ch;
}

char Rook::move(Position &dest, std::vector<std::vector<int>> &adjMatrix, char color){
    chMov = isAvailableMove(dest);
    Position p;
    //cout<<"Is av move: "<<ch<<endl;
    if(chMov!='N'){
        if(chMov=='C'){
            return chMov;
        }
        p.setP(getPosition().row,getPosition().col,getId());
        adjMatrix[p.row][chooseNumLetter(p.col)] = NO_EDGE;
        adjMatrix[dest.row][chooseNumLetter(dest.col)] = getId();
        setPosition(dest);
        if(chMov=='T'){
            return chMov;
        }
        else if(chMov=='Y'){
            return chMov;
        }
        setMoved(true);
    }
    else{
       return chMov;
    }
    return chMov;
}
void Rook::moveCastle(std::vector<std::vector<int>> &adjMatrix,char color,char castle){
    Position p;
    Position dest;
    if(color=='w'&&!hasmoved()){
        if(castle=='L'){
            dest.setP(0,'D',getId());
        }
        else if(castle=='R'){
            dest.setP(0,'F',getId());
        }
        else{
            //cout<<"Not available castle";
            return;
        }
    }
    else if(color=='b'&&!hasmoved()){
        if(castle=='L'){
            dest.setP(7,'D',getId());
        }
        else if(castle=='R'){
            dest.setP(7,'F',getId());
        }
        else{
            //cout<<"Not available castle";
            return;
        }
    }
    p.setP(getPosition().row,getPosition().col,getId());
    adjMatrix[p.row][chooseNumLetter(p.col)] = NO_EDGE;
    adjMatrix[dest.row][chooseNumLetter(dest.col)] = getId();
    setPosition(dest);
    setMoved(true);
}
Rook &Rook::operator=(const Rook &other){
    Figure::operator=(other);
    color = other.color;
    return *this;
}

#include "dbmanager.hpp"

int DBManager::connNumber = 0;

DBManager::DBManager()
{
    QFile dfile("assets:/db/DeepChess.db");
    QString filePath = QStandardPaths::writableLocation( QStandardPaths::StandardLocation::AppLocalDataLocation );
    filePath.append( "/DeepChess.db");
    if(!QFile::exists( filePath )){
        if (dfile.exists()) {
            if( QFile::exists( filePath ) )//TO DELETE after deploy
                QFile::remove( filePath );

            if( dfile.copy( filePath ) )
                QFile::setPermissions( filePath, QFile::WriteOwner | QFile::ReadOwner );

        }
    }
    //PUZZLES
    QFile dpfile("assets:/db/DeepChessPuzzle.db");
    QString pfilePath = QStandardPaths::writableLocation( QStandardPaths::StandardLocation::AppLocalDataLocation );
    pfilePath.append( "/DeepChessPuzzle.db");
    if(!QFile::exists( pfilePath )){
        if (dpfile.exists()) {
            //if( QFile::exists( pfilePath ) )
            //QFile::remove( pfilePath );

            if( dpfile.copy( pfilePath ) )
                QFile::setPermissions( pfilePath, QFile::WriteOwner | QFile::ReadOwner );

        }
    }

    //BOOKS
    QFile bookfile("assets:/openingbooks/DeepChessBook.bin");
    QString bookfilePath = QStandardPaths::writableLocation( QStandardPaths::StandardLocation::AppLocalDataLocation );
    bookfilePath.append( "/DeepChessBook.bin");
    if(!QFile::exists( bookfilePath )){
        if (bookfile.exists()) {

            if( bookfile.copy( bookfilePath ) )
                QFile::setPermissions( bookfilePath, QFile::WriteOwner | QFile::ReadOwner );

        }
    }

    ++connNumber;
    userData = QSqlDatabase::addDatabase(DRIVER, ("conn" + QString::number(connNumber)));
    userData.setDatabaseName( filePath );
    //PUZZLES
    ++connNumber;
    puzzleData = QSqlDatabase::addDatabase(DRIVER, ("conn" + QString::number(connNumber)));
    puzzleData.setDatabaseName( pfilePath );

    if(!userData.open()){
        dbExistsInfo = "DB cannot open!";
    }
    else{
        dbExistsInfo = "DB created and opened!";
        isDbOpened = true;

        QSqlQuery createTableGames(userData);
        createTableGames.prepare("CREATE TABLE IF NOT EXISTS SavedGamesNew (id INTEGER PRIMARY KEY AUTOINCREMENT,gamename TEXT NOT NULL,gamefile TEXT NOT NULL,pgnfile TEXT NULL)");
        if(!createTableGames.exec()){
            dbExistsInfo = "SaveSavedGamesNewdGames NOT created!";
            createTableGames.clear();
        }
        else{
            dbExistsInfo = "SavedGamesNew created!";
            createTableGames.clear();
        }

        QSqlQuery createTableRateCnt(userData);
        createTableRateCnt.prepare("CREATE TABLE IF NOT EXISTS RateCount (id INTEGER PRIMARY KEY AUTOINCREMENT,cnt INT NOT NULL,used INT NOT NULL)");
        if(!createTableRateCnt.exec()){
            dbExistsInfo = "RateCount NOT created!";
            createTableRateCnt.clear();
        }
        else{
            dbExistsInfo = "RateCount created!";
            createTableRateCnt.clear();
        }
        //GDPR
        QSqlQuery crTabGDPR(userData);
        crTabGDPR.prepare("CREATE TABLE IF NOT EXISTS gdpr (id INTEGER PRIMARY KEY AUTOINCREMENT,agreed INT NOT NULL,consent TEXT NULL)");
        if(!crTabGDPR.exec()){
            dbExistsInfo = "Gdpr NOT created!";
            crTabGDPR.clear();
        }
        else{
            dbExistsInfo = "GDPR created!";
            crTabGDPR.clear();
        }
        QSqlQuery checkCntGDPR(userData);
        int result=0;
        checkCntGDPR.prepare("SELECT COUNT(*) FROM gdpr ");

        if(!checkCntGDPR.exec()){
            checkCntGDPR.clear();
            return;
        }
        else{
            while(checkCntGDPR.next()){
                result = checkCntGDPR.value(0).toInt();
            }
            checkCntGDPR.clear();
        }
        if(result==0){
            QSqlQuery insert(userData);
            insert.prepare("INSERT INTO gdpr (agreed, consent) VALUES(?, ?) ");
            QString consent = "This App contains Ads By clicking Accept, You agree to receive relevant Ads otherwise, You can install paid version.";
            insert.addBindValue(0);
            insert.addBindValue(consent);
            insert.exec();
            insert.clear();
        }
        //POLYBOOK
        QSqlQuery createBookTab(userData);
        createBookTab.prepare("CREATE TABLE IF NOT EXISTS OpenBooks (id INTEGER PRIMARY KEY AUTOINCREMENT,bookname TEXT NOT NULL)");
        if(!createBookTab.exec()){
            dbExistsInfo = "Book Table NOT created!";
            createBookTab.clear();
        }
        else{
            dbExistsInfo = "Book Table created!";
            createBookTab.clear();
        }
        QSqlQuery checkBooktable(userData);
        int result1=0;
        checkBooktable.prepare("SELECT COUNT(*) FROM OpenBooks ");

        if(!checkBooktable.exec()){
            checkBooktable.clear();
            return;
        }
        else{
            while(checkBooktable.next()){
                result1 = checkBooktable.value(0).toInt();
            }
            checkBooktable.clear();
        }
        if(result1==0){
            QSqlQuery insertB(userData);
            insertB.prepare("INSERT INTO OpenBooks (bookname) VALUES(?) ");
            insertB.addBindValue(bookfilePath);
            insertB.exec();
            insertB.clear();
        }
    }
    //PUZZLES
    if(!puzzleData.open()){
        dbExistsInfo = "DBPuzzles cannot open!";
    }
    else{
        dbExistsInfo = "DBPuzzles created and opened!";
        QSqlQuery crTabCheckPuzz(puzzleData);
        crTabCheckPuzz.prepare("CREATE TABLE IF NOT EXISTS CheckPuzzleNew (id INTEGER PRIMARY KEY AUTOINCREMENT,adClicked INT NOT NULL,gmCnt INT NOT NULL)");
        if(!crTabCheckPuzz.exec()){
            dbExistsInfo = "CheckPuzzleNew NOT created!";
            crTabCheckPuzz.clear();
        }
        else{
            dbExistsInfo = "CheckPuzzleNew created!";
            crTabCheckPuzz.clear();
        }
        QSqlQuery checkCount(puzzleData);
        int result=0;
        checkCount.prepare("SELECT COUNT(*) FROM CheckPuzzleNew ");

        if(!checkCount.exec()){
            checkCount.clear();
            return;
        }
        else{
            while(checkCount.next()){
                result = checkCount.value(0).toInt();
            }
            checkCount.clear();
        }
        if(result==0){
            QSqlQuery insert(puzzleData);
            insert.prepare("INSERT INTO CheckPuzzleNew (adClicked, gmCnt) VALUES(?, ?) ");
            insert.addBindValue(0);
            insert.addBindValue(0);
            insert.exec();
            insert.clear();
        }
    }

    //STATISTICS
    QSqlQuery createStatTable(userData);
    createStatTable.prepare("CREATE TABLE IF NOT EXISTS Statistics (id INTEGER PRIMARY KEY AUTOINCREMENT,level INTEGER NULL,result INTEGER NULL,timecontrol INTEGER NULL,recdate INTEGER NULL)");
    if(!createStatTable.exec()){
        dbExistsInfo = "Statistics Table NOT created!";
        createStatTable.clear();
    }
    else{
        dbExistsInfo = "Statistics Table created!";
        createStatTable.clear();
    }

    //LASTLEVEL
    QSqlQuery createLastLevelTable(userData);
    createLastLevelTable.prepare("CREATE TABLE IF NOT EXISTS LastLevel (id INTEGER PRIMARY KEY AUTOINCREMENT,level INTEGER NULL)");
    if(!createLastLevelTable.exec()){
        dbExistsInfo = "LastLevel Table NOT created!";
        createLastLevelTable.clear();
    }
    else{
        dbExistsInfo = "LastLevel Table created!";
        createLastLevelTable.clear();
    }
    QSqlQuery checkCntLastLevel(userData);
    int levelResult=0;
    checkCntLastLevel.prepare("SELECT COUNT(*) FROM LastLevel ");

    if(!checkCntLastLevel.exec()){
        checkCntLastLevel.clear();
        return;
    }
    else{
        while(checkCntLastLevel.next()){
            levelResult = checkCntLastLevel.value(0).toInt();
        }
        checkCntLastLevel.clear();
    }
    if(levelResult == 0){
        QSqlQuery insertLevel(userData);
        insertLevel.prepare("INSERT INTO LastLevel (level) VALUES(?) ");
        insertLevel.addBindValue(1);
        insertLevel.exec();
        insertLevel.clear();
    }
}

bool DBManager::updateDBAchievemnts(QString level,int result)
{
    QSqlQuery updateCount(userData);
    updateCount.prepare("UPDATE Achievements SET wincount=? WHERE Level= ? ");
    updateCount.addBindValue(result);
    updateCount.addBindValue(level);

    if(!updateCount.exec()){
        dbExistsInfo = "Wincount NOT updated!";
        updateCount.clear();
        return false;
    }
    else{
        dbExistsInfo = "Wincount updated!";
        updateCount.clear();
        return true;
    }
}

int DBManager::queryDBAchievemnts(QString level)
{
    int winCount = 0;
    QSqlQuery selectWinCount(userData);
    selectWinCount.prepare("SELECT wincount FROM Achievements WHERE Level= ? ");
    selectWinCount.addBindValue(level);

    if(!selectWinCount.exec()){
        dbExistsInfo = "Problem with select!";
        selectWinCount.clear();
        return winCount;
    }

    while(selectWinCount.next()){
        QSqlRecord record = selectWinCount.record();
        winCount = record.value(0).toInt();
    }
    dbExistsInfo = "Wincount selected!";

    selectWinCount.clear();

    return winCount;
}

int DBManager::queryRateCount()
{
    QSqlQuery checkRate(userData);
    int exists = 0;
    int result=0, used = 0;
    checkRate.prepare("SELECT COUNT(*) FROM RateCount ");

    if(!checkRate.exec()){
        checkRate.clear();
    }
    else{
        while(checkRate.next()){
            exists = checkRate.value(0).toInt();
        }
        checkRate.clear();
    }
    if(exists == 0){
        QSqlQuery insert(userData);
        insert.prepare("INSERT INTO RateCount (cnt, used) VALUES(?, ?) ");
        insert.addBindValue(1);
        insert.addBindValue(0);
        insert.exec();
        insert.clear();
    }
    QSqlQuery select(userData);
    select.prepare("SELECT cnt, used FROM RateCount ");

    if(!select.exec()){
        select.clear();
    }
    else{
        while(select.next()){
            result = select.value(0).toInt();
            used = select.value(1).toInt();
        }
        select.clear();
    }
    if(result < GAMES_PASSED_FOR_RATE){
        int upd = result;
        ++upd;
        QSqlQuery update(userData);
        update.prepare("UPDATE RateCount SET cnt = ? ");
        update.addBindValue(upd);
        update.exec();
        update.clear();
    }
    if(used == 0)
        return result;
    else
        return 0;
}

bool DBManager::isUsedRate()
{
    int used = 0;
    QSqlQuery select(userData);
    select.prepare("SELECT used FROM RateCount ");

    if(!select.exec()){
        select.clear();
    }
    else{
        while(select.next()){
            used = select.value(0).toInt();
        }
        select.clear();
    }
    if(used > 0)
        return true;
    else
        return false;
}

void DBManager::setUsedRate()
{
    QSqlQuery update(userData);
    update.prepare("UPDATE RateCount SET used = ? ");
    update.addBindValue(1);
    update.exec();
    update.clear();
}

QString DBManager::exportFile(const QString &name, QChar &color, int plLevel, bool isGreaterThan11)
{
    QString folder;
    if(isGreaterThan11)
        folder = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    else
        folder = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
    QString _name = name;
    if(_name.contains(" ")){
        _name = _name.replace(" ","_");
    }
    QString f = folder+"/"+_name+".pgn";
    QFile file(f);
    if (!file.exists()) {
        // create the folder, if necessary
        std::unique_ptr<QDir> dir=std::make_unique< QDir>(folder);
        if (!dir->exists()) {
            dir->mkpath(".");
        }
        file.open(QIODevice::WriteOnly);
        QString s = queryDBGamesByName(name, 1);

        if(color=='w')
            s = "[White \"Player\"] \n[Black \"Deep Chess level "+QString::number(plLevel)+"\"] \n"+s;
        else if(color=='b')
            s = "[White \"Deep Chess level "+QString::number(plLevel)+"\"] \n[Black \"Player\"] \n"+s;

        QByteArray ba = s.toLocal8Bit();
        file.write(ba);
        if(file.exists()){
            dbExistsInfo = isGreaterThan11 ? "Exported in App location!" : "Exported in SDCard->Downloads!";
            file.close();
            return f;
        }
        else{
            dbExistsInfo = isGreaterThan11 ? "Cannot write to App location!" : "External Storage not mounted!";
            file.close();
            return "";
        }
    }
    else { //if (file.exists())
        dbExistsInfo = "File already exists!";
        return "";
    }
}

bool DBManager::loadPuzzle(QString &puzzleFen, QStringList &puzzleMoves)//TESTPUZZ
{
    QSqlQuery selectPuzzle(puzzleData);
    selectPuzzle.prepare("SELECT id,startpos,moves FROM puzzles WHERE status < 1 ORDER BY id ASC LIMIT 1");

    if(!selectPuzzle.exec()){
        dbExistsInfo = "Puzzle Not opened!";
        selectPuzzle.clear();
        return false;
    }
    else{
        while(selectPuzzle.next()){
            puzzleId = selectPuzzle.value(0).toInt();
            puzzleFen = selectPuzzle.value(1).toString();
            puzzleMoves = selectPuzzle.value(2).toString().split(" ",QString::SplitBehavior::SkipEmptyParts);
        }
        //testPuzz = puzzleId;//TESTPUZZ
        selectPuzzle.clear();
        return true;
    }
}

void DBManager::updatePuzzleStatus(int &puzzCnt)
{
    ++puzzCnt;
    int maxId = 0;
    QSqlQuery selectMaxId(puzzleData);
    selectMaxId.prepare("SELECT COUNT(*) FROM puzzles WHERE status < 1");
    selectMaxId.exec();
    while(selectMaxId.next()){
        maxId = selectMaxId.value(0).toInt();
    }

    if(maxId>1){
        QSqlQuery updateStatus(puzzleData);
        updateStatus.prepare("UPDATE puzzles SET status = 1 WHERE id = ? ");
        updateStatus.addBindValue(puzzleId);
        updateStatus.exec();
        updateStatus.clear();
    }
    else{
        QSqlQuery updateStatus(puzzleData);
        updateStatus.prepare("UPDATE puzzles SET status = 0 ");
        updateStatus.exec();
        updateStatus.clear();
    }
    puzzleId = 0;
}

bool DBManager::checkPuzzle()
{
    QSqlQuery select(puzzleData);
    select.prepare("SELECT adClicked FROM CheckPuzzleNew ");
    int result = 0;
    if(!select.exec()){
        select.clear();
    }
    else{
        while(select.next()){
            result = select.value(0).toInt();
        }
        select.clear();
    }
    if(result==0)
        return false;
    else
        return true;
}

int DBManager::checkGameCnt()
{
    QSqlQuery select(puzzleData);
    select.prepare("SELECT gmCnt FROM CheckPuzzleNew ");
    int result = 0;
    if(!select.exec()){
        select.clear();
    }
    else{
        while(select.next()){
            result = select.value(0).toInt();
        }
        select.clear();
    }
    return result;
}

void DBManager::disablePuzzle(bool condition)
{
    QSqlQuery disablePuzz(puzzleData);
    disablePuzz.prepare("UPDATE CheckPuzzleNew SET adClicked = ? ");
    if(condition)
        disablePuzz.addBindValue(0);
    else
        disablePuzz.addBindValue(1);

    disablePuzz.exec();
    disablePuzz.clear();
}

void DBManager::updateGameCnt(int gameCnt)
{
    QSqlQuery updateGCnt(puzzleData);
    updateGCnt.prepare("UPDATE CheckPuzzleNew SET gmCnt = ? ");
    updateGCnt.addBindValue(gameCnt);

    updateGCnt.exec();
    updateGCnt.clear();
}

bool DBManager::checkGDPR()
{
    QSqlQuery selectGDPR(userData);
    selectGDPR.prepare("SELECT agreed FROM gdpr ");
    int result = 0;
    if(!selectGDPR.exec()){
        selectGDPR.clear();
    }
    else{
        while(selectGDPR.next()){
            result = selectGDPR.value(0).toInt();
        }
        selectGDPR.clear();
    }
    if(result==0)
        return false;
    else
        return true;
}
void DBManager::updateGDPR()
{
    QSqlQuery updateGDPR(userData);
    updateGDPR.prepare("UPDATE gdpr SET agreed = 1 ");
    updateGDPR.exec();
    updateGDPR.clear();
}

QString DBManager::checkBooks()
{
    QSqlQuery selectBook(userData);
    selectBook.prepare("SELECT bookname FROM OpenBooks");
    QString result = "";
    if(!selectBook.exec()){
        selectBook.clear();
    }
    else{
        while(selectBook.next()){
            result = selectBook.value(0).toString();
        }
        selectBook.clear();
    }

    return result;
}

void DBManager::updateBooks(const QString name)
{
    QSqlQuery updateBooks(userData);
    updateBooks.prepare("UPDATE OpenBooks SET bookname = ? ");
    updateBooks.addBindValue(name);
    updateBooks.exec();
    updateBooks.clear();
}

void DBManager::updateLastLevel(int level)
{
    QSqlQuery updateLevel(userData);
    updateLevel.prepare("UPDATE LastLevel SET level = ? ");
    updateLevel.addBindValue(level);
    updateLevel.exec();
    updateLevel.clear();
}

int DBManager::getLastLevel()
{
    int level = 0;
    QSqlQuery selectLevel(userData);
    selectLevel.prepare("SELECT level FROM LastLevel ");
    if(!selectLevel.exec()){
        selectLevel.clear();
    }
    else{
        while(selectLevel.next()){
            level = selectLevel.value(0).toInt();
        }
        selectLevel.clear();
    }
    return level;
}

void DBManager::queryDBGames(std::vector<QString> &v)
{
    QSqlQuery queryTable(userData);
    QString s = "";
    queryTable.prepare("SELECT gamename FROM SavedGamesNew ORDER BY id DESC ");

    if(!queryTable.exec()){
        dbExistsInfo = "Problem Quering games!";
    }

    while(queryTable.next()){
        s = queryTable.value(0).toString();
        v.push_back(s);
    }
    dbExistsInfo = QString::number(v.size());
    queryTable.clear();
}

QString DBManager::queryDBGamesByName(const QString &name, const int type)
{
    QString str = "";
    if(type==0){
        QSqlQuery getGame(userData);
        getGame.prepare("SELECT gamefile FROM SavedGamesNew WHERE gamename = ? ");
        getGame.addBindValue(name);

        if(!getGame.exec()){
            dbExistsInfo = "Problem getting game!";
            getGame.clear();
        }
        else{
            int8_t counter = 0;
            while(getGame.next()){
                str = getGame.value(0).toString();
                dbExistsInfo = "Game Saved!";
                ++counter;
            }
            if(counter<=0)
                dbExistsInfo = "Game not found!";

            getGame.clear();
        }
    }
    else if(type==1){
        QSqlQuery getPGN(userData);
        getPGN.prepare("SELECT pgnfile FROM SavedGamesNew WHERE gamename = ? ");
        getPGN.addBindValue(name);

        if(!getPGN.exec()){
            dbExistsInfo = "Problem getting PGN!";
            getPGN.clear();
        }
        else{
            int8_t counter = 0;
            while(getPGN.next()){
                str = getPGN.value(0).toString();
                dbExistsInfo = "PGN Exported!";
                ++counter;
            }
            if(counter<=0)
                dbExistsInfo = "PGN not found!";

            getPGN.clear();
        }
    }
    return str;
}

bool DBManager::saveGame(const QString &name, QString game, QString PGNString, QString &gameEndResultTr)
{
    QSqlQuery tryNameExists(userData);
    tryNameExists.prepare("SELECT gamename FROM SavedGamesNew WHERE gamename = ? ");
    tryNameExists.addBindValue(name);

    if(!tryNameExists.exec()){
        dbExistsInfo = "";
        tryNameExists.clear();
        return false;
    }
    else{
        int8_t counter = 0;
        while(tryNameExists.next()){
            std::string s = tryNameExists.value(0).toString().toLocal8Bit().constData();
            ++counter;
        }
        if(counter>0){
            dbExistsInfo = "Name already exists!";
            tryNameExists.clear();
            return false;
        }
    }

    QSqlQuery saveGame(userData);
    saveGame.prepare("INSERT INTO SavedGamesNew (gamename, gamefile, pgnfile) VALUES(?, ?, ?) ");
    saveGame.addBindValue(name);
    saveGame.addBindValue(game);
    saveGame.addBindValue(PGNString);
    //TODO PGN LOGIC

    if(!saveGame.exec()){
        dbExistsInfo = "Problem with saving game!";
        saveGame.clear();
        return false;
    }
    else{
        dbExistsInfo = "Game Saved!";
        gameEndResultTr = QObject::tr("Game Saved!");
        saveGame.clear();
        return true;
    }
}

bool DBManager::deleteGame(const QString &name)
{
    QSqlQuery deleteGame(userData);
    deleteGame.prepare("DELETE FROM SavedGamesNew WHERE gamename = ? ");
    deleteGame.addBindValue(name);

    if(!deleteGame.exec()){
        dbExistsInfo = "Game Not deleted!";
        deleteGame.clear();
        return false;
    }
    else{
        dbExistsInfo = "Game deleted!";
        deleteGame.clear();
        return true;
    }
}
DBManager::~DBManager()
{
}

#include "chessfield.h"

ChessFieldButton::ChessFieldButton(QWidget *parent, int _figId, int _row, int _col, const QChar &color,
                                   const QString &bordColor, double fieldSize, const QString &boardPos,
                                   const QString &_textUp, const QString &_textDown)
    :QPushButton(parent)
{
    figId = _figId;
    row = _row;
    col = _col;
    isAvMov = false;
    this->color = color;
    borderColor = bordColor;
    isConnected = false;
    _boardPos = boardPos;
    this->setMinimumWidth(static_cast<int>(fieldSize));
    this->setMaximumWidth(static_cast<int>(fieldSize));
    this->setMinimumHeight(static_cast<int>(fieldSize));
    this->setMaximumHeight(static_cast<int>(fieldSize));
    textL = new QVBoxLayout(this);
    textUp = new QLabel();
    textUp->setText(_textUp);
    textUp->setStyleSheet("background:transparent;border:none;font-weight:bold;");
    textUp->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    textDown = new QLabel();
    textDown->setText(_textDown);
    textDown->setStyleSheet("background:transparent;border:none;font-weight:bold;");
    textDown->setAlignment(Qt::AlignRight | Qt::AlignBottom);
    textL->setContentsMargins(0,0,0,0);
    textL->addStretch();
    textL->addWidget(textUp, 1, Qt::AlignLeft | Qt::AlignTop);
    textL->addWidget(textDown, 1, Qt::AlignRight | Qt::AlignBottom);
    this->adjustSize();
}

ChessFieldButton::~ChessFieldButton()
{
}

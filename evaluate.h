#ifndef EVALUATE_H_INCLUDED
#define EVALUATE_H_INCLUDED

#include <atomic>
#include <string>

#include "types.h"

class PositionAI;

namespace Eval {

const Value Tempo = Value(20); // Must be visible to search

extern std::atomic<Score> Contempt;

std::string trace(const PositionAI& pos);

Value evaluate(const PositionAI &pos);
}

#endif // #ifndef EVALUATE_H_INCLUDED
